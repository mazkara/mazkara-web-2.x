<?php

class UserTest extends TestCase {
  public function setUp(){
    parent::setUp();
  }


	public function test_user_must_not_save_without_a_username(){
    $user = $this->getUserObject();
    $user['username'] = '';
    
    $validation = Validator::make($user, Account::$rules);

    $this->assertFalse($validation->passes());
	}

  public function test_user_must_save_with_a_username(){
    $repo = new UserRepository();
    $user = $this->getUserObject();
    $validation = Validator::make($user, Account::$rules);

    $this->assertTrue($validation->passes());
  }

  public function test_user_must_not_save_without_a_password(){
    $user = $this->getUserObject();
    $user['password'] = '';

    $validation = Validator::make($user, Account::$rules);

    $this->assertFalse($validation->passes());
    $this->pp($validation->errors());
  }

  public function test_user_must_not_save_without_a_name(){
    $user = $this->getUserObject();
    $user['name'] = '';
    $validation = Validator::make($user, User::$profile_rules);

    $this->assertFalse($validation->passes());
  }


  private function getUserObject(){
    $user = Fakefactory::make('User');
    $user = $user->toArray();
    $user['password'] = 'ad.1.3.22.3.3..4';
    $user['password_confirmation'] = $user['password'];
    $user['confirmation_code'] = '';
    $user['remember_token'] = '';

    return $user;
  }


}