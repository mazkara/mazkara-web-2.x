<?php
namespace Task;

use Mage\Task\AbstractTask;

class Migrations extends AbstractTask
{
  public function getName(){
    return 'Running migrations';
  }

  public function run(){

    $env = $this->getParameter('env', 'production'); 
    $folders = array(
                      'demo'=>'demo.fabogo.com',
                      'production'=>'fabogo.com',
                      'cron'=>'cron.mazkara.com',
                      'test'=>'test.mazkara.com'
                    );
    $folder = $folders[$env];
    echo "Migrating remaining migrations on ".$env.' ... ';
    $command = 'cd /data/sites/www/'.$folder.'/current; php artisan migrate;';
    if(in_array($env, ['cron', 'test', 'demo', 'production'])){
      $command = 'cd /var/www/'.$folder.'/current; php artisan migrate;';
    }
    $result = $this->runCommandRemote($command);

    return $result;
  }
}