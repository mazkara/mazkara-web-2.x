<?php

class GroupsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('groups')->truncate();


    $zn = Zone::firstOrCreate(['name'=>'Dubai']);

    $groups = Group::all();
    foreach($groups as $group){
      $group->city_id = $zn->id;
      $group->save();
    }

		/*$groups = array(

		);

    $businesses = Business::where('chain_id', '>', '0')->get();

    foreach($businesses as $business){
    	// get the chain business
    	$chain = Business::find($business->chain_id);
    	if($chain){
	    	$g = Group::firstOrCreate(['name'=>$chain->name, 'type'=>'chain']);
	    	$g->save();
	    	$chain->chain_id = $g->id;
	    	$chain->save();
	    	$business->chain_id = $g->id;
	    	$business->save();
    	}

    }

    $users = User::all();
    foreach($users as $user){
    	$user->resluggify();
    	$user->save();
    }*/

		// Uncomment the below to run the seeder
		// DB::table('groups')->insert($groups);
	}

}
