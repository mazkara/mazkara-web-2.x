<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotMerchantUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('merchant_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('merchant_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('merchant_user');
	}

}
