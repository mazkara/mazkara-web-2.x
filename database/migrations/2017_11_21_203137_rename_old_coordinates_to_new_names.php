<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameOldCoordinatesToNewNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->renameColumn('geolocation_latitude', 'geolocation_latitude_legacy');
            $table->renameColumn('geolocation_longitude', 'geolocation_longitude_legacy');

            $table->renameColumn('legacy_geolocation_latitude', 'geolocation_latitude');
            $table->renameColumn('legacy_geolocation_longitude', 'geolocation_longitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
