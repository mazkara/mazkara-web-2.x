<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotBrandBusinessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('brand_business', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('brand_id')->unsigned()->index();
			$table->integer('business_id')->unsigned()->index();
			$table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
			$table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brand_business');
	}

}
