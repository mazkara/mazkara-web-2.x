<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResetAdsetFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ad_sets', function($table)
		{
    	$table->integer('business_zone_id');
    	$table->integer('category_id');
	    $table->dropColumn('ad_zone_id');    	
	    $table->dropColumn('ad_id');    	
		});

		Schema::table('ads', function($table)
		{
    	$table->integer('ad_set_id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
