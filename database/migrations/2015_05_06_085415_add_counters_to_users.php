<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountersToUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
    	$table->integer('ratings_count')->default(0);
    	$table->integer('reviews_count')->default(0);
    	$table->integer('check_ins_count')->default(0);
    	$table->integer('followers_count')->default(0);
    	$table->integer('follows_count')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
