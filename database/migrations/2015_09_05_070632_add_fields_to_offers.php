<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOffers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('offers', function($table)
		{
			$table->renameColumn('body', 'title');
			$table->text('description');
			$table->integer('discount_amount');
			$table->string('discount_type');
			$table->renameColumn('price', 'offer_price');
			$table->integer('original_price');
			$table->text('toc');
			$table->integer('max_num_vouchers')->default(0);
			$table->integer('used_vouchers')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
