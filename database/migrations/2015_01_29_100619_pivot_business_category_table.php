<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotBusinessCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_category', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('business_id')->unsigned()->index();
			$table->integer('category_id')->unsigned()->index();
			$table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_category');
	}

}
