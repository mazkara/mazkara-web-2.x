<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::drop('taxes');
      Schema::create('taxes', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->integer('percentage')->default(0);
        $table->float('amount')->default(0);
        $table->integer('invoice_id')->unsigned()->index();
        $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
