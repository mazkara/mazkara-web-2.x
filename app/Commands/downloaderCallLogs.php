<?php

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
//use \Call_log;

class downloaderCallLogs extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:call_logs.downloader';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download All Call Logs.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->daily()->hours([3, 12, 19]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		
		$skip_id = $this->option('skip_id');

		$total = Call_log::query()->count();
		$pages = ceil($total/$count);
		$downloaded_calls = [];
		$failed_downloads = [];
		if($skip_id){
			//$photos = Photo::select()->where('imageable_type', '=', 'Business')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$call_logs = Call_log::select()->withUrl()->hoursAgo()->onlyNotDownloaded()->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' records after photo id '.$skip_id.' taken from page '.$page);
			foreach($call_logs as $call_log){
				if($call_log->download()==true){
					$this->info($call_log->id.' - call log downloaded and updated');
					$downloaded_calls[$call_log->id] = $call_log->call_recording_url;
				}else{
					$this->error($call_log->id.' - '.$call_log->call_recording_url.' - call log could not be downloaded');
					$failed_downloads[$call_log->id] = $call_log->call_recording_url;
					$call_log->markNotDownloaded();
				}
				$call_log = null;
			}

			$this->line(count($call_logs).' records updated from offset ');
			$call_logs = null;
			$this->info(memory_get_usage().' bytes allocated');

		}else{

			for($i=0;$i<=$pages;$i++):

				$call_logs = Call_log::select()->withUrl()->hoursAgo()->onlyNotDownloaded()->take($count)->skip($count*$i)->get();
				$this->line($count.' call log records taken from page '.$i);

				foreach($call_logs as $call_log){
					if($call_log->download()==true){
						$this->info($call_log->id.' - call log downloaded and updated');
						$downloaded_calls[$call_log->id] = $call_log->call_recording_url;

					}else{
						$this->error($call_log->id.' - '.$call_log->call_recording_url.' - call log could not be downloaded');
						$failed_downloads[$call_log->id] = $call_log->call_recording_url;
					}

					$call_log = null;
				}

				$this->line(count($call_logs).' records updated from offset '.$i);
				$call_logs = null;
				$this->info(memory_get_usage().' bytes allocated');
			endfor;
		}

    Mail::send('emails.notify-job', ['server'=>$_SERVER, 'downloaded_calls'=>$downloaded_calls, 'failed_downloads'=>$failed_downloads, 'job_details'=>'Downloaded '.count($downloaded_calls).' Calls '], function($message){
      $message->to('product@mazkara.com', 'Mazkara')->from('no-reply@mazkara.com')
              ->subject('Download Calls Cron Job Done '.date('Y-m-d H:i'));
    });

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
