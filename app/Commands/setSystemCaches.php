<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class setSystemCaches extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.cache';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reset All Cache.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		mzk_clear_all_cache();
		mzk_reset_all_zone_cache();
		$cities = MazkaraHelper::getCitiesList();
		foreach($cities as $city):
			MazkaraHelper::setLocale($city['slug']);
			mzk_reset_all_highlights_cache();
			mzk_reset_all_service_cache();
			mzk_reset_all_category_cache();
		endforeach;
		//
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
