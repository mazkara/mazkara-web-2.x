<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use \Offer as Offer;

class UpdateAllOffersState extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:offers.state.update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update entire offers States.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler){
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([1,22]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = 500;//$this->argument('count');

		$total = Offer::query()->byState('active')->count();
		$pages = ceil($total/$count);

		for($i=0;$i<=$pages;$i++):
			$offers = Offer::select()->byState('active')->take($count)->skip($count*$i)->get();
			$this->line($count.' records taken from page '.$i);

			foreach($offers as $offer){
				$offer->resetState();
				$this->info($offer->id.' - '.$offer->title.' updated');
				$business = null;
			}
			$this->line(count($offers).' records updated from offset '.$i);
			$offers = null;
			$this->info(memory_get_usage().' bytes allocated');
		endfor;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
