<?php
namespace App\Helpers;
use App\Models\Zone;
use App\Models\Service;
use App\Models\Category;
use App\Models\Business;
use App\Models\Campaign;
use App\Models\Ad;
use App\Models\Photo;

use Cache,Auth, Str, Facebook, Session, URL,  Redirect, Request, Location;
class MazkaraHelper{

  public static $current_locale;

  public static function fbToken($token = false, $fb = false){
    
    if(!Auth::check()){
      return false;
    }

    $session_name = 'facebook_access_token_id_'.Auth::user()->id;
    
    if($token){
      Session::put($session_name, (string) $token);
    }

//    if($fb){
//            Facebook::setDefaultAccessToken($token);
//
//    }

    return Session::get($session_name);
  }


  public static function getCacheName($index){
    $names = [
      'all-cities'=>'zones.cities.list'
    ];
    return isset($names[$index])?$names[$index]:$index;
  }

  public static function getCitiesList(){
    self::setCitiesList();
    $cache = self::getCacheName('all-cities');
    return Cache::get($cache);
  }

  public static function setCitiesList($force = false){
    $cache = self::getCacheName('all-cities');
    if (!Cache::has($cache) || ($force == true)){

      $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);
      Cache::forever($cache, Zone::cities()->get()->toArray());
    }


  }

  public static function getCurrentPageCacheName($page = null){
    $append = '';
    if(Auth::check()){
      $append.='-for-the-user-id-'.Auth::user()->id;
    }

    $page = !is_null($page) ? $page : Request::fullUrl();

    $str = 'mazkara-route-cached-'.Str::slug($page).$append;
    return $str;
  }

  public static function clearCurrentPageCacheName($page = null){
    $cache = self::getCurrentPageCacheName($page);
    Cache::forget($cache);

  }


  public static function getPluralName($name){
    if(strtolower($name) == 'personal fitness'){
      return str_plural($name.' Center');
    }

    return str_plural($name);
  }


  public static function getCategory($id){
    $categories = self::getAllCategoriesList();

    foreach($categories as $c){
      if($c['id']==$id){
        return $c;
      }
    }

    return false;
  }

  public static function getActiveCategory($id){
    $categories = self::getCategoriesList();
    foreach($categories as $c){
      if($c['id']==$id){
        return $c;
      }
    }

    return false;
  }

  public static function getHighlight($id){
    $highlights = self::getHighlightsList();
    foreach($highlights as $c){
      if($c['id'] == $id){
        return $c;
      }
    }

    return false;
  }
  public static function getHighlightSlug($id){
    $highlights = self::getHighlightsList();
    foreach($highlights as $c){
      if($c['id'] == $id){
        return $c['slug'];
      }
    }

    return false;
  }


  public static function getHighlightsList(){
    self::setHighlightsList();
    $cache = mzk_cache_locale_name('highlights.list');

    return Cache::get($cache);
  }

  public static function setHighlightsList($force = false){
    $cache = mzk_cache_locale_name('highlights.list');

    if (!Cache::has($cache) || ($force==true))
    {
      $zone = self::getLocaleObject();

      $highlights = $zone->activeHighlights()->remember(180)->get()->toArray();

      //$highlights = Highlight::showActive()->get()->toArray();
      foreach($highlights as $ii=>$vv){
        $highlights[$ii]['icon'] = ViewHelper::iconHighlight($vv['slug']);
      }
      Cache::forever($cache, $highlights);
    }
  }

  public static function isActiveHighlight($id){
    return self::getHighlight($id) ? true : false;
  }
  public static function isActiveCategory($id){
    return self::getActiveCategory($id) ? true : false;
  }


  public static function getZonesChildrenList($id){
    $zones = self::getZoneHierarchyList();
    $results = mzk_get_children_for($zones, $id);
    return $results;
  }


  public static function getZonesChildrenListAttributes($id, $attrib = 'id'){
    $zones = self::getZonesChildrenList($id);
    $cache = 'zones.hierarchy.list.for.attrib.'.$attrib.'.for.id.'.$id;

    if(!Cache::has($cache)){
      array_walk_recursive($zones, function($item, $key) use (&$results, &$i)
      {
        if($key == 'id'){
          $results[] = $item; 
        }
      });

      Cache::forever($cache, $results);
    }

    return Cache::get($cache);
  }



  public static function getZoneHierarchyList(){
    self::setZoneHierarchyList();
    //$locale = self::getLocaleObject();
    $cache = ('zones.hierarchy.list');
    return Cache::get($cache);
  }

  public static function setZoneHierarchyList($force = false){
    //$zone = self::getLocaleObject();
    $cache = ('zones.hierarchy.list');
    if (!Cache::has($cache) || ($force==true)){
      Cache::forget($cache);
      $results = Zone::all()->toTree()->toArray();
      Cache::forever($cache, $results);
    }
  }




  public static function getServicesHierarchyList(){
    self::setServicesHierarchyList();
    $zone_id = self::getLocaleID();

    $cache = mzk_cache_locale_name('services.hierarchy.list.for.'.$zone_id);

    return Cache::get($cache);
  }

  public static function setServicesHierarchyList($force = false){
    $zone_id = self::getLocaleID();
    $cache = mzk_cache_locale_name('services.hierarchy.list.for.'.$zone_id);
    if (!Cache::has($cache) || ($force==true)){
      Cache::forget($cache);
      $results = [];
      $zone = self::getLocaleObject();

      $s = $zone->activeParentServices()->get();

    
      foreach($s as $service){
        $results[$service->id] = $service->getAttributes();
        $results[$service->id]['children'] = Service::byLocaleActive($zone->id)->where('parent_id', '=', $service->id)->get()->toArray();
      }
      
      Cache::forever($cache, $results);
    }
  }


  public static function getServicesAttribute($id, $attribute = 'name'){
    $list = self::getServicesList();
    foreach($list as $list){
      if($list['id']==$id){
        return $list[$attribute];
      }
    }

    return '';
  }


  public static function getServicesAttributeFrom($id, $from = 'id', $attribute = 'name'){
    $list = self::getServicesList();
    foreach($list as $list){
      if($list[$from]==$id){
        return $list[$attribute];
      }
    }

    return '';
  }



  public static function getServicesList(){
    self::setServicesList();
    $cache = mzk_cache_locale_name('services.list');

    return Cache::get($cache);
  }

  public static function setServicesList($force = false){
    $cache = mzk_cache_locale_name('services.list');

    if (!Cache::has($cache) || ($force==true))
    {
      $zone = self::getLocaleObject();

      $srvs = $zone->activeServices()->get()->toArray();
      Cache::forever($cache, $srvs);
    }
  }


  public static function getServiceSlug($id){
    $cache = mzk_cache_locale_name('services.slug.list');

    self::setServicesSlugList();
    $ls = Cache::get($cache);

    return isset($ls[$id])?$ls[$id]:'';
  }

  public static function getServicesSlug($ids){
    self::setServicesSlugList();
    $cache = mzk_cache_locale_name('services.slug.list');
    $ls = Cache::get($cache);
    $rs = [];
    foreach($ids as $id){
      if(isset($ls[$id])){
        $rs[] = $ls[$id]; 
      }
    }

    return $rs;
  }


  public static function getServicesSlugList(){
    self::setServicesSlugList();
    $cache = mzk_cache_locale_name('services.slug.list');

    return Cache::get($cache);
  }

  public static function setServicesSlugList($force = false){
    $cache = mzk_cache_locale_name('services.slug.list');
    if (!Cache::has($cache) || ($force==true))
    {
      $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);

      $zone = self::getLocaleObject();

      $srvs = $zone->activeServices()->get()->lists('slug', 'id')->all();

      //$srvs = Service::showActive()->get()->lists('slug', 'id');
      Cache::forever($cache, $srvs);
    }
  }



  public static function getAllCategoriesList(){
    self::setAllCategoriesList();
    $cache = mzk_cache_locale_name('categories.all.list');

    return Cache::get($cache);
  }

  public static function setAllCategoriesList($force = false){
    //$force = true;
    $cache = mzk_cache_locale_name('categories.all.list');

    if (!Cache::has($cache) || ($force==true))
    {
      $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);
      $zone = self::getLocaleObject();

      $cats = $zone->activeCategories()->get()->toArray();

      foreach($cats as $ii=>$vv){
        $cats[$ii]['icon'] = ViewHelper::iconCategory($vv['slug']);
      }
      Cache::forever($cache, $cats);
    }
  }

  public static function getCategoriesList(){
    self::setCategoriesList();
    $cache = mzk_cache_locale_name('categories.list');

    return Cache::get($cache);
  }



  public static function setCategoriesList($force = false){
    $cache = mzk_cache_locale_name('categories.list');
    
    if (!Cache::has($cache) || ($force==true))
    {
      $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);

      $locale = self::getLocaleObject();

      $cat = $locale->categories()->lists('name','category_id')->all();

      $cats = Category::byLocaleActiveDisplayable()->remember(120)->get()->toArray();
      foreach($cats as $ii=>$vv){
        $cats[$ii]['icon'] = ViewHelper::iconCategory($vv['slug']);
        $cats[$ii]['type'] = 'CATEGORY';
      }
      Cache::forever($cache, $cats);
    }
  }

  public static function collectionToArray($co){
    $r = [];
    foreach($co as $vv){
      $r[] = $vv;
    }

    return $r;
  }


  public function getCategoriesDropDownList(){
    $c = self::getCategoriesList();
    $r = [];
    foreach($c as $vv){
    }
  }


  public static function getAdminDefaultLocaleLabel(){
    $citieslist = self::getCitiesList();
    foreach($citieslist as $city){
      if($city['slug']==self::getDefaultAdminLocale()){
        return $city['name'];
      }
    }

    return '';
  }


  public static function getAdminDefaultLocaleID(){
    $citieslist = self::getCitiesList();
    foreach($citieslist as $city){
      if($city['slug']==self::getDefaultAdminLocale()){
        return $city['id'];
      }
    }

    return '';
  }



  public static function getDefaultAdminLocale(){
    $session = 'mazkara.locale';

    if (!mzk_is_session_valid($session)){
      self::setDefaultAdminLocale(Auth::user()->defaultLocale()->slug);
    }

    return Session::get($session);
  }


  public static function getAdminLocale(){
    $session = 'mazkara.locale';
    if (!mzk_is_session_valid($session)){
      return self::getDefaultAdminLocale();
    }else{
      return Session::get($session);
    }

  }

  public static function affixDefaultAdminLocale(){
    $current = self::getAdminLocale();
    $session = 'mazkara.locale';

    if(in_array($current, self::getAdminCitiesSlugList())){
      Session::put($session, $current);
    }else{
      $city = self::getDefaultAdminLocale();
      Session::put($session, $city);
    }
  }

  public static function setDefaultAdminLocale($city){
    $session = 'mazkara.locale';

    if(in_array($city, self::getAdminCitiesSlugList())){
      Session::put($session, $city);
    }else{
      $city = self::getDefaultAdminLocale();
      Session::put($session, $city);
    }
  }  



  public static function getAdminCitiesSlugList(){
    if(!Auth::check()){
      return [];
    }

    $session = ('admin.zones.cities.list');

    if (!mzk_is_session_valid($session)){
      Session::put($session, Auth::user()->allowedZones()->lists('slug', 'id')->all());
    }

    return Session::get($session);
  }



  public static function getCitiesSlugList(){
    $cache = mzk_cache_name('zones.cities.slug.list');
    self::setCitiesSlugList();
    $r = Cache::get($cache);
    if(!is_array($r)){
      $r = $r->all();
    }

    return $r;//Cache::get($cache);
  }


  public static function setCitiesSlugList($force = false){
    $cache = mzk_cache_name('zones.cities.slug.list');

    if (!mzk_is_cache_valid($cache) || ($force==true)){
      $expiresAt = \Carbon\Carbon::now()->addMinutes(18000);
      Cache::forever($cache, Zone::cities()->get()->lists('slug', 'id')->all());
    }

    $r = Cache::get($cache);
    if(!is_array($r)){
      $r = $r->all();
    }
    return $r;
  }

  public static function getCitySlugFromID($city_id = null){
    $cities = self::getCitiesSlugList();
    if(($city_id == null)||($city_id == 0)){
      return self::getLocale();
    }

    return isset($cities[$city_id]) ? $cities[$city_id] : self::getLocale();
  }





  public static function getLocaleObject(){
    $lid = self::getLocaleID();
    return Zone::find($lid);
  }

  public static function getLocaleID(){
    $citieslist = self::getCitiesList();

    foreach($citieslist as $city){
      if($city['slug']==self::getLocale()){
        return $city['id'];
      }
    }

    return '';
  }

  public static function getLocaleLabel(){
    $citieslist = self::getCitiesList();
    foreach($citieslist as $city){
      if($city['slug']==self::getLocale()){
        return $city['name'];
      }
    }

    return '';
  }

  public static function getBasicLocale(){
    $session = 'mazkara.city';
    if(is_null(Request::segment(1))){
      return Session::get($session);
    }
    // the request segment has something but is it a city?

    if(!in_array(Request::segment(1), self::getCitiesSlugList() )) { 
      // no it doesn't look like a city
      // could be /assets or /users
      // send what is already set
      return Session::get($session);
    }else{
      return Request::segment(1);

    }

  }

  public static function isSetLocale(){
    $session = 'mazkara.city';
    $request_segment = Request::segment(1);
    if (  !mzk_is_session_valid($session) && (strlen($request_segment)==0) ) {
      return false;
    }



    return true;
  }


  public static function getLocale(){
    $session = 'mazkara.city';
    $request_segment = Request::segment(1);
    if (  mzk_is_session_valid($session) ) { // is the session already set
      // what if the user changed the city
      // check if we have a city similar to the slug list
      // no city in name? then assume the existing locale
      $session_locale = !is_null(self::$current_locale)?self::$current_locale:Session::get($session);
      if(is_null($request_segment)|| 
          (!is_null($request_segment)&& ( ($request_segment==$session_locale) || (in_array($request_segment, ['assets'] ))))){
        self::$current_locale = $session_locale;
        return $session_locale;
      }
      // the request segment has something but is it a city?

        // no it is in the city list - check is the city in the url the same as in the session

        if($request_segment==$session_locale){
          self::$current_locale = $session_locale;
          return $session_locale; //yes it is - send in the session
        }else{
          if(!in_array($request_segment, self::getCitiesSlugList() )) { 
            // no it doesn't look like a city
            // could be /assets or /users
            // send what is already set
            self::$current_locale = $session_locale;
            return $session_locale;
          }else{
            // no the city has changed and its a city change the locale
            self::setDefaultLocale();

            return self::getLocale();

          }

        }

    }else{
      self::setDefaultLocale();
      return self::getLocale();
    }
  }

  public static function setLocale($city){
    $session = 'mazkara.city';

    if(in_array($city, self::getCitiesSlugList())){
      Session::put($session, $city);
      self::$current_locale = $city;
    }
  }  

  public static function redirectToUsersCity(){
    $auto_delect_locale = isset($_GET['auto_delect_locale'])?true:false;
    if(!$auto_delect_locale){
      return false;
    }
    $request_segment = Request::segment(1);
    $location = self::detectLocation();

    $url = URL::full();
    $url = str_replace("/".$request_segment."/", "/".$location."/", $url);
    $url = str_replace("auto_delect_locale", "redirected", $url);

    return Redirect::to($url);    
  }

  public static function detectLocation(){
    $ip = Request::getClientIp(); //'27.106.17.74';//
    $cc = Location::get($ip)->countryCode;
    $city = '';
    if($cc == 'IN'){
      $cityName = Location::get($ip)->cityName;

      if(strstr(strtolower($cityName), 'mumbai')){
        $city = 'mumbai';
      }else{
        $city = 'pune';
      }
    }else{
      $city = 'dubai';
    }

    return $city;

  }


  public static function setDefaultLocale(){
    $l = Request::segment(1);
    $s = self::getCitiesSlugList();


    if( (!is_null($l) && ($index = array_search($l, $s))) ){
      self::setLocale($l);
    }else{

      $ip = Request::getClientIp(); //'27.106.17.74';//
      try{
        $cc = Location::get($ip)->countryCode;
        if($cc == 'IN'){
          $cityName = Location::get($ip)->cityName;

          if(strstr(strtolower($cityName), 'mumbai')){
            $city = 'mumbai';
          }else{
            $city = 'pune';
          }
        }else{
          $city = 'dubai';
        }

        self::setLocale($city);

      }catch(Exception $e){
        self::setLocale(array_values($s)[0]);
      }
      //self::setLocale(array_values($s)[0]);
    }

  }

  public static function getClaimUrl($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('claim.businesses.get.for.'.$city, array($business->id));
  }

  public static function postClaimUrl($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('claim.businesses.post.for.'.$city, array($business->id));
  }

  public static function slugSingle($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('businesses.view.by.slug.for.'.$city, array($business->slug));
  }

  public static function slugSinglePhotos($business, $city_slug = false){
    $city = $city_slug ? $city_slug: self::getCitySlugFromID($business->city_id);
    return route('businesses.photos.by.slug.for.'.$city, array($business->slug));
  }

  public static function slugSingleLocation($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('businesses.location.by.slug.for.'.$city, array($business->slug));
  }

  public static function slugSingleRateCards($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('businesses.rates.by.slug.for.'.$city, array($business->slug));
  }
  public static function slugSinglePackages($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('businesses.packages.by.slug.for.'.$city, array($business->slug));
  }

  public static function slugSingleReviews($business){
    $city = self::getCitySlugFromID($business->city_id);
    return route('businesses.reviews.by.slug.for.'.$city, array($business->slug));
  }

  public static function slugDeal($business, $deal){
    $city = self::getCitySlugFromID($business->city_id);

    return route('deals.'.strtolower($deal->type).'.by.slug.for.'.$city, array($business->slug, $deal->id));
  }


  public static function slugCity($city = null, $params = null){
    $city = $city ? $city->slug : self::getLocale();
    $q = '';
    $s = null;
    $c = false;
    if(is_array($params)){
      if(count($params)>0){
        unset($params['zone']);
        unset($params['subzone']);
        unset($params['city']);

        if(isset($params['service'])){
          $s = self::getServicesSlug($params['service']);
          $s = join(',', $s);
          unset($params['service']);
        }

        if(isset($params['category'])&& !is_null($params['category'])){
          $c = self::getCategory(array_pop($params['category']));  
          unset($params['category']);
        }


        $q = http_build_query($params);        
      }
    }

    if($c){
      return route('businesses.list.category.'.$c['slug'].'.by.city.append.for.'.$city, $s).( strlen(trim($q)) > 0 ? '?'.$q : '');//.'?'.$q;
    }


    return route('businesses.list.by.city.append.for.'.$city, $s).( strlen(trim($q)) > 0 ? '?'.$q : '');
  }

  public static function slugCityChain($city = null, $chain, $params = null){
    $city = $city ? $city->slug : self::getLocale();
    $q = '';

    if(is_array($params)){
      if(count($params)>0){
        unset($params['zone']);
        unset($params['subzone']);
        $q = http_build_query($params);        
      }
    }
    return route('businesses.chains.by.city.append.for.'.$city, $chain->slug).( strlen(trim($q)) > 0 ? '?'.$q : '');
  }
  public static function slugCityGroup($city = null, $chain, $params = null){
    $city = $city ? $city->slug : self::getLocale();
    $q = '';

    if(is_array($params)){
      if(count($params)>0){
        unset($params['zone']);
        unset($params['subzone']);
        $q = http_build_query($params);        
      }
    }
    return route('businesses.groups.by.city.append.for.'.$city, $chain->slug).( strlen(trim($q)) > 0 ? '?'.$q : '');
  }

  public static function slugCityZone($zone, $params = null){
    if(is_null($zone)){
      return self::slugCity($zone, $params );
    }
    if($zone->isCity()){
      return self::slugCity($zone, $params );
    }

    //$city = self::getCitySlugFromID($zone->ancestors()->first()->id);
    $zone = $zone->toArray();
    $city = self::getCitySlugFromID($zone['city_id']);

    $q = '';
    $s = null;
    $c = false;

    if(is_array($params)){
      if(count($params)>0){
        unset($params['zone']);
        unset($params['subzone']);
        unset($params['city']);

        if(isset($params['service'])){
          $s = self::getServicesSlug($params['service']);
          $s = join(',', $s);
          unset($params['service']);
        }

        if(isset($params['category'])){
          $c = self::getCategory(array_pop($params['category']));  
          unset($params['category']);
        }

        $q = http_build_query($params);        
      }
    }


    $route = 'businesses.list.by.city.and.zone.for.'.$city;

    if(($c)){
      $route = 'businesses.list.category.'.$c['slug'].'.by.city.and.zone.for.'.$city;
    }

    return route($route, [$zone['slug'], $s]).( strlen(trim($q)) > 0 ? '?'.$q : '');
  }

 
}
