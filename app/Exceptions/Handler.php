<?php namespace App\Exceptions;

use Exception, Request, Mail, Auth;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
        if (app()->environment('production'))
        {
            $this->sendErrorEmail(Request::instance(), $e);
        }

		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
        if (app()->environment('production'))
        {
          return response()->view('404', [], 404);
        }

        if ($this->isHttpException($e))
        {
          return $this->renderHttpException($e);
        }


        if (config('app.debug'))
        {
          return $this->renderExceptionWithWhoops($e);
        }

		return parent::render($request, $e);
	}

		/**
		* Render an exception using Whoops.
		* 
		* @param  \Exception $e
		* @return \Illuminate\Http\Response
		*/
    protected function renderExceptionWithWhoops(Exception $e)
    {
      $whoops = new \Whoops\Run;
      $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

      return new \Illuminate\Http\Response(
          $whoops->handleException($e),
          $e->getStatusCode(),
          $e->getHeaders()
      );
    }


    protected function sendErrorEmail($request, $e)
    {
        if(isset($_SERVER['HTTP_FROM'])){
            if(strpos($_SERVER['HTTP_FROM'], 'googlebot')!=false){
                return;
            }
            if(strpos($_SERVER['HTTP_FROM'], 'bingbot')!=false){
                return;
            }
        }

        if(isset($_SERVER['HTTP_USER_AGENT'])){

            if(strpos($_SERVER['HTTP_USER_AGENT'], 'bingbot')!=false){
                return;
            }
            
            if(strpos($_SERVER['HTTP_USER_AGENT'], 'AhrefsBot')!=false){
                return;
            }
            if(strpos($_SERVER['HTTP_USER_AGENT'], 'bot')!=false){
                return;
            }

        }

        if(isset($_SERVER['REQUEST_URI'])){
            if(strpos($_SERVER['REQUEST_URI'], 'admin/')!=false){
                return;
            }

            /*
            if(strpos($_SERVER['REQUEST_URI'], 'finance/')!=false){
                return;
            }
            if(strpos($_SERVER['REQUEST_URI'], 'crm/')!=false){
                return;
            }
            if(strpos($_SERVER['REQUEST_URI'], 'editor/')!=false){
                return;
            }
            if(strpos($_SERVER['REQUEST_URI'], 'content/')!=false){
                return;
            }*/

            if(strpos($_SERVER['REQUEST_URI'], 'img/')!=false){
                return;
            }
            if(strpos($_SERVER['REQUEST_URI'], 'css/')!=false){
                return;
            }
            if(strpos($_SERVER['REQUEST_URI'], 'images/')!=false){
                return;
            }
        }
        
        $code = $this->errorCodeFromException($e);

        if($code == 404){ // let snot log page not found errors for now
            return;
        }

        $data = [
            'exception' => (string)$e,
            'code'      => $code,
            'url'       => $request->fullUrl(),
            'loggedIn'  => Auth::check(),
            'remoteIP'  => $request->getClientIp(),
            'server'    => $_SERVER
        ];

        Mail::send('emails.app.error', ['data'=>$data], function($message) use ($data)
        {
            $message->to('ali@fabogo.com');
            $message->subject($data['url']." - Server Error - ".$data['code']);
        }); 
    }

    protected function errorCodeFromException(Exception $e)
    {
        if ($this->isHttpException($e)) {
            return $e->getStatusCode();
        }
        return $e->getCode();
    }


}
