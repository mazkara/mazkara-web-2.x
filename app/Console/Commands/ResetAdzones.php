<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use App\Models\Category as Category;
use App\Models\Zone as Zone;
use App\Models\Ad_zone as Ad_zone;

class ResetAdzones extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.adzones';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reset Ad zones.';
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		//return $scheduler->daily()->hours(4);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
    $categories = Category::all()->toArray();
    $cities = Zone::where('parent_id', '=', null)->get()->lists('name', 'id')->all();
    $zones = Zone::whereIn('parent_id', array_keys($cities))->get()->toArray();

    foreach($categories as $category){
			$this->info('Setting ad zones for '.$category['name']);

      foreach($zones as $zone){
        $a = Ad_zone::firstOrCreate(['zone_id'=>$zone['id'], 'category_id'=>$category['id']]);
        $a->title = str_plural($category['name']).' in '.$zone['name'];
        $a->save();
				$this->line('--- ad zones created for '.$category['name'].' in '. $zone['name'].' @id '.$a->id);
      }
    }
		$this->info('All done :)');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
