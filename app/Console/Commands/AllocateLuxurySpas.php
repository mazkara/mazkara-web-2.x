<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use App\Models\Category as Category;
use App\Models\Business as Business;

class AllocateLuxurySpas extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:allocate.luxury.spas';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Allocate spas and massage parlours that have highlighst to luxury spa category.';
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		//return $scheduler->daily()->hours(4);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$category = Category::where('slug', '=', 'luxury-spa')->first();

		$highlight_id = 12;

		$bids = \DB::table('business_highlight')->where('highlight_id', '=', $highlight_id)->lists('business_id','business_id');
		$categories = [2,4];
		$businesses = Business::query()->ofCategories($categories)->whereIn('id', $bids)->get();
    foreach($businesses as $business){
    	$business->categories()->attach($category->id);
      $business->save();
			$this->line($business->name.' allocated as Luxury.');
    }
		$this->info(count($businesses).' spas and centers updated - All done :)');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
