<?php
namespace App\Console\Commands;


use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Eloquent;
use App\Models\Role;
use App\Models\Permission;

class add_roles extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:roles.add';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Dynamically Add Roles.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		Eloquent::unguard();
		$name = $this->argument('name');
		$permissions = $this->option('permissions');

      $r = Role::firstOrCreate(['name'=>$name]);

      $r->save();
      $ps = array();
      $permissions = explode(',', $permissions);
      foreach($permissions as $permission){
        $p = Permission::firstOrCreate(['name'=>$permission]);
        $p->display_name = ucwords(str_replace('_', ' ', $permission));
        $p->save();
        $ps[] = $p->id;
      }
      $r->perms()->sync($ps);
		$this->line($name.' role added with permissions '.(join(',', $permissions)));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('name', InputArgument::REQUIRED, 'finance'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('permissions', null, InputOption::VALUE_OPTIONAL, '', null),
		);
	}

}
