<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;

use App\Models\Zone;
use DB;

class setup_depth_in_zones extends Command
{
    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $name = 'mazkara:zone.depth';

    protected $description = 'Update Zone Depth.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $zones = Zone::withDepth()->get()->toArray();
        foreach($zones as $z){
            $zone = Zone::find($z['id']);
            $zone->depth = $z['depth'];
            $zone->save();
            $this->line($zone->name.' depth has been set to '.$z['depth']);
        }
        $this->line('All done :)');
    }
}
