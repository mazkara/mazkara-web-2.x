<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use App\Models\Merchant as Merchant;

class ReallocateSalesPOCsToInvoices extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.salespocs';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reallocate Sales POCs to Invoices';
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		//return $scheduler->daily()->hours(4);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
    $merchants = Merchant::all();
    foreach($merchants as $merchant){
    	if($merchant->pocId() != ''){
    		$poc_id = $merchant->pocId();
    		$poc_name = $merchant->poc_name();
	    	$sql = "UPDATE invoices SET user_id=".$poc_id." where merchant_id = ".$merchant->id;
	    	DB::update($sql);
				$this->line('SQL('.$sql.') being executed');
				$this->line($merchant->name.' invoices being assigned to POC '.$poc_name);
    	}
    }

		$this->info('All done :)');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
