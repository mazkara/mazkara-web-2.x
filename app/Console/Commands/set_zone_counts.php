<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class set_zone_counts extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:set.zone.counts';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set Zone Counts.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		//$count = $count > 100 ? 100 : $count;

		$skip_id = $this->option('skip_id');
		$only_parents = $this->option('only_parents');

		//
		// grab all the zones and the services in a 

		if($skip_id){
			$zones = Zone::where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get()->toArray();
		}else{
			$zones = Zone::orderby('id', 'asc')->take($count)->skip($page*$count)->get()->toArray();
		}

		if($only_parents){
			$services = Service::query()->showParents()->get();
		}else{
			$services = Service::all();
		}

		foreach($zones as $zone){
			foreach($services as $service){
				$c = Service_zone_counter::firstOrCreate(['zone_id'=>$zone['id'], 'service_id'=>$service->id]);
	      $znes = array_merge([$zone['id']], array_keys(Zone::descendantsOf($zone['id'])->lists('name', 'id')));

	      if($service->getDescendants()->count()>0){
  	      $des_services = array_values($service->getDescendants()->lists('id','id'));
		      $des_services[] = $service->id;
					$c->business_count = Business::select()->ofZones($znes)->ofServices($des_services)->where('city_id', '>', 0)->isDisplayable()->count();
	      }else{
					$c->business_count = Business::select()->ofZones($znes)->ofServices([$service->id])->where('city_id', '>', 0)->isDisplayable()->count();
	      }

				$c->save();
				$this->line($zone['id'].' Businesses offering '.$service->name.' in '.$zone['name'].' - '.$c->business_count);
				$c = null;
				$znes = null;
				$service = null;
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 20),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
			array('only_parents', null, InputOption::VALUE_OPTIONAL, 'Just parents.', null),

		);
	}

}
