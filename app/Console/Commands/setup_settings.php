<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Zone;
use App\Models\Setting;

class setup_settings extends Command implements SelfHandling
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $name = 'mazkara:reset.settings';

    protected $description = 'Update settings.';

    public function __construct()
    {
        parent::__construct();

    }
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
      $cities = Zone::cities()->get();
      $settings = array(
                        ['name'=>'call_log_sms_to_user', 
                         'type'=>'sms', 
                         'default'=>''],
                        ['name'=>'call_log_sms_to_client', 
                         'type'=>'sms', 
                         'default'=>''],
                         ['name'=>'enable_acd', 
                         'type'=>'acd', 
                         'default'=>'1'],
                        );
      $o_name = $this->option('name');
      $o_type = $this->option('type');
      $o_default = $this->option('default');

      if(strlen($o_name)>0){
        $settings[] = [ 'name'=>$o_name,
                        'type'=>$o_type,
                        'default'=>$o_default
                      ];
      }

      foreach($cities as $city){
        foreach($settings as $setting){
          if($city->id > 0):
            $s = Setting::firstOrCreate([ 'name'=>$setting['name'], 
                                          'zone_id'=>$city->id]);
            $this->line($setting['name'].' - '.$city->id);
            $s->type = isset($s->type) && !empty($s->type) ? $s->type:$setting['type'];
            
            $s->value = isset($s->value) && !empty($s->value) ? $s->value:$setting['default'];
            $s->zone_id = $city->id;
            $s->save();
          endif;
        }
      }

    }

    protected function getOptions()
    {
        return array(
            array('name', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('type', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
            array('default', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }


}
