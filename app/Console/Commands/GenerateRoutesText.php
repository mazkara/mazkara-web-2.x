<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use File;
use \App\Models\Zone;
use \App\Models\Category;
use \App\Models\Service;
use \App\Models\Business;


class GenerateRoutesText extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:export.routes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Export all routes.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$directory = storage_path().'/media/';
		$filename = 'routes-dump-'.time().'.txt';

		$nl = "\n";

		File::put($directory.$filename, '');

		$cities = Zone::select()->cities()->get();
		$services = Service::select()->showOnly()->get();
		$categories = Category::all();
		$bar = $this->output->createProgressBar(count($cities));

		foreach($cities as $city):
			$route = '/'.$city->slug.'/salons-and-spas'.$nl;
			File::append($directory.$filename, $route);
			foreach($services as $service){
				$route = '/'.$city->slug.'/salons-and-spas/'.$service->slug.$nl;
				File::append($directory.$filename, $route);
			}

			foreach($categories as $category){
				$route = '/'.$city->slug.'/'.str_plural($category->slug).$nl;
				File::append($directory.$filename, $route);
				foreach($services as $service){
					$route = '/'.$city->slug.'/'.str_plural($category->slug).'/'.$service->slug.$nl;
					File::append($directory.$filename, $route);

				}
			}


			$subzones = Zone::select()->where('parent_id', '=', $city->id)->get();
			foreach($subzones as $subzone){
				$route = '/'.$city->slug.'/'.$subzone->slug.'-salons-and-spas'.$nl;
				File::append($directory.$filename, $route);

				foreach($services as $service){
					$route = '/'.$city->slug.'/'.$subzone->slug.'-salons-and-spas/'.$service->slug.$nl;
					File::append($directory.$filename, $route);
				}

				foreach($categories as $category){
					$route = '/'.$city->slug.'/'.$subzone->slug.'-'.str_plural($category->slug).$nl;
					File::append($directory.$filename, $route);

					foreach($services as $service){
						$route = '/'.$city->slug.'/'.$subzone->slug.'-'.str_plural($category->slug).'/'.$service->slug.$nl;
						File::append($directory.$filename, $route);

					}
				}


			}
	    $bar->advance();					

		endforeach;
			$bar->finish();
	
		$this->info('...');
		$this->info('File created at '.$directory.$filename);

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
