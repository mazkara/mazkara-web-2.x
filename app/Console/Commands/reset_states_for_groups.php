<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Image, File;

use App\Models\Category as Category;
use App\Models\Business as Business;

class reset_states_for_groups extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.group.states';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Mark Expired Groups as inactive.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->args(['page'=>1, 'count'=>0])->daily()->hours([3,23]);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$groups = Group::select()->isCustomActive()
											->whereRaw('valid_until <= NOW()')
											->orderby('id', 'asc')->get();
		$this->line(count($groups).' ccp to be deactived');

		foreach($groups as $group){
			$group->deactivateCCP();
			$group->save();
			$this->line($group->name.'('.$group->id.')'.' ccp deactived');
		}

		$this->line(' :) All done');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 0),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
			array('b_id', null, InputOption::VALUE_OPTIONAL, 'Business ID.', null),
		);
	}

}
