<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use File;
use App\Models\User as User;

class UpdateUsersMeta extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:update.users.meta';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update users meta.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		//$count = $count > 100 ? 100 : $count;
		$counter_file = storage_path().'/media/counter-users-meta';
		$skip_id = $this->option('skip_id');


		if(!$skip_id):

			if(File::exists($counter_file)){
				$skip_id = trim(File::get($counter_file));
			}
		endif;

		if($skip_id){
			$users = User::select()->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' users records after id '.$skip_id.' taken from page '.$page);
		}else{
			$users = User::select()->orderby('id', 'asc')->take($count)->skip($page*$count)->get();
			$this->line($count.' users records taken from page '.$page);
		}

		if(count($users) == 0){
			File::put($counter_file, 0);
		}

		foreach($users as $user):


			File::put($counter_file, $user->id);
			$user->updateMetaCache();

			$user->save();
			$this->line('User ID '.$user->id.' cache updated '.($user->avatar ? $user->avatar->image->url('micro') : '[no avatar]'));


		endforeach;


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 5000),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
