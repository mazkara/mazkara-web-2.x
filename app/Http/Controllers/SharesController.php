<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;
use App\Http\Controllers\Controller;
use App\Models\Share;
class SharesController extends Controller {

	/**
	 * Share Repository
	 *
	 * @var Share
	 */
	protected $share;

	public function __construct(Share $share)
	{
		$this->share = $share;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$shares = $this->share->all();

		return View::make('shares.index', compact('shares'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('shares.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Share::$rules);

		if ($validation->passes())
		{
			$this->share->create($input);

			return Redirect::route('shares.index');
		}

		return Redirect::route('shares.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$share = $this->share->findOrFail($id);

		return View::make('shares.show', compact('share'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$share = $this->share->find($id);

		if (is_null($share))
		{
			return Redirect::route('shares.index');
		}

		return View::make('shares.edit', compact('share'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Share::$rules);

		if ($validation->passes())
		{
			$share = $this->share->find($id);
			$share->update($input);

			return Redirect::route('shares.show', $id);
		}

		return Redirect::route('shares.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->share->find($id)->delete();

		return Redirect::route('shares.index');
	}

}
