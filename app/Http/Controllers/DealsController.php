<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;
use App\Models\Deal;
use App\Models\Favorite;

class DealsController extends Controller {

	/**
	 * Deal Repository
	 *
	 * @var Deal
	 */
	protected $deal;
	protected $breadcrumbs;

	public function __construct(Deal $deal)
	{
		$this->deal = $deal;
		$this->breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;
		$this->breadcrumbs->addCrumb('Home', '/');
    $this->breadcrumbs->addCssClasses('breadcrumb');
    $this->breadcrumbs->setDivider('›');

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$deals = $this->deal->all();

		$this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.deals.index', compact('deals'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout = View::make('layouts.master');
    $this->layout->content =  View::make('site.deals.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Deal::$rules);

		if ($validation->passes())
		{
			$this->deal->create($input);

			return Redirect::route('deals.index');
		}

		return Redirect::route('deals.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$deal = $this->deal->findOrFail($id);
		$business = $deal->business;
    $suggested_spas = Business::take(6)->get();

		$this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.deals.show', compact('deal', 'suggested_spas', 'business'));
	}

	public function slug($slug, $id)
	{
		$deal = $this->deal->findOrFail($id);
		$business = $deal->business;
    $suggested_spas = Business::take(6)->get();

    $zone = $business->zone;
    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity());

    $this->breadcrumbs->addCrumb('Salons, Spas and Fitness Centers '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name);

    $breadcrumbs = $this->breadcrumbs;



		$this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.deals.show', compact('deal', 'breadcrumbs', 'suggested_spas', 'business'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$deal = $this->deal->find($id);

		if (is_null($deal))
		{
			return Redirect::route('deals.index');
		}

		$this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.deals.edit', compact('deal'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Deal::$rules);

		if ($validation->passes())
		{
			$deal = $this->deal->find($id);
			$deal->update($input);

			return Redirect::route('deals.show', $id);
		}

		return Redirect::route('deals.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->deal->find($id)->delete();

		return Redirect::route('deals.index');
	}


	public function favourite($id){
		$user = Auth::user();
		if(!$user->hasFavourited('Deal', $id)){
			Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Deal', 'favorable_id'=>$id]);
		}
	}
	public function unfavourite($id){
		$user = Auth::user();
		if($user->hasFavourited('Deal', $id)){
			$user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Deal')->delete();
		}
	}



}
