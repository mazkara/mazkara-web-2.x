<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

//use Artdarek\OAuth\OAuthServiceProvider;
use Vinelab\Http\Client as HttpClient;

use Auth, Input, Session, Storage, View, Redirect, Response;
use Location, DB, Config, Mail, Request, Image, MazkaraHelper, Socialite, Validator, Lang;
use \SammyK\LaravelFacebookSdk\LaravelFacebookSdk as Facebook;
use App\Models\Account;
use App\Models\User;
use App\Models\Activity;
use App\Models\Zone;
use App\Models\Service;
use App\Models\Review;
use App\Models\Favorite;
use App\Models\Selfie;
use App\Models\Video;
use App\Models\Post;
use App\Models\Business;

/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends Controller
{

  protected $user;

  public function __construct(User $user, Activity $activity){
    $this->user = $user;
    $this->feed_manager = $activity;
  }

  /**
   * Displays the form for account creation
   *
   * @return  Illuminate\Http\Response
   */
  public function create(){
    if (Request::ajax()){
      return View::make('site.users.signup-ajax');
    }else{
      return View::make('site.users.signup');
    }
  }

  public function edit(){
    $user = User::find(Auth::user()->id);
    return View::make('site.users.edit', compact('user'));
  }

  public function show($id){
    $user = User::find($id);//Auth::user()->id);
    $show_inactive = Auth::check() ? Auth::user()->hasRole('admin') : false;

    if($user->isBlocked() && !($show_inactive)){
      return Redirect::to('/');
    }
    //$suggested_spas = Business::get()->take(5);
    //$suggested_salons = Business::get()->take(5);
    
    //$activities = Activity::select()->byUserIds([$id])->orderby('id', 'desc')->paginate(10);

    return View::make('site.users.show', compact('user'));
  }

  public function follow($id){
    $user = Auth::user();
    if(!$user->hasFavourited('User', $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>'User', 'favorable_id'=>$id]);

      $data = array('user_id' =>  $user->id, 
                    'verb'  => 'followed', 
                    'itemable_type' =>  'User', 
                    'itemable_id' =>  $id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $user->id;
      $feed->verb = 'followed'; 
      $feed->itemable_type = 'User';
      $feed->itemable_id = $id;
      $feed->save();




//      $data = array('user_id' =>  $id, 
//                    'verb'  => 'are-followed', 
//                    'itemable_type' =>  'User', 
//                    'itemable_id' =>  $user->id);
//      $feed = $this->feed_manager->create($data);
//      $feed->user_id = $id;
//      $feed->verb = 'are-followed'; 
//      $feed->itemable_type = 'User';
//      $feed->itemable_id = $user->id;
//      $feed->save();


      //FeedManager::followUser($user->id, $id);      
    }
  }

  public function unfollow($id){
    $user = Auth::user();
    if($user->hasFavourited('User', $id)){
      $user->favorites()->where('favorable_id', '=', $id)
                        ->where('favorable_type', '=', 'User')->delete();
      //FeedManager::unFollowUser($user->id, $id);      
    }
  }

  public function updateAccount(){
    $input = array_except(Input::all(), '_method');

    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    if($validation->passes())
    {
      $account = User::find(Auth::user()->id);
      foreach($data as $ii=>$vv){
        $account->$ii = $vv;
      }
//    $account->update($data);
      $account->save();
      $deletablePhotos = Input::only('deletablePhotos');
      $images = Input::only('avatar');

      if(count($deletablePhotos['deletablePhotos'])>0){
        $account->removeImage();
      }

      $account->saveImage($images['avatar']);

      return Redirect::to(action('UsersController@show', $account->id))->with('notice', 'Woo Hoo! You updated your profile!');

    }

    return Redirect::back()//route('admin.accounts.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  public function update()
  {
    $user = User::find(Auth::user()->id);
    //$repo = App::make('UserRepository');
//
    //$oldUser = clone $user;
    //$user->username = Input::get( 'username' );
    $user->email = Input::get( 'email' );

    $password = Input::get( 'password' );
    $passwordConfirmation = Input::get( 'password_confirmation' );

    if(!empty($password)) {
      if($password === $passwordConfirmation) {
        $user->password = bcrypt($password);
        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        unset($user->password_confirmation);
      } else {
        // Redirect to the new user page
        return Redirect::to('users/edit')->with('error', 'Passwords do not match');
      }
    } else {
      unset($user->password);
      unset($user->password_confirmation);
    }

    if ($user->save()) {
      return Redirect::to('users/edit')
            ->with( 'success', 'Account updated' );
    } else {
      $error = $user->errors()->all(':message');
      return Redirect::to('users/edit')
              ->withInput(Input::except('password', 'password_confirmation'))
              ->with('error', $error);
    }
  }

  /**
   * Stores new account
   *
   * @return  Illuminate\Http\Response
   */
  public function store()
  {

    $data = Input::all();//(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    $user_validation = Validator::make($data, [
      'name' => 'required|max:255',
      //'username' => 'required|unique:users',
      'email' => 'required|email|max:255|unique:users',
      'password' => 'required|confirmed|min:6'
    ]);

    if($user_validation->passes()):
      //$repo = App::make('UserRepository');
      //$user = $repo->signup(Input::all());
      $user =  User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => bcrypt($data['password'])
      ]);

      if ($user->id) {
        $user->quickSetRegistered($data);

        Mail::queue(
          'site.users.emails.signup',
          compact('user'),
          function ($message) use ($user) {
            $message->to($user->email, $user->username)
                    ->subject('Howdy, welcome to '.mzk_seo_name());
          }
        );

        Auth::loginUsingId($user->id, true);
        $user->setLastLoginIP();

        Mail::queue('emails.user-signup', ['data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
          $message->to('users@fabogo.com', 'Fabogo')->subject('Fabogo: New User Signup ['.time().']');
        });

        mzk_post_profile_wizrocket($user->id, $user->getSafeAttributes());
        return Redirect::to($_SERVER['HTTP_REFERER']);//intended('/');

        //return Redirect::intended();//action('UsersController@login')
            //->with('notice', Lang::get('confide::confide.alerts.account_created'));

      } else {
        $error = $user->errors()->all(':message');
        return Redirect::action('UsersController@create')
            ->withInput(Input::except('password'))
            ->with('error', $error);
      }
    else:
        $error = $user_validation->errors()->all(':message');

        return Redirect::action('UsersController@create')
            ->withInput(Input::except('password'))
            ->with('errors', $error);
    endif;
  }

  /**
   * Displays the login form
   *
   * @return  Illuminate\Http\Response
   */
  public function login()
  {
    if (Auth::user()) {
      $user = User::find(Auth::user()->id);
      $user->setLastLoginIP();

      return Redirect::to('/');
    } else {
      if (Request::ajax()){
        return View::make('site.users.login-ajax');
      }else{
        return  View::make('site.users.login');
      }
    }
  }

  /**
   * Attempt to do login
   *
   * @return  Illuminate\Http\Response
   */
  public function doLogin()
  {
    //$repo = \App::make('Apps\UserRepository');
    $input = Input::all();

    //    if ($repo->login($input)) {
    if(Auth::attempt(['email' => $input['email'], 'password' => $input['password']], true)){
      return Redirect::to($_SERVER['HTTP_REFERER']);//intended('/');
    }elseif(Auth::attempt(['username' => $input['email'], 'password' => $input['password']], true)){
      return Redirect::to($_SERVER['HTTP_REFERER']);//intended('/');
    } else {
      //if ($repo->isThrottled($input)) {
      //  $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
      //} elseif ($repo->existsButNotConfirmed($input)) {
      //  $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
      //} else {
      $err_msg = 'The Credentials are incorrect, please check your email and password!';
      //}

      return Redirect::action('UsersController@login')
          ->withInput(Input::except('password'))
          ->with('error', $err_msg);
    }
  }

  /**
   * Attempt to confirm account with code
   *
   * @param  string $code
   *
   * @return  Illuminate\Http\Response
   */
  public function confirm($code)
  {
    if (Confide::confirm($code)) {
      $notice_msg = Lang::get('confide::confide.alerts.confirmation');
      return Redirect::action('UsersController@login')
            ->with('notice', $notice_msg);
    } else {
      $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
      return Redirect::action('UsersController@login')
            ->with('error', $error_msg);
    }
  }

  /**
   * Displays the forgot password form
   *
   * @return  Illuminate\Http\Response
   */
  public function forgotPassword(){
    return View::make('site.users.forgot_password');
  }

  /**
   * Attempt to send change password link to the given email
   *
   * @return  Illuminate\Http\Response
   */
  public function doForgotPassword()
  {
    if (Confide::forgotPassword(Input::get('email'))) {
      $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
      return Redirect::action('UsersController@login')
          ->with('notice', $notice_msg);
    } else {
      $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
      return Redirect::action('UsersController@doForgotPassword')
          ->withInput()
          ->with('error', $error_msg);
    }
  }

  /**
   * Shows the change password form with the given token
   *
   * @param  string $token
   *
   * @return  Illuminate\Http\Response
   */
  public function resetPassword($token)
  {
    $this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.users.reset_password')->with('token', $token);
  }

  /**
   * Attempt change password of the user
   *
   * @return  Illuminate\Http\Response
   */
  public function doResetPassword()
  {
    $repo = App::make('UserRepository');
    $input = array(
      'token'                 =>Input::get('token'),
      'password'              =>Input::get('password'),
      'password_confirmation' =>Input::get('password_confirmation'),
    );

    // By passing an array with the token, password and confirmation
    if ($repo->resetPassword($input)) {
      $notice_msg = Lang::get('confide::confide.alerts.password_reset');
      return Redirect::action('UsersController@login')
          ->with('notice', $notice_msg);
    } else {
      $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
      return Redirect::action('UsersController@resetPassword', array('token'=>$input['token']))
          ->withInput()
          ->with('error', $error_msg);
    }
  }

  public function getProviderRegister(){
    $account = new Account();
    $account->provider = Session::get('provider')?Session::get('provider'):Input::get('provider');
    $account->provider_id = Session::get('provider_id')?Session::get('provider_id'):Input::get('provider_id');
    $user = new User();
    $user->email = Session::get('email')?Session::get('email'):Input::get('email');
    $user->gender = Session::get('gender')?Session::get('gender'):Input::get('gender');

    $this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.users.provider', compact('account', 'user'));
  }

  public function postProviderRegister(){

    $data = Input::only('provider', 'provider_id', 'meta');
    $validation = Validator::make($data, Account::$provider_rules);
    $account = new Account($data);

    $profile_data = Input::only(User::$profile_fields);
    $profile_validation = Validator::make($profile_data, User::$profile_rules);

    if($validation->passes()&&($profile_validation->passes())):

      $repo = App::make('UserRepository');
      $user = $repo->signup(Input::except('provider', 'provider_id', 'meta'));

      if (($user->id)) {

        $role = Role::where('name', '=', 'user')->first();
        $user->attachRole($role);
        $user->confirmed = 1;
        $user->save();
        $user->update($profile_data);

        $account->user_id = $user->id;
        $account->save();

        if (Config::get('confide::signup_email')) {
          Mail::queue(
            Config::get('confide::email_account_confirmation'),
            compact('user'),
            function ($message) use ($user) {
              $message
                  ->to($user->email, $user->username)
                  ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
            }
          );
        }
        Auth::loginUsingId($user->id, true);

        Mail::queue('emails.user-signup', ['data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
          $message->to('users@fabogo.com', 'Fabogo')->subject('Fabogo: New User Signup via facebook ['.time().']');
        });

        return Redirect::to('/');

      } else {
        $error = $user->errors()->all(':message');
          
        return Redirect::back()->withInput(Input::except('password'))
                  ->withErrors($validation)
                  ->with('provider', $account->provider )
                  ->with('provider_id', $account->provider_id )
                  ->with('error', join(',', $error));
      }
    else:
      return Redirect::back()->withInput(Input::except('password'))
                  ->withErrors($validation)
                  ->withErrors($profile_validation);
    endif;
  }


  public function authWithFacebook() {
    $fb = app(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);

    try{
      $token = $fb->getJavaScriptHelper()->getAccessToken();
    }catch (Facebook\Exceptions\FacebookSDKException $e){
      // Failed to obtain access token
      dd($e->getMessage());
    }

    if(!$token){
      // Obtain an access token.
      try{
        $token = $fb->getAccessTokenFromRedirect();
      }catch (Facebook\Exceptions\FacebookSDKException $e){
        dd($e->getMessage());
      }
    }

    if(!$token){
      $input = Input::get('fbObject');
      if(isset($input['accessToken'])){
        $token = $input['accessToken'];
      }
    }

    // if (! $token->isLongLived()) {
    //   $oauth_client = $fb->getOAuth2Client();
    //   try{
    //     $token = $oauth_client->getLongLivedAccessToken($token);
    //   }catch(Facebook\Exceptions\FacebookSDKException $e){
    //     dd($e->getMessage());
    //   }
    // }

    $fb->setDefaultAccessToken($token);

    $response = $fb->get('/me?fields=id,name,email');
    $facebook_user = $response->getGraphUser();


    $result = array('id'=>$facebook_user->getId(),
                    'name'=>$facebook_user->getName(),
                    'email'=>$facebook_user->getEmail());
    
    $fb_id = $result['id'];
    $email = $result['email'];
    $name = $result['name'];

      $account = new Account(['provider'=>Account::FACEBOOK, 
                              'provider_id'=>$result['id'], 
                              'meta'=>$result]);

      // do we have a user with this facebook id?
      if($user = $this->user->findByFacebookId($result['id'])){
        Auth::loginUsingId($user->id, true);
        MazkaraHelper::fbToken($token);
  

      }elseif(!isset($result['email'])){


        $email = $fb_id.'@facebook.com';


        $user =  User::create(User::getRepoDefaults($email));
        $user->quickSetRegistered(['name'=>$name]);
        $user->accounts()->save($account);

        Auth::loginUsingId($user->id, true);

        Mail::queue('emails.user-signup', ['data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
          $message->to('users@fabogo.com', 'Fabogo')->subject('Fabogo: New User Signup via facebook ['.time().']');
        });
        mzk_post_profile_wizrocket($user->id, $user->getSafeAttributes());

        

      }elseif($user = $this->user->findByEmail($result['email'])){

        $account = $user->accounts()->save($account);
        Auth::loginUsingId($user->id, true);
        MazkaraHelper::fbToken($token);
        mzk_post_profile_wizrocket($user->id, $user->getSafeAttributes());

      }else{
        $user =  User::create(User::getRepoDefaults($email));
        $user->quickSetRegistered(['name'=>$name]);
        $user->accounts()->save($account);

        Auth::loginUsingId($user->id, true);
        MazkaraHelper::fbToken($token);
        Mail::queue('emails.user-signup', ['data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
          $message->to('users@fabogo.com', 'Fabogo')->subject('Fabogo: New User Signup via facebook ['.time().']');
        });
        mzk_post_profile_wizrocket($user->id, $user->getSafeAttributes());
      }
      $user->setLastLoginIP();
      $redirectto = $_SERVER['HTTP_REFERER'];
    return Redirect::to($redirectto);//intended('/');
  }

  public function loginWithFacebook() {
    $fb = app(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
    $callback = Config::get('services.facebook.redirect');
    $scope = Config::get('services.facebook.scope');

    $login_url = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl($callback, $scope);

    return Redirect::to($login_url);
  }



  public function loginWithGoogle() {
    Session::put('redirect_to', $_SERVER['HTTP_REFERER']);
    return Socialite::driver('google')->redirect();
  }

  public function authWithGoogle() {

    $g_user = Socialite::driver('google')->user();
    $g_id = $g_user->getId();
    $email = $g_user->getEmail();
    $name = $g_user->getName();
    $token = $g_user->token;
    
    $account = new Account(['provider'=>Account::GOOGLE, 
                            'provider_id'=>$g_id, 
                            'meta'=>$g_user]);



      // do we have a user with this facebook id?
      if($user = $this->user->findByGoogleId($g_id)){
        Auth::loginUsingId($user->id, true);
      }elseif($user = $this->user->findByEmail($email)){
        $user->accounts()->save($account);
        Auth::loginUsingId($user->id, true);
      }else{

        $user =  User::create(User::getRepoDefaults($email));
        $user->quickSetRegistered(['name'=>$name]);
        $user->accounts()->save($account);

        Auth::loginUsingId($user->id, true);
        Mail::queue('emails.user-signup', ['data'=>$user->toArray(), 'server'=>$_SERVER], function($message) {
          $message->to('users@fabogo.com', 'Fabogo')->subject('Fabogo: New User Signup via Google ['.time().']');
        });
      }
      $user->setLastLoginIP();

      if(mzk_is_session_valid('redirect_to')){
        $redirect = Session::get('redirect_to');
        Session::forget('redirect_to');
        return Redirect::to($redirect);
      }else{
        return Redirect::intended('/');
      }
  }


  public function attachFacebookAccount(){
    // get data from input

    $user = User::find(Auth::user()->id);


    $fb = app(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);

    try {
        $token = $fb->getJavaScriptHelper()->getAccessToken();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        // Failed to obtain access token
        dd($e->getMessage());
    }

    if (! $token) {
      // Obtain an access token.
      try {
          $token = $fb->getAccessTokenFromRedirect();
      } catch (Facebook\Exceptions\FacebookSDKException $e) {
          dd($e->getMessage());
      }
    }

    if(!$token){
      $input = Input::get('fbObject');
      if(isset($input['accessToken'])){
        $token = $input['accessToken'];
      }
    }

    $fb->setDefaultAccessToken($token);

    $response = $fb->get('/me?fields=id,name,email');
    $facebook_user = $response->getGraphUser();


    $result = array('id'=>$facebook_user->getId(),
                    'name'=>$facebook_user->getName(),
                    'email'=>$facebook_user->getEmail());
    
    $fb_id = $result['id'];
    $email = $result['email'];
    $name = $result['name'];

      $account = new Account(['provider'=>Account::FACEBOOK, 
                              'provider_id'=>$result['id'], 
                              'meta'=>$result]);


    // do we have a user with this facebook id?
    if($usr = $this->user->findByFacebookId($result['id'], $user->id)){
      return Redirect::to('users/edit')->with('error', 'This account is already associated with another user');
    }elseif($usr = $this->user->findByEmail($result['email'], $user->id)){
      return Redirect::to('users/edit')->with('error', 'This account is already associated with another user');

    }else{

      $user->accounts()->save(new Account(['provider'=>Account::FACEBOOK, 
                                          'provider_id'=>$result['id'], 
                                          'meta'=>$result]));
      return Redirect::to('users/edit')->with('notice', 'Awesome! You have your facebook account now associated!');
    }





  }

  public function attachGoogleAccount(){
    // get data from input
    $code = Input::get( 'code' );
    $user = User::find(Auth::user()->id);

    // get fb service
    $gl = SocialOAuth::consumer('Google');

    // check if code is valid
    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {
      // This was a callback request from google, get the token
      $token = $gl->requestAccessToken( $code );

      // Send a request with it
      $result = json_decode( $gl->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );


      // do we have a user with this facebook id?
      if($user = $this->user->findByGoogleId($result['id'], $user->id)){
        return Redirect::to('users/edit')->with('error', 'This account is already associated with another user');
      }elseif($user = $this->user->findByEmail($result['email'], $user->id)){
        return Redirect::to('users/edit')->with('error', 'This account is already associated with another user');
      }else{
        $user->accounts()->save(new Account(['provider'=>Account::GOOGLE, 
                                            'provider_id'=>$result['id'], 
                                            'meta'=>$result]));
        return Redirect::to('users/edit')->with('notice', 'Awesome! You have your Google account now associated!');
      }

    }else {         // if not ask for permission first
      // get fb authorization
      $url = $gl->getAuthorizationUri();

      // return to facebook login url
      return Redirect::to( (string)$url );
    }

  }

  public function detachFacebookAccount(){
    $user = User::find(Auth::user()->id);
    $accounts = $user->accounts()->where('provider', '=', 'facebook')->get();

    foreach($accounts as $account){
      $account->delete(); 
    }
    return Redirect::to('users/edit')->with('notice', 'You just removed your facebook account!');
  }


  public function detachGoogleAccount(){
    $user = User::find(Auth::user()->id);
    $accounts = $user->accounts()->where('provider', '=', 'google')->get();

    foreach($accounts as $account){
      $account->delete(); 
    }
    return Redirect::to('users/edit')->with('notice', 'You just removed your google account!');
  }




  public function photos($id){

    $posts = Selfie::select()->with('cover', 'likes', 'comments')->orderBy('id', 'DESC')->paginate(40);

    $service_ids = DB::table('post_service')->distinct('service_id')->take(5)->lists('service_id');
    $service_tags = Service::whereIn('id', $service_ids)->get();
    $params = [];
    $filters = [];
    $user = User::find($id);
  
    return View::make('site.users.selfies.index', compact('posts', 'service_tags', 'filters', 'user', 'params', 'suggested_posts'));
  }

  public function videos($id){
    $posts = Video::select()->with('cover', 'likes', 'comments')->ofAuthors($id)->orderBy('id', 'DESC')->paginate(40);

    $service_ids = DB::table('post_service')->distinct('service_id')->take(5)->lists('service_id');
    $service_tags = Service::whereIn('id', $service_ids)->get();
    $params = [];
    $filters = [];
    $user = User::find($id);
  
    return View::make('site.users.videos.index', compact('posts', 'service_tags', 'filters', 'user', 'params', 'suggested_posts'));
  }

  public function photoStepOne($id){
    $user = User::find($id);
    $input['file'] = Request::file('avatar');
    $rules = ['file' => 'mimes:jpg,png,gif,jpeg'];

    $validator = Validator::make(
      $input, $rules
    );

    if ($validator->fails())
        return 'false';

    $identifier = str_random(20);
    $image = Image::make(Request::file('avatar'));
    $image->encode('jpg')->save(public_path(). '/data/temp/' . $identifier . '.jpg');
    $image_url = '/data/temp/' . $identifier . '.jpg';
    return View::make('site.users.partials.crop-avatar', compact('user', 'image_url', 'image'));
  }

  public function photoStepTwo($id){
    $user = User::find($id);
    $input = Input::all();

    $url = public_path().$input['img_url'];
    $img = Image::make($url);
    $img->crop( round($input['picture_width']), 
                round($input['picture_height']), 
                round($input['picture_x']), 
                round($input['picture_y']));
    $filename = public_path().'/data/temp/user_avatar_'.$id.'_'.md5(time()).'.jpg';
    $img->save($filename);
    $user->saveImage($filename);
    //Storage::delete($filename);
  }







  /**
   * Log the user out of the application.
   *
   * @return  Illuminate\Http\Response
   */
  public function logout()
  {
    $redirect = Redirect::back();
    Auth::logout();

    return $redirect;//Redirect::intended('/');
  }


}
