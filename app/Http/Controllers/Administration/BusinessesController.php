<?php
namespace App\Http\Controllers\Administration;


use Confide, BaseController, View, Config, Validator, Redirect, Input;
use  Location, Auth, DB, Request, Image, Excel, Response;

use App\Models\Activity;

use App\Models\Business;
use App\Models\Category;
use App\Models\Zone;
use App\Models\Service;
use App\Models\Highlight;
use App\Models\Photo;
use App\Models\Deal;
use App\Models\Promo;
use App\Models\Review;
use App\Models\Package;
use App\Models\User;
use App\Models\Discount;
use App\Models\Promotion;
use App\Models\Service_item;


use App\Http\Controllers\Controller;

class BusinessesController extends  Controller {

	/**
	 * Business Repository
	 *
	 * @var Business
	 */
	protected $business, $zone, $feed_manager;

	public function __construct(Business $business, Activity $activity, Zone $zone){
		$this->business = $business;
    $this->feed_manager = $activity;
		$this->zone = $zone;
    $this->layout = 'layouts.admin-content';
    //$this->setupLayout();
	}

	public function export(){

    $input = Input::all();

    $nm = 'Mazkara_Data_Dump_Page_'.($input['page']+1);

Excel::create($nm, function($excel) {

    $excel->sheet('Businesses', function($sheet) {
    $input = Input::all();

    $skip = 1000*$input['page'];


			$columns = DB::connection()->getSchemaBuilder()->getColumnListing("businesses");
			$categories = Category::all()->lists('slug', 'id')->all();
			$services = Service::all()->lists('slug', 'id')->all();
			$highlights = Highlight::all()->lists('slug', 'id')->all();

			$data = [];

			$businesses = Business::select()->byLocale()->take(1000)->skip($skip)->get();

			foreach($businesses as $business){
				$one = [];
				foreach($columns as $c){
					if($c == 'meta'){
						continue;
					}
					if(($c == 'updated_at')||($c == 'created_at')){
						$one[$c] = (string)$business->$c;
						// /continue;

					}else{
						$one[$c] = is_array($business->$c)?join(',', $business->$c):$business->$c;
					}
				}

				$meta = $business->meta; 
        $cats = $business->categories->lists('id', 'id')->all();
				foreach($categories as $ii=>$vv){
					$one['Category:'.$vv] = isset($cats[$ii])?1:0;//isset($meta['categories']->$ii)?1:0;
				}

        $one['categories_count'] = count($cats);
        $sers = $business->services->lists('id', 'id')->all();
				foreach($services as $ii=>$vv){
					$one['Service:'.$vv] = isset($sers[$ii])?1:0;//isset($meta['services']->$ii)?1:0;
				}			
        $one['services_count'] = count($sers);
        $his = $business->highlights->lists('id', 'id')->all();
				foreach($highlights as $ii=>$vv){
					$one['Highlight:'.$vv] = isset($his[$ii])?1:0;//isset($meta['highlights']->$ii)?1:0;
				}
        $one['highlights_count'] = count($his);

				$data[] = $one;			
			}



        $sheet->fromArray($data);

    });

})->export('csv');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

  public function bulk(){
    $input = Input::all();
    if(isset($input['ids'])):
      switch($input['action']){
        case 'chain':
          foreach($input['ids'] as $id){
            $b = Business::find($id);
            $b->chain_id = $input['chain'];
            $b->save();
          }
        break;
      }
    endif;


    return Redirect::to('/content/businesses')->with('notice', 'The entries have been updated');
  }

  public function toggleDebug($id){
    $business = $this->business->find($id);
    $business->toggleDebug();
    $business->save();
    if($business->isCallDebuggable()){
      $body = 'Debug '.$business->id.' '.$business->name.' - activated by '.Auth::user()->name;
        \Mail::raw($body, 
          function($message) 
              use ($body) {
          $message->to('bugs@fabogo.com', 'Fabogo')
                  ->from('hello@fabogo.com', 'Fabogo Debugger')
                  ->subject($body.'['.time().']')
                  ->replyTo('no-reply@fabogo.com');
      });

    }else{
      $body = 'Debug '.$business->id.' '.$business->name.' - deactivated by '.Auth::user()->name;
        \Mail::raw($body, 
          function($message) 
              use ($body) {
          $message->to('bugs@fabogo.com', 'Fabogo')
                  ->from('hello@fabogo.com', 'Fabogo Debugger')
                  ->subject($body.'['.time().']')
                  ->replyTo('no-reply@fabogo.com');
      });

    }
    return Redirect::back();
  }

	public function index(){

		$businesses = $this->business->query()->byLocale();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $businesses->searchBasic($input['search']);
      $params['name'] = $input['search'];
      $params['search'] = $input['search'];
    }

    if(Input::has('byID') && ($input['byID']!="")){
      $businesses->where('id', 'LIKE', $input['byID']);
      $params['byID'] = $input['byID'];
    }

    if(Input::has('bySlipID') && ($input['bySlipID']!="")){
      $businesses->where('ref', 'LIKE', $input['bySlipID']);
      $params['bySlipID'] = $input['bySlipID'];
    }

    if(Input::has('byLotID') && ($input['byLotID']!="")){
      $businesses->where('lot_id', 'LIKE', $input['byLotID']);
      $params['byLotID'] = $input['byLotID'];
    }

    if(Input::has('zone') && ($input['zone']!='')){
      $businesses->ofZones([$input['zone']]);
      $params['zone'] = $input['zone'];
    }

    if(Input::has('comments') && ($input['comments']!='')){
    	if($input['comments'] == 'unviewed'){
    		$businesses->whereHas('comments', function($q){
    			$q->where('comments.viewed', '<>', '1');
    		});

    	}elseif($input['comments'] == 'viewed'){
    		$businesses->whereHas('comments', function($q){
    			$q->where('comments.viewed', '=', '1');
    		});
    	}else{
	    	$businesses->has('comments');

    	}

      $params['comments'] = $input['comments'];
    }


    if(Input::has('deals') && ($input['deals']!='')){
      $businesses->hasDeals( ($input['deals']=='any') ? false : $input['deals']);
      $params['deals'] = $input['deals'];
    }
    if(Input::has('richness_index') && ($input['richness_index']!='')){
      $businesses->byRichnessIndex( $input['richness_index']);
      $params['richness_index'] = $input['richness_index'];
    }

    if(Input::has('packages') && ($input['packages']!='')){
      $businesses->hasActivePackages();
      $params['packages'] = $input['packages'];
    }

    if(Input::has('offers') && ($input['offers']!='')){
      $businesses->hasActivePackages();
      $params['offers'] = $input['offers'];
    }
    if(Input::has('specials') && ($input['specials']!='')){
      $businesses->hasActiveOffers();
      $params['specials'] = $input['specials'];
    }

    if(Input::has('deals_status') && ($input['deals_status']!='')){
      $businesses->byDealsStatus($input['deals_status']);
      $params['deals_status'] = $input['deals_status'];
    }

    if(Input::has('type') && ($input['type']!='')){
      $businesses->byBusinessType($input['type']);
      $params['type'] = $input['type'];
    }


    if(Input::has('chain') && ($input['chain']!='')){
      $businesses->ofChains([$input['chain']]);
      $params['chain'] = $input['chain'];
    }

    if(Input::has('service') && ($input['service']!='')){
      $businesses->ofServices([$input['service']]);
      $params['service'] = $input['service'];
    }

    if(Input::has('category') && ($input['category']!='')){
      $businesses->ofCategories([$input['category']]);
      $params['category'] = $input['category'];
    }

    if(Input::has('active') && ($input['active']!='')){
      $businesses->activeState($input['active']);
      $params['active'] = $input['active'];
    }

    if(Input::has('cost') && ($input['cost']!='')){
      $businesses->byCostEstimate($input['cost']);
      $params['cost'] = $input['cost'];
    }

    if(Input::has('sort') && ($input['sort']!='')){
      $params['sort'] = $input['sort'];
    	switch($params['sort']){
        case 'idAsc':
          $businesses->orderBy('id', 'ASC');
        break; 
        case 'idDesc':
          $businesses->orderBy('id', 'DESC');
        break; 
    		case 'nameAsc':
	    		$businesses->orderBy('name', 'ASC');
    		break; 
				case 'nameDesc':
	    		$businesses->orderBy('name', 'DESC');
    		break; 
				case 'lastUpdateAsc':
	    		$businesses->orderBy('updated_at', 'ASC');
    		break; 
				case 'lastUpdateDesc':
	    		$businesses->orderBy('updated_at', 'DESC');
    		break; 
    	}
    }else{
  		$businesses->orderBy('businesses.id', 'DESC');
    }

    if(Input::has('map')){
	    if($input['map']=='true'){
				$businesses->hasLocation();
	    }elseif($input['map']=='false'){
				$businesses->hasNoLocation();
	    }
	    $params['map'] = $input['map'];
    }


    if(Input::has('has_services')){
	    if($input['has_services']=='true'){
				$businesses->hasServices();
	    }elseif($input['has_services']=='false'){
				$businesses->hasNoServices();
	    }
	    $params['has_services'] = $input['has_services'];
    }

    if(Input::has('has_categories')){
	    if($input['has_categories']=='true'){
				$businesses->hasCategories();
	    }elseif($input['has_categories']=='false'){
				$businesses->hasNoCategories();
	    }
	    $params['has_categories'] = $input['has_categories'];
    }

    if(Input::has('has_highlights')){
	    if($input['has_highlights']=='true'){
				$businesses->hasHighlights();
	    }elseif($input['has_highlights']=='false'){
				$businesses->hasNoHighlights();
	    }
	    $params['has_highlights'] = $input['has_highlights'];
    }


    if(Input::has('photos')){

	    if($input['photos']=='true'){
				$businesses->with('photos')->hasPhotos();
	    }elseif($input['photos']=='false'){
				$businesses->with('photos')->hasNoPhotos();
	    }
	    $params['photos'] = $input['photos'];
    }

    if(Input::has('rate')){
	    if($input['rate']=='true'){
				$businesses->hasRateCards();
	    }elseif($input['rate']=='false'){
				$businesses->hasNoRateCards();
	    }
	    $params['rate'] = $input['rate'];
    }
    //->remember(60)
		$zones = $this->zone->byLocale()->defaultOrder()->get()->linkNodes();

    $businesses = $businesses->paginate(20);
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.index', compact('businesses', 'zones', 'params'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
		$current_location = Location::get();
    //->remember(60)
		$categories = Category::select()->where('parent_id', null)->get(); //byLocaleActive()

		$services = Service::byLocaleActive()->where('parent_id', null)->get();
    $zones = $this->zone->byLocale()->defaultOrder()->get();

		$highlights = Highlight::all();

		$selected_categories =[];
		$selected_services =[];
		$selected_highlights =[];
    return View::make('administration.businesses.create', compact('zones', 'selected_categories', 'categories', 'services', 'selected_services', 'highlights', 'selected_highlights'));
	}

  public function resluggify($id){
    $business = Business::find($id);
    $business->logThisSlug();
    $business->resluggify();
    $business->save();
    return Redirect::back();    
  }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = array_except(Input::except('images', 'cover', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
    if($input['type']=='business'){
      $validation = Validator::make($input, Business::$rules);
      unset($input['zones']);
      $data = Input::except('images', 'zones', 'rate_card', 'categories', 'cover',  'timings', 'highlights', 'deletablePhotos', 'services');

    }
    $zones = [];
    if($input['type']!='business'){
      $validation = Validator::make($input, Business::$basic_rules);
      unset($input['zone_id']);      
      $zones = $input['zones'];
      unset($input['zones']);
      $data = Input::except('images', 'zones',  'rate_card', 'categories', 'cover',  'timings', 'highlights', 'deletablePhotos', 'services');
    }

		//$data = Input::except('images', 'rate_card','categories', 'highlights', 'services');
		if ($validation->passes()){
			$business = $this->business->create($data);
			$cat = Input::only('categories');
			$business->categories()->attach($cat['categories']);
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);
      $business->save();

      if($input['type']=='business'){
        $business->updateZoneCache();

      }

      if($input['type']=='home-service'){
        $business->zones()->sync($zones);
        $business->save();
      }


      $business->updateRichnessMatrix();
      $business->updateCategoriesCount();
      $business->updateCategories();
      $business->save();
      $business->updateMetaCache();
			$images = Input::only('cover');
			$business->saveCover($images['cover']);

      $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
      $fdata = ['user_id'=>Auth::user()->id, 
                'itemable_type'=>'Business',
                'itemable_id'=>$business->id,
                'verb'=>'created',
                'meta'=>['business'=>['id'=>$business->id, 'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
                'owner'=>$owner]
                ];
      $feed = $this->feed_manager->create($fdata);

			
			return Redirect::route('admin.businesses.index');
		}

		return Redirect::route('admin.businesses.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}





	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

  public function getAccessibleBusiness($id){
    if(Auth::user()->hasRole('admin'))
    {
      $business = $this->business->find($id);
    }else{
      $business = $this->business->byLocale()->where('id', '=', $id)->first();
    }

    if(!is_object($business)){
      return Redirect::route('admin.businesses.index');
    }

    return $business;
  }

	public function show($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

  	//$this->layout = View::make('layouts.admin');
    $services = Service::all();

    $activities = $this->feed_manager->select()
                                        ->where('itemable_type', '=', 'Business')
                                        ->where('itemable_id', '=', $id)
                                        ->orderby('id', 'desc')->get();//paginate(10);


    return View::make('administration.businesses.show', compact('business', 'activities', 'services'));
	}





  public function getVirtualNumberAllocation($id)
  {
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

    //$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.virtual_numbers.show', compact('business'));
  }






	public function getRateCards($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.rate-cards', compact('business'));
	}

	public function postRateCards($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

		$images = Input::only('images');
		$rate_cards = Input::only('rate_card');
		$business->saveRateCards($rate_cards['rate_card']);
		$deletablePhotos = Input::only('deletablePhotos');

		$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

    $orderables = Input::only('orderables');
    if(isset($orderables['orderables'])):
      foreach($orderables['orderables'] as $p_id=>$p_order){
        $p = Photo::find($p_id);
        if(is_object($p)){
          $p->sort = $p_order;
          $p->save();
        }
      }
    endif;
    $business->setPopularity();
    $business->updateRichnessMatrix();

		return Redirect::route('admin.businesses.show.rate_cards', $id);//('BusinessesController@getRateCards', $id);
	}




	public function getPhotos($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.photos', compact('business'));
	}



  public function getCrop($id, $photoId)
  {
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }
    $photo = Photo::find($photoId);
    //$img = \Intervention\Image\ImageManager();
    $image = \Intervention\Image\ImageManagerStatic::make($photo->image->url('xlarge'));


    //$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.crop', compact('business', 'photo', 'image'));
  }


  public function postCrop($id, $photoId)
  {
    $business = $this->getAccessibleBusiness($id);
    $input = Input::all();
    if(!isset($business->id)){
      return $business; 
    }

    $business->cover_x_pos = $input['x'];
    $business->cover_y_pos = $input['y'];

    $business->save();
    return Redirect::route('admin.businesses.show', array('id'=>$id));
  }



	public function getPhoto($id, $photoId)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }
		$photo = Photo::find($photoId);
    //$img = \Intervention\Image\ImageManager();
		$image = \Intervention\Image\ImageManagerStatic::make($photo->image->url());


		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.photo', compact('business', 'photo', 'image'));
	}


	public function postPhoto($id, $photoId)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$photo = Photo::find($photoId);

		$img = Image::make($photo->image->url());
		$img->crop(round($input['w']), round($input['h']), round($input['x']), round($input['y']));
		$filename = storage_path().'/media/'.md5(time()).'.jpg';
		$img->save($filename);

		//$img->save($photo->image->url());//exit;
		$photo->image = $filename ;
		$photo->save();
    $business->updateRichnessMatrix();

		return Redirect::route('admin.businesses.show.photo', array('id'=>$id, 'photoId'=>$photoId));
	}

  public function postStockPhoto($id)
  {
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }
    $input = Input::only('has_stock_cover_image');
    $business->has_stock_cover_image = $input['has_stock_cover_image'];

    $business->save();

    $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
    $fdata = ['user_id'=>Auth::user()->id, 
              'itemable_type'=>'Business',
              'itemable_id'=>$business->id,
              'verb'=>'assigned-stock-cover-photo',
              'meta'=>['business'=>['id'=>$business->id, 'stock_photo'=>$business->has_stock_cover_image, 'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
              'owner'=>$owner]
              ];
    $feed = $this->feed_manager->create($fdata);

    return Redirect::back();
  }

  public function metaUpdate($id){
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

    $business->updateAllCache();
    $business->save();
    return Redirect::back()->with('notice', 'Cache Updated');
  }



	public function fixPhotos($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		foreach($business->photos as $photo):
			$img = Image::make($photo->image->url());
			$url = $photo->image->url();
			$url = explode('/', $photo->image->url());
			$fname = $url[count($url)-1];
			$dir = storage_path().'/media/photo_'.$photo->id.md5(time());
			mkdir($dir);

			$filename = $dir.'/'.$fname;
			$img->save($filename);

			$photo->image = $filename ;
			$photo->save();
		endforeach;
		
		return Redirect::back();//('admin.businesses.show.photo', array('id'=>$id, 'photoId'=>$photoId));
	}

	public function fixRateCards($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		foreach($business->rateCards as $photo):
			$img = Image::make($photo->image->url());
			$url = $photo->image->url();
			$url = explode('/', $photo->image->url());
			$fname = $url[count($url)-1];
			$dir = storage_path().'/media/rate_photo_'.$photo->id.md5(time());
			mkdir($dir);

			$filename = $dir.'/'.$fname;
			$img->save($filename);

			$photo->image = $filename ;
			$photo->save();
		endforeach;
		
		return Redirect::back();//('admin.businesses.show.photo', array('id'=>$id, 'photoId'=>$photoId));
	}


	public function fixDealPhotos($id)
	{
		$deal = Deal::find($id);
		foreach($deal->artworks as $photo):
			$img = Image::make($photo->image->url());
			$url = $photo->image->url();
			$url = explode('/', $photo->image->url());
			$fname = $url[count($url)-1];
			$dir = storage_path().'/media/deal_photo_'.$photo->id.md5(time());
			mkdir($dir);

			$filename = $dir.'/'.$fname;
			$img->save($filename);

			$photo->image = $filename ;
			$photo->save();
		endforeach;
		
		return Redirect::back();//('admin.businesses.show.photo', array('id'=>$id, 'photoId'=>$photoId));
	}


	public function rotatePhoto($id, $photoId, $degrees)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$photo = Photo::find($photoId);

		$img = Image::make($photo->image->url());
		$img->rotate($degrees);//crop(round($input['w']), round($input['h']), round($input['x']), round($input['y']));
		$filename = storage_path().'/media/'.md5(time()).'.jpg';
		$img->save($filename);

		$photo->image = $filename ;
		$photo->save();
		
		return Redirect::back();
	}


	public function postPhotos($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$input = array_except(Input::except('images', 'is_service', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

		$images = Input::only('images');
		$business->saveImages($images['images']);

		$deletablePhotos = Input::only('deletablePhotos');
    $deletablePhotos = is_array($deletablePhotos['deletablePhotos']) ? $deletablePhotos['deletablePhotos']:[];
		$business->removeAllImages($deletablePhotos);

    $orderables = Input::only('orderables');
    $is_service = Input::only('is_service');
    $is_service = is_array($is_service)?$is_service['is_service']:[];
    $is_service = is_array($is_service)?$is_service:[];

    if(isset($orderables['orderables'])):
      $orderables = Input::only('orderables');
      $orderables = is_array($orderables)?$orderables:[];

      foreach($orderables['orderables'] as $p_id=>$p_order){
        if(in_array($p_id, $deletablePhotos)){
          continue;
        }
        $p = Photo::find($p_id);
        $p->sort = $p_order;
        $p->type = (in_array((string)$p_id, $is_service) ? Photo::SERVICE : Photo::IMAGE);
        $p->save();
      }
    endif;


    $cover = Input::only('is_cover');
    $business->updateImageForCover($cover['is_cover']);
    $business->setPopularity();
    $business->updateMetaCache();

    $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
    $fdata = ['user_id'=>Auth::user()->id, 
              'itemable_type'=>'Business',
              'itemable_id'=>$business->id,
              'verb'=>'uploaded-photos',
              'meta'=>['business'=>['id'=>$business->id, 'photos_count'=>count($images['images']),
                                   'deleted_photos'=>count($deletablePhotos), 
                                   'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
              'owner'=>$owner]
              ];
      $feed = $this->feed_manager->create($fdata);
    $business->updateRichnessMatrix();

    if(Request::ajax()):
      return Response::Json([]);
    else:
  		return Redirect::route('admin.businesses.show.photos', $id);
    endif;
	}

  public function postBulkPhotos($id)
  {
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

    $input = array_except(Input::except('images', 'is_service', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

    $images = Input::only('images');
    $business->saveImages($images['images']);

    $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
    $fdata = ['user_id'=>Auth::user()->id, 
              'itemable_type'=>'Business',
              'itemable_id'=>$business->id,
              'verb'=>'uploaded-photos',
              'meta'=>['business'=>['id'=>$business->id, 'photos_count'=>count($images['images']),
                                   'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
              'owner'=>$owner]
              ];
      $feed = $this->feed_manager->create($fdata);
    $business->updateRichnessMatrix();

    if(Request::ajax()):
      return Response::Json([]);
    else:
      return Redirect::route('admin.businesses.show.photos', $id);
    endif;
  }

  public function postCheatRates($id){
    $input = Input::all();

    // delete all fake ratings
    $business = $this->business->find($id);
    Review::query()->isCheatRate()->byBusiness($id)->delete();
    $all_data = [];
    foreach($input['ratings'] as $rating=>$count){
      for($i=0;$i<$count;$i++){
        $data = ['rating'=>$rating];
        $data['ip'] = $rating.Auth::user()->id.time().$i;
        $data['user_id'] = 0;
        $data['business_id'] = $id;
        $data['body'] = ' ';
        $data['is_cheat'] = 1;
        $all_data[] = $data;
      }

    }

    Review::insert($all_data);

    $business->updateReviewCount();
    $business->updateRatingsCount();
    $business->updateAverageRating();

    return Redirect::back();//route('admin.businesses.show.photos', $id);
  }



	public function getBasic($id)
	{
    $business = $this->getAccessibleBusiness($id);

    if(!isset($business->id)){
      return $business; 
    }

    //remember(60)->
		//$categories = Category::byLocaleActive()->where('parent_id', null)->get();
    $categories = Category::select()->where('parent_id', null)->get(); //byLocaleActive()

		$selected_categories = $business->categories()->lists('category_id')->all();
    $zones = $this->zone->byLocale()->defaultOrder()->get()->linkNodes();

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.basic', compact('zones', 'business', 'categories', 'selected_categories'));
	}


	public function postBasic($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

    $input = array_except(Input::except('images', 'cover', 'deletablePhotos',  'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');


    if($input['type']=='business'){
      $validation = Validator::make($input, Business::$rules);
      unset($input['zones']);
      $data = Input::except('images', 'zones', 'rate_card', 'categories', 'cover',  'timings', 'highlights', 'deletablePhotos', 'services');

    }
    $zones = [];
    if($input['type']!='business'){
      $validation = Validator::make($input, Business::$basic_rules);
      unset($input['zone_id']);      
      $zones = $input['zones'];
      unset($input['zones']);
      $data = Input::except('images', 'zones',  'rate_card', 'categories', 'cover',  'timings', 'highlights', 'deletablePhotos', 'services');
    }

		//$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'zones', 'categories', 'deletablePhotos', 'cover', 'timings', 'highlights', 'deletablePhotos', 'services');

		if ($validation->passes()){
			$business->update($data);

			$deletablePhotos = Input::only('deletablePhotos');
			$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

			$images = Input::only('cover');
			$business->saveCover($images['cover']);

			$cat = Input::only('categories');
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);
      if($input['type']=='business'){
        $business->updateZoneCache();
      }

      if($input['type']!='business'){
        $business->zones()->sync($zones);
        $business->save();
      }

      $business->updateRichnessMatrix();

			$business->updateCategoriesCount();
			$business->updateCategories();
			$business->updateMetaCache();
      $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
      $fdata = ['user_id'=>Auth::user()->id, 
                'itemable_type'=>'Business',
                'itemable_id'=>$business->id,
                'verb'=>'edited',
                'meta'=>['business'=>['id'=>$business->id, 
                                     'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
                'owner'=>$owner]
                ];
      $feed = $this->feed_manager->create($fdata);

			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}

	public function getServices($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$services = Service::where('parent_id', null)->byLocaleActive()->get();
    $selected_services = $business->services()->lists('service_id')->all();
    $selected_service_prices = $business->services()->withPivot('starting_price')->lists('starting_price', 'service_id');
    $selected_known_for = $business->services()->withPivot('known_for')->lists('known_for', 'service_id');

    //$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.services', 
                                          compact('business', 
                                                  'services', 'selected_service_prices', 
                                                  'selected_services', 'selected_known_for'));
	}

	public function postServices($id){
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

    $services = Input::only('services');
    $starting_prices = Input::only('starting_prices');
    $known_for = Input::only('known_for');

    $services = isset($services['services'])?$services['services']:[];
    $starting_prices = isset($starting_prices['starting_prices'])?$starting_prices['starting_prices']:[];
    $known_for = isset($known_for['known_for'])?$known_for['known_for']:[];

    foreach($services as $ii=>$vv){
      $services[$ii] = [
                          'starting_price'=>strlen(trim($starting_prices[$ii]))>0?$starting_prices[$ii]:null,
                          'known_for'=>strlen(trim($known_for[$ii]))>0?$known_for[$ii]:0,

                        ];
    }

    $validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');
		//$business->update($data);
		$business->services()->sync($services);
		$business->updateServicesCount();
		$business->updateServices();
		$business->updateMetaCache();

      $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
      $fdata = ['user_id'=>Auth::user()->id, 
                'itemable_type'=>'Business',
                'itemable_id'=>$business->id,
                'verb'=>'edited-services',
                'meta'=>['business'=>['id'=>$business->id, 
                                     'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
                'owner'=>$owner]
                ];
      $feed = $this->feed_manager->create($fdata);

    $business->updateRichnessMatrix();

		return Redirect::route('admin.businesses.show', $id);
	}

  public function postAjaxServices($id){
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

    $services = Input::only('services');
    $starting_prices = Input::only('starting_prices');
    $known_for = Input::only('known_for');

    $services = isset($services['services'])?$services['services']:[];
    $starting_prices = isset($starting_prices['starting_prices'])?$starting_prices['starting_prices']:[];
    $known_for = isset($known_for['known_for'])?$known_for['known_for']:[];

    $srvces = [];
    foreach($services as $ii=>$vv){
      $srvces[$vv] = [
                        'starting_price'=>strlen(trim($starting_prices[$ii]))>0?$starting_prices[$ii]:null,
                        'known_for'=>strlen(trim($known_for[$ii]))>0?$known_for[$ii]:0,
                      ];
    }


    $data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');
    //$business->update($data);
    $business->services()->sync($srvces);
    $business->updateServicesCount();
    $business->updateServices();
    $business->updateMetaCache();

    return Response::json([]);
  }



  public function search(){
    $q = Input::get('term');
    $outlets = Business::query()->select('name', 'zone_cache', 'id')->byLocale(mzk_get_localeID())->searchBasic($q)->onlyActive()->take(20)->get();

    $result = [];
    foreach($outlets as $v){
      $result[] = [ 'label'=>$v->name.', '.$v->zone_cache,
                    'value'=>$v->name.', '.$v->zone_cache,
                    'id'=>$v->id];
    }

    return Response::json($result);

  }



	public function getHighlights($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		//$highlights = Highlight::remember(60)->byLocaleActive()->get();
    $highlights = Highlight::byLocaleActive()->get()->toArray();
    $active_highlight_ids = Highlight::byLocaleActive()->lists('highlight_id','highlight_id')->all();
    $highlights_all = Highlight::all()->toArray();

		$selected_highlights = $business->highlights()->lists('highlight_id','highlight_id')->all();
    
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.highlights', compact('business', 'highlights',
                                                                      'active_highlight_ids',
                                                                      'highlights_all',
                                                                      'selected_highlights'));
	}

	public function postHighlights($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		$business->update($data);
		$hi = Input::only('highlights');
		$business->highlights()->sync($hi['highlights']?$hi['highlights']:[]);
		$business->updateHighlightsCount();
		$business->updateHighlights();
		$business->updateMetaCache();
      $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
      $fdata = ['user_id'=>Auth::user()->id, 
                'itemable_type'=>'Business',
                'itemable_id'=>$business->id,
                'verb'=>'edited-highlights',
                'meta'=>['business'=>['id'=>$business->id, 
                                     'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
                'owner'=>$owner]
                ];

      $feed = $this->feed_manager->create($fdata);
    $business->updateRichnessMatrix();

		return Redirect::route('admin.businesses.show', $id);
	}

  public function postBrands($id){
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

    $brands = Input::only('brands');
    $business->brands()->sync($brands['brands']?$brands['brands']:[]);

    return Redirect::route('admin.businesses.show', $id);
  }

	public function getTimings($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }


		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.timings', compact('business'));
	}


	public function postTimings($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		$business->update($data);
		$timings = Input::only('timings');
		$business->saveTimings($timings['timings']);
    $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
    $fdata = ['user_id'=>Auth::user()->id, 
              'itemable_type'=>'Business',
              'itemable_id'=>$business->id,
              'verb'=>'edited-timings',
              'meta'=>['business'=>['id'=>$business->id, 
                                   'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
              'owner'=>$owner]
              ];
      $feed = $this->feed_manager->create($fdata);
    $business->updateRichnessMatrix();

		return Redirect::route('admin.businesses.show', $id);

	}

	public function getLocation($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.location', compact('business'));
	}


	public function postLocation($id)
	{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		$business->update($data);



    $owner = User::select('id', 'name','email')->where('id', '=',Auth::user()->id)->get()->first()->toArray();
    $fdata = ['user_id'=>Auth::user()->id, 
              'itemable_type'=>'Business',
              'itemable_id'=>$business->id,
              'verb'=>'edited-location',
              'meta'=>['business'=>['id'=>$business->id, 
                                   'name'=>$business->name, 'zone_cache'=>$business->zone_cache],
              'owner'=>$owner]
              ];
      $feed = $this->feed_manager->create($fdata);

    $business->updateRichnessMatrix();


		return Redirect::route('admin.businesses.show', $id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){

    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }


		if (is_null($business)){
			//return Redirect::route('admin.businesses.index');
		}

		$selected_categories = $business->categories()->lists('category_id');

    //remember(60)->
		$categories = Category::byLocaleActive()->where('parent_id', null)->get();
		$services = Service::byLocaleActive()->where('parent_id', null)->get();
		$highlights = Highlight::get();

		$selected_services = $business->services()->lists('service_id');
		$selected_highlights = $business->highlights()->lists('highlight_id');

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.businesses.edit', compact('business', 'selected_categories', 'categories', 'services', 'selected_services', 'highlights', 'selected_highlights'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if ($validation->passes())
		{
    $business = $this->getAccessibleBusiness($id);
    if(!isset($business->id)){
      return $business; 
    }

			$business->update($data);

			$images = Input::only('images');
			$business->saveImages($images['images']);
			$rate_cards = Input::only('rate_card');
			$business->saveRateCards($rate_cards['rate_card']);
			$deletablePhotos = Input::only('deletablePhotos');
			$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

			$cat = Input::only('categories');
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);


			$cat = Input::only('services');
			$business->services()->sync($cat['services']?$cat['services']:[]);

			$hi = Input::only('highlights');
			$business->highlights()->sync($hi['highlights']?$hi['highlights']:[]);

			$timings = Input::only('timings');
			$business->saveTimings($timings['timings']);
      $business->updateMetaCache();
      $business->updateRichnessMatrix();

			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::route('admin.businesses.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}






	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createDeal($business_id, $type)
	{
		$business = $this->business->find($business_id);
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.deals.create', compact('business', 'type'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeDeal($business_id, $type){

		$business = $this->business->find($business_id);
		$input = Input::except('artworks', '_token');
		$input['business_id'] = $business_id;

		$DEAL_CLASS= Deal::getTypeClass(strtolower($type));

		$validation = Validator::make($input, $DEAL_CLASS::$rules);

		if ($validation->passes()){
			$deal = $DEAL_CLASS::create($input);

			$photo = Input::only('artworks');
			$deal->saveImages($photo['artworks']);

			return Redirect::route('admin.businesses.show', array($business_id));
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editDeal($business_id, $type, $id)
	{
		$business = $this->business->find($business_id);
		
		$DEAL_CLASS= Deal::getTypeClass(strtolower($type));
		$deal = $DEAL_CLASS::find($id);

		if (is_null($deal))
		{
			return Redirect::back();
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.deals.edit', compact('business', 'deal', 'type'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateDeal($business_id, $type, $id)
	{
		$business = $this->business->find($business_id);

		$input = array_except(Input::all(), '_method');
		$input['business_id'] = $business_id;
		$DEAL_CLASS= Deal::getTypeClass(strtolower($type));

		$validation = Validator::make($input, $DEAL_CLASS::$rules);
		$deletablePhotos = Input::only('deletablePhotos');

		if ($validation->passes()){
			$deal = $DEAL_CLASS::find($id);
			$deal->update($input);
			$photo = Input::only('artworks');
			$deal->saveImages($photo['artworks']);
			$deletablePhotos = Input::only('deletablePhotos');
			$deal->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

			return Redirect::route('admin.businesses.show', array($business_id));
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function destroyDeal($id){
		Deal::find($id)->delete();
		return Redirect::back();
	}

  public function toggleMenuSample($id){
    $business = $this->getAccessibleBusiness($id);
      $data = [];

    if($business->has_sample_menu == 1){
      $data['has_sample_menu'] = 0;
    }else{
      $data['has_sample_menu'] = 1;

    }

    $business->update($data);
    $input = Input::all();
    if(isset($input['return'])){
      return Redirect::back();
    }else{
      return Redirect::to(route('admin.businesses.show', array($business->id)).'#items');
    }
  }

  public function copyServicesToVenue(){


    $input = Input::all();
    $source_business = $input['source_business'];
    $target_business = $input['target_business'];

    if($source_business==$target_business){
      return Redirect::to(route('admin.businesses.show', array($business->id)).'#items')
          ->with('notice', 'Did not copy as both source and target were the same ');

    }

    $source_business = $this->business->find($source_business);
    $target_business = $this->business->find($target_business);
    $services = $source_business->services;

    $s = [];
    foreach($services as $service){

      $s[$service->id] = ['starting_price'=>$service->pivot->starting_price];
    }
    $target_business->services()->sync($s);

    return Redirect::back()
          ->with('notice', 'Items have been copied from '.$source_business->name.' to '.$target_business->name);


  }

  public function copyItemsToVenue(){
    $input = Input::all();
    $source_business = $input['source_business'];
    $target_business = $input['target_business'];

    if($source_business==$target_business){
      return Redirect::to(route('admin.businesses.show', array($business->id)).'#items')
          ->with('notice', 'Did not copy as both source and target were the same ');

    }

    $source_business = $this->business->find($source_business);
    $target_business = $this->business->find($target_business);
    $items = $source_business->serviceItems()->get()->toArray();
    foreach($items as $item){
      $item['business_id'] = $target_business->id;
      unset($item['id']);
      Service_item::create($item);
    }

    return Redirect::to(route('admin.businesses.show', array($target_business->id)).'#items')
          ->with('notice', 'Items have been copied from '.$source_business->name.' to '.$target_business->name);

  }


  public function cheatRate(){
    $input = Input::all();
    $data = ['rating'=>Input::only('rating')];
    $data['ip'] = Auth::user()->id.time();
    $business = $this->business->find($input['business_id']);
    $business->rate($data, true);

    $result = [ 'average'=>$business->rating_average, 
                'rating'=>$input['rating'], 'num-rating'=>Review::query()->isCheatRate()->byRating($data['rating'])->byBusiness($input['business_id'])->get()->count(),
                'count'=>$business->accumulatedReviewsCount()];
    return Response::json($result);
  }

  public function deleteCheatRate(){
    $input = Input::all();
    $data = ['rating'=>Input::only('rating')];
    $cheat_review = Review::query()->isCheatRate()->byRating($data['rating'])->byBusiness($input['business_id'])->first();
    $cheat_review->delete();
    $business = $this->business->find($input['business_id']);
    $business->updateReviewCount();
    $business->updateAverageRating();

    $result = [ 'average'=>$business->rating_average, 
                'rating'=>$input['rating'], 'num-rating'=>Review::query()->isCheatRate()->byRating($data['rating'])->byBusiness($input['business_id'])->get()->count(),
                'count'=>$business->accumulatedReviewsCount()];
    return Response::json($result);
  }



  public function toggleHoldVirtualNumber($id){
    $business = $this->business->find($id);
    if($business->isCurrentlyAllocatedAVirtualNumber() == false){
      return false;
    }
    
    $cn = $business->current_virtual_number_allocation();
    $cn->toggleHold();
    return Response::json($cn->toArray());

  }



  public function listFacebookLikeBoxed(){

    $businesses = $this->business->query()->byLocale();
    $params = [];
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $businesses->search($input['search']);
      $params['name'] = $input['search'];
      $params['search'] = $input['search'];
    }

    if(Input::has('byID') && ($input['byID']!="")){
      $businesses->where('id', 'LIKE', $input['byID']);
      $params['byID'] = $input['byID'];
    }

    

   if(Input::has('facebook') && (!in_array($input['facebook'], ['', 'all']))){
      if($input['facebook'] == 'facebook'){
        $businesses->facebookPageActive();
      }else{
        $businesses->facebookPageInactive();
      }
      $params['facebook'] = $input['facebook'];
    }



    if(Input::has('sort') && ($input['sort']!='')){
      $params['sort'] = $input['sort'];
      switch($params['sort']){
        case 'idAsc':
          $businesses->orderBy('id', 'ASC');
        break; 
        case 'idDesc':
          $businesses->orderBy('id', 'DESC');
        break; 
        case 'nameAsc':
          $businesses->orderBy('name', 'ASC');
        break; 
        case 'nameDesc':
          $businesses->orderBy('name', 'DESC');
        break; 
        case 'lastUpdateAsc':
          $businesses->orderBy('updated_at', 'ASC');
        break; 
        case 'lastUpdateDesc':
          $businesses->orderBy('updated_at', 'DESC');
        break; 
      }
    }else{
      $businesses->orderBy('businesses.id', 'DESC');
    }

    $businesses = $businesses->paginate(20);

    return View::make('administration.facebook.index', compact('params', 'businesses'));
  }


  public function getFacebookLikeBox($id){
    $business = $this->business->find($id);
    return View::make('administration.facebook.form', compact('business'));
  }

  public function postFacebookLikeBox($id){
    $business = $this->business->find($id);
    $input = Input::all();

    $business->facebook_like_box_id = $input['facebook_like_box_id'];
    $business->facebook_like_box_status = $input['facebook_like_box_status'];
    $business->facebook_like_box_valid_until = $input['facebook_like_box_valid_until'];
    $business->save();
    return Redirect::to(route('admin.businesses.facebook.index'))->with('notice', $business->name.' facebook page details saved');

  }



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this->business->find($id)->delete();

		return Redirect::route('admin.businesses.index');
	}

}
