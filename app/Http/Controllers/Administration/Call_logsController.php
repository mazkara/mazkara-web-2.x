<?php
namespace App\Http\Controllers\Administration;


use View, Config, Validator, Mail, DB, Response, Redirect, Input, MazkaraHelper;

use App\Models\Call_log;
use App\Models\Virtual_number;
use App\Models\Comment;
use App\Models\Virtual_number_allocation;
use App\Models\Business;
use GuzzleHttp\Client as HttpClient;

use App\Http\Controllers\Controller;


class Call_logsController extends Controller {

	/**
	 * Call_log Repository
	 *
	 * @var Call_log
	 */
	protected $call_log, $business, $log;

	public function __construct(Call_log $call_log, Business $business)
	{
		$this->call_log = $call_log;
		$this->business = $business;
		$this->log = array('body'=>'', 'subject'=>'');
		$this->send_log = true;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$call_logs = $this->call_log->all();

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.index', compact('call_logs'));
	}

	public function emailLog(){
		if($this->send_log != true){
			return false;
		}

		$subject = $this->log['subject'];

	    Mail::raw($this->log['body'], 
					function($message) 
							use ($subject) {
		      $message->to('ali@fabogo.com', 'Fabogo')
		        			->from('hello@fabogo.com', 'Fabogo Call Debugger')
		              ->subject($subject)
		              ->replyTo('no-reply@fabogo.com');
	    });
	}

	public function setLogSubject($msg){
		$this->log['subject'] = $msg;
	}

	public function appendLogBody($msg){
		$nl ="\n";
		$this->log['body'].=$msg.$nl;
	}

	public function logCall(){

		$input = Input::only(Call_log::$fields);
		$all = Input::all();


		$validation = Validator::make($input, Call_log::$rules);
		$called_number = '+'.trim(str_replace('+', '',trim($input['called_number'])));
		$caller_number = '+'.trim(str_replace('+', '',trim($input['caller_number'])));

		$this->appendLogBody("Call Recieved\n=======\nCall Details\n======\n");
		foreach($input as $i=>$v){
			$this->appendLogBody($i." : ".$v);
		}
		if(!isset($all['menu'])){
			$all['menu'] = 'None';
		};

		$input['type_of_call'] = ($all['menu'] == 'None') ? 0 : $all['menu'];

		// if validation doesnt pass then return an error message
		if (!$validation->passes()){
			$result = mzk_api_success_response($validation->errors());
			$this->appendLogBody('Validation Failed');
			$this->appendLogBody(implode(',', $validation->errors()->all()));
			$this->setLogSubject('Call Log Validation failed '.time());
			$this->emailLog();

			return Response::Json($result);
		}

		//is the called number an existing virtual number we have?
		$vn = Virtual_number::query()->byBody($called_number);//'+97145508313');//
		$this->appendLogBody('Virtual Number Identification in process for: '.$called_number);

		if($vn->count() == 0){
			$this->appendLogBody('Virtual Number was not found');
			$this->setLogSubject('Call Log failed:[Virtual Number was not found] '.time());
			// $this->emailLog();

			$result = mzk_api_success_response('Called number does not exist.');
			return Response::Json($result);
		}

		$this->appendLogBody('Virtual Number identified of ID '.$vn->get()->first()->id);

		// is this number allocated to a businesss
		$current_business = $vn->get()->first()->current_business();//->get()->first();
		$this->appendLogBody('Identifying Business allocated to Virtual Number');

		if(!$current_business){
			$this->appendLogBody('Virtual Number not allocated to existing venue');
			$this->setLogSubject('Call Log failed:[Virtual Number not allocated to existing venue] '.time());
			//$this->emailLog();
			$result = mzk_api_success_response('Called number is not allocated to an existing venue');
			return Response::Json($result);
		}

		if($current_business->isCallDebuggable()!=true){
			$this->send_log = false;
		}

		$this->appendLogBody('Business allocated to Virtual Number identified as '.$current_business->id.' - '.$current_business->name);

		$input['business_id'] = $current_business->id;
		// set if the call logged for this virtual number is a ppl invoiced one or not
		$cvn = $vn->get()->first()->current_virtual_number_allocations();
		$input['is_ppl'] = $cvn->is_ppl;
		$this->appendLogBody('Current Virtual numbers PPL status is: '.$cvn->is_ppl);

		// create the call
		$call_log_object = $this->call_log->create($input);
		$this->appendLogBody('Call has been logged and stored to database with ID: '.$call_log_object->id);
					
		//prepare to send email alert to the merchant
		$call_log = $call_log_object->toArray();
    $call_log['business_name'] = $current_business->name;

		$this->appendLogBody('Identifying merchant account for Business: '.$current_business->id.' - '.$current_business->name);

    $merchant = $current_business->merchants()->first();

    if(!$merchant){
			$this->appendLogBody('Could not identify merchant account');
			$this->setLogSubject('Call Log failed:[Merchant not found] '.time());
			$this->emailLog();
			$result = mzk_api_success_response('Called number is not allocated to an existing merchant');
			return Response::Json($result);

    }

		$this->appendLogBody('Merchant identified as : '.$merchant->id.' - '.$merchant->name);

		$this->appendLogBody('Acquiring preferred emails for merchant : '.$merchant->id.' - '.$merchant->name);
    $emails = [];
    foreach($merchant->businesses()->withPivot(['preferred_email'])->get() as $b){
    	if($b->id == $current_business->id){
    		if(strlen(trim($b->preferred_email))>0){
	    		$emails[] = $b->preferred_email;
    		}
    	}
    }

    $this->appendLogBody('Total acquired preferred emails: '.count($emails).' : ['.join(',', $emails).']');

    if(count($emails)==0){
	    $emails = $merchant->email;
	    $emails = explode(',', $emails);
	    $this->appendLogBody('No preferred emails found send to merchants email address: ['.join(',', $emails).']');
    }

		$email = array_pop($emails);
		$emails = count($emails)==0?[]:$emails;

    $poc = $merchant->pocs()->first(); 

		$this->appendLogBody('Retrieving POC for merchant '.$merchant->id.' - '.$merchant->name);

    if(!$poc){
			$this->appendLogBody('Could not identify POC for merchant account');
			$this->setLogSubject('Call Log failed:[POC for Merchant not found] '.time());
			$this->emailLog();
			$result = mzk_api_success_response('POC not found for merchant');
			return Response::Json($result);
    }

    $poc_email = $poc->email; 
    $emails[] = $poc_email;

    $this->appendLogBody('Acquire POC email address: ['.$poc_email.']');

    $this->appendLogBody('Complete list of emails to be sent alerts: ['.join(',', $emails).']');

    // set the correct time of the call for the email
    // the time of the calls by default are in INDIAN STANDARD TIME

    if($current_business->isUAE()){

    	$call_log['date_time'] = mzk_time_convert_from_ist_to_uae($call_log['dated'].' '.$call_log['timed']); 
    }else{
    	$call_log['date_time'] = $call_log['dated'].' '.$call_log['timed'];
    }

    // mark call state variable as recieved or missed
		$call_log['date_time'] = mzk_f_date($call_log['date_time'], 'd/m/Y h:ia');

		$timezone = $current_business->isUAE()?'uae':'ist';
    $this->appendLogBody('Set Time zone to '.$timezone);

//    if($timezone == 'uae'){
//      $call_log['date_time'] = mzk_time_convert_from_ist_to_uae($call_log['dated'].' '.$call_log['timed']); 
//    }

		$call_state = strstr($call_log['call_transfer_status'], 'Connected') ? 'received':'missed';

    $result = mzk_api_response(compact('call_log'), 200, true, 'Success');

    $this->appendLogBody('Call state for call set to '.$call_state);

    if(strstr($call_log['call_transfer_status'], 'Not Connected')){
    	$this->appendLogBody('Call was not connected - no emails or smss sent to merchant');
			$this->emailLog();
			return Response::Json([]);
    }
    
    $send_sms = $merchant->id == 18?true:true;

    if($call_log_object->isAccessibleByClient()){
    	$this->appendLogBody('Call type is of either 0 or 3 - sms will be sent');
    	$send_sms = true;
    }else{
    	$this->appendLogBody('Call type is neither 0 nor 3 - sms will not be sent');
    	$send_sms = false;
    }

		if($send_sms == true):
			$client = new HttpClient();
    	$this->appendLogBody('Prepare sms for dispatch');

			// sms to user commented out
			/* $url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage';
			$url.='&send_to='.$call_log['called_number'];
			$msg = 'Hello! Glad you used us to find your Beauty & Wellness venue.';
			$msg .='Please give us a missed call on +971556874464 if you were not satisfied';
			$msg .=' with your experience.And keep visiting us (www.mazkara.com) to discover salons,';
			$msg .=' spas & fitness centres around you in the future.';
			$url.='&msg='.urlencode($msg);


			$url.='&msg_type=TEXT&userid=2000154800&auth_scheme=plain';
			$url.='&password=nXOjprcpf&v=1.1&format=text';
		  $response = $client->get($url); */

			// sms to client
			// get the clients phone number
			$client_dude = $merchant;//->users()->first();
			// if the client has a phone
			if(trim($client_dude->phone)!=''):
	    	$this->appendLogBody('Merchant has Phone attached to recieve sms');

				$contents = \DB::table('settings')->where('zone_id', '=', $merchant->city_id)->lists('value','name');

				$url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage';
				$url.='&msg_type=TEXT&auth_scheme=plain&v=1.1&format=text';

				// send message to merchant
				$tos = explode(',', $client_dude->phone);
				foreach ($tos as $ii=>$to) {
					$to = trim($to);
					$to = ltrim($to, '+');
					$to = ltrim($to, '0');
					$tos[$ii] = $to;
				}

				$to = join(',', $tos);

					$urla ='&send_to='.$to;

					$msg = $contents['call_log_sms_to_client'];

					$msg = str_replace('{{client_name}}', $merchant->name, $msg);
					$msg = str_replace('{{call_status}}', $call_state.' ', $msg);
					$msg = str_replace('{{called_number}}', $called_number.' ', $msg);
					$msg = str_replace('{{caller_number}}', $caller_number.' ', $msg);
					$msg = str_replace('{{called_date_time}}', $call_log['date_time'], $msg);

					if(trim($msg) == ''){
						$msg = 'You just received a call from Fabogo. ';
						$msg .='Caller No:'.$call_log['called_number'].' Time: '.$call_log['date_time'];
					}

		    	$this->appendLogBody('Message prepared for client >>>>>>');
		    	$this->appendLogBody($msg);
		    	$this->appendLogBody("<<<<<<");

					$urla.='&msg='.urlencode($msg);
					if(substr($to, 0, 2)=='91'){
						$urla.='&userid=2000154800&password=nXOjprcpf';
					}else{
						$urla.='&userid=2000155758&password=WwuTDt';
					}

		    	$this->appendLogBody('URL to post for sms: '.$urla);


					if(mzk_is_mobile_phone($to)){

			    	$this->appendLogBody('SMS has been posted - Response >>>>>>');

				    $response = $client->get($url.$urla);
				    $str_response = (string) $response->getBody(); // returns all the contents
				    // ob_start();
				    // var_dump($response);
				    // $str_response = ob_get_contents();
				    // ob_clean();

			    	$this->appendLogBody($str_response);
			    	$this->appendLogBody('<<<<<<');

					}else{
			    	$this->appendLogBody('SMS has not been posted - phone is not a mobile phone');
					}

					/*if($input['business_id'] == 738){

				    $response = $client->get($url.$urla.'&send_to=971564156571');

				    Mail::raw('STR:'.$url.$urla, 
							function($message) 
									 {
					      $message->to('ali@fabogo.com', 'Fabogo Troubleshoot')
					        			->from('hello@fabogo.com', 'Call Alert Troubleshoot')
					              ->subject('SMS STATUS FOR 738')
					              ->replyTo('no-reply@fabogo.com');
				    });
					}*/
			else:
	    	$this->appendLogBody('SMS has not been posted - client has no valid phone');
			endif;


			$send_user_sms = false;
	    if($send_user_sms == true):
				// send message to user
				$to = $call_log['caller_number'];
				$to = ltrim($to, '+');
				$to = ltrim($to, '0');

				$urlb ='&send_to='.$to;
				
				$msg = $contents['call_log_sms_to_user'];
				$msg = str_replace('{{business_name}}', $current_business->name.' '.$current_business->zone_cache, $msg);

				if(trim($msg) == ''){
					$msg = 'Thanks for calling '.$current_business->name.' using Fabogo. Review them on Fabogo and contribute to the beauty community. ';
				}

				$urlb.='&msg='.urlencode($msg);
				if(substr($to, 0, 2)=='91'){
					$urlb.='&userid=2000154800&password=nXOjprcpf';
				}else{
					$urlb.='&userid=2000155758&password=WwuTDt';
				}

				if(mzk_is_mobile_phone($to)){
			    $response = $client->get($url.$urlb);
			  }
	    endif;
		endif;

    if($call_log_object->isAccessibleByClient()){
    	$this->appendLogBody('Call type is of either 0 or 3 - email alert will be sent');

	    Mail::send('emails.notify-call', 
							['data'=>$call_log, 'server'=>$_SERVER], 
					function($message) 
							use ($call_log, $emails, $poc_email, $email) {
				    	$state = strstr($call_log['call_transfer_status'], 'Connected')?'received':'missed';
		      $message->to($email, 'Fabogo')
		        			->from('hello@fabogo.com', 'Fabogo Call Alert')
		        			->cc($emails)
		              ->subject('You just '.$state.' a call from Fabogo')
		              ->replyTo('no-reply@fabogo.com');
	    });
    	$this->appendLogBody('Email alert sent to emails '.join(',', $emails));

    }else{
    	$this->appendLogBody('Call type is neither 0 nor 3 - email alert will not be sent');
    }

  	$this->appendLogBody('Call Log Process Completed <<<<');
  	$this->emailLog();

		$result = mzk_api_success_response($result);
		return Response::Json($result);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.create');
	}

	public function testSMS(){
		$client = new HttpClient();

		$phone = '+919005882762';// '+971506789677';
		$url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage';
		$url.='&msg_type=TEXT&auth_scheme=plain&v=1.1&format=text';

		// send message to merchant
		$to = $phone;
		$to = ltrim($to, '+');
		$to = ltrim($to, '0');

		$urla ='&send_to='.$to;
		$call_log = Call_log::select()->first()->toArray();
		$contents = \DB::table('settings')->where('zone_id', '=', 88)->lists('value','name');
  	$call_log['date_time'] = $call_log['dated'].' '.$call_log['timed'];

		$msg = $contents['call_log_sms_to_client'];

		$msg = str_replace('{{client_name}}', 'Beauty Bay', $msg);
		$msg = str_replace('{{call_status}}', 'recieved'.' ', $msg);
		$msg = str_replace('{{called_number}}', $call_log['called_number'].' ', $msg);
		$msg = str_replace('{{caller_number}}', $call_log['caller_number'].' ', $msg);
		$msg = str_replace('{{called_date_time}}', $call_log['date_time'], $msg);

		$urla.='&msg='.urlencode($msg);
		if(substr($to, 0, 2)=='91'){
			$urla.='&userid=2000154800&password=nXOjprcpf';
		}else{
			$urla.='&userid=2000155758&password=WwuTDt';
		}

		if(mzk_is_mobile_phone($to)){
	    $response = $client->get($url.$urla);
		}
		var_dump($msg, $url.$urla);

		return Response::Json($response);

	}


	public function getIndex($id)
	{
		$call_logs = $this->call_log->query();
		$business = $this->business->find($id);
		$call_logs = $call_logs->byBusiness($business->id)->onlyConnected()->orderBy('call_logs.id', 'DESC');
		$call_logs = $call_logs->paginate(20);

    return View::make('administration.call_logs.index', compact('call_logs', 'business'));
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$this->call_log->create($input);

			return Redirect::route('admin.call_logs.index');
		}

		return Redirect::route('admin.call_logs.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$business = $this->business->findOrFail($id);
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.show', compact('business'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$call_log = $this->call_log->find($id);

		if (is_null($call_log))
		{
			return Redirect::route('admin.call_logs.index');
		}
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.call_logs.edit', compact('call_log'));
	}

	public function getCallLogsForInvoiceForm(){
		$data = Input::all();
		$outlets = count($data['outlets'])>0 ? $data['outlets'] : [];
		$start_date = $data['start_date'] ? $data['start_date'] : date('Y-m-d');
		$end_date = $data['end_date'] ? $data['end_date'] : date('Y-m-d');
		$unit_price = $data['unit_price'] ? $data['unit_price'] : 5;

		$call_logs = Call_log::query()
														->billable()
														->byBusinesses($outlets)
														->onlyConnected()
														->betweenDates($start_date, $end_date)
														->get();
		$result = array();
		$view = View::make('administration.invoices.ppl.call-logs-list', compact('data', 'call_logs'));
		$result['html'] = $view->render();
		return Response::Json($result);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Call_log::$rules);

		if ($validation->passes())
		{
			$call_log = $this->call_log->find($id);
			$call_log->update($input);

			return Redirect::route('admin.call_logs.show', $id);
		}

		return Redirect::route('admin.call_logs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->call_log->find($id)->delete();

		return Redirect::route('admin.call_logs.index');
	}

}
