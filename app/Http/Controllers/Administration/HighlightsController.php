<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, MazkaraHelper;

use App\Models\Highlight;

use App\Http\Controllers\Controller;

class HighlightsController extends Controller {

	/**
	 * Highlight Repository
	 *
	 * @var Highlight
	 */
	protected $highlight;

	public function __construct(Highlight $highlight)
	{
		$this->highlight = $highlight;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$highlights = $this->highlight->all();

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.highlights.index', compact('highlights'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.highlights.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Highlight::$rules);

		if ($validation->passes())
		{
			$highlight = $this->highlight->create($input);
			$highlight->attachCities();
			$highlight->zones()->updateExistingPivot(MazkaraHelper::getAdminDefaultLocaleID(), ['state'=>$input['state']]);
			
			mzk_reset_all_highlights_cache();
			
			return Redirect::route('admin.highlights.index');
		}

		return Redirect::route('admin.highlights.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$highlight = $this->highlight->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.highlights.show', compact('highlight'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$highlight = $this->highlight->find($id);

		if (is_null($highlight))
		{
			return Redirect::route('admin.highlights.index');
		}

		//$this->layout = View::make('layouts.admin');
    return   View::make('administration.highlights.edit', compact('highlight'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Highlight::$rules);

		if ($validation->passes())
		{
			$highlight = $this->highlight->find($id);
			$highlight->update($input);
			$highlight->zones()->updateExistingPivot(MazkaraHelper::getAdminDefaultLocaleID(), ['state'=>$input['state']]);
			mzk_reset_all_highlights_cache();

			return Redirect::route('admin.highlights.show', $id);
		}

		return Redirect::route('admin.highlights.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->highlight->find($id)->delete();

		return Redirect::route('admin.highlights.index');
	}

}
