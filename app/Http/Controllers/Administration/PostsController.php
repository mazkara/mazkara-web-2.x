<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, MazkaraHelper;

use App\Models\Post;


use App\Http\Controllers\Controller;


class PostsController extends Controller {

	/**
	 * Post Repository
	 *
	 * @var Post
	 */
	protected $post;

	public function __construct(Post $post)
	{
		$this->post = $post;
    $this->layout = 'layouts.admin-editor';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$posts = $this->post->query()->onlyPosts();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('state') && ($input['state']!="")){
      $posts->ofStates($input['state']);
      $params['state'] = $input['state'];
    }

    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

		$posts = $posts->orderby('id', 'desc')->paginate(20);

		return  View::make('administration.posts.index', compact('params', 'posts'));
	}

	public function published()
	{

		$posts = $this->post->query()->onlyPosts();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }

    $posts->ofStates('published');

    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

		$posts = $posts->orderby('id', 'desc')->paginate(20);

		return  View::make('administration.posts.published', compact('params', 'posts'));
	}


  public function all()
  {

    $posts = $this->post->query();
    $params = [];
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }


    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

    $posts = $posts->orderby('id', 'desc')->paginate(20);

    return  View::make('administration.posts.all', compact('params', 'posts'));
  }



	public function rejects()
	{

		$posts = $this->post->query()->onlyPosts();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }

    $posts->ofStates('reject');

    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

		$posts = $posts->orderby('id', 'desc')->paginate(20);

		return  View::make('administration.posts.rejects', compact('params', 'posts'));
	}

  public function drafts()
  {

    $posts = $this->post->query()->onlyPosts();
    $params = [];
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }

    $posts->ofStates('draft');

    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

    $posts = $posts->orderby('id', 'desc')->paginate(20);

    return  View::make('administration.posts.drafts', compact('params', 'posts'));
  }


	public function pending()
	{

		$posts = $this->post->query()->onlyPosts();
		$params = [];
		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }

    $posts->ofStates('publish-for-review');

    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

		$posts = $posts->orderby('id', 'desc')->paginate(20);

		return  View::make('administration.posts.pending', compact('params', 'posts'));
	}
  public function archives()
  {

    $posts = $this->post->query()->onlyPosts();
    $params = [];
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $posts->ofSearch($input['search']);
      $params['search'] = $input['search'];
    }

    $posts->ofStates('archive');

    if(Input::has('author_id') && ($input['author_id']!="")){
      $posts->ofAuthors($input['author_id']);
      $params['author_id'] = $input['author_id'];
    }

    $posts = $posts->orderby('id', 'desc')->paginate(20);

    return  View::make('administration.posts.archives', compact('params', 'posts'));
  }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return   View::make('administration.posts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$input = Input::all();
		$validation = Validator::make($input, Post::$post_rules);
		if ($validation->passes())
		{
			$post = $this->post->create(Input::only(Post::$fields));
			
			$post->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$post->services()->sync($input['services']);
			}

			return Redirect::back();
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = $this->post->findOrFail($id);

		return   View::make('administration.posts.show', compact('post'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = $this->post->find($id);

		if (is_null($post))
		{
			return Redirect::route('admin.posts.index');
		}

		return   View::make('administration.posts.edit', compact('post'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Post::$post_update_rules);

		if ($validation->passes())
		{
			$post = $this->post->find($id);
			$post->update(Input::only(Post::$fields));
			$post->save();
      $post->saveCover(isset($input['cover'])?$input['cover']:null);

			if(isset($input['services']) && is_array($input['services'])){
				$post->services()->sync($input['services']);
			}

			return Redirect::route('admin.posts.show', $id);
		}

		return Redirect::route('admin.posts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function archive($id)
  {
    $this->post->find($id)->archive();

    return Redirect::back()->with('notice', 'Post has been archived');
  }

	public function destroy($id)
	{
		$this->post->find($id)->delete();

		return Redirect::route('admin.posts.index');
	}

}
