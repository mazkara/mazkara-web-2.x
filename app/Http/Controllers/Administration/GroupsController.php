<?php
namespace App\Http\Controllers\Administration;

use Confide, BaseController, View, Response, Config, Validator, Redirect, Input;

use App\Models\Group;
use App\Models\Business;

use App\Models\Highlight;

use App\Http\Controllers\Controller;

class GroupsController extends Controller {

	/**
	 * Group Repository
	 *
	 * @var Group
	 */
	protected $group;

	public function __construct(Group $group)
	{
		$this->group = $group;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$groups = $this->group->query();

		$input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $groups->search($input['search']);
    }

		$groups = $groups->byLocale()->byType('group')->orderby('name', 'asc')->paginate(20);//->get();

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.groups.index', compact('groups'));
	}

	public function getOutlets(){
		$input = Input::all();
		$businesses = Business::query()->searchBasic($input['term'])->byLocale()->get()->toArray();
		foreach($businesses as $ii=>$business){
			$html = View::make('administration.groups.partials.business', ['business' => $business]);
			$businesses[$ii]['html'] = (string) $html;
		}

		return Response::json($businesses);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.groups.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Group::$rules);

		if ($validation->passes())
		{
			$group = $this->group->create(Input::except('cover', 'banner', 'businesses-search', 'businesses', 'deletableBusinesses',  'deletablePhotos'));

			$group->saveCover(isset($input['cover'])?$input['cover']:null);
			$group->saveBanner(isset($input['banner'])?$input['banner']:null);

			$group->city_id = mzk_get_localeID();

			$input = Input::only('businesses', 'users', 'sales_admins');
	    $group->grouped_businesses()->sync(is_array($input['businesses'])?$input['businesses']:[]);

			$group->save();

			return Redirect::route('admin.groups.index');
		}

		return Redirect::route('admin.groups.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$group = $this->group->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.groups.show', compact('group'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$group = $this->group->find($id);

		if (is_null($group))
		{
			return Redirect::route('admin.groups.index');
		}

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.groups.edit', compact('group'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

	
		$input = array_except(Input::all(), '_method', 'cover', 'banner', 'businesses-search', 'businesses', 'deletableBusinesses');
		$validation = Validator::make($input, Group::$rules);

		if ($validation->passes())
		{
			$group = $this->group->find($id);
			$deletablePhotos = Input::only('deletablePhotos');
			$group->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

			$group->update(Input::except('_method', 'cover', 'banner', 'businesses', 'businesses-search', 'businesses', 'deletableBusinesses',  'deletableBusinesses'));

			$input = Input::only('businesses', 'users', 'sales_admins');
	    $group->grouped_businesses()->sync(is_array($input['businesses'])?$input['businesses']:[]);

			$input = Input::all();

			$group->saveCover(isset($input['cover'])?$input['cover']:null);
			$group->saveBanner(isset($input['banner'])?$input['banner']:null);

			return Redirect::route('admin.groups.show', $id);
		}

		return Redirect::route('admin.groups.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->group->find($id)->delete();

		return Redirect::route('admin.groups.index');
	}

}
