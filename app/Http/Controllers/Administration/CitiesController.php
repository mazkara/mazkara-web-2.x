<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input, Cache;

use App\Models\User;
use App\Models\Zone;

use App\Http\Controllers\Controller;

class CitiesController extends Controller {

	/**
	 * Zone Repository
	 *
	 * @var Zone
	 */
	protected $zone;

	public function __construct(Zone $zone)
	{
		$this->zone = $zone;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$zones = Zone::cities()->get();
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.cities.index', compact('zones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.cities.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Zone::$rules);
		$data = Input::only(Zone::$fields_for_city);
		if ($validation->passes())
		{
			$images = Input::only('images');
			$zone = $this->zone->create($data);
			$zone->saveImages($images['images']);
			
			mzk_reset_all_zone_cache();
			
			return Redirect::route('admin.cities.index');
		}

		return Redirect::route('admin.cities.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$zone = $this->zone->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.cities.show', compact('zone'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$zone = $this->zone->find($id);

		if (is_null($zone))
		{
			return Redirect::route('admin.cities.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.cities.edit', compact('zone'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method', 'deletablePhotos');
		$validation = Validator::make($input, Zone::$rules);
		$data = Input::only(Zone::$fields_for_city);

		if ($validation->passes()){
			$zone = $this->zone->find($id);
			$zone->update($data);

			mzk_reset_all_zone_cache();

			return Redirect::route('admin.cities.show', $id);
		}

		return Redirect::route('admin.cities.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$zone = $this->zone->find($id);
		$zone->delete();

		return Redirect::route('admin.cities.index');
	}

}
