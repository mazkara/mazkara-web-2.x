<?php
namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\Category;
use App\Models\Deal;
use App\Models\Mzk_meta;
use App\Models\Invoice;
use App\Models\Photo;
use App\Models\Counter;
use App\Models\Counterbase;

use App\Helpers\MazkaraHelper;
use Auth, View, ViewHelper, Input, Redirect, Response;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

  public function setCurrentLocale($slug){
    MazkaraHelper::setDefaultAdminLocale($slug);
    return Redirect::back();
  }

  public function launchpad(){
    $dashboards = ['sms'];//['crm','content','finance'];
    if(!Auth::user()->hasRole('admin')){
      $dashboards = [''];
    }

    if(Auth::user()->canAccessDashboard('finance')){
      $dashboards[] = 'finance';
    }

    if(Auth::user()->canAccessDashboard('crm')){
      $dashboards[] = 'crm';
    }


    if(Auth::user()->canAccessDashboard('content')){
      $dashboards[] = 'content';
    }

    if(Auth::user()->canAccessDashboard('restricted')){
      $dashboards[] = 'restricted';
    }

//    if(Auth::user()->canAccessDashboard('settings')){
//      $dashboards[] = 'settings';
//    }

    if(Auth::user()->canAccessDashboard('editor')){
      $dashboards[] = 'editor';
    }


    $this->layout = View::make('layouts.admin-base');
    return View::make('administration.home.launchpad', compact('dashboards')); 

  }

	public function dashboard()
	{
    if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('moderator')){
      $this->adminDashboard();
    }elseif(Auth::user()->hasRole('sales')||Auth::user()->hasRole('sales-admin')){
      return Redirect::to('/crm/merchants');
    }
  }

  public function tag(){
    $input = Input::all();
    $obj = null;
    switch($input['taggable_type']){
      case 'Photo':
        $obj = Photo::find($input['taggable_id']);
      break;
      case 'Invoice':
        $obj = Invoice::find($input['taggable_id']);
      break;
    }

    if(!is_null($obj)){
      $obj->retag($input['tags']);
    }
  }

  public function editorDashboard(){
    return Redirect::to('/editor/posts');
  }

  public function liveStatistics(){
    $this->layout = View::make('layouts.admin-base');
    return View::make('administration.home.statistics'); 
  }

  public function uploadFile(){
    $result = array();
    foreach($_FILES as $file_name=>$file){

      $photo = new \App\Models\Photo();
      $photo->image = $file;
      $photo->save();

      $image = new \App\Models\Api\Image();
      $image->image_url = $photo->image->url();
      $image->image_thumbnail_url = $photo->image->url('thumbnail');
      $image->save();
      
      $result[] = array('id'=>$image->id, 
                        'url'=>$image->image_url, 
                        'thumb'=>$image->image_thumbnail_url);
    }

    return Response::json($result);
  }

  public function getLiveStatistics(){

    $input = Input::all();
    $counterBase = new Counterbase();
    $total_calls_ever = Counter::select()->byType('call-views')->byLocale()->sum('views');
    if(mzk_is_counters_divided()){
      $total_calls_ever_archive = $counterBase->select()->byType('call-views')->byLocale()->sum('views');
    }else{
      $total_calls_ever_archive = 0;
    }
    $total_calls_ever = $total_calls_ever+$total_calls_ever_archive;

    $total_calls_today = Counter::select()->byType('call-views')->byLocale()->today()->sum('views');

    $start_date = \Carbon\Carbon::now()->subDays(1);
    $end_date  = \Carbon\Carbon::now()->addDays(1);
    $total_calls_yesterday = Counter::select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->sum('views');

    $start_date = \Carbon\Carbon::now()->subDays(7);
    $total_calls_week = Counter::select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->sum('views');

    $start_date = \Carbon\Carbon::now()->subDays(30);
    $total_calls_month = Counter::select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->sum('views');

    $start_date = isset($input['start_date'])?$input['start_date']:\Carbon\Carbon::now()->subDays(10);
    $end_date  = isset($input['end_date'])?$input['end_date']:\Carbon\Carbon::now()->addDays(2);

    $total_calls_dates = Counter::select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->sum('views');
    if(mzk_is_counters_divided()){
      $total_calls_dates_archive = $counterBase->select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->sum('views');

    }else{
      $total_calls_dates_archive = 0;
    }

    $total_calls_dates = $total_calls_dates + $total_calls_dates_archive;

    $data = [];
    $counts = Counter::select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->get();
    if(mzk_is_counters_divided()){

      $counts_archive = $counterBase->select()->byType('call-views')->byLocale()->betweenDates($start_date, $end_date)->get();
    }else{
      $counts_archive = [];
    }

    $buffer = [];
    foreach($counts as $vv){
      $d = strtotime($vv->dated)*1000;
      $buffer[$d] = (isset($buffer[$d])?$buffer[$d]:0) + $vv->views;
    }

    foreach($counts_archive as $vv){
      $d = strtotime($vv->dated)*1000;
      $buffer[$d] = (isset($buffer[$d])?$buffer[$d]:0) + $vv->views;
    }

    $date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

    foreach($date_range as $ii=>$vv){
      if($ii==0){
        continue;
      }
      $dt = strtotime($vv->format('Y-m-d'));
      $data[] = ['date'=>($dt), 'calls'=>( isset($buffer[$dt]) ? $buffer[$dt] : 0)];
    }


    array_pop($data);

    $top_10 = Counter::selectRaw('sum(counters.views) as total, counters.*, businesses.name as business_name')
                        ->betweenDates($start_date, $end_date)
                        ->byType(Counter::CALL_VIEWS)->byLocale()
                        ->where('countable_type')->groupby('countable_id')
                        ->orderby('total', 'desc')->get()->take(50);



    $meta = Mzk_meta::select()->byLocale()->byCallViews()->get()->first();

    $feed = '';
    if($meta):
      foreach($meta->body as $b){
        if(is_string($b)){
          $str = $b;
        }else{
          $str = $b->name.' got a call at '. date('r', $b->time).' - ['.$b->ip.']';
        }

        $feed.='<div style="color:'.ViewHelper::genColorCodeFromText($str).'"><small>'.$str.'</small></div>';
      }
    endif;



    $top = [];
    $top_html = '';
    foreach($top_10 as $one){
      $top[] = ['id'=>$one->business_id, 'name'=>$one->business_name, 'total_views'=>$one->total];
      $top_html.='<tr>';
      $top_html.='<td>';
      $top_html.='<a href="/content/businesses/'.$one->countable_id.'">'.$one->business_name.'</a>';
      $top_html.='</td>';
      $top_html.='<td>';
      $top_html.=$one->total.'';
      $top_html.='</td>';
      $top_html.='</tr>';
    }

    $start_date = date('Y-m-d', strtotime($start_date));
    $end_date = date('Y-m-d', strtotime($end_date));


    return Response::json(compact('total_calls_ever','total_calls_today','total_calls_yesterday',
                        'total_calls_week', 'top_html', 'data', 'feed', 'total_calls_month','start_date','end_date','total_calls_dates')) ;
  }

  public function crmDashboard(){
    return Redirect::to('/crm/virtual_numbers');
  }

  public function financeDashboard(){
    return Redirect::to('/finance/invoices');
  }

  public function adminDashboard(){


		$total_listings = Business::select('id')->byLocale()->count();
		$active_listings = Business::select('id')->byLocale()->activeState('active')->count();
		$inactive_listings = Business::select('id')->byLocale()->activeState('inactive')->count();
    $businesses_with_locations = Business::select('id')->byLocale()->hasLocation();
		$map_listings = $businesses_with_locations->count();
		$no_map_listings = $total_listings - $map_listings;

    $businesses_with_photos = Business::select('id')->byLocale()->hasPhotos()->lists('id')->all();
		$no_photo_listings = $total_listings - count($businesses_with_photos);
		$photo_listings = $total_listings - $no_photo_listings;

    $businesses_with_rate_card = Business::select('id')->byLocale()->hasRateCards()->lists('id')->all();
		$no_rate_card_listings = $total_listings - count($businesses_with_rate_card);
		$rate_card_listings = $total_listings - $no_rate_card_listings;

    $businesses_with_services = Business::select('id')->byLocale()->hasServices()->lists('id')->all();
    $has_services_listings = 	count($businesses_with_services);
  	$has_no_services_listings = $total_listings - $has_services_listings;

    $businesses_with_categories = Business::select('id')->byLocale()->hasCategories()->lists('id')->all();
    $has_categories_listings = 	count($businesses_with_categories);
  	$has_no_categories_listings = $total_listings - $has_categories_listings;

    $businesses_with_highlights = Business::select('id')->byLocale()->hasHighlights()->lists('id')->all();
    $has_highlights_listings = count($businesses_with_highlights);	
  	$has_no_highlights_listings = $total_listings - $has_highlights_listings;
    $businesses_with_packages = Business::select('id')->byLocale()->hasActivePackages()->count();

    $businesses_with_deals = [];
    $businesses_with_deals['All Listings'] = ['url'=>'?deals=any', 'count'=>Business::select('id')->byLocale()->hasDeals()->count()];
    foreach(Deal::$types as $ii=>$vv){
      foreach(Deal::$statuses as $status=>$vs){
        $businesses_with_deals[$vs.' '.
                                MazkaraHelper::getPluralName($vv)] = [ 'url'=>'?deals='.$ii.
                                                                                '&deals_status='.$status, 
                                                                        'count'=>Business::select('id')
                                                                                  ->hasDeals($ii)->byLocale()
                                                                                  ->byDealsStatus($status)
                                                                                  ->count()];
      }
    }





    $completed_listing_ids = array_intersect($businesses_with_locations->lists('id')->all(), 
                                          $businesses_with_photos, 
                                          $businesses_with_rate_card,
                                          $businesses_with_services, 
                                          $businesses_with_categories, 
                                          $businesses_with_highlights);

    //has rate card, has photos, has location, has services
    $completed_listings = Business::select('id')->byLocale()->whereIn('id', $completed_listing_ids)->count();

		//$cats = Category::remember(60)->get();

    $cats = Category::all();
		$categories = [];
		foreach($cats as $category){
			$categories[] = [
												'id'=>$category->id,
												'name'=>$category->get_plural_name(),
                        // cookie cutter fix
												'count'=>Business::select('id')->byLocale()->ofCategories([$category->id])->count()
                        //$category->locale() ?$category->locale()->pivot->business_count:0

                        //$category->locale(MazkaraHelper::getAdminDefaultLocaleID())
                          //              ->pivot->business_count
                      //DB::select(DB::raw('select COUNT(DISTINCT business_category.business_id) as c from business_category WHERE business_category.category_id = '.$category->id))[0]->c
											];
		}

		$this->layout = View::make('layouts.admin-content');
    return View::make('administration.home.dashboard', 
    															compact('active_listings', 'inactive_listings',
  																				'no_map_listings', 'no_photo_listings', 
  																				'map_listings', 'photo_listings', 
  																				'rate_card_listings', 
                                          'businesses_with_packages',
																					'has_services_listings',
																					'has_no_services_listings',
																					'has_categories_listings',
																					'has_no_categories_listings',
																					'has_highlights_listings',
																					'has_no_highlights_listings',
                                          'completed_listings',
                                          'categories',
                                          'businesses_with_deals',
  																				'no_rate_card_listings','total_listings') );
	}

	public function showWelcome()
	{
		return View::make('hello');
	}

}
