<?php
namespace App\Http\Controllers\Administration;

use Auth, View, Config, Response, App, Validator, Redirect, Mail;
use Input, DB, MazkaraHelper, ViewHelper;

use App\Models\Highlight;

use App\Models\Deal;
use App\Models\Tax;
use App\Models\Call_log;
use App\Models\Mzk_meta;
use App\Models\Counter;
use App\Models\Business;
use App\Models\Invoice_item;
use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Category;
use App\Models\Payment_applicable;

use App\Http\Controllers\Controller;


class InvoicesController extends Controller {

  /**
   * Invoice Repository
   *
   * @var Invoice
   */
  protected $invoice;

  public function __construct(Invoice $invoice)
  {
    $this->invoice = $invoice;
    $this->layout = 'layouts.admin-finance';
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $invoices = $this->invoice->query();//->byLocale();
    $params = [];
    $input = Input::all();

    if(Input::has('search') && ($input['search']!="")){
      $invoices->search($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('state') && ($input['state']!="")){
      $invoices->byStates($input['state']);
      $params['state'] = $input['state'];
    }

    if(Input::has('merchant_id') && ($input['merchant_id']!="")){
      $invoices->byMerchants($input['merchant_id']);
      $params['merchant_id'] = $input['merchant_id'];
    }

    if(Input::has('sales_poc') && ($input['sales_poc']!="")){
      $invoices->byPOCs($input['sales_poc']);
      $params['sales_poc'] = $input['sales_poc'];
    }

    $invoices = $invoices->orderby('id', 'desc')->paginate(100);
    
    return  View::make('administration.invoices.index', compact('params', 'invoices'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return View::make('administration.invoices.create');
  }

  public function getInvoice(){
    $input = Input::all();
    $invoice = Invoice::where('title', '=', $input['invoice_no'])->get()->first();
    $result = false;
    if($invoice){
      $result = ['invoice'=>$invoice->toArray()];
    }
    return Response::json($result);//::back();
  }

  public function createPPLForm(){
    return    View::make('administration.invoices.ppl.create');
  }

  public function getCallLogs(){
    $params = Input::all();
    $call_logs = Call_log::query();
  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */

  public function storePPL(){
    $input = Input::all();
    $validation = Validator::make($input, Invoice::$rules);

    $data = Input::only(Invoice::$fields);
    $outlets = count($input['outlets'])>0 ? $input['outlets'] : [];
    $start_date = $data['start_date'] ? $data['start_date'] : date('Y-m-d');
    $end_date = $data['end_date'] ? $data['end_date'] : date('Y-m-d');
    $unit_price = $input['unit_price'] ? $input['unit_price'] : 5;

    $items = Input::only('items');

    if($validation->passes()){
      $data['type'] = 'item';
      $invoice = $this->invoice->create($data);
      $invoice->setAsPPL();
      $invoice->save();

      $call_log_ids = Call_log::query()
                              ->billable()
                              ->byBusinesses($outlets)
                              ->onlyConnected()
                              ->betweenDates($start_date, $end_date)
                              ->lists('id', 'id');

      foreach($items['items'] as $itm){
        $item = new Invoice_item($itm);
        $item = $invoice->items()->save($item);
      }

      $invoice->businesses()->sync($outlets);

      \DB::table('call_logs')->whereIn('id', $call_log_ids)
              ->update(array( 'invoice_item_id' => $item->id, 
                              'state'=>Call_log::STATE_BILLED));

      return Redirect::route('admin.invoices.show', [$invoice->id]);
    }

    return Redirect::route('admin.invoices.create')
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');

  }

  public function store(){

    $input = Input::all();
    $validation = Validator::make($input, Invoice::$rules);
    $outlets = count($input['outlets']) > 0 ? $input['outlets'] : [];
    $data = Input::only(Invoice::$fields);
    $items = Input::only('items');

    if ($validation->passes()){
      $invoice = $this->invoice->create($data);
      foreach($items['items'] as $itm){
        $item = new Invoice_item($itm);
        $item->type = 'item';       
        $invoice->items()->save($item);
      }

      $html = View::make('administration.invoices.pdf', compact('invoice'))->render();
      $pdf = App::make('dompdf.wrapper');
      $path = storage_path().'/media/invoice-'.md5(time()).'.pdf';
      $pdf->loadHTML($html)->save($path);
      $invoice->businesses()->sync($outlets);

      $data = ['invoice'=>$invoice];
      //    Mail::queue('emails.invoice-ready', $data, function($message) use($invoice, $path)
      //    {
      //        $message->from('finance@fabogo.com', 'Mazkara Finance');
      //
      //        $message->to($invoice->poc->email)->subject('You have an invoice #'.$invoice->title.' ['.time().']');
      //
      //        $message->attach($path);
      //    });

      return Redirect::route('admin.invoices.index');
    }

    return Redirect::route('admin.invoices.create')
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }




  public function attach(){
    $data = Input::all();

    $invoice = Invoice::find($data['invoice_id']);
    $invoice->saveAttachments($data['attachments'], $data['label']);
    return Redirect::back();
  }

  public function allocatePaymentsApplicable($id){
    $data = Input::all();
    $payment_applicables = [];
    $invoice = Invoice::find($id);
    $payment = Payment::find($data['payment_id']);
    $total_amount_applied = 0;

    foreach($data['amount'] as $ii=>$amount){
      $total_amount_applied+=$amount;
      $payment_applicable = ['amount' =>  $data['amount'][$ii], 
                                'start_date'=>$data['start_date'][$ii],
                                'end_date'  =>$data['end_date'][$ii],
                                'payment_id'=>$data['payment_id'],
                                'invoice_id'=>$id
                                ];
      Payment_applicable::create($payment_applicable);
      $invoice->deductFromAmountApplicable($amount);
      $payment->deductFromAmountApplicable($amount);
    }

    $payment->attachToInvoice($invoice, $total_amount_applied);

    return Redirect::back();
  }


  public function saveTax(){
    $data = Input::only(Tax::$fields);
    $data['amount'] = 0;
    $tax = Tax::create($data);

    return Redirect::back();

  }

  public function saveBounceCharges(){
    $input = Input::all();
    $data = [];
    $data['desc'] = $input['desc'];
    $data['price'] = $input['amount'];
    $data['total'] = $input['amount'];
    $data['qty'] = 1;
    $data['type'] = 'bounce-charges';
    $data['invoice_id'] = $input['invoice_id'];
    //$data['amount'] = 0;
    $item = Invoice_item::create($data);
    $invoice = Invoice::find($data['invoice_id']);
    $invoice->setExcess();
    return Redirect::back();
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $invoice = $this->invoice->findOrFail($id);

    return    View::make('administration.invoices.show', compact('invoice'));
  }

  public function getPdf($id){
    $invoice = $this->invoice->findOrFail($id);
    $businesses = [];
    $items = [];
    if($invoice->isPPL()){
      $business_ids = $invoice->call_logs()->lists('business_id','business_id');
      $businesses = Business::whereIn('id', $business_ids)->get();
      
      foreach($businesses as $business){
        $item = $invoice->items()->onlyItems()->first();

        $item->business = $business;
        $item->num_calls = $invoice->call_logs()->where('business_id','=', $business->id)->count();
        $item->calls_total = $item->price*$item->num_calls;
        $items[] = $item;
      }
    }
    $html =   View::make('administration.invoices.pdf', compact('invoice', 'items', 'businesses'))->render();
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML($html);
    return $pdf->download('invoice-'.($invoice->title).'.pdf');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $invoice = $this->invoice->find($id);

    if (is_null($invoice))
    {
      return Redirect::route('admin.invoices.index');
    }

    $merchant = $invoice->merchant;

    return    View::make('administration.invoices.edit', compact('invoice', 'merchant'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, Invoice::$rules);
    $outlets = count($input['outlets'])>0 ? $input['outlets'] : [];

    $data = Input::only(Invoice::$fields);
    $items = Input::only('items');
    $existing_items = Input::only('existing_items');

    if ($validation->passes())
    {

        $invoice = $this->invoice->find($id);
        $invoice->update($data);


        if (is_array($existing_items) && (isset($existing_items['existing_items']))):
            foreach ($existing_items['existing_items'] as $id=>$itm){
                $item = Invoice_item::find($id);//($itm);
                $item->type = 'item';
                $invoice->items()->save($item);
            }
        endif;

        $invoice->businesses()->sync($outlets);

        if (is_array($items) && (isset($items['items']))):
            foreach($items['items'] as $itm){
                $item = new Invoice_item($itm);
                $item->type = 'item';
                $invoice->items()->save($item);
            }
        endif;

        return Redirect::route('admin.invoices.show', [$invoice->id]);
    }

    return Redirect::route('admin.invoices.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }

  public function updatePPL($id){
    $input = array_except(Input::all(), '_method');
    $validation = Validator::make($input, Invoice::$rules);
    $invoice = $this->invoice->find($id);


    $data = Input::only(Invoice::$fields);
    $outlets = count($input['outlets'])>0 ? $input['outlets'] : [];
    $start_date = $data['start_date'] ? $data['start_date'] : date('Y-m-d');
    $end_date = $data['end_date'] ? $data['end_date'] : date('Y-m-d');
    $unit_price = $input['unit_price'] ? $input['unit_price'] : 5;

    $items = Input::only('items');

    $existing_item_ids = $invoice->items()->onlyItems()->lists('id','id');


    if($validation->passes()){

      // first unset the previouslet selected calls
      \DB::table('call_logs')->whereIn('invoice_item_id', $existing_item_ids)
              ->update(array( 'invoice_item_id' => null, 
                              'state'=>Call_log::STATE_UNBILLED));

      $invoice->update($data);
      $invoice->setAsPPL();
      $invoice->save();
      // remove existing items
      foreach($invoice->items as $item){
        $item->delete();
      }

      $invoice->businesses()->sync($outlets);

      $call_log_ids = Call_log::query()
                              ->billable()
                              ->byBusinesses($outlets)
                              ->onlyConnected()
                              ->betweenDates($start_date, $end_date)
                              ->lists('id', 'id');

      foreach($items['items'] as $itm){
        $item = new Invoice_item($itm);
        $item->type = 'item';
        $item = $invoice->items()->save($item);
      }

      \DB::table('call_logs')->whereIn('id', $call_log_ids)
              ->update(array( 'invoice_item_id' => $item->id, 
                              'state'=>Call_log::STATE_BILLED));

      return Redirect::route('admin.invoices.show', [$invoice->id]);
    }

    return Redirect::route('admin.invoices.edit', $id)
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');
  }



  public function updateInvoiceItem(){
    $input = Input::all();

    $data = ['id' => $input['pk']];

    $invoice_item = Invoice_item::find($data['id']);

    $invoice_item->$input['name'] = $input['value'];
    $invoice_item->save();
    return Redirect::back();

  }




  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $this->invoice->find($id)->delete();

    return Redirect::route('admin.invoices.index');
  }

  public function deletePaymentApplicables($id){
    $payment_applicable = Payment_applicable::find($id);

    $invoice = Invoice::find($payment_applicable->invoice_id);
    $payment = Payment::find($payment_applicable->payment_id);

    $invoice->undeductFromAmountApplicable($payment_applicable->amount);
    $payment->undeductFromAmountApplicable($payment_applicable->amount);
    $payment_applicable->delete();

    $payment->detachFromInvoice($invoice);
    $total = 0;
    foreach($invoice->payment_applicables as $payment_applicable){
      $total = $total + $payment_applicable->amount;
    }
    if($total > 0){
      $payment->attachToInvoice($invoice, $total);
    }
    return Redirect::back();

  }

}
