<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\Setting;
use App\Models\Job;
use GuzzleHttp\Client as HttpClient;

use App\Http\Controllers\Controller;

class SmsController extends Controller {

	/**
	 * Job Repository
	 *
	 * @var Job
	 */

	public function __construct(Job $job)
	{
    $this->layout = 'layouts.admin-crm';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

    return  View::make('administration.sms.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getSend()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.sms.send');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function postSend(){
		$input = Input::all();
		$client = new HttpClient();

		// sms to user
		$to = $input['phone'];
		$to = ltrim($to, '+');
		$to = ltrim($to, '0');
		$url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage';
		$url.='&send_to='.$to;
		$msg = $input['body'];
		$url.='&msg='.urlencode($msg);
		$url.='&msg_type=TEXT&v=1.1&format=text&auth_scheme=plain';
		if(substr($to, 0, 2)=='91'){
			$url.='&userid=2000154800&password=nXOjprcpf';
		}else{
			$url.='&userid=2000155758&password=WwuTDt';
		}
    $response = $client->get($url);
		//echo $url;
    //dump($response);die();
		return Redirect::back()
			->withInput()
			->with('notice', 'Message(s) sent');
	}


	public function getSettings()
	{
		//$this->layout = View::make('layouts.admin');
		$settings = Setting::byLocale()->byType('sms')->get();
    
    return  View::make('administration.sms.settings', compact('settings'));
	}

	public function postSettings()
	{
		$input = Input::all();
		$settings = [];
		foreach($input['settings'] as $id=>$setting){
			$s = ['id'=>$id];
			foreach($setting as $name=>$value){
				$s['name'] = $name;
				$s['value'] = $value;
			}
			$settings[] = $s;
		}
		foreach($settings as $s){
			$setting = Setting::find($s['id']);
			$setting->name = $s['name'];
			$setting->value = $s['value'];
			$setting->save();
		}
    return  Redirect::back()->with('notice', 'Settings have been saved');
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$job = $this->job->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.jobs.show', compact('job'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$job = $this->job->find($id);

		if (is_null($job))
		{
			return Redirect::route('jobs.index');
		}

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.jobs.edit', compact('job'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Job::$rules);

		if ($validation->passes())
		{
			$job = $this->job->find($id);
			$job->update($input);

			return Redirect::route('admin.jobs.show', $id);
		}

		return Redirect::route('admin.jobs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->job->find($id)->delete();

		return Redirect::route('admin.jobs.index');
	}

}
