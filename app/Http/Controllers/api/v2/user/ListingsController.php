<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Service, Timing, Highlight, Photo;
use User, DB, Post, Question, Response, Zone, Check_in, Favorite,MazkaraHelper, Paginator;

class ListingsController extends \BusinessesController {
  use ControllerTrait;

  protected function setupBusinessSingle($business, $user, $lat = false, $lon = false, $offers = false){
      $business->resetMetaForApi();

      $business->user_has_checked_in = $user->hasCheckedIn($business->id);
      $business->user_has_favourited = $user->hasFavourited('Business', $business->id);

      $distance_from = false;

      if(!is_null($lat) && !is_null($lon)){
        $distance_from = mzk_distance_between($business->geolocation_latitude, 
                                              $business->geolocation_longitude, 
                                              $lat, $lon);
      }
      
      $data = [
        'id'=>$business->id,
        'name'=>$business->name,
        'description'=>$business->description,
        'website'=>$business->website,
        'zone_id'=>$business->zone_id,
        'geolocated'=>$business->geolocated,
        'geolocation_city'=>$business->geolocation_city,
        'geolocation_state'=>$business->geolocation_state,
        'geolocation_country'=>$business->geolocation_country,
        'geolocation_address'=>$business->geolocation_address,
        'created_at'=>(string)$business->created_at,
        'updated_at'=>(string)$business->updated_at,
        'active'=>$business->active,
        'chain_id'=>$business->chain_id,
        'landmark'=>$business->landmark,
        'geolocation_longitude'=>$business->geolocation_longitude,
        'geolocation_latitude'=>$business->geolocation_latitude,
        'rate_card_count'=>$business->rate_card_count,
        'image_count'=>$business->image_count,
        'reviews_count'=>$business->reviews_count,
        'favorites_count'=>$business->favorites_count,
        'checkins_count'=>$business->checkins_count,
        'services_count'=>$business->services_count,
        'categories_count'=>$business->categories_count,
        'highlights_count'=>$business->highlights_count,
        'zone_cache'=>$business->zone_cache,
        'rating_average'=>$business->rating_average,
        'user_has_checked_in' => $user->hasCheckedIn($business->id),
        'user_has_favourited' => $user->hasFavourited('Business', $business->id),
        'phone'=>$business->displayablePhone(),
        'is_open' => $business->isOpenNow(),
        'is_open_until' => $business->openUntilNow(),
        'will_open' => $business->willOpen(),        
        'has_offers' => $business->active_offers_count > 0 ? true : false,
        'city_id'=>$business->city_id,
        'slug'=>$business->slug

      ];

      if($offers!=false){
        $data['offers'] = [];
        $offers = $business->offers()->onlyActive()->get();

        $photos  = array();
        foreach($business->photos as $photo){
          $photos[$photo->id] = $photo->image->url('xlarge');
        }

        foreach($offers as $ii=>$offr){
          $offer = $offr->toArray();
          $data['offers'][$ii] = $offer;
          $data['offers'][$ii]['price_meta'] = $offr->price_meta;
          mzk_shuffle_assoc($photos);
          $data['offers'][$ii]['currency'] = mzk_currency_symbol($business->city_id);
          $p = Photo::where('imageable_type', '=','Zone')->where('imageable_id', '=', $offer['id'])->get();
          $data['offers'][$ii]['photos'] = $photos;
        }
      }

    if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage()){
      $data['photo'] = mzk_cloudfront_image($business->getCoverUrl('largeCropped'));
    }else{
      $data['photo'] = '';
    }

      if(isset($business->distance)){
        $data['distance_raw'] = $business->distance;
        if($data['distance_raw']>=1){
          $data['distance'] = round($data['distance_raw'], 1).'km';
        }else{
          $data['distance'] = (round($data['distance_raw'], 3)*1000).'m';
        }
      }

      if(isset($this->params['service']) && (count($this->params['service'])>0)){
        $service = $this->params['service'][0];
        $service = $business->services()->where('services.id', '=', $service)->first();
        if($service /*&& ($business->has_sample_menu ==0)*/){
          $data['service_name'] = $service->name;//['']
          $data['service_starting_price'] = $service->pivot->starting_price;
          $data['service_currency'] = 'AED';
          $data['service_display_starting_price'] = $service->getDisplayablePrice(MazkaraHelper::getCitySlugFromID($business->city_id));
        }/*else{
          $data['service_name'] = '';
          $data['service_starting_price'] = '';
          $data['service_currency'] = '';
          $data['service_display_starting_price'] = '';
        }*/
      }

    return $data;
  }

  public function incrementBusinessCalls(){
    $id = Input::get('id');
    $business = $this->business->findOrFail($id);

    $business->incrementCallsCount();
    $result = mzk_api_response([], 200, true, 'Success');
    return $this->response->array($result);
  }

	public function getIndex(){
    $lat = Input::has('latitude') ? Input::get('latitude') : false;
    $lon = Input::has('longitude') ? Input::get('longitude') : false;
    $offers = Input::has('offers') ? Input::get('offers') : false;

    $params = [];
    $key = Input::get('key');
    $apiKey = ApiKey::where('key', '=', $key)->get()->first();

    $this->initListingsNoLocale();

    if(($lat!=false) && ($lon!=false)){
      $params['latitude'] = $lat;
      $params['longitude'] = $lon;
    }else{
      if($apiKey){
        if($apiKey->current_zone>0){
          $z = Zone::find($apiKey->current_zone);
          $this->businesses = $this->businesses->byLocale($z->isCity()?$z->id:$z->city_id);
        }
      }
    }

    $this->filterAllListings(['nearby'=>true]);
    if(($lat!=false) && ($lon!=false)){
      $this->SortListings('distance');
    }else{

      $this->SortListings();

    }

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);


    $businesses = $this->businesses->isDisplayable()->paginate(20);//take(20)->get();
    $entries = [];
    
    foreach($businesses as $i=>$business){
      $businesses[$i] = $this->setupBusinessSingle($business, $user, $lat, $lon, $offers);
    }

    $businesses = $businesses->toArray();

    //set the current users location if he selected a zone
    if((Input::has('zone')) && (Input::get('zone')!=-1)){
      if($apiKey){
        $z = Input::get('zone');
        $apiKey->current_zone = is_array($z) ? array_pop($z) : $z;
        $apiKey->save();
      }
    }

    if((Input::has('zone')) && (Input::get('zone')==-1)){ // user enabled auto detect location
      // dirty hack - pick the zone of the first business entry
      if($businesses['current_page'] == 1){
        if(count($businesses['data'])>0){
          $apiKey->current_zone = ($businesses['data'][0]['zone_id']);
          $apiKey->save();
        }
      }
    }

    $params = Input::all();

    $result = mzk_api_response(compact('businesses', 'params'), 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
  }


  public function getCurrentCity($apiKey){
    $result = false;
    if($apiKey->current_zone>0){
      $zn = Zone::where('id', '=', $apiKey->current_zone)->get()->first();
      $current_zone = $zn;
      if($zn->isCity()){
        $current_city = $zn->toArray();
        $result = $current_city['id'];
      }else{
        $current_city = Zone::where('id', '=', $zn->city_id)->get()->first()->toArray();
        $result = $current_city['id'];
      }

    }
    return $result;
  }

  public function getConciseIndex(){
    $this->initListingsNoLocale();
    $key = Input::get('key');
    $apiKey = ApiKey::where('key', '=', $key)->get()->first();

    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $this->businesses = $this->businesses->byLocale($z->isCity()?$z->id:$z->city_id);
      }
    }

    $this->filterAllListings();

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    $this->SortListings();

    $businesses = $this->businesses->isDisplayable()->paginate(20);//take(20)->get();

    $entries = [];
    foreach($businesses as $i=>$business){
      $business->user_has_checked_in = $user->hasCheckedIn($business->id);
      $business->user_has_favourited = $user->hasFavourited('Business', $business->id);

      $businesses[$i] = $business;

      $businesses[$i] = [
        'id'=>$business->id,
        'name'=>$business->name,
        'geolocation_address'=>$business->geolocation_address,
        'zone_cache'=>$business->zone_cache,
        'rating_average'=>$business->rating_average
      ];
    }

    $businesses = $businesses->toArray();
    
    $result = mzk_api_response(compact('businesses'), 200, true, 'Success');

    return $this->response->withArray($result)->setStatusCode(200);
  }



  public function getShow(){
    $id = Input::get('id');
    $business = $this->business->findOrFail($id);
    $result = [];
    $key = Input::get('key');

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    $apiKey = ApiKey::where('key', '=', $key)->first();

    $current_locale = $this->getCurrentCity($apiKey);

    $business->resetMetaForApi();
    $business->phone = $business->displayablePhone();
    $result['business'] = $business->toArray();
    $result['zone'] = count($business->zone) ? $business->zone->toArray():(object)[];
    $result['categories'] = count($business->categories) ? $business->categories->toArray():[];

    if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage()){
      $result['photo'] = mzk_cloudfront_image($business->getCoverUrl('largeCropped'));
    }else{
      $result['photo'] = '';
    }

    $result['timings'] = count($business->timings) ? $business->timings->toArray():[];
    $result['services'] = count($business->services) ? $business->services->toArray():[];
    $result['highlights'] = [];//count($business->highlights) ? $business->highlights:[];
    $reviews = count($business->completedReviews()) ? $business->completedReviews()->get():[];
    $result['offers'] = [];
    $offers = count($business->offers()->onlyActive()->get()) ? $business->offers()->onlyActive()->get():[];

    $result['business']['is_open'] = $business->isOpenNow();
    $result['business']['is_open_until'] = $business->openUntilNow();

    foreach($business->highlights as $ii=>$vv){
      if($vv->isActive($current_locale)){
        $vv = $vv->toArray();
        $vv['icon'] = 'https://s3.amazonaws.com/mazkaracdn/mobile/icons/'.$vv['slug'].'.png';
        $result['highlights'][] = $vv;
      }
    }

    $s = Service::query()->showParents()->get();
    $items = [];

    if(($business->has_sample_menu==0)):
      foreach($s as $ii=>$v){
        $children = [];

        if(count($items)>2){
          continue;
        }

        $cs = $business->services()->where('services.parent_id', '=', $v->id)->get();

        foreach($cs as $ix=>$child){
          if($ix > 2){
            continue;
          }
          $children[] = ['id'=>$child->id, 
                                'name'=>$child->name, 
                                'price'=>$child->pivot->starting_price,
                                'currency'=>'AED',
                                'service_display_starting_price' => $child->getDisplayablePrice(MazkaraHelper::getCitySlugFromID($business->city_id))
                                ];
        }

        if(count($children)>0){
          $items[] = ['parent'=>$v->name, 'children'=>$children];
        }
      }
    endif;

    $result['items'] = $items;

    foreach($reviews as $ii=>$review){
      if($review->user_id > 0){
        $usr = $review->user;
        $business = $review->business;

        $f = ["description","phone","email","website","geolocated","zone_id","geolocation_city","geolocation_state","geolocation_country","geolocation_address","created_at","updated_at",
"slug","active","facebook","twitter","instagram","google","chain_id","landmark","created_by_id","updated_by_id","deleted_by_id","geolocation_longitude",
"geolocation_latitude","rate_card_count","image_count","reviews_count","favorites_count","checkins_count","services_count","categories_count","highlights_count","timings_count","zone_cache","rating_average","active_deals_count",
"total_deals_count","total_packages_count","active_packages_count","total_ratings_count","cost_estimate","active_offers_count","city_id","ref","lot_id","has_sample_menu","has_stock_cover_image","popularity"];

        foreach($f as $vv){
          unset($review->business->$vv);
        }

        unset($review->business->meta);

        $review->business->photo = $review->business->hasThumbnail() ? $business->thumbnail('largeCropped') : '';
        $review->user->photo = '';//['user']['photo'] =  '';
        if(count($usr->avatar)>0){
          $review->user->photo = $usr->avatar->image->url('small');
        }

        $fs = ['username','email','password','confirmation_code','remember_token','confirmed',
                'created_at','avatar', 'updated_at','twitter','instagram','slug','check_ins_count'];
        foreach($fs as $vv){
          unset($review->user->$vv);
        }

      }else{
        //$review = $review->toArray();
      }

      //$result[] = $review;
    }

    $reviews = $reviews->toArray();

    $result['reviews'] = $reviews;

    //$result['photos'] = $business->photos() ? $business->photos->toArray():[];

    $result['user_has_checked_in'] = $user->hasCheckedIn($id);
    $result['user_has_favourited'] = $user->hasFavourited('Business', $id);
    $user_review = $business->completedReviews()->where('reviews.user_id', '=', $user->id)->get()->count()>0?true:false;
    
    //dump(DB::getQueryLog());
    
    $result['user_has_reviewed'] = $user_review == false ? false : true;
    $result['user_review'] = $user_review == false ? (object)array() : $business->completedReviews()->where('user_id', '=', $user->id)->get()->first();
    $pics = [];
    $result['photos'] = array();
    foreach($business->photos as $photo){
      $result['photos'][] = ['id'=>$photo->id, 'thumb'=>$photo->image->url('small'), 'medium'=>$photo->image->url('medium'), 'full'=>$photo->image->url()];
      $pics[$photo->id] = $photo->image->url('xlarge');
    }
    $result['rateCards'] = array();//$business->rateCards() ? $business->rateCards->toArray():[];
 
    foreach($business->rateCards as $photo){
      $result['rateCards'][] = ['id'=>$photo->id, 'thumb'=>$photo->image->url('small'), 'medium'=>$photo->image->url('medium'), 'full'=>$photo->image->url()];
    }

    $result['packages'] = array();

    foreach($business->packages as $photo){
      if($photo->hasImage()):
        $result['packages'][] = ['id'=>$photo->id, 
                                'thumb'=>$photo->photo->image->url('small'), 
                                'full'=>$photo->photo->image->url()];
      endif;

    }

    foreach($offers as $ii=>$offr){

      $offer = $offr->toArray();
      $result['offers'][$ii] = $offer;
      $result['offers'][$ii]['price_meta'] = $offr->price_meta;

      mzk_shuffle_assoc($pics);
      $result['offers'][$ii]['currency'] = mzk_currency_symbol($result['business']['city_id']);
      $p = Photo::where('imageable_type', '=','Zone')->where('imageable_id', '=', $offer['id'])->get();
      $result['offers'][$ii]['photos'] = $pics;//$p->count() > 0 ?$p->first()->image->url():'';
    }
    
    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);

  }

  public function getServicesShow(){
    $id = Input::get('id');
    $business = $this->business->findOrFail($id);
    $result = [];
    $key = Input::get('key');

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    $s = Service::query()->showParents()->get();
    $items = [];
    if($business->has_sample_menu==0):
      foreach($s as $ii=>$v){
        $children = [];

        $cs = $business->services()->where('services.parent_id', '=', $v->id)->get();

        foreach($cs as $ix=>$child){
          $children[] = ['id'=>$child->id, 
                                'name'=>$child->name, 
                                'price'=>$child->pivot->starting_price,
                                'currency'=>'AED',
                                'service_display_starting_price' => $child->getDisplayablePrice(MazkaraHelper::getCitySlugFromID($business->city_id))
                                ];
        }

        if(count($children)>0){
          $items[] = ['parent'=>$v->name, 'children'=>$children];
        }
      }
    endif;


    $result['items'] = $items;

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);

  }

  public function actionToggleCheckInOut(){
    $id = Input::get('id');
    $key = Input::get('key');
    $checkin = Input::get('checkin');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if($checkin=='true'){

      if(!$user->hasCheckedIn($id)){
        Check_in::create(['user_id'=>$user->id, 'business_id'=>$id]);
        $result = mzk_api_response([], 200, true, 'Success - User checked in');
      }else{
        $result = mzk_api_response([], 200, false, 'User was already checked in');
      }
    }else{

      if($user->hasCheckedIn($id)){
        $user->check_ins()->where('business_id', '=', $id)->delete();
        $result = mzk_api_response([], 200, true, 'Success - User checked out');
      }else{
        $result = mzk_api_response([], 200, false, 'User was already checked out');
      }
    }

    return $this->response->array($result);

  }

  public function actionCheckin(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if(!$user->hasCheckedIn($id)){
      Check_in::create(['user_id'=>$user->id, 'business_id'=>$id]);
      $result = mzk_api_response([], 200, true, 'Success - User checked in');
    }else{
      $result = mzk_api_response([], 200, false, 'User was already checked in');
    }
    return $this->response->array($result);

  }

  public function actionCheckout(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if($user->hasCheckedIn($id)){
      $user->check_ins()->where('business_id', '=', $id)->delete();
      $result = mzk_api_response([], 200, true, 'Success - User checked out');
    }else{
      $result = mzk_api_response([], 200, false, 'User was already checked out');
    }
    return $this->response->array($result);
  }

  public function actionFavourite(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);

    if(!$user->hasFavourited('Business', $id)){
      Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Business', 'favorable_id'=>$id]);
      $data = array('user_id' =>  $user->id, 
                    'verb'  =>  'likes', 
                    'itemable_type' =>  'Business', 
                    'itemable_id' =>  $id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $user->id; 
      $feed->verb = 'likes'; 
      $feed->itemable_type = 'Business';
      $feed->itemable_id = $id;
      $feed->save();

      $result = mzk_api_response([], 200, true, 'Success - User has favorited');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already favorited');
    }
    return $this->response->array($result);
  }

  public function actionUnfavourite(){
    $id = Input::get('id');
    $key = Input::get('key');
    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    if($user->hasFavourited('Business', $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Business')->delete();
      $result = mzk_api_response([], 200, true, 'Success - User has unfavorited');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already unfavorited');
    }
    return $this->response->array($result);
  }





  public function actionToggleFavourite(){
    $id = Input::get('id');
    $key = Input::get('key');

    $fav = Input::get('favourite');

    $user_id = $this->getUserIdFromApi($key);
    $user = User::find($user_id);
    if($fav=='true'):
      if(!$user->hasFavourited('Business', $id)){
        Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Business', 'favorable_id'=>$id]);
        $data = array('user_id' =>  $user->id, 
                      'verb'  =>  'likes', 
                      'itemable_type' =>  'Business', 
                      'itemable_id' =>  $id);
        $feed = $this->feed_manager->create($data);
        $feed->user_id = $user->id; 
        $feed->verb = 'likes'; 
        $feed->itemable_type = 'Business';
        $feed->itemable_id = $id;
        $feed->save();

        $result = mzk_api_response([], 200, true, 'Success - User has favorited');
      }else{
        $result = mzk_api_response([], 200, false, 'User had already favorited');
      }
    else:
      if($user->hasFavourited('Business', $id)){
        $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Business')->delete();
        $result = mzk_api_response([], 200, true, 'Success - User has unfavorited');
      }else{
        $result = mzk_api_response([], 200, false, 'User had already unfavorited');
      }
    endif;

    return $this->response->array($result);
  }












  protected function getUserIdFromApi($key){
    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }
  }




}
