<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;
use App\Models\Invoice_item;

class Invoice_itemsController extends Controller {

	/**
	 * Invoice_item Repository
	 *
	 * @var Invoice_item
	 */
	protected $invoice_item;

	public function __construct(Invoice_item $invoice_item)
	{
		$this->invoice_item = $invoice_item;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$invoice_items = $this->invoice_item->all();

		return View::make('invoice_items.index', compact('invoice_items'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('invoice_items.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Invoice_item::$rules);

		if ($validation->passes())
		{
			$this->invoice_item->create($input);

			return Redirect::route('invoice_items.index');
		}

		return Redirect::route('invoice_items.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$invoice_item = $this->invoice_item->findOrFail($id);

		return View::make('invoice_items.show', compact('invoice_item'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$invoice_item = $this->invoice_item->find($id);

		if (is_null($invoice_item))
		{
			return Redirect::route('invoice_items.index');
		}

		return View::make('invoice_items.edit', compact('invoice_item'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Invoice_item::$rules);

		if ($validation->passes())
		{
			$invoice_item = $this->invoice_item->find($id);
			$invoice_item->update($input);

			return Redirect::route('invoice_items.show', $id);
		}

		return Redirect::route('invoice_items.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->invoice_item->find($id)->delete();

		return Redirect::route('invoice_items.index');
	}

}
