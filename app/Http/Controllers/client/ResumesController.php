<?php
namespace App\Http\Controllers\client;

use Confide, View, Config, Validator, Redirect, Input;
use  App, Auth, Response;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Resume;
use App\Models\Call_log;
use App\Models\Review;
use App\Models\Ad_slot;
use App\Models\Activity;
use App\Models\Business ;
use App\Models\Comment;


class ResumesController extends Controller {

	/**
	 * Review Repository
	 *
	 * @var Review
	 */
	protected $resume, $business, $feed_manager;

	public function __construct(Resume $resume, Business $business, Activity $activity){

		$this->resume = $resume;
		$this->business = $business;
    $this->feed_manager = $activity;
	}

	public function index($bid = false){

		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);
		$show_resumes = false;
    if($business->canAccessResumesReport()){
      $show_resumes = true;
    }else{
      $show_resumes = false;
    }

	  if($show_resumes == false){
	  	return Redirect::to('/partner/'.$bid);
	  }

    $params = Input::all();

    $nationality = mzk_get_countries_list();

    $available_nationalities = $this->resume->get()->lists('nationality', 'nationality')->all();

    $ns = array_intersect(array_keys($nationality), array_values($available_nationalities));
    
    $nationalities = [];
    foreach($ns as $vv){
      $nationalities[$vv] = isset($nationality[$vv])?$nationality[$vv]:$vv;
    }

		$resumes = $this->resume->query()->byLocale($business->city_id);

    if(isset($params['search']) && !empty($params['search'])){
      $resumes = $resumes->whereRaw('((name like "%'.$params['search'].'%") || (location like "%'.$params['search'].'%") || (experience like "%'.$params['search'].'%") || (nationality like "%'.$params['search'].'%"))');
    }

    if(isset($params['location']) && !empty($params['location']))
    {
      $resumes = $resumes->where('location', 'like', $params['location'].'%');
    }
    if(isset($params['experience']) && !empty($params['experience'])){
      $resumes = $resumes->where('experience', '=', $params['experience']);
    }

    if(isset($params['specialization']) && !empty($params['specialization'])){
      $resumes = $resumes->where('specialization', 'like', '%'.$params['specialization'].'%');//->bySpecializations([$params['specialization']]);
    }

    if(isset($params['nationality']) && !empty($params['nationality'])){
      $resumes = $resumes->where('nationality', 'like', $params['nationality'].'%');
    }

    if(isset($params['phone']) && !empty($params['phone'])){
      $resumes = $resumes->where('phone', 'like', $params['phone'].'%');
    }

    if(isset($params['email_address']) && !empty($params['email_address'])){
      $resumes = $resumes->where('email_address', 'like', $params['email_address'].'%');
    }

    if(isset($params['current_company']) && !empty($params['current_company'])){
      $resumes = $resumes->where('current_company', 'like', $params['current_company'].'%');
    }

    if(isset($params['gender']) && !empty($params['gender'])){
      $resumes = $resumes->where('gender', '=', $params['gender']);
    }







    $specializations = $this->resume->getSpecializations();
    $experiences = $this->resume->getExperiences();

    if(isset($params['start']) && !empty($params['start'])){
      $params['end'] = isset($params['end'])?$params['end']:\Carbon\Carbon::now()->toDateString();
      $resumes = $resumes->betweenDates($params['start'], $params['end']);
    }

    $resumes = $resumes->orderby('id', 'DESC')->paginate(20);
    $results = array();
    $current_date = false;


		return View::make('client.resumes.index', 
    																			compact('resumes', 'nationalities', 'results', 'business', 'params', 'specializations', 'experiences'));
	}

	public function getRatingsData($bid = false){
		$input = Input::all();
		// get start and end date
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);

		$start_date = isset($input['start']) ? $input['start'] : $business->created_at;
    $end_date = isset($input['end']) ? $input['end'] : date('Y-m-d');

		$weeks = mzk_get_weeks_between_range_array($start_date, $end_date);

    $line_chart = [];
    
    for($i=0; $i<5; $i++){
	    $line_chart[$i.' to '.($i+1)] = [];
	    foreach($weeks as $vv){
		 		$line_chart[$i.' to '.($i+1)][] = [	strtotime($vv[0]), 
		 																				$this->review->query()->betweenDates($vv[0], $vv[1])
		 																										->byBusiness($current_business_id)
		 																										->betweenRatings($i, ($i+1))->count()]; 
	    }
    }

		return Response::json($line_chart);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	* public function index()
	* {
	* 	$reviews = $this->review->all();
*
* 	* 	return View::make('reviews.index', compact('reviews'));
	* }
	 */

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('reviews.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = Auth::user()->id;
		$validation = Validator::make($input, Review::$rules);

		MazkaraHelper::clearCurrentPageCacheName(URL::previous());
		if ($validation->passes())
		{
			$review = $this->review->create($input);
      $data = array('user_id' =>  $input['user_id'], 
                    'verb'  =>  (trim($input['body'])!=""?'reviewed':'rated'), 
                    'itemable_type' =>  'Review', 
                    'itemable_id' =>  $review->id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $input['user_id'];
      $feed->verb = 'reviewed'; 
      $feed->itemable_type = 'Review';
      $feed->itemable_id = $review->id;
      $feed->save();

			return Redirect::back();//route('businesses.show', array('id'=>$input['business_id']));
		}

		return Redirect::back()//route('businesses.show', array('id'=>$input['business_id']))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($bid = false, $id)
	{
		$review = $this->review->findOrFail($id);
		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = $this->business->find($current_business_id);


		return View::make('client.reviews.show', compact('review', 'business'));

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$review = $this->review->find($id);

		if (is_null($review))
		{
			return Redirect::route('reviews.index');
		}

		return View::make('reviews.edit', compact('review'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$input['user_id'] = Auth::user()->id;
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		$validation = Validator::make($input, Review::$rules);

		if ($validation->passes())
		{
			$review = $this->review->find($id);
			$review->update($input);

			return Redirect::back();//('reviews.show', $id);
		}
		return Redirect::back();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($bid, $id){

		$review = $this->review->find($id);
    $business = $review->business;
    $review->delete();
    $business->updateRatingAndReviewsCount();
		MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		return Redirect::back();//('reviews.index');
	}

}
