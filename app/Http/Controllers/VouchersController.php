<?php
namespace App\Http\Controllers;

use \App\Http\Controllers\Controller;
use Location, Request, Lang;
use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Voucher;
use App\Models\Photo;

use Input, Redirect, Response, DB, View;
use MazkaraHelper;
class VouchersController extends Controller {

	/**
	 * Voucher Repository
	 *
	 * @var Voucher
	 */
	protected $voucher;

	public function __construct(Voucher $voucher)
	{
		$this->voucher = $voucher;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$vouchers = $this->voucher->all();

		return View::make('vouchers.index', compact('vouchers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('vouchers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$data = $input;
		$validation = Validator::make($input, Voucher::$rules);

		if ($validation->passes())
		{
			$this->voucher->create($input);

			return Redirect::route('vouchers.index');
		}

		return Redirect::route('vouchers.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$voucher = $this->voucher->findOrFail($id);

		return View::make('vouchers.show', compact('voucher'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$voucher = $this->voucher->find($id);

		if (is_null($voucher))
		{
			return Redirect::route('vouchers.index');
		}

		return View::make('vouchers.edit', compact('voucher'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Voucher::$rules);

		if ($validation->passes())
		{
			$voucher = $this->voucher->find($id);
			$voucher->update($input);

			return Redirect::route('vouchers.show', $id);
		}

		return Redirect::route('vouchers.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->voucher->find($id)->delete();

		return Redirect::route('vouchers.index');
	}

}
