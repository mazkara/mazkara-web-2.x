<?php
namespace App\Models;

use Eloquent;

class Merchant_module_access extends Eloquent{

  protected $table = 'api_mmerchantmoduleaccess';
  const CREATED_AT = 'createdAt';
  const UPDATED_AT = 'updatedAt';

  protected $fillable = array('merchantUser_id','module_id','access'); 

  public function user(){
    $this->belongsTo('User', 'merchantUser_id');
  }

  public function module(){
    $this->belongsTo('Merchant_module', 'module_id');
  }

}
