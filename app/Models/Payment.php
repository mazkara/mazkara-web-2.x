<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Payment extends Ardent {
	protected $guarded = array();
  protected $morphClass = 'Payment';

	public static $rules = array(
		'type' => 'required',
		'amount' => 'required',
		'currency' => 'required',
		//'chq_number' => 'required',
		//'chq_bank' => 'required',
		//'chq_date' => 'required',
		//'state' => 'required'
	);

	protected static function getTypes(){
		$types = ['cash'=>'Cash', 'cheque'=>'Cheque'];
		return $types;
	}


  public function photo(){
    return $this->morphOne('Photo', 'imageable');
  }

  public function saveImage($image){
    if(is_null($image)){
      return false;
    }

    $this->removeImage();

    $photo = new Photo();
    $photo->image = $image;
    $photo->type = Photo::AVATAR;
    $photo->save();
    $this->photo()->save($photo);
  }

  public function removeImage(){
    if(count($this->photo)>0){
      $this->photo()->deleteBasic();
    }
  }

  public function hasImage(){
    if(count($this->photo)>0){
      return true;
    }
  }

  public function payment_applicables(){
    return $this->hasMany('App\Models\Payment_applicable');
  }

	public static function chequeDefaultNames(){
		return ['Mazkara FZ LLC'=>'Mazkara FZ LLC',
						'Mazkara Advertising LLC'=>'Mazkara Advertising LLC',
						'Mazkara Media Pvt Ltd '=>'Mazkara Media Pvt Ltd '];
	}

	public static function getStates(){
		$states = ['recieved'=>'Recieved','deposited'=>'Deposited','returned'=>'Returned','cleared'=>'Cleared'];
		return $states;
	}
	
	public static function getCurrencies(){
		$currencies = ['AED'=>'AED - UAE Dirham', 'INR'=>'INR - Indian Rupee'];
		return $currencies;
	}

	public function canBeAllocated(){
		return $this->amount_applicable > 0 ? true : false;
	}

	public function invoices(){
		return $this->belongsToMany('App\Models\Invoice')->withPivot('amount');
	}

	public function beforeCreate(){
		$this->setAmountApplicable();
		$this->setInitialStateIfCheque();
	}

	public function beforeSave(){
		$this->setState();
	}

	public function setAmountApplicable(){
		$this->amount_applicable = $this->amount;
	}

	public function stateRecievedToDeposited(){
		$this->state = 'deposited';
		$this->save();

	}

	public function stateDepositedToReturned($reason = ''){
		$this->state = 'returned';
		$this->state_meta = $reason;
		$this->save();
	}

	public function stateReturnedToDeposited(){
		$this->state = 'deposited';
		$this->save();
	}

	public function isCleared(){
		return $this->state == 'cleared' ? true : false;
	}

	public function stateDepositedToCleared(){
		$this->state = 'cleared';
		$this->save();
		$this->applyAmountToInvoices();//($this->invoice);
	}

	public function setInitialStateIfCheque(){
		if($this->isCheque()){
			$this->state = 'recieved';
		}
	}

	public function isCheque(){
		return $this->type =='cheque' ? true : false;
	}

	public function setState(){
		if($this->type=='cash'){
			$this->state = 'cleared';
		}
	}

	public function isApplicablePayment(){
		if($this->type=='cash'){
			return true;
		}

		if($this->isCheque()){
			if($this->state=='cleared'){
				return true;
			}
		}

		return false;
	}

	public function applyAndAttachToInvoice($invoice){
		$amount_applied = $this->getApplicableAmountForInvoice($invoice);
		$this->attachToInvoice($invoice, $amount_applied);
		$this->applyAmountToInvoice($invoice);
		//$this->applyAmountToInvoices();
	}

	public function unapplyAnddettachFromInvoice($invoice){
		$this->removeAmountFromInvoices($invoice);
		$this->detachFromInvoice($invoice);
		//$this->applyAmountToInvoices();

	}

	public function detachFromInvoice($invoice){
		$this->invoices()->detach($invoice->id);
		$this->save();
	}

	public function reset(){
		$this->amount_applicable = $this->amount;
		$this->save();
	}

	public function allocateToInvoice($invoice){
		$amount_applied = $this->getApplicableAmountForInvoice($invoice);
		$this->attachToInvoice($invoice, $amount_applied);
		// set the new amount payable
		$this->markApplicableAmountForInvoice($invoice, $amount_applied);
	}

	public function deallocateFromInvoice($invoice){
		// set the new amount payable
		$this->removeAmountFromInvoices($invoice);
		$this->detachFromInvoice($invoice);

	}


	public function attachToInvoice($invoice, $amount_applied = false){

		$this->invoices()->attach(array($invoice->id => array('amount'=>$amount_applied)));
		$this->save();
	}

	public function removeAmountFromInvoices($invoice){
		// is this cash or a cleared cheque?
		$inv = $this->invoices()->wherePivot('invoice_id', $invoice->id)->first();

		$redeemable_amount = $inv->pivot->amount;


		if($this->isApplicablePayment()):
			$invoice->amount_due = $invoice->amount_due + $redeemable_amount;
			$this->amount_applicable = $this->amount_applicable + $redeemable_amount;

			$invoice->save();
		endif;
		$this->save();

	}


  public function deductFromAmountApplicable($applied_amount){
    // is this cash or a cleared cheque?
    if($this->amount_applicable >= $applied_amount){
      $this->amount_applicable = $this->amount_applicable - $applied_amount;
      $this->save();
    }
  }
  public function undeductFromAmountApplicable($applied_amount){
    $this->amount_applicable = $this->amount_applicable + $applied_amount;
    $this->save();
  }



	public function applyAmountToInvoice($invoice){
		// is this cash or a cleared cheque?
		$applied_amount = 0;
		if($this->isApplicablePayment()):
			$applied_amount = $this->getApplicableAmountForInvoice($invoice);
			if($invoice->amount_due > $this->amount_applicable){
				$invoice->amount_due = $invoice->amount_due - $applied_amount;
				$this->amount_applicable = 0;
			}else{
				$this->amount_applicable = $this->amount_applicable - $applied_amount;
				$invoice->amount_due = 0; 
			}

			$invoice->save();
		endif;
		return $applied_amount;
	}

	public function getApplicableAmountForInvoice($invoice){
		// is this cash or a cleared cheque?
		$applied_amount = 0;
		// if the remaining amount on invoice is greated than the amount payable by payment
		// the payable amount is all payable
		if($invoice->amount_due > $this->amount_applicable){
			$applied_amount = $this->amount_applicable;
		}else{
			// else the payable amount is equal to the amount due on the invoice
			$applied_amount = $invoice->amount_due;
		}

		return $applied_amount;
	}

	public function markApplicableAmountForInvoice($invoice, $applied_amount = false){
		$applied_amount= $applied_amount?$applied_amount:$this->getApplicableAmountForInvoice($invoice);
		// is this cash or a cleared cheque?
		if($this->isApplicablePayment()):
			if($invoice->amount_due > $this->amount_applicable){
				$invoice->amount_due = $invoice->amount_due - $applied_amount;
				$this->amount_applicable = 0;
			}else{
				$this->amount_applicable = $this->amount_applicable - $applied_amount;
				$invoice->amount_due = 0; 
			}

			$this->save();
			$invoice->save();
		endif;
		return $applied_amount;
	}





	public function applyAmountToInvoices(){
		// is this cash or a cleared cheque?
		$data = array();
		if($this->isApplicablePayment()):
			foreach($this->invoices as $invoice):
				$invoice->reset();
				$invoice->resetAmountApplicable();
			endforeach;

		endif;
	}

  public function merchant(){
    return $this->belongsTo('App\Models\Merchant');
  }

  public function scopeByMerchants($query, $merchants){
  	if(!is_array($merchants)){
  		$merchants = [$merchants];
  	}

  	return $query->whereIn('merchant_id', $merchants);
  }

  public function scopeByStates($query, $states){
  	if(!is_array($states)){
  		$states = [$states];
  	}

  	return $query->whereIn('state', $states);
  }

  public function scopeSearch($query, $search){
  	return $query->where('title', 'like', $search.'%');
  }

  public function scopeSearchByInvoice($query, $search){
  	$invoice_ids = Invoice::select()->search($search)->lists('id', 'id')->all();
  	$payment_ids = \DB::table('invoice_payment')
  												->whereIn('invoice_id', $invoice_ids)
  												->lists('payment_id','payment_id');

  	return $query->whereIn('id', $payment_ids);
  }







}
