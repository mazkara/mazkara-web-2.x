<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Zone;
use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;


class Advert extends Eloquent
{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_advertise';
  }

  public static $rules = [
'description'=>'required',
'slot'=>'required',
'start_date' => 'required|date_format:Y-m-d|before:end_date',
'end_date' => 'required|date_format:Y-m-d|after:start_date',
'business_id'=>'required',
'service_ids'=>'required',
'zone_ids'=>'required'
  ];

  public $fields = array( 'description', 'slot','start_date', 'end_date', 
                          'business_id', 'campaign_id','service_id', 'zone_id');

  protected $fillable = array( 'description', 'slot','start_date', 'end_date', 
                          'business_id', 'campaign_id','service_id', 'zone_id');

}
