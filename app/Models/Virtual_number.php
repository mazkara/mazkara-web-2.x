<?php
namespace App\Models;

use App\Models\Zone;

use Eloquent;
use App\Models\Virtual_number_allocation;

class Virtual_number extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'body' => 'required|unique:virtual_numbers',
		//'state' => 'required'
	);

  public static $edit_rules = array(
    'body' => 'required|unique:virtual_numbers',
    //'state' => 'required'
  );

  public function virtual_number_allocations(){
    return $this->hasMany('App\Models\Virtual_number_allocation', 'virtual_number_id');
  }

  public function current_virtual_number_allocations(){
    return $this->virtual_number_allocations()->where('virtual_number_allocations.state', '=', 'active')->first();
  }

  public function scopeOnlyAllocatable($query){
    $allocated_numbers = Virtual_number_allocation::where('state', '=', 'active')->get()->lists('virtual_number_id', 'virtual_number_id')->all();
    return $query->whereNotIn('id', $allocated_numbers);
  }

  public function scopeByBody($query, $body){
    return $query->where('body', '=', $body);
  }

  public function scopeBySearch($query, $body){
    return $query->where('body', 'LIKE', '%'.$body.'%');
  }

  public function current_business(){
    $c = $this->current_virtual_number_allocations();
    if(!is_null($c)){
      return $c->business()->first();
    }

    return false;
  }

  public function current_business_name(){
    $c = $this->current_business();
    if(!($c)){
      return '<em>None</em>';
    }

    return $c->name;
  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function scopeOnlyActive($query){
    return $query->where('state', '=', '');
  }

  public function deactivate(){
    $this->state = 'inactive';
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }

}
