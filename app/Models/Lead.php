<?php
namespace App\Models;

use Eloquent;

class Lead extends Eloquent {
	protected $guarded = array();
  protected $morphClass = 'Lead';

	public static $rules = array(
		'name' => 'required'
	);

  public static $fields = array('name', 'email', 'phone', 'interested_in', 'inquiry_at');

  public function businesses(){
    return $this->belongsToMany('\App\Models\Business')->withPivot('allocated_at', 'created_at');;
  }
  public function scopeBetweenDates($query, $start, $end){
    return $query->where('created_at', '>', $start)->where('created_at', '<=', $end);
  }

  public function scopeByBusiness($query, $bid){
    return $query->whereHas('businesses', function ($query) use ($bid) {
                        $query->where('businesses.id', '=', $bid);
                });  
  }


  public function scopeIsAllocated($query){
    return $query->has('businesses');  
  }

  public function scopeIsNotAllocated($query){
    return $query->doesntHave('businesses');
  }


  public function city_name(){
    return $this->city_id > 0 ? $this->city->name : '';
  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
    $this->save();
  }

}
