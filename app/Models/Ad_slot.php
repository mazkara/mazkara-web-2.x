<?php
namespace App\Models;


class Ad_slot {
	protected $guarded = array();

	public static $rules = array();
  protected static $slots = [ 1 =>'Slot 1', 2 =>'Slot 2', 3 =>'Slot 3', 4 =>'Slot 4', 5 =>'Slot 5', 
                              6 =>'Slot 6', 7 =>'Slot 7', 8 =>'Slot 8', 9 =>'Slot 9', 10 =>'Slot 10',
                              11 =>'Slot 11', 12 =>'Slot 12', 13 =>'Slot 13', 14 =>'Slot 14', 15 =>'Slot 15'
                            ];

  public static function getAvailableSlots($ad_zone_id = null, $exclude=null, $campaign_id = null){
    // select all the slot ids from adsets for this zone id in campaigns between the dates given
    // return only those slots that are NOT in the above
    $slots = self::$slots;
    if($campaign_id && $ad_zone_id){
      $booked_slots = Ad_set::where('ad_zone_id', '=',$ad_zone_id)
                        ->where('id', '<>',$exclude)
                        ->get()->toArray();//lists('slot'); // add in the search the date as well here
      foreach($booked_slots as $b){
        unset($slots[$b['slot']]);
      }
    }

    return $slots;
  }
}
