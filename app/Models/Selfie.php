<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Selfie extends Post implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;

  protected $morphClass = 'Selfie';


  public static $rules = array(
    'title' => 'required',
    //'cover'=>'required|image',
  );

  public static $fields = array(
    'title', 'caption', 'body'
  );
  protected static $singleTableType = 'photo';
  protected static $persisted = [ 'title','slug','caption','body',
                                  'author_id','state','published_on'];


  public function services(){
    return $this->belongsToMany('App\Models\Service', 'post_service', 'post_id', 'service_id');
  }

}
