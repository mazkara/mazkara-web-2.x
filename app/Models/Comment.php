<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use App\Models\User;

use Auth;
class Comment extends Ardent {
	protected $guarded = array();

  const CALL_LOG_DISPUTE = 'disputed-call-log';
  const CALL_LOG_FOLLOWUP = 'followup-call-log';

	public static $rules = array(
		'body' => 'required'
	);

  public function beforeCreate(){
    $this->user_id = $this->user_id > 0 ? $this->user_id : Auth::user()->id;
  }

  public static function getTypes(){

    return array('bug'=>'Technical Bug', 
          'followup'=>'Follow up needed',
          'callback'=>'Call Back',
          'inaccurate'=>'Inaccurate Data');
  }

  public function markViewed(){
    $this->viewed = 1;
    $this->save();
  }

  public function isDeletableBy($usr){
    return ($this->user_id == $usr->id) || $usr->hasRole('admin') || $usr->hasRole('moderator');
  }

  protected $classes = [
    'Activity'=>'App\Models\Activity',
    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Invoice'=>'App\Models\Invoice',
    'Highlight'=>'App\Models\Highlight',
    'Photo'=>'App\Models\Photo',
    'Post'=>'App\Models\Post',
    'Selfie'=>'App\Models\Selfie',
    'Video'=>'App\Models\Video',
    'Favorite'=>'App\Models\Favorite',
    'Group'=>'App\Models\Group',
    'Review'=>'App\Models\Review',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Zone'=>'App\Models\Zone',
  ];

  public function getCommentableTypeAttribute($cls) {
      
      $cls = ucwords($cls);
      // to make sure this returns value from the array
      return array_get($this->classes, $cls, $cls);
      // which is always safe, because new 'class'
      // will work just the same as new 'Class'
  }

  public function commentable()
  {
    return $this->morphTo();
  }  

  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function scopeViewed($query){
    return $query->where('viewed', '=', '1');
  }

  public function scopeNotViewed($query){
    return $query->where('viewed', '<>', '1');
  }

  public function getDisplayableUsersName(){
    // is this comment made by the merchant who owns this review
    // is the user a merchant?

    $business_commented = $this->commentable->business;

    if($business_commented && ($this->user->isMerchantForBusiness($business_commented->id))){
      return $business_commented->name.' Management<br/>';
    }else{
      return $this->user->full_name;
    }
  }

}
