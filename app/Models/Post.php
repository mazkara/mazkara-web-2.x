<?php
namespace App\Models;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Auth, Image;
use App\Models\Photo;
use Intervention\Image\ImageManager;
use Watson\Rememberable\Rememberable;

class Post extends Ardent implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;
  use SingleTableInheritanceTrait;
  use Rememberable;
  
  protected $table = "posts";
  protected $morphClass = 'Post';

  protected static $singleTableTypeField = 'type';

  protected static $singleTableSubclasses = [ 'App\Models\Video', 'App\Models\Question', 
                                              'App\Models\Answer', 'App\Models\Selfie',
                                              'App\Models\Link'/*, 'App\Models\Article'*/];
  protected static $singleTableType = 'post';
  protected static $persisted = [ 'title', 'slug', 'slugname', 'views', 'caption','body','cover_photo_title', 
                                  'cover_photo_alt', 'seo_page_title', 'seo_page_meta_desc', 'author_id',
                                  'state', 'published_on', 'url', 'share_url_title', 'share_url_image'];

  const ARTICLE = 'post';
  const VIDEO = 'video';
  const SELFIE = 'photo';
  const QUESTION = 'question';
  const ANSWER = 'answer';
  const URL = 'url';

  protected $sluggable = array(
    'build_from' => 'title',
    'save_to'    => 'slug',
    'max_length' => 200,
    'unique'=>false,
    'on_update' => true,    
  );

  public function getSlugnameAttribute(){
    return $this->title;
  }

  public function updateFavoriteCount(){
    
  }

  public function view(){
    $this->incrementViewCount();
    $this->save();
  }

  public function incrementViewCount(){
    $this->views = $this->views+1;
  }



	public static $post_rules = array(
		'title' => 'required',
		//'slug' => 'required',
    'cover'=>'required',
		'caption' => 'required',
		'body' => 'required',
		//'author_id' => 'required',
		//'state' => 'required',
		//'published_on' => 'required'
	);

  public static $post_update_rules = array(
    'title' => 'required',
    'caption' => 'required',
    'body' => 'required',
  );

  public static $video_rules = array(
    'title' => 'required',
    //'slug' => 'required',
    'caption' => 'required',
    'body' => 'required',
  );

  public static $selfie_rules = array(
    'title' => 'required',
    //'cover'=>'required',
    //'slug' => 'required',
    //'caption' => 'required',
    //'body' => 'required',
  );

  public static $fields = array(
    'title', 'caption', 'body', 'state', 'published_on', 'url', 
    'share_url_title', 'share_url_image' , 'cover_photo_title', 
    'cover_photo_alt', 'seo_page_title', 'seo_page_meta_desc'
  );

  public function getPageSEOMetaDesc(){
    if(strlen(trim($this->seo_page_meta_desc))>0){
      return $this->seo_page_meta_desc;
    }

    return $this->caption;
  }

  public function getPageSEOTitle(){
    if(strlen(trim($this->seo_page_title))>0){
      return $this->seo_page_title;
    }

    return $this->title;
  }

  public function getCoverPhotoTitle(){
    if(strlen(trim($this->cover_photo_title))>0){
      return $this->cover_photo_title;
    }

    return 'Cover image for '.$this->title;

  }

  public function getCoverPhotoAlt(){
    if(strlen(trim($this->cover_photo_alt))>0){
      return $this->cover_photo_alt;
    }

    return 'Cover image for '.$this->title;
  }

  public function hasCover($size = ''){
    if(isset($this->has_cover)){
      return $this->has_cover;
    }

    if(count($this->cover) > 0){
      return true;
    }else{
      return false;
    }
  }

  public function url(){
    return route('posts.show.by.slug', array($this->slug));
  }

  public function getVideoIdFromURL(){
    $url = trim($this->body);
    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
    $id = $matches[1];
    return $id;

  }

  public function videoUrl(){
    if($this->isVideo()){
      $id = $this->getVideoIdFromURL();
      return 'https://www.youtube.com/embed/'.$id.'?rel=0&showinfo=0&color=white&iv_load_policy=3';
    }
  }

  public function isVideo(){
    return $this->type == 'video' ? true : false;
  }

  public function scopeByStatus($query, $state){
    return $query->where('state', '=', $state);
  }


  public function scopeIsViewable($query){
    return $query->ofStates('published');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }

  public function scopeByType($query, $type = 'post'){
    return $query->where('type', '=', $type);
  }

  public function scopeOnlyPosts($query){
    return $query->byType('post');
  }

  public function scopeOnlyVideos($query){
    return $query->byType('video');
  }

  public function scopeOnlySelfies($query){
    return $query->byType('photo');
  }

  public function scopeOnlyUserPosts($query){
    return $query->whereIn('type', [self::URL, self::SELFIE]);
  }

  public function scopeOnlyQuestions($query){
    return $query->byType(self::QUESTION);
  }

  public function scopeExceptQuestionsAndAnswers($query){
    return $query->whereNotIn('type', [self::QUESTION, self::ANSWER]);
  }


  public function getAuthorsFullNameAttribute(){
    return $this->author->name;
  }

  public function getAuthorsDesignationAttribute(){
    $d = $this->author->designation;
    if(trim($d)==''){
      $d = 'Author';
    }

    if(trim($this->author->designation_at)!=''){
      $d.=' at '.$this->author->designation_at;
    }

    return $d;
  }

  public function setStateDraft(){
    $this->state = 'draft';
  }

  public function setStateArchive(){
    $this->state = 'archive';
  }


  public function setStateReview(){
    $this->state = 'publish-for-review';
  }

  public function archive(){
    $this->setStateArchive();
    $this->save();
  }

  public function unarchive(){
    $this->setStateDraft();
    $this->save();
  }

  public function isEditable(){
    $editable_states = ['draft'];
    
    if(!Auth::user()->hasRole('admin')){
      $editable_states[] = 'published';
    }

    if(!in_array($this->state, $editable_states)){
      return false;
    }
    return true;
  }
  public function isEditableBy($user_id){
    if(Auth::user()->hasRole('admin')){
      return true;
    }
    return ($this->author_id == $user_id) ? true : false;
  }

  public function num_comments(){
    return count($this->comments()->get());
  }

  public function comments(){
    return $this->morphMany('Comment', 'commentable');
  }

  public function num_likes(){

    return count($this->likes()->get());
  }

  public function likes(){
    return $this->morphMany('Favorite', 'favorable')->where('type', '=', Favorite::FAVOURITE);
  }

  public function bookmarks(){
    return $this->morphMany('Favorite', 'favorable')->where('type', '=', Favorite::BOOKMARK);
  }

  public function bookmark($user_id){
    if($this->isBookmarked($user_id)){
      return false;
    }

    $f = new Favorite(['type'=>Favorite::FAVOURITE]);
    $f->type = Favorite::BOOKMARK;
    $f->user_id = $user_id;
    $f->save();
    $this->bookmarks()->save($f);

    return $f;
  }

  public function unbookmark($user_id){
    $b = $this->isBookmarked($user_id);
    if($b == false){
      return false;
    }

    foreach($b as $bb){
      $bb->delete();
    }

    return true;
  }


  public function isBookmarked($user_id){
    $b = $this->bookmarks()->where('user_id', '=', $user_id)->get();
    return count($b) > 0 ? $b:false;
  }

  public function liked($user_id){
    return $this->likes()->where('user_id', '=', $user_id)->count() > 0;
  }

  public function beforeCreate(){
    $this->assignAuthor();
    $this->setCity();
    $this->setStateIfnotPost();
    $this->setPublishDateIfnotPost();
  }

  public function afterCreate(){
    $this->setCoverImageIfVideo();
  }

  public function setStateIfnotPost(){
    if($this->type != 'post'){
      $this->state = 'published';
    }else{
      $this->state = empty($this->state) ? 'draft' : $this->state;
    }
  }

  public function setCoverImageIfVideo(){
    if($this->isVideo()){
      $id = $this->getVideoIdFromURL();
      $uri1 = 'http://img.youtube.com/vi/'.$id.'/0.jpg';
      $uri2 = 'http://img.youtube.com/vi/'.$id.'/hqdefault.jpg';
      
      $manager = new ImageManager();
      $img = $manager->make($uri1);

      $fname = $id.'.jpg';
      $dir = storage_path().'/media/youtube_cover_'.$id.md5(time());
      mkdir($dir);

      $filename = $dir.'/'.$fname;
      $img->save($filename);
      $this->saveCoverForVideo($filename);
    }
    return;
  }

  public function setPublishDateIfnotPost(){
    {
      $this->published_on = date('Y-m-d');
    }
  }

  public function setCity($locale = false){
    $this->city_id = $locale == false ? mzk_get_localeID() : $locale;
  }
  
  public function getBySlug($slug){
    $post = $this->where('slug', '=', $slug)->first();
    return $post;
  }
  
  public function assignAuthor($author_id = false){
    if(Auth::check()){
      $this->author_id = $author_id ? $author_id : Auth::user()->id;
    }
  }

  public function getCoverUrlAttribute(){
    return $this->cover()->get()->count()>0 ? mzk_cloudfront_image($this->cover()->first()->image->url()) : '';
  }

  public static function getStates(){
    return ['draft'=>'Draft', 'publish-for-review'=>'Publish For Review', 'published'=>'Published', 'reject'=>'Rejected'];
  }

  public static function getStatesForAuthor(){
    return ['draft'=>'Draft', 'publish-for-review'=>'Publish For Review'];
  }

  public function scopeOfServices($query, $services){
    if(!is_array($services)){
      $services = [$services];
    }

    $with_services = \DB::table('post_service')
                            ->whereIn('service_id', $services)
                            ->lists('post_id');

    return $query->whereIn('id', $with_services);
  }

  public function scopeOfSearch($query, $search){
    return $query->where('title', 'like', '%'.$search.'%');
  }

  public function scopeOfAuthors($query, $author_id){
    return $query->where('author_id', '=', $author_id);
  }

  public function scopeOfBookmarkedBy($query, $user_id){

    $bookmarked = \DB::table('favorites')
                            ->where('type', '=', Favorite::BOOKMARK)
                            ->where('user_id', '=', $user_id)
                            ->whereIn('favorable_type', 
                                      ['App\Models\Post','App\Models\Video', 'App\Models\Question', 'App\Models\Answer', 'App\Models\Selfie', 'App\Models\Article'])
                            ->lists('favorable_id');

    return $query->whereIn('id', $bookmarked);
  }

  public function scopeOfStates($query, $state){
    if(!is_array($state)){
      $state = [$state];
    }

    return $query->whereIn('state', $state);
  }

	public function services(){
		return $this->belongsToMany('App\Models\Service', 'post_service', 'post_id', 'service_id');
	}

	public function author(){
		return $this->belongsTo('App\Models\User', 'author_id');
	}

	public function cover(){
    return $this->morphOne('App\Models\Photo', 'imageable')->whereIn('photos.type', [Photo::COVER, Photo::POST, Photo::VIDEO]);
	}

	public function photos(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', Photo::IMAGE)->orderby('sort', 'ASC');
	}

  public function getCoverUrl($size = 'medium'){
    $temp_size = 'image_'.$size.'_url';
    if(isset($this->$temp_size)){
      return mzk_cloudfront_image($this->$temp_size);
    }

    $cover = $this->cover()->first();
    if($cover){
      return mzk_cloudfront_image($cover->image->url($size));
    }

    return '';
  }

  public function saveCover($image){
    if(!is_null($image)){
      $this->cover()->delete();
      $this->saveAllImages([$image], Photo::POST);
    }
  } 

  public function saveCoverForVideo($image){
    if(!is_null($image)){
      $this->cover()->delete();
      $this->saveAllImages([$image], Photo::VIDEO);
    }
  } 


  public function saveImages($images){
    $this->saveAllImages($images, Photo::IMAGE);
  }

  protected function saveAllImages($images, $type){
    $result = [];
    if(!is_array($images)){
      return $result;
    }

    foreach($images as $image)
    {
      if(is_null($image)){
        continue;
      }
      $photo = new Photo(['type'=>$type]);
      $photo->image = $image;
      $photo->type = $type;
      $photo->save();
      $this->photos()->save($photo);
      $result[] = $photo;
    }
    return $result;
  }

}
