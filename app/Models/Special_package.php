<?php
namespace App\Models;

use Eloquent;

class Special_package extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		//'title' => 'required',
		//'photo_id' => 'required',
		//'business_id' => 'required',
		//'state' => 'required'
	);

  public function photo(){
    return $this->morphOne('App\Models\Photo', 'imageable');
  }

  public function business(){
    return $this->belongsTo('App\Models\Business'); 
  }

  public function hasImage(){
    return $this->photo ? $this->photo->hasImage():false;
  }

  public function saveImage($image){
    if(is_null($image)){
      return false;
    }

    $this->removeImage();

    $photo = new Photo();
    $photo->image = $image;
    $photo->type = Photo::PACKAGE;
    $photo->save();
    $this->photo()->save($photo);
  }

  public function removeImage(){
    if(count($this->photo)>0){
      $this->photo()->deleteBasic();
    }
  }

  public function services(){
    return $this->belongsToMany('App\Models\Service'); 
  }
}
