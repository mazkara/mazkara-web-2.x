<?php

namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Watson\Rememberable\Rememberable;

class Payment_applicable extends Ardent
{
  use Rememberable;

  protected $fillable = ['amount','id', 'start_date','end_date','payment_id','invoice_id'];

  public function invoice(){
    return $this->belongsTo('Invoice');
  }

  public function payment(){
    return $this->belongsTo('Payment');
  }

  public function scopeByInvoice($query, $invoice_id = false){
    return $query->where('invoice_id', '=', $invoice_id);
  }

  public function scopeByPayment($query, $payment_id = false){
    return $query->where('payment_id', '=', $payment_id);
  }

  public function scopeByMonthYear($query, $month = false, $year = false){
    $month = $month ? $month : date('m');
    $year = $year ? $year : date('Y');

    $start_of = \Carbon\Carbon::createFromDate($year, $month, 1);
    $end_of = \Carbon\Carbon::createFromDate($year, $month, date("t", strtotime($start_of)));

    return $query->whereRaw('(start_date < "'.$end_of->toDateString().'" AND end_date > "'.$start_of->toDateString().'")');
  }
}
