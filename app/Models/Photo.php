<?php
namespace App\Models;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use Codesleeve\Stapler\Style;
use Codesleeve\Stapler\File\Image\Resizer;
use \Imagine\Image\Box;
use \Imagine\Image\Point;

use Cviebrock\EloquentTaggable\Taggable;

use Eloquent;

Class Photo extends Eloquent implements StaplerableInterface{
  use EloquentTrait;
  use Taggable;
  
	protected $guarded = array();

	public static $rules = array();
  protected $morphClass = 'Photo';

  const AVATAR = 'avatar';
  const PROMOTION = 'promotion';
  const IMAGE = 'image';
  const SERVICE = 'service';
  const COVER = 'cover';
  const LOGO = 'logo';
  const POST = 'post';
  const VIDEO = 'video';
  const RATECARD = 'rate-card';
  const PACKAGE = 'package';
  const ORIGINAL = 'ORIGINAL';

  protected $classes = [
    'Activity'=>'App\Models\Activity',
    'Ad'=>'App\Models\Ad',
    'Brand'=>'App\Models\Brand',
    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Deal'=>'App\Models\Deal',
    'Favorite'=>'App\Models\Favorite',
    'Group'=>'App\Models\Group',
    'Highlight'=>'App\Models\Highlight',
    'Photo'=>'App\Models\Photo',
    'Post'=>'App\Models\Post',
    'Question'=>'App\Models\Question',
    'Review'=>'App\Models\Review',
    'Selfie'=>'App\Models\Selfie',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Video'=>'App\Models\Video',
    'Zone'=>'App\Models\Zone',
  ];

  public function getImageableTypeAttribute($cls) {
      $cls = ucwords($cls);
      // to make sure this returns value from the array
      return array_get($this->classes, $cls, $cls);
      // which is always safe, because new 'class'
      // will work just the same as new 'Class'
  }












  public function imageable(){
      return $this->morphTo();
  }  

  public function hasImage(){
    return ($this->image)?true:false;
  }

  public function isCover(){
    return $this->type == self::COVER ? true : false;
  }
  
  public function isService(){
    return $this->type == self::SERVICE ? true : false;
  }

  public function __construct(array $attributes = array()) {



     // Define an attachment named 'baz' that has a watermarked style.  Here, we define a style named 'watermarked'
    // that's a closure (so that we can do some complex watermarking stuff):
//    $this->hasAttachedFile('marked', [
//        'styles' => [
//            'thumbnail' => ['dimensions' => '100x100', 'auto-orient' => true, 'convert_options' => ['quality' => 100]],
//            'micro'     => '50X50',
//            'watermarked' => function($file, $imagine) {
//                $watermark = $imagine->open('/path/to/images/watermark.png');   // Create an instance of ImageInterface for the watermark image.
//                $image     = $imagine->open($file->getRealPath());              // Create an instance of ImageInterface for the uploaded image.
//                $size      = $image->getSize();                                 // Get the size of the uploaded image.
//                $watermarkSize = $watermark->getSize();                         // Get the size of the watermark image.
//
//                // Calculate the placement of the watermark (we're aiming for the bottom right corner here).
//                $bottomRight = new Imagine\Image\Point($size->getWidth() - $watermarkSize->getWidth(), $size->getHeight() - $watermarkSize->getHeight());
//
//                // Paste the watermark onto the image.
//                $image->paste($watermark, $bottomRight);
//
//                // Return the Imagine\Image\ImageInterface instance.
//                return $image;
//            }
//        ],
//        'url' => '/system/:attachment/:id_partition/:style/:filename'
//    ]);

    // Define an attachment named 'qux'.  In this attachment, we'll use alternative style notation to define a slightly more
    // complex thumbnail style.  In this example, the thumbnail style will be a 100x100px auto-oriented image with 100% quality: 
    $styles = [
            'thumbnail' => ['dimensions' => '100x100#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'bigThumbnail' => ['dimensions' => '400x400#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'small' => ['dimensions' => '175x175#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            /**/'xlarge' => ['dimensions' => '1600x1200', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            /**/'largeCropped' => ['dimensions' => '800x600#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            /*-----*/'xlargeCropped' => ['dimensions' => '1600x1200#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            /*-----*/'xlargeCroppedPortrait' => ['dimensions' => '1200x1600#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            /*-----*/'largeCroppedPortrait' => ['dimensions' => '600x800#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'banner' => ['dimensions' => '800x480#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'bannerSmall' => ['dimensions' => '400x240#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'medium' => ['dimensions' => '400x275#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            /*X*/'mediumPortrait' => ['dimensions' => '275x400#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'micro'     => ['dimensions' => '50x50#', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            'blurred' => ['dimensions' => '800x480', 'auto-orient' => true, 'convert_options' => ['quality' => 70]],
            
            'large' => function($file, $imagine) {
                $watermark = $imagine->open(storage_path().'/app/assets/images/watermark-200.png');   // Create an instance of ImageInterface for the watermark image.
                $watermarkSize = $watermark->getSize();    // Get the size of the watermark image.
                $image     = $imagine->open($file->getRealPath());              // Create an instance of ImageInterface for the uploaded image.

                $dir = storage_path().'/media/photos_watermarked_'.md5(time());
                mkdir($dir);
                $filename = $dir.'/'.$file->getFilename();
                $size = $image->getSize()->widen(800);
                $image->resize($size)->save($filename);                                 // Get the size of the uploaded image.
                
                $image     = $imagine->open($filename);              // Create an instance of ImageInterface for the uploaded image.
                $size = $image->getSize();//->widen(800);                                 // Get the size of the uploaded image.

                // Calculate the placement of the watermark (we're aiming for the bottom right corner here).
                $bottomRight = new \Imagine\Image\Point(($size->getWidth() - $watermarkSize->getWidth())/2, ($size->getHeight() - $watermarkSize->getHeight())/2);
                // Paste the watermark onto the image.
                $image->paste($watermark, $bottomRight);
                // Return the Imagine\Image\ImageInterface instance.
                return $image;
            },
            'base' => function($file, $imagine) {
                $image     = $imagine->open($file->getRealPath());              // Create an instance of ImageInterface for the uploaded image.

                $size = $image->getSize();        // Get the size of the uploaded image.
                
                $watermark = null;

                //if(($size->getWidth()>180)&&($size->getWidth()<512)){
                if(($size->getWidth()>180)&&($size->getWidth()<512)){
                  $watermark = $imagine->open(storage_path().'/app/assets/images/watermark-200.png');   // Create an instance of ImageInterface for the watermark image.
                }

                if(($size->getWidth()>512)){
                  $watermark = $imagine->open(storage_path().'/app/assets/images/watermark.png');   // Create an instance of ImageInterface for the watermark image.
                }

                if($watermark){
                  $watermarkSize = $watermark->getSize();    // Get the size of the watermark image.


                  // Calculate the placement of the watermark (we're aiming for the bottom right corner here).
                  $bottomRight = new \Imagine\Image\Point(($size->getWidth() - $watermarkSize->getWidth())/2, ($size->getHeight() - $watermarkSize->getHeight())/2);
                  // Paste the watermark onto the image.
                  $image->paste($watermark, $bottomRight);
                }

                // Return the Imagine\Image\ImageInterface instance.
                return $image;
            },

        ];
    if( is_array($attributes) && isset($attributes['type']) ){
      if(($attributes['type']==self::RATECARD) || ($attributes['type']==self::PACKAGE)){
        unset($styles['xlarge']);
        unset($styles['xlargeCropped']);
        unset($styles['largeCropped']);
        //unset($styles['medium']);
        unset($styles['xlargeCroppedPortrait']);
        unset($styles['largeCroppedPortrait']);
        unset($styles['mediumPortrait']);
        unset($styles['blurred']);
        unset($styles['banner']);
        unset($styles['bannerbannerSmall']);
      }elseif($attributes['type']==self::AVATAR){
        unset($styles['large']);
        unset($styles['base']);
        unset($styles['banner']);
        unset($styles['bannerbannerSmall']);
      }elseif($attributes['type']==self::POST){
        unset($styles['blurred']);
        unset($styles['large']);
        unset($styles['base']);
        unset($styles['banner']);
        unset($styles['bannerbannerSmall']);
        unset($styles['xlargeCropped']);
        unset($styles['largeCropped']);
        unset($styles['xlargeCroppedPortrait']);
        unset($styles['largeCroppedPortrait']);
        unset($styles['mediumPortrait']);
      }elseif($attributes['type']==self::COVER){
        unset($styles['blurred']);
        unset($styles['large']);
        unset($styles['base']);
        unset($styles['banner']);
        unset($styles['bannerbannerSmall']);
      }elseif($attributes['type']==self::VIDEO){
        unset($styles['xlarge']);
        unset($styles['xlargeCropped']);
        unset($styles['largeCropped']);
        unset($styles['xlargeCroppedPortrait']);
        unset($styles['largeCroppedPortrait']);
        unset($styles['blurred']);
        unset($styles['mediumPortrait']);
        unset($styles['large']);
        unset($styles['base']);
        unset($styles['banner']);
        unset($styles['bannerbannerSmall']);        
      }elseif(in_array($attributes['type'], [self::LOGO, self::ORIGINAL])){
        $styles = [];
      }
    }

    $this->hasAttachedFile('image', [
        'styles' => $styles,
      
 

 
        'storage' => 's3',
        's3_client_config' => [
            'key' => 'AKIAI37S25ETHXHU7YMQ',
            'secret' => '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',


            // 'key' => 'AKIAJB6C26YXI6SME7EQ',
            // 'secret' => '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',
            'region' => 'us-east-1',
            'version'=> 'latest',
            'credentials'=>[
              'key' => 'AKIAI37S25ETHXHU7YMQ',
              'secret' => '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',
              // 'key' => 'AKIAJB6C26YXI6SME7EQ',
              // 'secret' => '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',
            ]
        ],
        's3_object_config' => [
            'Bucket' => 'mazkaracdn'
        ],
        'default_url' => '/defaults/:style/missing.png',
        'keep_old_files' => true        
    ]);


        // IMPORTANT:  the call to the parent constructor method
        // should always come after we define our attachments.
        parent::__construct($attributes);
    }

    function deleteBasic(){
      $sql ='delete from photos where id = '.$this->id;

      $d = \DB::delete($sql);
    }


}


