<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

class Setting extends Eloquent
{
  //

  protected $fillable = array('name', 'city_id', 'type', 'value');
  public function zone(){
    return $this->belongsTo('App\Models\Zone');
  }

  public function scopeByLocale($query, $locale = false){
    $locale = $locale ? $locale : mzk_get_localeID();
    return $query->where('zone_id', '=', $locale);
  }

  public function scopeByType($query, $type = 'sms'){
    return $query->where('type', '=', $type);
  }


}
