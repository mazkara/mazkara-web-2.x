<?php
namespace App\Models;

use Eloquent;

use App\Models\Photo;

class Brand extends Eloquent {
	protected $guarded = array();
  protected $morphClass = 'Brand';

	public static $rules = array(
		'name' => 'required'
	);

  public static $fields = array('name');

  public function logo(){
    return $this->morphOne('Photo', 'imageable');
  }

  public function saveLogo($image){
    if(is_null($image)){
      return false;
    }

    $this->removeLogo();

    $photo = new Photo();
    $photo->image = $image;
    $photo->type = Photo::LOGO;
    $photo->save();
    $this->logo()->save($photo);
  } 

  public function removeLogo(){
    if(count($this->logo)>0){
      $this->logo()->deleteBasic();
    }
  }


}
