<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;


class Business extends Eloquent
{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_business';
  }

  public static $rules = [];



  public $fields = array( 'description','name','phone_number','email','city_name',
                          'zone_cache','address','active','slug','gender_spec','latitude',
                          'longitude','rating_average','is_home_service','is_featured','share_link',
                          'old_id','chain_id','merchant_id','zone_id','display_json');

  public $fillables = array( 'description','name','phone_number','email','city_name',
                          'zone_cache','address','active','slug','gender_spec','latitude',
                          'longitude','rating_average','is_home_service','is_featured','share_link',
                          'old_id','chain_id','merchant_id','zone_id','display_json');

  public function getDisplayableAttribute(){
    return $this->id.' - '.$this->name.', '.$this->zone_cache.', '.$this->city_name;
  }

}
