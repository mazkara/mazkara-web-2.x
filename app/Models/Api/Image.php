<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


class Image extends Eloquent {
// implements StaplerableInterface

  protected $guarded = array();
  protected $connection = 'mysql_adhacks';


  public static $rules = [];

  public function __construct(array $attributes = array()) {
    $this->table = 'api_image';
    parent::__construct($attributes);
  }

}
