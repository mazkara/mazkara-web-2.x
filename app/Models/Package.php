<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Package extends Deal implements SluggableInterface{

  use SluggableTrait;

  public function getSlugnameAttribute(){
    return $this->title.' '.$this->business->name;
  }

  protected $sluggable = array(
    'build_from' => 'slugname',
    'save_to'    => 'slug',
    'max_length' => 200,
    'unique'     => true,      
  );

	protected $guarded = array();

  //protected $table_type = 'Package';
  protected static $singleTableType = 'Package';
  protected static $persisted = ['offer_amount','starts','ends'];

	public static $rules = array(
		'title' => 'required',
		'caption' => 'required',
		'description' => 'required',
		'fine_print' => 'required',
		'offer_amount' => 'required'
	);
}
