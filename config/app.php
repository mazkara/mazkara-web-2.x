<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => env('APP_DEBUG'),

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => 'http://v2.mazkara.local/',

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'UTC',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'en',

	/*
	|--------------------------------------------------------------------------
	| Application Fallback Locale
	|--------------------------------------------------------------------------
	|
	| The fallback locale determines the locale to use when the current one
	| is not available. You may change the value to correspond to any of
	| the language folders that are provided through your application.
	|
	*/

	'fallback_locale' => 'en',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => env('APP_KEY', 'SomeRandomString'),

	'cipher' => 'AES-256-CBC',//MCRYPT_RIJNDAEL_128,

	/*
	|--------------------------------------------------------------------------
	| Logging Configuration
	|--------------------------------------------------------------------------
	|
	| Here you may configure the log settings for your application. Out of
	| the box, Laravel uses the Monolog PHP logging library. This gives
	| you a variety of powerful log handlers / formatters to utilize.
	|
	| Available Settings: "single", "daily", "syslog", "errorlog"
	|
	*/

	'log' => 'daily',

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => [

		/*
		 * Laravel Framework Service Providers...
		 */
		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Bus\BusServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Foundation\Providers\FoundationServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Pipeline\PipelineServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Auth\Passwords\PasswordResetServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		
		/*
		 * Application Service Providers...
		 */
		'App\Providers\AppServiceProvider',
		// 'App\Providers\BusServiceProvider',
		// 'App\Providers\ConfigServiceProvider',
		'App\Providers\EventServiceProvider',
		'App\Providers\RouteServiceProvider',
		'App\Providers\SessionServiceProvider',
    'Cviebrock\EloquentSluggable\SluggableServiceProvider',

    'Codesleeve\LaravelStapler\Providers\L5ServiceProvider',
		'Collective\Html\HtmlServiceProvider',
		'Creitive\Breadcrumbs\BreadcrumbsServiceProvider',
		'Illuminate\Broadcasting\BroadcastServiceProvider',
		Laravel\Socialite\SocialiteServiceProvider::class,
		Stevebauman\Location\LocationServiceProvider::class,
		Intervention\Image\ImageServiceProvider::class,
    //Aws\Laravel\AwsServiceProvider::class,
    'Zizaco\Entrust\EntrustServiceProvider',
    'AdamWathan\BootForms\BootFormsServiceProvider',
    'Jenssegers\Date\DateServiceProvider',
    Barryvdh\Debugbar\ServiceProvider::class,
    SammyK\LaravelFacebookSdk\LaravelFacebookSdkServiceProvider::class,
    Barryvdh\DomPDF\ServiceProvider::class,
		'Maatwebsite\Excel\ExcelServiceProvider',    
    \Cviebrock\EloquentTaggable\ServiceProvider::class,
	],

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => [

		'App'       => 'Illuminate\Support\Facades\App',
		'Artisan'   => 'Illuminate\Support\Facades\Artisan',
		'Auth'      => 'Illuminate\Support\Facades\Auth',
		'Blade'     => 'Illuminate\Support\Facades\Blade',
		'Bus'       => 'Illuminate\Support\Facades\Bus',
		'Cache'     => 'Illuminate\Support\Facades\Cache',
		'Config'    => 'Illuminate\Support\Facades\Config',
		'Cookie'    => 'Illuminate\Support\Facades\Cookie',
		'Crypt'     => 'Illuminate\Support\Facades\Crypt',
		'DB'        => 'Illuminate\Support\Facades\DB',
		'Eloquent'  => 'Illuminate\Database\Eloquent\Model',
		'Event'     => 'Illuminate\Support\Facades\Event',
		'File'      => 'Illuminate\Support\Facades\File',
		'Hash'      => 'Illuminate\Support\Facades\Hash',
		'Input'     => 'Illuminate\Support\Facades\Input',
		'Inspiring' => 'Illuminate\Foundation\Inspiring',
		'Lang'      => 'Illuminate\Support\Facades\Lang',
		'Log'       => 'Illuminate\Support\Facades\Log',
		'Mail'      => 'Illuminate\Support\Facades\Mail',
		'Password'  => 'Illuminate\Support\Facades\Password',
		'Queue'     => 'Illuminate\Support\Facades\Queue',
		'Redirect'  => 'Illuminate\Support\Facades\Redirect',
		'Redis'     => 'Illuminate\Support\Facades\Redis',
		'Request'   => 'Illuminate\Support\Facades\Request',
		'Response'  => 'Illuminate\Support\Facades\Response',
		'Route'     => 'Illuminate\Support\Facades\Route',
		'Schema'    => 'Illuminate\Support\Facades\Schema',
		'Session'   => 'Illuminate\Support\Facades\Session',
		'Storage'   => 'Illuminate\Support\Facades\Storage',
		'Str'       => 'Illuminate\Support\Str',
		'URL'       => 'Illuminate\Support\Facades\URL',
		'Validator' => 'Illuminate\Support\Facades\Validator',
		'View'      => 'Illuminate\Support\Facades\View',
		'Gate' => Illuminate\Support\Facades\Gate::class,

		'Image' => Intervention\Image\Facades\Image::class,
		
		'Form' => 'Collective\Html\FormFacade',
		'Html' => 'Collective\Html\HtmlFacade',
    'Intervention\Image\ImageServiceProvider',
		'Img' => 'Intervention\Image\ImageServiceProvider',
    'Breadcrumbs' => 'Creitive\Breadcrumbs\Facades\Breadcrumbs',
		'Location' => 'Stevebauman\Location\Facades\Location',
		'Socialite' => Laravel\Socialite\Facades\Socialite::class,
    'BootForm' => 'AdamWathan\BootForms\Facades\BootForm',
    //'AWS' => Aws\Laravel\AwsFacade::class,
    'Date' => Jenssegers\Date\Date::class,
    'Entrust'=>    Zizaco\Entrust\EntrustServiceProvider::class,

    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Comment'=>'App\Models\Comment',
    'Deal'=>'App\Models\Deal',
    'Discount'=>'App\Models\Discount',
    'Promo'=>'App\Models\Promo',
    'Group'=>'App\Models\Group',
    'Favorite'=>'App\Models\Favorite',
    'Invoice'=>'App\Models\Invoice',
    'Payment'=>'App\Models\Payment',
    'Merchant'=>'App\Models\Merchant',
    'Service_item'=>'App\Models\Service_item',
    'Special_package'=>'App\Models\Special_package',
    'Combo_item'=>'App\Models\Combo_item',

    'Question'=>'App\Models\Question',

    'Offer'=>'App\Models\Offer',
    'Photo'=>'App\Models\Photo',
    'Review'=>'App\Models\Review',
    'Role'=>'App\Models\Role',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Zone'=>'App\Models\Zone',
    'Business_zone'=>'App\Models\Business_zone',
    'Advert_campaign'=>'App\Models\Advert_campaign',
    'Post'=>'App\Models\Post',
    'Selfie'=>'App\Models\Selfie',
    'Video'=>'App\Models\Video',
    'Menu_group'=>'App\Models\Menu_group',

    'MazkaraHelper'=>'App\Helpers\MazkaraHelper',
    'ViewHelper'=>'App\Helpers\ViewHelper',
		
		'Excel' => 'Maatwebsite\Excel\Facades\Excel',
		'Debugbar' => Barryvdh\Debugbar\Facade::class,
    'Facebook' => SammyK\LaravelFacebookSdk\FacebookFacade::class,
    'PDF' => Barryvdh\DomPDF\Facade::class,
	],

];
