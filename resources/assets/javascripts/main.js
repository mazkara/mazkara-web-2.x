$(function(){
  $('.toggler').click(function(){
    id = $(this).attr('rel');
    $('#'+id).toggleClass('hidden');
  });

  $('.togglerSlide').click(function(){
    id = $(this).attr('rel');
    $('#'+id).slideToggle();
  });
});