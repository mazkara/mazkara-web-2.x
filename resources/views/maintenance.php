<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie ie6"> <![endif]--> 
<!--[if IE 7 ]>	<html lang="en" class="ie ie7"> <![endif]--> 
<!--[if IE 8 ]>	<html lang="en" class="ie ie8"> <![endif]--> 
<!--[if IE 9 ]>	<html lang="en" class="ie ie9"> <![endif]--> 
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Trendset - Coming Soon!!!</title>
<meta name="description" content="Trendset Page under construction">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold"> 
<style type="text/css">

/*
Author: WebThemez
Author URL: http://webthemez.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
*/
@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,800,600);
@import url(http://fonts.googleapis.com/css?family=Lobster+Two:700);
body {
	color:#FFF; 
	font-family: 'Open Sans', sans-serif;
	min-width:960px;
	top:0;
	left:0;
	position:absolute;
	height:100%;
	width:100%;
	margin:0;
	background-size:cover;
	background-color: #ED1596;
}
.ie body {
	filter: dropshadow(color=#000000, offx=0, offy=1);	
}
h1 {
	font-size:68px;
	letter-spacing:-2px;  
	text-align:center; 
	padding-top:30px;
	font-weight:700;
	font-family: 'Lobster Two', cursive;
	text-shadow: 2px 2px rgba(12, 11, 11, 0.56);
}
.ie h1 {
	filter: dropshadow(color=#000000, offx=0, offy=3);
	padding-bottom:12px;	
} 
.ie h2 {
	filter: dropshadow(color=#000000, offx=0, offy=3);
}
h3 {
	font-size:25px;
	margin:0.2em 0; 
}
.ie h3 {
	filter: dropshadow(color=#000000, offx=0, offy=3);
}
h4 {
	margin-bottom:5px;	
}
p, pre {
	margin:0 0 10px 0; 
}
code {
	text-transform:uppercase !important;
	color:#EEE;	
	border-bottom:1px dotted #666;
}
small {
	background:url(../images/alpha.png);
	background:rgba(0, 0, 0, 0.8) url();	
	border:1px solid #111;
	font-size:15px;
	padding:0 5px;
}
a {
	color: rgba(255, 255, 255, 0.57);
	text-decoration:none;	
}
a:hover {
	color:#BBB;	
} 
#Header {
margin-bottom: 62px;
padding-top:50px;
position: relative;
width: 100%;
line-height: 110px;
z-index: 1;
}
.wrapper {
margin: 0 auto;
position: relative;
width: 920px;
z-index: 1;
}
#socialIcons {
display:block;
}
#socialIcons ul {
margin: 0px; 
padding:0px;
text-align: center;
}
#socialIcons ul li {
margin-right: 5px;
height: 34px;
line-height: 34px;
list-style-type: none;
display: inline-block; 
}
#socialIcons ul li a{
width:34px;
height:34px; 
display: inline-block;
}
.twitterIcon, .facebookIcon, .linkedInIcon, .pintrestIcon{
background: url('../images/social-icons.png') -74px 0px;
}
.facebookIcon{
background-position: 10px 0px;
}
.linkedInIcon{
background-position: -245px 0px;
}
.pintrestIcon{
background-position: -331px 0px;
}
.tempBy{
display:block;
text-align:center;
margin-top:40px;
}
#Content h2 {
display: inline-block;
margin: 25px 0 45px;
padding: 0px;
text-align: center;
font-size: 28px; 
font-weight: 300;
color: #fff;
line-height: 36px;
}
#WindowSlide {
margin: 0 auto;
position: relative;
width: 634px;
height: 170px;
display: block;
overflow: hidden;
}
#Mail {
position: absolute;
width: 634px;
top: 0px;
left: 0px;
}
#Content h3 {
text-align: center;
font-size: 20px;
font-weight: normal;
display: block;
clear: both;
}

#subscribe p{
text-align:center;
}
#subscribe input {
background: rgba(255, 255, 255, 0.52);
color: #000; 
vertical-align: middle;
width: 293px;
border: 1px solid rgba(255, 255, 255, 0.76);
padding: 0 10px;
height: 40px;
}
#subscribe input[type="button"]{
background: rgba(255, 255, 255, 1);
width: auto;
padding: 0px 25px;
cursor: pointer;
margin-left: -5px;
font-weight: bold; 
height: 42px;
display: inline-block;
}


.callback,
.simple {
  font-size: 20px;
  background: #27ae60;
  padding: 0.5em 0.7em;
  color: #ecf0f1;
  margin-bottom: 50px;
  -webkit-transition: background 0.5s ease-out;
  transition: background 0.5s ease-out;
}
.callback{
  cursor: pointer;
}
.ended {
  background: #c0392b;
}
.countdown {
text-align:center;
}
.intro{
font-size:22px;
font-weight:normal;
margin-bottom:30px;
}
.styled{
  margin-bottom: 30px;
}
.styled div {
display: inline-block;
font-size: 80px;
font-weight: 900;
text-align: center;
margin: 0 1px;
width: 136px;
padding: 10px 30px 53px; 
height: 100px;
background:rgba(255, 255, 255, 0.0);
text-shadow: none;
vertical-align: middle; 
border-right:1px solid rgba(255, 255, 255, 0.36);
text-shadow: 2px 2px rgba(12, 11, 11, 0.56);
}
.styled div:last-child{
border:none;
}
#overlay{ 
background: rgba(0, 0, 0, 0.04) url(../images/overlays/06.png) top left repeat;
background:rgba(0, 0, 0, 0.14);
position: fixed;
top: 0px;
width: 100%;
bottom: 0px;
opacity:0.8;
}
/* IE7 inline-block hack */
*+html .styled div{
  display: inline;
  zoom: 1;
}
.styled div:first-child {
  margin-left: 0;
}
.styled div span {
display: block;
border-top: 1px solid rgba(255, 255, 255, 0.36);
padding-top: 3px;
font-size: 21px;
font-weight: 400;
text-align: center;
}
footer{
width:100%;
height:30px;
background:rgba(0, 0, 0, 0.64);
position:absolute;
bottom:0px;
}
footer span{
float:right;
margin:10px;
}
@media(max-width:768px){
	body {
	min-width: initial !important;
	}
	.wrapper{width:100%;}
	.styled div {
	  margin-bottom:10px; 
	}
}
@media(max-width:420px){
	h1{
	float:initial;
	text-align: center;
	margin-left:0px;
	margin-bottom:0px;
	}
	.styled div {
	  margin-bottom:10px;
	  font-size: 40px;
	  font-weight: normal; 
	  text-align: center;
	  width:80px; 
	  border-radius:80px; 
	  height:80px;  
	}
	#Content h2 {
	margin: 0px 0px 0px 0px;
	padding: 0px;
	text-align: center;
	font-size: 29px;
	font-weight: 300;
	}
	.styled {
	margin-bottom: 30px;
	}
	#subscribe input[type="button"]{
	margin-top:10px;
	}
	#subscribe input{
	width:80%;
	}
	footer{
	position:relative !important;
	}
}





</style>
</head>

<body id="home ">
<div id="Header">
<div class="wrapper">
	<center>
	<p style="line-height:0px;margin-top:70px;font-size:40px">mazkara</p>	
</center>
	</div>
</div>
<div id="Content" class="wrapper"> 
<h3 class="intro" style="margin-top:0px;margin-bottom:0px;">Our website is under construction. <br/>Stay tuned for something fabulous! Subscribe to be notified.</h3>
<div id="subscribe"> 
<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
<div id="mc_embed_signup">
<form action="//mazkara.us10.list-manage.com/subscribe/post?u=8faceadb6f80267f911698358&amp;id=66843b3e54" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">

<div class="mc-field-group">
	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_8faceadb6f80267f911698358_66843b3e54" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
</div>
</div>

<div id="overlay"></div>

<!--Scripts-->
<script>
<?php if (App::environment('production')): ?>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61958488-1', 'auto');
  ga('send', 'pageview');
<?php endif;?>

</script>
</body>
</html>
