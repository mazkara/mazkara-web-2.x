@extends('layouts.page')
@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <h1 class="text-center">Your email {{$email}} has been unsubscribed.</h1>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p class="text-center">Please allow up to 48 hours for the application to take effect.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
      </div>
    </div>
</div>
@stop