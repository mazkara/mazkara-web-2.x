@extends('layouts.parallax')
@section('content')
<div class="container relative">
  <div class="w100pc absolute top-0-mobile " style="z-index: 999;">
    @include('elements.searcher')
  </div>
</div>
<div id="tf-home" class="container text-center" 
     style="position:absolute; height:430px;width:100%;z-index:0;
            background:url({{mzk_assets('assets/fab-about-us.jpg')}});"></div>

<div class="text-center" style="height:395px;">
  <div class="content" >
    <div class="container " style="margin-top:125px;">
      <div class="fs220 mt50 text-right pull-right text-shadow">
        <span class="force-white">Inspired by the fabulous in you  </span>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <!-- <div class="col-md-12 bg-white"> -->
    <div class="col-md-10 bg-white col-md-offset-1">

      <h1 class="text-center fw300 notransform">About Fabogo</h1>
      <div class"row ">
        <div class="col-md-12   text-center">
          <div class="fs125">
          <p class="text-left">
          At Fabogo, we are inspired by human happiness. The stride of confidence, the rush of adrenaline & the feeling of serenity.
          </p>
          <p class="text-left">
          We are inspired by the simple things that make each of you unique.
          </p>
          <p class="text-left">
          The spirit of your personality, the glint in your eyes, the spring in your step or that moment you let go of your day. We are inspired by the fabulous in you.
          </p>
          <p class="text-left">
          Fabogo celebrates you.
          </p>
          <p class="text-left">
          Go on, be Fab!
          </p>
        </div>
      <h1 class="text-center fw300 notransform">Our Goal</h1>
          <div class="fs125">
          <p class="text-left">
    Our goal is to disrupt the beauty & wellness industry.
    We are endlessly working towards the mission of making our users feel fab on the go. 
    </p>
    <p class="text-left">

    With our slick technology, rich content and seamless user experience we enable our users to explore trends and discover the best venues, services and people around them.      
          </p>
        </div>

        </div>
      </div>
</div></div>

</div><p class="p10"></p>
<img src="{{mzk_assets('assets/fab-about-banner.jpg')}}" width="100%" />

<div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <p class="pt20"></p>
        <h1 class="text-center pt20 p15 pb10  notransform">Meet the <span class="turquoise">Team</span></h1>
          <p class="pt20"></p>
          <div class="row">
            <div class="col-md-4 text-center">
              <img src="{{mzk_assets('assets/team/roy.jpg')}}" class="bw img-circle" />
              <p class="fs125 mt5 mb0">Prasanjeet Roy</p>
              <p class="mb15">Co-Founder & CEO</p>
            </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/ali3.jpg')}}" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Mohammad Ali Akmal</p>
                    <p class="mb15">Co - Founder & CTO</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/abdulla-shaikh-sales.jpg')}}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Abdulla Shaikh</p>
                    <p class="mb15">Sales</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/akhil-khorana-content.jpg')}}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Akhil Khorana</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/munish.jpg')}}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Munish Thakur</p>
                    <p class="mb15">Android</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/joel.jpg')}}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Joel D'sa</p>
                    <p class="mb15">Content</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/sumedha.jpg')}}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Sumedha Dhoot</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/akshay-gokalgandhi-operations.jpg')}}" width="200" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Akshay Gokalgandhi</p>
                    <p class="mb15">Operations</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{mzk_assets('assets/team/gopal-randhir-graphic-design.jpg')}}" width="200" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Gopal Randhir</p>
                    <p class="mb15">Design</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/harshita.PNG') }}" width="200" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Harshita Shah</p>
                    <p class="mb15">Human Resources</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/ruchika-singh-sales.jpg') }}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Ruchika Singh</p>
                    <p class="mb15">Sales</p>
                </div>

                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/siva.jpg') }}" class="img-circle bw " width="200" />
                    <p class="fs125 mt5 mb0">Sivaram Adabala</p>
                    <p class="mb15">Digital Marketing</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/raveena.jpg') }}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Raveena Poon</p>
                    <p class="mb15">Sales</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/siddharth.jpg') }}" class="img-circle bw " width="200" />
                    <p class="fs125 mt5 mb0">Siddharth Hiremath</p>
                    <p class="mb15">Content</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/karan-monga-operations.jpg') }}" width="200" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Karan Monga</p>
                    <p class="mb15">Operations</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/kishore-babani-sales.jpg') }}" width="200" class="bw img-circle" />
                    <p class="fs125 mt5 mb0">Kishore Babani</p>
                    <p class="mb15">Sales</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/megha-sharma-sales-operations.jpg') }}" class="img-circle bw " width="200" />
                    <p class="fs125 mt5 mb0">Megha Sharma</p>
                    <p class="mb15">Sales Operations</p>
                </div>
                <div class="col-md-4 text-center">
                    <img src="{{ mzk_assets('assets/team/nitesh-d-costa-sales.jpg') }}" width="200" class="img-circle bw " />
                    <p class="fs125 mt5 mb0">Nitesh D'Costa</p>
                    <p class="mb15">Sales</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <img src="{{ mzk_assets('assets/team/surabhi-mokashi-marketing.jpg') }}" class="img-circle bw " width="200" />
                    <p class="fs125 mt5 mb0">Surabhi Mokashi</p>
                    <p class="mb15">Marketing</p>
                </div>
            </div>

            <div class="row">
            </div>

        </div>
    </div>

            </div>
@stop
