@extends('layouts.home')
@section('content')
<div class="tf-section   pt0 pb0   ">
  <div class="container bg-white    pt0 pb0">
    <div class="">
    <div class="row">
      <div class="col-md-12 pr0-mobile pl0-mobile text-left">
        <span class="fw900 fs110 pl15 pt0  mb0 show-only-mobile">{{ strtoupper('Popular Services') }}</span>
        <span class="fw900 fs150  pt12  mb15 hide-only-mobile">{{ strtoupper('Popular Services') }}</span>
       </div>
    </div>
    <div class="row">
      <div class="pr0-mobile pl0-mobile pr0 pl0 pt10">
      @foreach($cards as $vv)
        <div class="sq-card col-md-3 col-xs-6 p0 relative h150 h150-mobile" style="border-bottom:1px solid #ffffff;border-left:1px solid #ffffff;background-image:url({{$vv['image']}})">
          <a href="{{$vv['url']}}" class="force-white">
          <div class="va-container overlap-transparent-3x va-container-v va-container-h ">
            <div class="va-middle text-center text-contrast">
              <div class="fs150 ">
                <b>{{ ($vv['name'])}}</b>
              </div>
              <div class="fs80">{{$vv['business_count']}} VENUES</div>
            </div>
          </div>
        </a>
        </div>

      @endforeach
    </div>
    </div>
  </div>
</div></div>

<div class="tf-section   pt0 pb0   ">
  <div class="container     pt0 pb0">

    <div class="row">
      <div class="col-md-12 pr0-mobile pl0-mobile text-left">

        <span class="pull-left">
          <div class="fw900 fs110 pl15 mb10 mt10 show-only-mobile">{{ strtoupper('Featured Venues') }}</div>
          <div class="fw900 fs150  pt10  mb10 hide-only-mobile">{{ strtoupper('Featured Venues') }}</div>
        </span>
       </div>
    </div>
    <div class="row no-gutter " style="padding-left:3px; padding-right:3px;">
      @foreach($featured_venues as $business)
        <div class="col-md-20 pr0-mobile pl0-mobile" style="padding-left:3px;padding-right:3px;" >
          @include('site.posts.partials.business')
        </div>
      @endforeach
    </div>
    <div class="row">
      <div class="col-md-12 pr0-mobile pl0-mobile text-left">
        <span class="pull-left">
          <div class="fw900 fs110 pl15  mb10 mt0 show-only-mobile">{{ strtoupper('Latest Stories') }}</div>
          <div class="fw900 fs150   pt5 mb10 hide-only-mobile">{{ strtoupper('Latest Stories') }}</div>
        </span>
        <span class="pull-right mt5 pt5 pr5">
          <a href="{{ route('posts.index.all')}}?{{_apfabtrack('body')}}" class="btn fw900  btn-xs bg-white btn-default">SEE ALL</a></span>
        <div class="clearfix"></div>
       </div>
    </div>
    <div class="row no-gutter " style="padding-left:3px; padding-right:3px;">
      @foreach($posts as $post)
        <div class="col-md-20 pr0-mobile pl0-mobile" style="padding-left:3px;padding-right:3px;" >
          @include('site.posts.partials.minimal')
        </div>
      @endforeach
    </div>
    <div class="hide-only-mobile" style="height:15px"></div>
  </div>
</div>

<div class="bg-home-banner-blue">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="h50"></div>
      <div class="h50 show-only-mobile"></div>
      <div class="hide-only-mobile text-center">
        <div class="fw300 p10 fs175 force-white">Brighten up your day with our FabFinds</div>
        <div class="p10">
          <a class=" btn-lg btn btn-dark-teal border-radius-25 fw900 "  href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}">
            CLICK NOW
          </a>
        </div>
        <div class="fw300 p10 fs110 force-white">Exclusively curated for you</div>
      </div>
      <div class="show-only-mobile text-center">
        <div class="fw900 p10 fs175 force-white">Brighten up your day with<br/>our FabFinds</div>
        <div class="p10">
          <a class=" btn-lg btn btn-dark-teal border-radius-25 fw900 "  href="{{ MazkaraHelper::slugCity(null, ['specials'=>true]) }}">
            CLICK NOW
          </a>
        </div>
        <div class="fw300 p10 fs110 force-white">Exclusively curated for you</div>
      </div>

      <div class="h50 show-only-mobile"></div>
      <div class="h50"></div>
    </div>
  </div>

</div>


@section('js')

<script type="text/javascript">
$(function(){
  function setupPopoversForMobileAndDesk(){
    $(".popover-mobile").popover({
      container: 'body',
      html: true,
      content: function () {
        p = $(this).data('phone').split('|');
        html = '';
        for(i in p){
          html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
        }
        return html;
      }
    }).click(function(e) {
        e.preventDefault();
    });

    $(".popover-desk").popover({

        container: 'body',
        html: true,
        content: function () {
          p = $(this).data('phone').split('|');
          html = '';
          for(i in p){
            html = html + '<div><a class="btn btn-default dpb text-center" href="javascript:void(0)">'+p[i]+'</a></div>';
          }
          return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });
  }

  setupPopoversForMobileAndDesk();


});
</script>
@stop
@endsection
