<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <p>Hello,</p>
    <div>
      {{ $job_details }}
      <p>
        @if(count($photos_done)>0)
          These were downloaded :)<br/>
          <table>
          @foreach($photos_done as $vv)
            <tr>
              <td>{{ $vv }}</td>
            </tr>
          @endforeach
          </table>
        @endif
      </p>
    </div>
    <hr/>
    <p style="color:red;">
      @if(count($photos_missed)>0)
        These could not be downloaded ;(<br/>
        <table>
        @foreach($photos_missed as $vv)
          <tr>
            <td>{{ $vv }}</td>
          </tr>
        @endforeach
        </table>
      @endif
    </p>
    <p>Have a nice day</p>
  </body>
</html>
