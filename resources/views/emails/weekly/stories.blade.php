

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Responsive Email Template</title>

<style type="text/css">
  .ReadMsgBody {width: 100%; background-color: #ffffff;}
  .ExternalClass {width: 100%; background-color: #ffffff;}
  body   {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
  table {border-collapse: collapse;}

  @media only screen and (max-width: 640px)  {
          body[yahoo] .deviceWidth {width:440px!important; padding:0;}
          body[yahoo] .center {text-align: center!important;}
      }

  @media only screen and (max-width: 479px) {
          body[yahoo] .deviceWidth {width:280px!important; padding:0;}
          body[yahoo] .center {text-align: center!important;}
      }

</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Georgia, Times, serif">

<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
      <td bgcolor="#f3f5f0" valign="top" width="100%">
      <!-- Start Header-->
      <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="margin:0 auto;">
        <tr>
          <td width="100%" bgcolor="#313131">

            <!-- Logo -->
            <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td style="padding:20px 30px" class="center">


                        <a href="http://www.fabogo.com">
                          <img src="https://s3.amazonaws.com/mazkaracdn/assets/fabogo-logo.png" alt='Fabogo'    />
                        </a>

                    </td>
                </tr>
            </table><!-- End Logo -->


          </td>
        </tr>
      </table><!-- End Header -->

      <!-- One Column -->
      <table width="580"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeed" style="margin:0 auto;">
        <tr>
          <td valign="top" style="padding:30px 0px 0px 0px;" bgcolor="#ffffff">

                  <table cellspacing="0" cellpadding="0" width="580" border="0" align="center" style="text-align: center; padding:30px;">
                    <tbody><tr>
                      <td width="100%" valign="middle" class="fullCenter" style="text-align: center; font-family: Helvetica, Arial, sans-serif, 'Open Sans'; font-size: 30px; line-height: 38px; font-weight: 800;">
                        Hey {{ $user['name']}}!
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" height="10"></td>
                    </tr>

                    <tr>
                      <td width="100%" valign="middle" class="fullCenter" style="text-align: center; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px; font-weight: 400;">
                        Here's what's trending on Fabogo!
                      </td>
                    </tr>
                                        <tr>
                      <td width="100%" height="10"></td>
                    </tr>

                    <!-- Button Center -->
                  </tbody></table>
          </td>
        </tr>
      </table><!-- End One Column -->


  <!-- 2 Column Images & Text Side by SIde -->
  <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#ffffff" style="margin:0 auto;">

  @foreach($posts as $ii=>$post)

    <tr>
      <td style="padding:10px 0">
        <table align="left" width="49%" cellpadding="0" cellspacing="0" border="0" class="deviceWidth">
          <tr>
            <td valign="top" align="center" class="center" style="padding-top:20px">
              <a title="{{{ $post->title }}}" href="{{ $post->url() }}" style="border:0px; text-decoration:none;" >
                <img src="{{ $post->cover->image->url('medium') }}" alt="" border="0" style="border-radius: 4px; width: 267px; display: block;" class="deviceWidth"  />
              </a>
            </td>
          </tr>
        </table>
        <table align="right" width="49%" cellpadding="0" cellspacing="0" border="0" class="deviceWidth">
          <tr>
            <td style="font-size: 12px; font-weight: normal; text-align: left; font-family: Georgia, Times, serif; line-height: 24px; vertical-align: top; padding:10px 8px 10px 8px">
              <table>
                <tr>
                  <td valign="middle" style="padding:0 10px 10px 0">

                        <a title="{{{ $post->title }}}" href="{{ $post->url() }}" style="text-decoration: none; font-size: 16px; color: #000; font-weight: bold; font-family:Arial, sans-serif ">
                      {{{ mzk_str_trim($post->title, 80) }}}
                    </a><br/>

                      <i><a style="color:#000;display:inline-block; text-decoration:none;border:0px;" href="{{route('users.profile.show', $post->author->id)}}">
                          {{ $post->authors_full_name }}
                          </a></i> - <span style="font-weight: 700; color: #d1a663;">{{ $post->authors_designation }}</span>                    
                  </td>
                </tr>
              </table>

                        



              <p style="mso-table-lspace:0;mso-table-rspace:0; margin:0; color:#aaa">
                        {{{ mzk_str_trim($post->caption, 100) }}} <a href="{{ $post->url() }}" style="text-decoration: none; color: #d1a663; font-weight: 600; font-style: italic;" class="underline">Continue Reading</a>
                <br/>

                <table width="200" align="left">
                  <tr>
                    <td style="padding:5px 0;" align="left">
                        <span style="font-size:11px;"><img style="width:22px;" src="https://s3.amazonaws.com/mazkaracdn/css/images/icon-views.png" /> {{ $post->views }} Views</span>
                        &nbsp;
                        <span style="font-size:11px;"><img style="width:16px;" src="https://s3.amazonaws.com/mazkaracdn/css/images/icon-likes.png" />{{ $post->num_likes() }} Likes</span>

                    </td>
                  </tr>
                </table>
              </p>
            </td>
          </tr>
        </table>
      </td>
    </tr>



@endforeach


  </table><!-- End 2 Column Images & Text Side by SIde -->




    </td>
  </tr>
</table> <!-- End Wrapper -->
<div style="background-color:#f3f5f0; display:none; white-space:nowrap; font:15px courier; color:#f3f5f0;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</div>
</body>
</html>













<table width="100%" border="0" cellpadding="0"  bgcolor="#e7e7e7" cellspacing="0" align="center" class="full">
  <tbody>
    <td width="100%" valign="top" bgcolor="#e7e7e7" align="center">
    
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
        <tbody>
            <tr><td height="30">
                        <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/email-footer-filler.jpg" height="1" style="width:600px;" />

            </td></tr><tr>
            <tr>
              <td width="100%" align="center" style="padding-top:15px;">

                  
             
              <a style="text-decoration:none;" href="https://play.google.com/store/apps/details?id=com.mazkara.user" target="_blank">
                <img height="45" src="https://s3.amazonaws.com/mazkaracdn/assets/home/google-store.png">
              </a>
              <a style="text-decoration:none;" href="https://itunes.apple.com/us/app/id1086643190" target="_blank">
                <img height="45" src="https://s3.amazonaws.com/mazkaracdn/assets/home/apple-store.png">
              </a>
              </td>
              </tr>
            <tr><td height="10"></td></tr><tr>


              <tr>
                <td height="70" valign="middle" align="center" style="padding:10px;">
                  <div class="contentEditableContainer contentTextEditable">
                    <div class="contentEditable" align="center" >
                      <table border="0" cellspacing="0" cellpadding="0" align='center'>
                        <tr>
                          <td valign="top"  width='55'>
                            <div class="contentEditableContainer contentFacebookEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://www.facebook.com/gofabogo" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/facebook.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='facebook' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://www.linkedin.com/company/fabogo" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/linkedin.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='linkedin' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://instagram.com/gofabogo?" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/instagram.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='instagram' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentTwitterEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://twitter.com/gofabogo"data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/twitter.jpg"  data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='twitter' style='margin-right:40x;'>
                              </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a target='_blank' href="https://plus.google.com/+Fabogo" data-default="placeholder"  style="text-decoration:none;">
                                  <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/google.jpg"  width="40" height="40" data-max-width="40" alt='Google plus' style='margin-right:0px;' />
                                </a>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>



                    </div>
                  </div>
                </td>
              </tr>
              <tr><td height="20"></td></tr>
      </tbody></table>
    </td>
  </tr>

<!--


</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
  <tbody><tr>
    <td width="100%" valign="top" bgcolor="#ffffff" align="center">
    
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
        <tbody><tr>
          <td align="center">
            
            <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
              <tbody><tr>
                <td width="100%" align="center">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                    <tbody><tr>
                      <td width="100%" height="15"></td>
                    </tr>
                  </tbody></table>
                  
                   
                  <table width="280" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                    <tbody><tr>
                      <td valign="top" width="100%" style="text-align: left; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 13px; color: #a3a3a3; line-height: 22px; font-weight: 400; font-style: italic;" class="fullCenter">
                        © 2016 Mazkara FZ LLC - All Rights Reserved
                        <p>This newsletter was sent to [Email] from www.fabogo.com because you subscribed. </p>
                      </td>
                    </tr>
                  </tbody></table>
                  
                  <table width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
                    <tbody><tr>
                      <td width="100%" height="15"></td>
                    </tr>
                  </tbody></table>
                  
                  <table width="280" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                    <tbody><tr>
                      <td valign="top" width="100%" style="text-align: right; font-family: Myriad Pro, Helvetica, Arial, sans-serif; font-size: 13px; color: #a3a3a3; line-height: 22px; font-weight: 400; font-style: italic;" class="fullCenter">
                        Rather not receive our newsletter anymore? [unsubscribe] instantly.
                      </td>
                    </tr>
                  </tbody></table>
                  
                </td>
              </tr>
            </tbody></table>
            
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="full">
              <tbody><tr>
                <td width="100%" height="14"></td>
              </tr>
              <tr>
                <td width="100%" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
              </tr>
            </tbody></table>
            
          </td>
        </tr>


      </tbody></table>
    
    </td>
  </tr> -->


<tr>
      <td colspan="2" style="padding:10px;">
      <center>
      <p style="font-size:10px; padding:10px; margin:10px;">This newsletter was sent to [Email] from www.mazkara.com because you subscribed. Rather not receive our newsletter anymore? <unsubscribe>Unsubscribe</unsubscribe> instantly.</p>
      </center>
      </td>
    </tr>
</tbody></table>




</body>
</html>