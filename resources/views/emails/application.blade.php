<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>{{ $data['name']}} applied to a job on Fabogo</h2>
    <p>Sent from Fabogo Jobs</p>
    <div>
      Position Applied to: {{ $data['job']->title}}, {{ $data['job']->location }}<br/>
      Name: {{ $data['name']}}<br/>
      Email: {{ $data['email']}}<br/>
      Phone: {{ $data['phone']}}<br/>
      3 reasons to join: {{ $data['message']}}<br/>
    </div>
    <hr/>
  </body>
</html>
