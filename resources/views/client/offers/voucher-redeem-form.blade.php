<div class="bg-white  col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 p0">                    
  <div class="bg-lite-gray text-center p10 mt0">
    <h4 class="mb0">VALIDATE VOUCHER</h4>
  </div>
  <div class="  mt10">
    <div class=" pt10 text-center pb10">
      <p>The voucher code entered is <spa class="text-success">valid</spa>.</p>
      <div class="fs125 mb10">{{ ($voucher->offer->title) }}</div>
      
    </div>
    <div class="pl15 pr15">
      <p class="fw300 fw300">{{ $voucher->offer->description }}</p>
        @if(count($voucher->offer->toc)>0)
          <div class="pb10 pr10  fs90 ">
            Terms & Conditions
          </div>
          <div class=" pr10 fw300 pb0">
            <ul class="ml10 fs90">
              @foreach($voucher->offer->toc as $toc)
                <li>{{$toc}}</li>
              @endforeach
            </ul>
          </div>
        @endif
      </div>
    <div class="pt5 pb5 text-center">
      {{ mzk_offer_price($voucher->offer, ' ', 'dpib  br0 pink p5 pl10 pr10 fs125 border-pink bg-white' ) }}
      <p class="mt10">
        ONLY <span class="text-danger">{{$voucher->offer->available_vouchers}}</span> VOUCHERS LEFT!
      </p>
    </div>

    <div class="row">
      <div class="col-md-6 pr0 ">
        <a href="javascript:void(0)" class="btn no-border-radius btn-redeem-voucher btn-success btn-lg dpb">REDEEM</a>
      </div>
      <div class="col-md-6 pl0">
        <a href="#" class="btn no-border-radius btn-do-not-redeem-voucher btn-default btn-lg dpb">CANCEL</a>
      </div>
    </div>


  </div>
</div>

<script type="text/javascript">
$(function(){
  $('.btn-redeem-voucher').click(function(){
    $.ajax({
      type: "POST",
      url: '{{ route('client.voucher.redeem', [mzk_client_bid(), $voucher->id] ) }}',
      data: data,
      success: function(result) {
        $.magnificPopup.instance.items[0] = {
          src: result['html'], type: 'inline' };
        $.magnificPopup.instance.updateItemHTML();
        return false;
      }
    });

    return false;

  });

  $('.btn-do-not-redeem-voucher').click(function(){
    $.magnificPopup.close();
  });


});
</script>