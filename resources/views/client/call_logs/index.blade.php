@extends('layouts.client')
@section('content')
<div class="content">

    <div class="container">



<div class="row">
	<div class="col-md-9">
	    <h3 class="portlet-title">
Call Logs for {{$business->name}}</h3>

<?php $timezone = $business->isUAE()?'uae':'ist';?>
@if ($call_logs->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th></th>
				<th>Date/Time({{ strtoupper($timezone) }})</th>
				<th>Caller </th>
				<th>Audio</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach ($call_logs as $call_log)
				<tr>
          @if($call_log->canBeResolvedByUser())
            <td><small>{{ $call_log->id }}</small></td>
          @endif
					<td>
            @if($timezone == 'uae')
              {{{ \Carbon\Carbon::parse(mzk_time_convert_from_ist_to_uae($call_log->dated.' '.$call_log->timed))->format('D, jS M \'y  h:iA') }}}
            @else
              {{{ \Carbon\Carbon::parse($call_log->dated.' '.$call_log->timed)->format('D, jS M \'y h:iA') }}}
            @endif
          </td>
					<td>{{{ $call_log->caller_number }}}</td>
					<td>
            @if(!$call_log->isConnected())
              <span class="label label-default">
                {{ strtoupper($call_log->call_transfer_status) }} CALL
              </span>
            @endif
            
          @if(strlen($call_log->media_url))
             <audio controls>
            <source src="{{ $call_log->media_url }}" type="audio/ogg">
            <source src="{{ $call_log->media_url }}" type="audio/mpeg">
          Your browser does not support the audio element.
            </audio>
            @endif
          </td>
          <td class="text-right">
            <span class=""><!-- PUT CHECK FOR PPL HERE -->
              @if($call_log->isPPL())
                @if($call_log->isLead())
                  <i class="fa fa-money" title="This call is a potential lead and is billable."></i>
                @endif
                {{ mzk_call_logs_state_helper($call_log->state) }}
                @if($call_log->isDisputable())
                  <a href="javascript:void(0)" class="btn lnk-to-dispute btn-default btn-xs" data-call="{{ $call_log->id }}">DISPUTE</a>
                @endif
                @if($call_log->canBeResolvedByUser())
                  @if($call_log->isResolvable())
                    <a href="javascript:void(0)" class="btn lnk-to-resolve btn-default btn-xs" data-call="{{ $call_log->id }}">RESOLVE</a>
                  @endif

                  @if($call_log->isResetable())
                    <a href="{{route('partner.call_logs.reset', [mzk_client_bid(), $call_log->id])}}" class="btn lnk-to-reset btn-primary btn-xs" data-call="{{ $call_log->id }}">CLEAR</a>
                  @endif
                  @if($call_log->isClear())
                    <a href="{{route('partner.call_logs.clear.to.billed', [mzk_client_bid(), $call_log->id])}}" class="btn btn-warning btn-xs" data-call="{{ $call_log->id }}">MAKE BILLABLE</a>
                  @endif
                  
                @endif
              @endif
            </span>
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $call_logs->appends($params)->render() }}
  <div class="pull-right">
    {{ count($call_logs) }} / {{ $call_logs->total() }} entries
  </div></div>
</div>

@else

	There are no call logs
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
@endif
</div>
<div class="col-md-3">

  <div class="row">
    <div class="col-md-12 text-center">
      <div class="row-stat">
        <h2>{{$call_logs->total()}}</h2>
        <small class="text-muted">Total Calls made</small><br />
        <small class="text-muted">{{date('jS F Y', strtotime($start_date)) }} - {{date('jS F Y', strtotime($end_date))}}</small>
      </div>
    </div>
  </div>

  <div class="portlet portlet-boxed">
    <div class="portlet-header">
      <h4>Filter Calls</h4>
    </div>
    <div class="portlet-body">
      <form >
        <div class="input-daterange  " id="datepicker">
          <input type="text" placeholder="Select Start Date" value="{{date('Y-m-d', strtotime($start_date))}}" class="input-sm form-control" id="start" name="start" />
          <br/>
          <input type="text" placeholder="Select End Date" value="{{date('Y-m-d', strtotime($end_date))}}" class="input-sm form-control" id="end" name="end" />
        </div>
        <br/>
        <p>
          <input type="submit" class="btn-block btn btn btn-primary" value="Apply" />
        </p>
      </form>
      <?php 
      $user = User::find(Auth::user()->id);
      if($user->hasRole('client') || $user->hasRole('admin')):?>
        <p><a href="{{ route('partner.call_logs.export', [$business->id])}}" class="btn btn-block btn-info"><i class="fa fa-download"></i> Export All Caller Numbers</a></p>
      <?php 
      endif;?>
    </div>
  </div>

</div>
    </div> <!-- /.container -->

  </div>
</div>

<script type="text/javascript">
$(function(){
    $('.input-daterange').datepicker({
      format: "yyyy-mm-dd", autoclose:true
  });    

  $('.lnk-to-dispute').click(function(){
    var call_log_id = $(this).data('call');
    BootstrapDialog.show({
      title: 'Dispute this call?',
      message: $('<div></div>').load("/partner/{{mzk_client_bid()}}/call_logs/dispute/"+call_log_id)
    });
  });


  $('.lnk-to-resolve').click(function(){
    var call_log_id = $(this).data('call');
    BootstrapDialog.show({
      title: 'Resolve this disputed call?',
      message: $('<div></div>').load("/partner/{{mzk_client_bid()}}/call_logs/resolve/"+call_log_id)
    });
  });
});
</script>
@stop