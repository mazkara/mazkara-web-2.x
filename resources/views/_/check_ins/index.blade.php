@extends('layouts.scaffold')

@section('main')

<h1>All Check_ins</h1>

<p>{{ link_to_route('check_ins.create', 'Add New Check_in', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($check_ins->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>User_id</th>
				<th>Business_id</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($check_ins as $check_in)
				<tr>
					<td>{{{ $check_in->user_id }}}</td>
					<td>{{{ $check_in->business_id }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('check_ins.destroy', $check_in->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('check_ins.edit', 'Edit', array($check_in->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no check_ins
@endif

@stop
