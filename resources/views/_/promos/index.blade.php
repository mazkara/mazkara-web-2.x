@extends('layouts.scaffold')

@section('main')

<h1>All Promos</h1>

<p>{{ link_to_route('promos.create', 'Add New Promo', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($promos->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Caption</th>
				<th>Description</th>
				<th>Fine_print</th>
				<th>Offer_amount</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($promos as $promo)
				<tr>
					<td>{{{ $promo->title }}}</td>
					<td>{{{ $promo->caption }}}</td>
					<td>{{{ $promo->description }}}</td>
					<td>{{{ $promo->fine_print }}}</td>
					<td>{{{ $promo->offer_amount }}}</td>
					<td>{{{ $promo->starts }}}</td>
					<td>{{{ $promo->ends }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('promos.destroy', $promo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('promos.edit', 'Edit', array($promo->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no promos
@endif

@stop
