@extends('layouts.scaffold')

@section('main')

<h1>All Credit_notes</h1>

<p>{{ link_to_route('credit_notes.create', 'Add New Credit_note', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($credit_notes->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Invoice_id</th>
				<th>Amount</th>
				<th>Desc</th>
				<th>Type</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($credit_notes as $credit_note)
				<tr>
					<td>{{{ $credit_note->invoice_id }}}</td>
					<td>{{{ $credit_note->amount }}}</td>
					<td>{{{ $credit_note->desc }}}</td>
					<td>{{{ $credit_note->type }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('credit_notes.destroy', $credit_note->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('credit_notes.edit', 'Edit', array($credit_note->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no credit_notes
@endif

@stop
