@extends('layouts.parallax')
@section('content')


<div class="container">
  <div class="row">
    <div class="col-md-10 bg-white col-md-offset-1">
      <div class"row ">
        <div class="col-md-12   text-center">
          <div class="fs125">
            <img src="https://s3.amazonaws.com/mazkaracdn/assets/banned.jpg" />
          <p class="text-left">
          You have been banned from access to fabogo by the administration for having indulged in activities that, to put it simply were <b>not fab!</b>
          </p>
          <p class="text-left">
          If you think this is a mistake you can always send us an <a href="mailto:hello@fabogo.com">email</a> explaining why. 
          </p>
          <p>&nbsp;<br/>&nbsp;<br/></p>
        </div>
        </div>
      </div>
</div></div>

</div>

@stop
