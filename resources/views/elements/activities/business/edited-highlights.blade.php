<?php 
  $owner = $activity->meta['owner'];
  $business = $activity->meta['business'];
?>
<div class="text">
  <i class="glyphicon glyphicon-th-large"></i>
  {{ $owner->name }} edited the highlights for <a href="{{ route('admin.businesses.show', [$business->id]) }}">{{ $business->name }}, {{ $business->zone_cache }}</a> 

</div>

