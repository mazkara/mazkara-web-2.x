<?php
$all_zones = ViewHelper::zonesComboAsSelectableArray();
$default_subzone =  null;
$default_subzone_id =  null;
if(isset($current_subzone)){
  $current_subzone = is_object($current_subzone)?$current_subzone->toArray():$current_subzone;
  $default_subzone = $current_subzone['name'];
  $default_subzone_id = $current_subzone['id'];
}
if(isset($default_search)){
  $default_search = $default_search;
}else{
  $default_search = null;
}

?>

<nav id="tf-menu" class="navbar mb0 container   navbar-default no-radius on" >
  <div id="top-main-logo-container" >
  <div class="container">
    <a class="pull-left ml10 mt10 mb10" href="/?{{ _apfabtrack(['header', 'logo']) }}">
     <img src="{{ mzk_assets('assets/current/fabogo-logo.svg')}}" />
    </a>

<!--  <ul class="nav dpib navbar-top-links pull-left navbar-left ">
    <li class="dropdown hide-only-mobile" style="margin-left:-20px;padding-top: 11px; font-size:11px;">
      <a class="btn btn-default btn-xs no-border-radius  dropdown-toggle city" data-toggle="dropdown" href="#" style="color:white;padding-right:5px; padding-bottom:2px;padding-left:5px;padding-top:0px; margin-top:5px;padding-bottom:0px;">
        {{ ucwords(MazkaraHelper::getLocale()) }} <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user no-border-radius">
        @foreach(['dubai'=>'Dubai', 'pune'=>'Pune'] as $c=>$city)
        <li><a href="{{ route('home.change.locale', [MazkaraHelper::getLocale(), $c]) }}">
          {{ $city }}
        </a></li>
        @endforeach
      </ul>
    </li>
  </ul>
-->

  @include('elements.top-menu-links')

    <div class="form-inline navbar-form navbar-left collapse navbar-collapse" id="top-navbar-search-form" role="navigation">
      <div class=" " style="width:100%;">
        <div id="search-box-holder" class="inner-addon left-addon" style="position:relative;">
          <label class="glyphicon glyphicon-search  " style="z-index:99;" rel="tooltip "></label>
          {{ Form::text('search', $default_search, 
                              array('class'=>'no-border-radius form-control 
                                              pointer-cursor selectable border-radius-5-mobile 
                                              border-left-radius-5 ', 
                                'id'=>'search-selector',
                                'style'=>'',
                                'data-content'=>"Hey! Tell us what you're looking for.",                                          
                                'data-placement'=>"bottom",
                                'placeholder'=>'Salon, service or category')) }}
          <div style="width:100%;z-index:9999;text-align:center;height:80px;background-color:#fff;top:30px;position:absolute;border: 1px solid #ccc; padding-top:30px; z-index:998;display:none;" class="search-box-preloader bt0  border-bottom-radius-5">
            <b>Coming right up!</b> <img src="{{ mzk_assets('assets/indicator.gif')}}" />
          </div>
        </div><div id="search-location-holder" class="inner-addon left-addon" style=" ">
          <label class="glyphicon glyphicon-map-marker  " style="z-index:99;" rel="tooltip "></label>
          {{ Form::text(' ', isset($where)?$where:MazkaraHelper::getLocaleLabel(), 
                          array('class'=>'form-control border-radius-5-mobile no-border-radius 
                                          pointer-cursor selectable', 
                                'id'=>'location-selector', 'style'=>'vertical-align: middle;float:none;',
                                'placeholder'=>'Please Type a Location')) }}
        </div><div class="input-group-btn" style="width:1%;display:inline-block;">
          <button id="search-bar-button" type="submit" class="btn border-radius-5-mobile no-border-radius border-right-radius-5  btn-turquoise">
            <span class="hide-only-mobile"><i class="fa fa-search"></i></span>
            <span class="show-only-mobile">SEARCH</span>
          </button>
        </div>
      </div>
    </div>
    <form id="search-bar-form" class="hidden navbar-left navbar-form" action="/search/businesses" method="POST">
      {{ Form::hidden('zone[]', $default_subzone_id, array('class'=>'form-control selectable', 'id'=>'location-selector-name'))}}
      {{ Form::hidden('search', $default_search, array('class'=>'', 'id'=>'search-selector-name'))}}
      {{ Form::hidden('category[]', (isset($default_category)?$default_category:''), array('class'=>'form-control selectable', 'id'=>'location-selector-category'))}}
      {{ Form::hidden('service[]', (isset($default_service)?$default_service:''), array('class'=>'form-control selectable', 'id'=>'location-selector-service'))}}
    </form>


    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
      <ul class="nav  navbar-nav navbar-right">
        <li>
        </li>

        @if((1==1) || (MazkaraHelper::getLocale()=='dubai'))
        @endif
        @if(Auth::check())
          @if (Auth::user()->hasRole('client'))
            <li>
              <a href="{{{ URL::to('/partner') }}}" class="page-scroll   ">Dashboard!</a>
            </li>
          @endif
        @endif

        @include('elements.menu')
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
  </div>
    <div ng-app="navigationApp" id="top-main-menu-container" >
      <div class="relative">
        <div ng-controller="NavigationController" class="collapse pl0 pr0 navbar-collapse" id="bs-main-navbar-collapse-1">
          <ul class="nav navbar-nav yamm  navbar-secondary">
            @include('elements.main-menu')
          </ul>
        </div>
      </div>
  </div><!-- /.container-fluid -->
</nav>
<div id="fullscreen-overlay"></div>