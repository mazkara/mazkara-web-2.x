<?php
$all_zones = ViewHelper::zonesComboAsSelectableArray();
$default_subzone =  null;
$default_subzone_id =  null;
if(isset($current_subzone)){
  $current_subzone = is_object($current_subzone)?$current_subzone->toArray():$current_subzone;
  $default_subzone = $current_subzone['name'];
  $default_subzone_id = $current_subzone['id'];
}
if(isset($default_search)){
  $default_search = $default_search;
}else{
  $default_search = null;
}

?>


<div class="row">
  <div class="col-md-12">
    <div class="form-inline navbar-form navbar-left collapse navbar-collapse pl0" id="top-navbar-search-form-01" role="navigation"  style="width:100%;">
      <div class=" " style="padding-left:30px;padding-right:40px;width:100%;">
        <div id="search-box-holder" class="inner-addon left-addon relative">
          <label class="glyphicon glyphicon-search  " style="font-size:14px;padding:13px 10px;" rel="tooltip "></label>
          {{ Form::text('search', $default_search, 
                                array('class'=>'p10 no-border-size 
                                                no-box-shadow 
                                                border-radius-5-mobile 
                                                border-left-radius-5 
                                                no-border-radius 
                                                pointer-cursor pr15 pl15 
                                                form-control selectable', 
                                'id'=>'search-selector',
                                'style'=>'line-height:1.7;height:auto;border-right:1px solid #EFEFEF !important;font-size:14px;',
                                'data-content'=>"Hey! Tell us what you're looking for.",                                          
                                'data-placement'=>"bottom",
                                'placeholder'=>'Search for salon, spa or a service')) }}
          <div style="width:100%;z-index:9999;text-align:center;height:80px;background-color:#fff;top:44px;position:absolute;border: 1px solid #ccc; padding-top:30px;z-index:998;display:none;" class="search-box-preloader bt0  border-bottom-radius-5">
            <b>Coming right up!</b> <img src="{{ mzk_assets('assets/indicator.gif')}}" />
          </div>
        </div><div id="search-location-holder" class="inner-addon left-addon" style="width:30%;">
          <label class="glyphicon glyphicon-map-marker  " style="font-size:14px;padding:13px 10px; ;" rel="tooltip "></label>
          {{ Form::text(' ', isset($where)?$where:MazkaraHelper::getLocaleLabel(), 
                                  array('class'=>'form-control 
                                                  no-box-shadow no-border-size 
                                                  no-border-radius 
                                                  border-radius-5-mobile  
                                                  pointer-cursor selectable 
                                                  pr15 pl15 p10  ', 
                                        'id'=>'location-selector', 
                                        'style'=>'height:auto;font-size:14px; vertical-align: middle;float:none;line-height:1.7;',
                                        'placeholder'=>'Please Type a Location')) }}
        </div><div class="input-group-btn" style="display:inline-block;">
          <button id="search-bar-button" style="margin-left: 0px; min-width: 120px; line-height: 1.7; height: auto; font-size: 14px; padding-top: 10px; padding-bottom: 8px; font-weight: 800;" type="submit" class="btn border-radius-5-mobile no-border-radius border-right-radius-5 btn-turquoise">
            SEARCH
          </button>
        </div>
      </div>
    </div>
    <form id="search-bar-form" class="hidden navbar-left navbar-form" action="/search/businesses" method="POST">
      {{ Form::hidden('zone[]', $default_subzone_id, array('class'=>'form-control selectable', 'id'=>'location-selector-name'))}}
      {{ Form::hidden('search', $default_search, array('class'=>'', 'id'=>'search-selector-name'))}}
      {{ Form::hidden('category[]', (isset($default_category)?$default_category:''), array('class'=>'form-control selectable', 'id'=>'location-selector-category'))}}
      {{ Form::hidden('service[]', (isset($default_service)?$default_service:''), array('class'=>'form-control selectable', 'id'=>'location-selector-service'))}}
    </form>
</div>
</div>
