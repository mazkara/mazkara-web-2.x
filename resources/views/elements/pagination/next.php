<?php
	$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);

	$trans = $environment->getTranslator();
?>

<?php if ($paginator->getLastPage() > 1): ?>
	<ul class="next-pagination pagination">
		<?php
			echo $presenter->getNext('Load more');
		?>
	</ul>
<?php endif; ?>
