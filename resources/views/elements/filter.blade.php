<div class="btn-group" role="group" >
  <?php 
  $afilter = $params;
  if(is_array($afilter[$ii])):
    foreach($afilter[$ii] as $ix=>$iv){
      unset($afilter[$ii][$ix]);
      ?>
        &nbsp;<a class="btn-xs btn btn-default" href="{{URL::to(URL::current().'?'.http_build_query($afilter))}}"><b>{{ucwords($vv[$iv])}}</b></a>
        <a class="btn-xs btn btn-danger" href="{{URL::to(URL::current().'?'.http_build_query($afilter))}}"><i class="fa fa-times"></i></a>&nbsp; 
      <?php
      $afilter = $params;

    }
  ?>
  <?php
  else:
    unset($afilter[$ii]);
  ?>
    &nbsp;<a class="btn-xs btn btn-default" href="{{URL::to(URL::current().'?'.http_build_query($afilter)) }}"><b>{{ucwords($ii)}}</b> {{$vv}}</a>
    <a class="btn-xs btn btn-danger" href="{{URL::to(URL::current().'?'.http_build_query($afilter))}}"><i class="fa fa-times"></i></a>&nbsp;
  <?php
  endif;
?>

</div>      
