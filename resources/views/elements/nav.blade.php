<nav id="tf-menu" class="navbar  mb0 container  navbar-default no-radius on" >
  <div id="top-main-logo-container" >
  <div class="container">
    <a class="pull-left ml10 mt10 mb10" href="/?{{ _apfabtrack(['header', 'logo']) }}">
     <img src="{{ mzk_assets('assets/current/fabogo-logo.svg')}}" />
    </a>
      <!-- /.dropdown -->
      @include('elements.top-menu-links-home')
      <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
      <ul class="nav  navbar-nav navbar-right">
      @if((1==1))
        <li>
          <span class="show-only-mobile">
          <a href="#download-app" style="margin-top:15px;" class="btn hidden show-only-mobile lnk-download-app btn-turquoise btn-sm pull-right mr5  btn dpib show-only-mobile btn-default ">
            <b>Download App</b>
          </a>
        </span>
        </li>
        @endif
        @if (Auth::check())
          @if (Auth::user()->hasRole('client'))
            <li>
              <a href="{{{ URL::to('/partner') }}}?{{ _apfabtrack(['header']) }}" class="page-scroll ">Dashboard!</a>
            </li>
          @endif
        @endif
        @include('elements.menu')
      </ul>
    </div><!-- /.navbar-collapse -->
    </div>
  </div>
  <div ng-app="navigationApp" id="top-main-menu-container" >
    <div class=" relative">
      <div ng-controller="NavigationController" class="collapse navbar-collapse pr0 pl0" id="bs-main-navbar-collapse-1">
        <ul class="nav navbar-nav yamm navbar-secondary">
          @include('elements.main-menu')
        </ul>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</nav>
<div id="fullscreen-overlay"></div>