<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')


  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        <li>
          <a href="#"><i class="fa fa-star fa-fw"></i> SMS Panel<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="{{ route('admin.sms.index') }}">SMS Dashboard</a>
            </li>
            <li>
              <a href="{{ route('admin.sms.get') }}">Send Single SMS</a>
            </li>
            <li>
              <a href="{{ route('admin.sms.get.settings') }}">Settings</a>
            </li>

          </ul>
          <!-- /.nav-second-level -->
        </li>

      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>