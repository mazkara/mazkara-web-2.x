<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')


  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        <li>
          <a href="{{ route('admin.reviews.index') }}">Reviews/Tips</a>
        </li>

        <li>
          <a href="{{ route('admin.auth.get.settings') }}">ACD Settings</a>
        </li>
        @if(Auth::user()->can("manage_users"))
          <li>
            <a href="{{ route('admin.accounts.index') }}">Users</a>
          </li>
          <li>
            <a href="{{ route('admin.accounts.reports') }}">Reports(Users)</a>
          </li>
          <li>
            <a href="{{ route('admin.reviews.reports') }}">Reports(Reviews)</a>
          </li>
        @endif
          <li class="alert-warning">
            <a href="{{ route('admin.selfies.index') }}">All User Posts</a>
          </li>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>