@extends('layouts.page')
@section('content')

<div id="tf-home" class="text-center container" style="position:absolute; height:400px;;width:100%;z-index:0;background:url({{mzk_assets('assets/add-business.jpg')}}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)">

    </div>
    <div class="text-center" style="height:365px;;">
      <div class="content" ></div></div>

<div class="container">
    <div class="row">
    <div class="col-md-7 col-md-offset-1 bg-white pt20 ">
            <h2 class="text-left fw300 notransform">Add Your Business</h2>
            <p class="lead text-left fs125">
                Fill in the form and a Customer representative will contact you shortly
            </p>
            <p class="text-center">
            </p>
  <style type="text/css">
    label{text-align:left !important;}
  </style>
        {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action('/add-your-business')->encodingType('multipart/form-data')->attribute('onsubmit', "document.getElementById('submit-btn').disabled=true;") }}

         @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
              {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
            </div>
          @endif
          @if (Session::get('notice'))
            <div class="alert alert-success">
              {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
            </div>
          @endif
          {{ BootForm::text('Full name*', 'name')->placeholder('Your Name')->required() }}
          {{ BootForm::text('Phone*', 'phone')->placeholder('Phone Number')->required() }}
          {{ BootForm::email('Email*', 'email')->placeholder('Email address')->required() }}
          {{ BootForm::textarea('Message*', 'message')->placeholder('Any details')->required() }}
    <form action="<?php $_SERVER['PHP_SELF']; ?>"  method="POST">
            <div class="g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
            <script type="text/javascript"
                    src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>">
            </script><br>

          {{ BootForm::token() }}
        <div class="form-group">
          <label for="message" class="col-lg-3 control-label"> </label>
          <div class="col-lg-8">
            {{ Form::submit('Send', array('id'=>'submit-btn', 'class' => 'btn  btn-turquoise')) }}
          </div>
        </div>
          {{ BootForm::close() }}
        </div>
    </div>
</div>
@stop
