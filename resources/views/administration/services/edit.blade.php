@extends('layouts.admin-content')
@section('content')

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Service</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

{{ Form::model($service, array('class' => 'form-horizontal', 'files'=>true,  'method' => 'PATCH', 'route' => array('admin.services.update', $service->id))) }}

  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>
    <div class="form-group">
        {{ Form::label('slug', 'Slug:', array('class'=>'col-md-2 control-label')) }}
        <div class="col-sm-10">
          {{ Form::text('slug', Input::old('Slug'), array('class'=>'form-control', 'placeholder'=>'Slug')) }}
        </div>
    </div>

  <div class="form-group">
    {{ Form::label('parent_id', 'Parent_id:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('parent_id', Service::Selector('Select Parent Service'), Input::old('parent_id'), array('class'=>'form-control')) }}
    </div>
  </div>
      <div class="form-group">
          {{ Form::label('categories', 'Categories:', array('class'=>'col-md-2 control-label')) }}
          <div class="col-sm-10">
            <ul class="list-unstyled" >
              <?php $selected_categories = array_keys($service->categories->lists('name','id')->all());
              ?>
              @foreach($current_categories as $i=>$category)
                @if($category->isActive())
                <li>
                  {{ Form::checkbox('categories['.$i.']', $category->id, in_array($category->id, $selected_categories) ) }}
                  {{ $category->name }}
                </li>
                @endif
              @endforeach

            </ul>
          </div>

      </div>

  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
    </div>
  </div>
  <div class="form-group">
      {{ Form::label('images', 'Images(s):', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
          {{ Form::file('images[]', array('multiple'=>true)) }}
      </div>

      @foreach($service->photos as $one_photo)
          <img src="{{ $one_photo->image->url('thumbnail') }}" class="img-thumbnail" />
          {{ Form::checkbox("deletablePhotos[]", $one_photo->id, false, array('style'=>' ;', 'class'=>'deletable') ) }}
      @endforeach
  </div>

  <div class="form-group">
    {{ Form::label('state', 'Active:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('state', ['active'=>'Active', 'inactive'=>'Inactive'],
                                  ($service->isActive()?'active':'inactive'),
                               array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.services.show', 'Cancel', $service->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
  </div>

{{ Form::close() }}

@stop
