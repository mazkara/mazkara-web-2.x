@if(!isset($payment))
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
          ->post()
          ->action(route('admin.payments.store'))
          ->encodingType('multipart/form-data') }}
@else
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
        ->put()
        ->action(route('admin.payments.update', $payment->id))->encodingType('multipart/form-data') }}
  {{ BootForm::bind($payment) }}
@endif



  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}
    </div>
  @endif

  {{ BootForm::select('Type', 'type', Payment::getTypes())->addClass('payment-type')}}

  {{ BootForm::text('Amount', 'amount') }}
  {{ BootForm::select('Merchant', 'merchant_id', Merchant::all()->lists('name', 'id')->all()) }}
  {{ BootForm::select('Currency', 'currency', Payment::getCurrencies()) }}
  {{ BootForm::text('Cheque number', 'chq_number')->placeholder('Number')->addClass('required chq-element') }}
  {{ BootForm::select('Cheque In Name of', 'chq_name', Payment::chequeDefaultNames())->addClass('required chq-element') }}
  {{ BootForm::text('Cheque Bank', 'chq_bank')->placeholder('Bank')->addClass('required chq-element') }}
  {{ BootForm::text('Cheque Date', 'chq_date')->addClass('dateinput required chq-element') }}
  <!-- {{ BootForm::select('State', 'state', Payment::getStates()) }}
-->
{{ BootForm::file('Photo', 'photo')->addClass(' chq-element') }}
    @if(isset($payment))
      @if($payment->photo)
        <a href="{{ $payment->photo->image->url() }}" class="lightbox">
          <img src="{{ $payment->photo->image->url('thumbnail') }}" class="img-thumbnail" />
        </a>
        {{ Form::checkbox("deletablePhotos[]", $payment->photo->id, false ) }}
        Delete?
      @endif
    @endif

  {{ BootForm::token() }}
  {{ BootForm::submit('Save') }}

{{ BootForm::close() }}
<script type="text/javascript">
$(function(){
  $('form').first().submit(function(event){
    if($('.payment-type').first().val()=='cash'){
      return;
    }

    $('input.chq-element.required, select.chq-element.required').each(function(k, v){
      if($(v).val()==''){
        alert('All cheque fields must be filled out!')
        event.preventDefault();
        return false;
      }

    });
  });

  $('input.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });

    $('.payment-type').change(function(){
        if($(this).val()=='cheque'){
            $('.chq-element').show();
        }else{
            $('.chq-element input').val('');
            $('.chq-element ').hide();
        }
    });

    if($('.payment-type').first().val()=='cheque'){
        $('.chq-element').show();
    }else{
        $('.chq-element input').val('');
        $('.chq-element ').hide();
    }

})
</script>