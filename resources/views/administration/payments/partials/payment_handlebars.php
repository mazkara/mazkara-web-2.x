<script id="payment-template" type="text/x-handlebars-template">
<div style="padding:10px; border:1px solid #dadada;">
<p style="text-transform:uppercase;"><b>{{type}}</b></p>
<p><b>Total Amount:</b> {{currency}} {{amount}}</p>
<p><b>Payable Amount Due:</b> {{currency}} {{amount_applicable}}</p>
<input type="hidden" name="payment_id" value="{{id}}" />
<input type="hidden" name="payment_currency" value="{{currency}}" />
<input type="hidden" name="payment_amount_applicable" id="payment_amount_applicable" value="{{amount_applicable}}" />
</div>

</script>
