<script id="invoice-template" type="text/x-handlebars-template">
<h3>{{title}}</h3>
<p><b>Total Amount:</b> {{currency}} {{amount}}</p>
<p><b>Payable Amount Due:</b> {{currency}} {{amount_due}}</p>
<input type="hidden" name="invoice_id" value="{{id}}" />
</script>
