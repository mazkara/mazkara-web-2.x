@extends('layouts.admin-finance')
@section('content')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.js"></script>
<div class="row">
  <div class="col-md-12">
<h1>{{{ ucwords($payment->type) }}} payment #{{ $payment->id }}</h1>
<p>
  <div class="alert alert-{{mzk_invoice_state_label_css($payment->state)}}">
    {{ ucwords($payment->state) }}
  
    <p class="pull-right">

      @if(($payment->state == 'recieved'))
        <a class="btn btn-xs btn-warning" href="{{ route('admin.payments.recieved-to-deposited', [$payment->id]) }}">Mark Deposited</a>    
      @elseif($payment->state == 'returned')
        <a class="btn btn-xs btn-warning" href="{{ route('admin.payments.returned-to-deposited', [$payment->id]) }}">Mark Deposited</a>    
      @elseif($payment->state == 'deposited')
        <a class="btn btn-xs btn-danger" id="lnk-cheque-returned" href="javascript:void(0)">Mark Returned</a>    
        <a class="btn btn-xs btn-success" href="{{ route('admin.payments.deposited-to-cleared', [$payment->id]) }}">Mark Cleared</a>    
      @endif
    </p>
  </div>
</p>

<p class="pull-right">

  {{ link_to_route('admin.payments.edit', 'Edit', array($payment->id), array('class' => 'btn btn-sm btn-info')) }}

  {{ link_to_route('admin.payments.index', 'Return to All payments', null, array('class'=>'btn btn-sm btn-primary')) }}</p>

<dl class="dl-horizontal">
  <dt>Type</dt>
  <dd>{{{ $payment->type }}}</dd>

  @if($payment->isCheque())
    <dt>Cheque number</dt>
    <dd>{{{ $payment->chq_number }}}</dd>
    <dt>Cheque bank</dt>
    <dd>{{{ $payment->chq_bank }}}</dd>
    <dt>Cheque date</dt>
    <dd>{{{ mzk_f_date($payment->chq_date) }}}</dd>
  @endif
  <dt>Amount</dt>
  <dd>{{{ $payment->currency.' '.$payment->amount }}}</dd>
  @if($payment->hasImage())
    <dt>Scan</dt>
    <dd>
      <a href="{{ $payment->photo->image->url() }}" target="_blank">
        <img src="{{ $payment->photo->image->url('thumbnail') }}" />
      </a>
    </dd>
  @endif

</dl>
<hr/>
<div class="row">
  <div class="col-md-6">
<div class="panel panel-default">
  <div class="panel panel-heading">Associated Invoices</div>
  <div class="panel panel-body">
    <table class="table">
      @foreach($payment->invoices as $invoice)
        <tr>
          <td>{{{ $invoice->title }}}</td>
          <td>{{{ mzk_f_date($invoice->dated) }}}</td>
          <td>{{{ $invoice->currency }}} {{{ $invoice->amount }}}</td>
          <td><a class="btn btn-xs btn-danger" href="{{ route('admin.payments.deallocate', $payment->id) }}?invoice_id={{$invoice->id}}"><i class="fa fa-times"></i></a></td>
        </tr>
      @endforeach
    </table>
  </div>
</div>

<div class="activity-feed">
  @foreach($activities as $activity)
    @include('elements.activities.payment.'.$activity->verb)
  @endforeach
</div>
</div>
<div class="col-md-6">
@if($payment->canBeAllocated())
  <div class="row">
    <div class="col-md-12">
    {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
          ->action(route('admin.payments.allocate', $payment->id))->encodingType('multipart/form-data') }}
      <div class="panel panel-warning">
        <div class="panel panel-heading">Find Invoice to Pay against</div>
        <div class=" panel-body">
    {{ BootForm::text('Invoice No.', 'invoice_no')->placeholder('Enter Invoice Number') }}

          <a href="javascript:void(0)" class="btn btn-primary" id="lnk-get-invoice-from-no">GET INVOICE</a>

        </div>
      </div>

    @if (Session::get('error'))
      <div class="alert alert-error alert-danger">
        {{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}
      </div>
    @endif
    @if (Session::get('notice'))
      <div class="alert alert-success">
        {{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}
      </div>
    @endif

    {{ BootForm::hidden('invoice_id', '') }}
    <div class="panel panel-default">
        <div class=" panel-body">
           <div id="invoice-holder" class="well">
        </div>
      </div>
      <div class="panel-footer allocate-payment-holder">
    {{ BootForm::token() }}

        {{ BootForm::submit('Allocate Payment to Invoice') }}

      </div>
    </div>
    
  {{ BootForm::close() }}
    </div>
    @include('administration.payments.partials.invoice_handlebars')
  </div>
@endif


</div></div>
</div>
</div>




<div id="form-cheque-returned" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.payments.deposited-to-returned', [$payment->id]))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Reason', 'reason')->placeholder('Reason for Return') }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>


<script type="text/javascript">
$(function(){

  $('#lnk-cheque-returned').click(function(){
    html = $('#form-cheque-returned').html();
    bootbox.dialog({
      title: "Cheque Returned",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: function(){
              $('.bootbox-body form').submit();
            }
          }
        }
      
    });
  });



  $('#lnk-get-invoice-from-no').click(function(){
    $.ajax({
      url: '/finance/invoices/search',
      data: { invoice_no: $('input[name=invoice_no]').first().val() }
    }).success(function(r){
      if(r==false){
        alert('No such invoice exists.');
      }else{
        var source   = $("#invoice-template").html();
        var template = Handlebars.compile(source);

        var html    = template(r.invoice);
        $('#invoice-holder').html(html);

        if(r.invoice.currency != '{{$payment->currency}}'){
          alert('You cannot allocate this payment as the currency for both are different!');
          $('.allocate-payment-holder').hide();
        }else{
          $('.allocate-payment-holder').show();
        }
      }
    });

    return false;
  })
})
</script>

@endSection
