@if(isset($lead))
  {{ Form::model($lead, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.leads.update', $lead->id))) }}
@else
  {{ Form::open(array('route' => 'admin.leads.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif
  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('phone', 'Phone:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('phone', Input::old('phone'), array('class'=>'form-control', 'placeholder'=>'Phone')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('interested_in', 'Interested In:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('interested_in', Input::old('interested_in'), array('class'=>'form-control', 'placeholder'=>'Interested In?')) }}
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($lead))
        {{ link_to_route('admin.leads.show', 'Cancel', $lead->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>
  </div>
{{ Form::close() }}
