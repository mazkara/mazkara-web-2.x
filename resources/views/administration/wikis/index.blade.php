@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="pull-right btn-group">
      {{ link_to_route('admin.wikis.create', 'Add Wiki', null, array('class' => 'btn  btn-success')) }}
    </div>
    <h1>All Wikis</h1>

    @include('elements.messages')
    <div class="well">
      {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
        {{ Form::text('search', Input::get('search'), array('class'=>'form-control mr5 col-md-6', 'placeholder'=>'Search')) }}
        {{ Form::select('service_id', ([''=>'Service?'] + $services->lists('name', 'id')->all()), Input::get('service_id'), array('class'=>'form-control mr5 ', 'style'=>'width:200px;')) }}
        <button type="submit" class="btn btn-default">Filter</button>
      {{ Form::close() }}
    </div>
    @if ($wikis->count())
      <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Service</th>
            <th>Edited On</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($wikis as $wiki)
            <tr>
              <td>
                {{ link_to_route('admin.wikis.edit', $wiki->id, array($wiki->id) ) }}
              </td>
              <td>{{ $wiki->service->name }}</td>
              <td>{{ $wiki->updated_at }}</td>
              <td>
                <div class="btn-group"> 
                  <button class="btn btn-default btn-xs dropdown-toggle" 
                          type="button" data-toggle="dropdown" 
                          aria-haspopup="true" aria-expanded="false"> 
                      <span class="fa fa-cog"></span> 
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right text-center"> 
                    <li>
                      {{ link_to_route('admin.wikis.edit', 'Edit Wiki', array($wiki->id) ) }}
                    </li>
                    <li>
                      {{ Form::open(array('style' => 'display: inline-block;', 
                                          'onsubmit'=>'return confirm("Are you sure you want to delete this wiki?")', 
                                          'method' => 'DELETE', 
                                          'route' => array('admin.wikis.destroy', $wiki->id))) }}
                        <button style="padding:3px 20px;border:0px; background-color: transparent;"> Delete </button>
                      {{ Form::close() }}
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
            
          @endforeach
        </tbody>
      </table>

      {{ $wikis->appends($params)->render() }}
    @else
      There are no advert_campaigns
    @endif
  </div>
</div>


@stop