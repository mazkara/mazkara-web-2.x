@extends('layouts.admin-content')
@section('content')
<h1>All Businesses</h1>
<div class="well">
@if(Auth::user()->hasRole('admin'))
<form style='display: inline-block;' action='{{ route('admin.businesses.export') }}' class='form-inline' method='GET'>
  <?php 
  $ttl = ceil($businesses->total()/1000);
  $op = [];
  for($i=0;$i<$ttl;$i++){
    $op[$i] = '1000 from Page '.($i+1);
  }
  ?>
  {{ Form::select('page', ($op), '', array('class'=>'form-control',  'style'=>'width:200px;')) }}
  <button type="submit" class="btn btn-warning">Export To CSV</button>

</form>
@endif
{{ link_to_route('admin.businesses.create', 'Add New Business', null, array('class' => 'btn  btn-success')) }}

</div>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text(  'byID', Input::get('byID'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;',
                    'placeholder'=>'By Business ID')) }}

    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}
    {{ Form::text(  'bySlipID', Input::get('bySlipID'), array('class'=>'form-control col-md-3', 'style'=>'width:200px;',
                    'placeholder'=>'By Slip ID')) }}
    {{ Form::text(  'byLotID', Input::get('byLotID'), array('class'=>'form-control col-md-3', 'style'=>'width:200px;',
                    'placeholder'=>'By Lot ID')) }}
    
    <select id="zone" name="zone" class="form-control" style="width:150px;">
      <option value="">Zone?</option>
      <option value="0">No Zone</option>
      @foreach ($zones as $zone)
        <?php
          //if($zone->isActive())
          {
            echo '<option value="'.$zone->id.'" '.($zone->id == Input::old('zone') ? ' selected="selected" ' : '' ).'>'.$zone->stringPath().'</option>';
          }
        ?>
      @endforeach
    </select>

    {{ Form::select('category', ([''=>'Category?'] + Category::lists('name', 'id')->all()), Input::get('category'), array('class'=>'form-control ', 'placeholder'=>'Category')) }}
    {{ Form::select('service', ([''=>'Service?'] + Service::lists('name', 'id')->all()), Input::get('service'), array('class'=>'form-control ', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    {{ Form::select('map', ([''=>'Location?', 'true'=>'Has Location', 'false'=>'No Location']), Input::get('map'), array('class'=>'form-control ', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    {{ Form::select('photos', ([''=>'Photos?', 'true'=>'Has Photos', 'false'=>'No Photos']), Input::get('photos'), array('class'=>'form-control ', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    {{ Form::select('rate', ([''=>'Rate Card?', 'true'=>'Has Rate Card', 'false'=>'No Rate Card']), Input::get('rate'), array('class'=>'form-control ', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    {{ Form::select('has_services', ([''=>'Has Service?', 'true'=>'Has Service', 'false'=>'No Services']), Input::get('has_services'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('has_highlights', ([''=>'Has Highlight?', 'true'=>'Has Highlight', 'false'=>'No Highlights']), Input::get('has_highlights'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('has_categories', ([''=>'Has Category?', 'true'=>'Has Category', 'false'=>'No Categories']), Input::get('has_categories'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('deals', ([''=>'Has DPPs of type?', 'any'=>'All DPPs'] + Deal::$types), Input::get('deals'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('deals_status', ([''=>'Has DPPs with status?'] + Deal::$statuses), Input::get('deals_status'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('packages', ([''=>'Has Packages?', 'any'=>'With Packages']), Input::get('offers'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('offers', ([''=>'Has Offers?', 'any'=>'With Offers']), Input::get('packages'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
    {{ Form::select('chain', ([''=>'Chain?'] + Group::byLocale()->orderby('name', 'ASC')->lists('name', 'id')->all()), Input::get('chain'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}   
    {{ Form::select('cost', ([''=>'Cost?'] + Business::getCostOptions()), Input::get('cost'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}   
    {{ Form::select('type', ([''=>'Business Type?'] + Business::getTypes()), Input::get('type'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}   

    {{ Form::select('richness_index', ([''=>'Richness Index?'] + range(0, 15)), Input::get('richness_index'), array('class'=>'form-control ', 'style'=>'width:100px;' )) }}   

    {{ Form::select('sort', ([ ''=>'Sort by?', 
                              'idAsc'=>'ID Ascending', 
                              'idDesc'=>'ID Descending', 
                              'nameAsc'=>'Name Ascending', 
                              'nameDesc'=>'Name Descending', 
                              'lastUpdateAsc'=>'Last Update Ascending',
                              'lastUpdateDesc'=>'Last Update Descending',
                              ]),
                           Input::get('sort'), array('class'=>'form-control', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    {{ Form::select('comments', [''=>'Commented?', 'any'=>'All Commented'/**, 'unviewed'=>'Unviewed Comments',
                     'viewed'=>'Viewed Comments'**/], Input::get('comments'), array('class'=>'form-control ', 'style'=>'width:150px;' )) }}

    {{ Form::select('active', ([''=>'Active?'] + Business::activeStates()), Input::get('active'), array('class'=>'form-control',  'style'=>'width:100px;','placeholder'=>'Active')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>
{{ Form::open(array('style' => '', 'class'=>'form-inline', 
                  'method' => 'POST', 'route'=>('admin.businesses.bulk'))) }}

<div class="well">

    {{ Form::select('action', ([''=>'Bulk Action?', 'chain'=>'Bulk Allocate Chain' ]), Input::get('action'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}   

    {{ Form::select('chain', (['0'=>'No Chain'] + Group::byLocale()->orderby('name', 'ASC')->lists('name', 'id')->all()), Input::get('chain'), array('class'=>'form-control ', 'style'=>'width:250px;' )) }}   

    <button type="submit" class="btn btn-default">Filter</button>

</div>
@if ($businesses->count())
  <div class="list-group">

		@foreach ($businesses as $business)

      <input type="checkbox" name="ids[]" value="{{ $business->id }}" style="float:left" />
      <a href="{{ route('admin.businesses.show', array($business->id) ) }}" class="list-group-item " style="margin-left:30px;">
        <div class="pull-right">
          <small>Last Updated {{{ Date::parse($business->updated_at)->ago() }}}</small><br/>
          <small>{{ $business->accumulatedReviewsCount() }} reviews/ratings</small><br/>
          <small><b>Avg. rating:</b> {{ $business->average_rating}}</small>
        </div>
        <h4 class="list-group-item-heading">
          {{{ $business->id }}} -          
          {{{ $business->name }}}
          <small>
            <span class="label label-{{ $business->getRichnessColor() }}" title="{{ $business->getRichnessDetails() }}">
              {{ '# '.$business->richness_index }}
            </span>
          </small>


        </h4>
        <p class="list-group-item-text">
          <b>Phone:</b>{{{ join( ',', $business->phone) }}}
          <b>Email:</b>{{{ join( ',',$business->email) }}}
          <br/>
          <b>Website:</b>{{{ $business->website }}}
          <b>Zone:</b>{{{ $business->zone_cache }}}
        </p>
        <p class="list-group-item-text">
          <span class="label label-{{ $business->active == 'active' ?'success':'warning'}}">{{{ $business->active }}}</span>
          @if($business->services_count == 0)
            <span class="label label-danger">No Services</span>
          @endif
          @if($business->categories_count == 0)
            <span class="label label-danger">No Categories</span>
          @endif
          @if($business->image_count == 0)
            <span class="label label-danger">No Photos</span>
          @endif
          @if($business->rate_card_count == 0)
            <span class="label label-danger">No Rate Cards</span>
          @endif
          @if(0 ==($business->geolocation_latitude + $business->geolocation_longitude))
            <span class="label label-danger">Location Not Set</span>
          @endif
          <?php $tc = count($business->comments()->get()); ?>
          @if($tc>0)
          <span title="{{$tc}} comments " class="pull-right label label-warning "><i class="fa fa-comments-o"></i> 
            {{$tc}}</span>
          @endif
        </p>
      </a>
		@endforeach      
  </div>

  <div class="row">
    <div class="col-md-12">
	{{ $businesses->appends($params)->render() }}
  <div class="pull-right">
    {{ count($businesses) }} / {{ $businesses->total() }} entries
  </div></div>
</div>
@else
	There are no businesses
@endif
  {{ Form::close() }}
@stop