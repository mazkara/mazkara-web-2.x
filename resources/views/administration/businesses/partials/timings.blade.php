<?php $replace = '';?>
<?php 
$days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
if(isset($timing)):
    $replace = $timing->id;
else:
    $replace = '_REPLACE_';
endif;?>
<div class="row content">
    <div class=" col-md-6">
        <div class="btn-group toggles " data-toggle="buttons">
            @for($i=0; $i < 7; $i++)
                <?php
                $checked = false;
                if(isset($timing)){
                    $cday = $days[$i];
                    $checked = strstr($timing->daysOfWeek, $cday);
                }
                    
                
                ?>
              <label class="btn btn-default {{ $checked ? 'active' :'' }} ">
                <input type="checkbox" class="toggable" autocomplete="off" checked="{{ $checked ? 'checked' :'' }}" value=" {{ $days[$i] }} " /> {{ $days[$i]}}
              </label>
            @endfor
            <input name="timings[{{ $replace }}][daysOfWeek]" class="daysOfWeek" value="{{ isset($timing)? $timing->daysOfWeek : '' }}" type="hidden" />
        </div>
    </div>
    <div class=" col-md-2">
        {{ Form::text("timings[$replace][open]", isset($timing)?$timing->open:Input::old('open'), array('class'=>'form-control timepicker', 'placeholder'=>'Open')) }}
    </div>
    <div class=" col-md-2">
        <p>{{ Form::text("timings[$replace][close]", isset($timing)?$timing->close:Input::old('close'), array('class'=>'form-control timepicker', 'placeholder'=>'Close')) }}</p>
    </div>
@if(isset($timing))
    {{ Form::hidden("timings[$replace][id]", $timing->id) }}
@endif
    <div class=" col-md-2">
@if(isset($timing))
    {{ Form::checkbox("timings[$replace][deletable]", $timing->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
    <a href="javascript:void(0)" class="delete-existing btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
@else
    <a href="javascript:void(0)" class="delete btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
@endif
    </div>

</div>
