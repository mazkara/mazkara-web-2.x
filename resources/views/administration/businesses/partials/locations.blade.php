{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@postLocation', $business->id))) }}
<?php 
$default_city = '';
$default_state = '';
$landmark = '';
if(is_null(Input::old('geolocation_city')) && is_null($business->geolocation_city)){
  $default_city = ($business->zone_cache == '' ? '' : $business->zone_cache);
}else{
  $default_city = Input::old('geolocation_city');
}

if(is_null(Input::old('geolocation_state')) && is_null($business->geolocation_state)){
  $default_state = ($business->city_id > 0 ? Zone::find($business->city_id)->name : '');
}else{
  $default_state = Input::old('geolocation_state');
}

if(is_null(Input::old('landmark'))&& is_null($business->landmark)){
  $landmark = ', '.$default_city.', '.$default_state;
}else{
  $landmark = Input::old('landmark');
}

?>
  <div class="form-group">
    <fieldset>
        <legend>Location</legend>
        <div class="row">
            <div class="col-md-6">
                <div id="map" style="height:400px">
                </div> 
            </div>
            <div class="col-md-6 text-left" >
                <div class="form-group">
                    <a href="javascript:void(0)" id="link-get-users-location" class=" btn btn-info">
                        Get Current Location
                    </a>
                </div>
                <div class="form-group">
                    {{ Form::label('landmark', 'Address(Human readable+Landmark):', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('landmark', $landmark, array('class'=>'form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('geolocation_latitude', 'Latitude:', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('geolocation_latitude', Input::old('geolocation_latitude'), array('class'=>'form-control', 'placeholder'=>'Geolocation_latitude')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('geolocation_longitude', 'Longitude:', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('geolocation_longitude', Input::old('geolocation_longitude'), array('class'=>'form-control', 'placeholder'=>'Geolocation_longitude')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('geolocation_address', 'Street:', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('geolocation_address', Input::old('geolocation_address'), array('class'=>'form-control', 'placeholder'=>'Geolocation_address')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('geolocation_city', 'City:', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('geolocation_city', $default_city, array('class'=>'form-control', 'placeholder'=>'Geolocation_city')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('geolocation_state', 'State:', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('geolocation_state', $default_state, array('class'=>'form-control', 'placeholder'=>'Geolocation_state')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('geolocation_country', 'Country:', array('class'=>'col-md-3 control-label')) }}
                    <div class="col-sm-6">
                      {{ Form::text('geolocation_country', Input::old('geolocation_country'), array('class'=>'form-control', 'placeholder'=>'Geolocation_country')) }}
                    </div>
                </div>


            </div>
        </div>            
    </fieldset>
  </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
<script>
$(function(){

    $(document).on('click', '.toggles .btn', function(){
        inp = $(this).find('input');
        hd = $(this).parents('.toggles').first().find('input:hidden');
        if(!$(this).hasClass('active')){
            $(hd).val( S($(hd).val() + $(inp).val()).trim() );
        }else{
            $(hd).val( S(S((hd).val()).replaceAll($(inp).val(), '')).trim() );
        }

    });

    geocoder = new google.maps.Geocoder()
    var setBusinessPosition = function(position){
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        $('#geolocation_latitude').val(lat).change();
        $('#geolocation_longitude').val(lng).change();
        lc = new google.maps.LatLng(lat, lng);

        var mapContext = business_map.locationpicker('map');
        mapContext.marker.setPosition(lc);
        mapContext.map.panTo(lc);

        geocoder.geocode({
                    latLng: lc
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        mapContext.locationName = results[0].formatted_address;
                        mapContext.addressComponents = get_address_component_from_google_geocode(results[0].address_components);
                        updateControls(mapContext.addressComponents);
                    }
                });

    };
    var noLocation = function(){

    };

    var updateControls = function(addressComponents) {
        $('#geolocation_address').val(addressComponents.addressLine1);
        $('#geolocation_city').val(addressComponents.city);
        $('#geolocation_state').val(addressComponents.stateOrProvince);
        $('#geolocation_country').val(addressComponents.country);

    };

    var business_map = $('#map').locationpicker({
        radius: 0,
        @if(isset($business))
            location: { latitude:<?php echo $business->geolocation_latitude;?>, longitude:<?php echo $business->geolocation_longitude;?> },
        @endif
        enableAutocomplete: false,
        inputBinding: {
            latitudeInput: $('#geolocation_latitude'),
            longitudeInput: $('#geolocation_longitude'),
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            updateControls(addressComponents);
            $('#geolocated').val(1);            
        },
    });
    $('#link-get-users-location').click(function(){
        $.geolocation.get({win: setBusinessPosition, fail: noLocation});
    })

})

</script>