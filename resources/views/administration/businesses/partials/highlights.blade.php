{{ Form::model($business, array('class' => 'form-horizontal', 'id'=>'frm-manage-highlights',  'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@postHighlights', $business->id))) }}
<?php $genders = [4,5,6];?>
        <div class="form-group">
            {{ Form::label('highlights', 'Highlights:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <ul class="list-unstyled" >
                    @foreach($highlights_all as $highlight)
                    <?php 
                      if(in_array($highlight['id'], $genders)){
                        continue;
                      } ?>

                      @if(in_array($highlight['id'], $active_highlight_ids))
                        <li>
                            {{ Form::checkbox('highlights[]', $highlight['id'], in_array($highlight['id'], $selected_highlights) ) }}
                            {{ $highlight['name'] }}
                        </li>
                      @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('genders', 'Gender:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <ul class="list-unstyled gender-checkboxes" >
                    @foreach($highlights as $highlight)
                    <?php 
                      if(!in_array($highlight['highlight_id'], $genders)){
                        continue;
                      } ?>
                        <li>
                            {{ Form::checkbox('highlights[]', $highlight['highlight_id'], in_array($highlight['highlight_id'], $selected_highlights) ) }}
                            {{ $highlight['name'] }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('inactive_highlights', 'Inactive Highlights:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-10">
                <ul class="list-unstyled" >
                    @foreach($highlights_all as $highlight)
                    <?php 
                      if(in_array($highlight['id'], $genders)){
                        continue;
                      } ?>

                      @if(in_array($highlight['id'], $active_highlight_ids))
                      @else
                        <li style="color:gray">
                            {{ Form::checkbox('highlights[]', $highlight['id'], in_array($highlight['id'], $selected_highlights) ) }}
                            {{ $highlight['name'] }}(inactive)
                        </li>
                      @endif
                    @endforeach
                </ul>
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
<script>
$(function(){
  $('#frm-manage-highlights').submit(function(){
    if($('.gender-checkboxes input:checked').length == 0){
      alert('You must select atleast one gender based highlight!');
      return false;
    }else{
      return true;
    }
  });

  $(document).on('click', '.toggles .btn', function(){
    inp = $(this).find('input');
    hd = $(this).parents('.toggles').first().find('input:hidden');
    if(!$(this).hasClass('active')){
      $(hd).val( S($(hd).val() + $(inp).val()).trim() );
    }else{
      $(hd).val( S(S((hd).val()).replaceAll($(inp).val(), '')).trim() );
    }
  });
    
  $('#services-list').bonsai({
      expandAll: true,
      checkboxes: true, // depends on jquery.qubit plugin
      handleDuplicateCheckboxes: true // optional
    });

})

</script>