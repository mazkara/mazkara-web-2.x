{{ Form::open(array('id'=>'item-form', 'class'=>'form-vertical', 
                    'route' => 'admin.service_items.store','files'=>true)) }}
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <select name="services" multiple="multiple" data-live-search="true" id="item-service_ids" class="form-control">
          @foreach (Service::query()->showParents()->orderby('name', 'asc')->get() as $parent)
            <optgroup label="{{$parent->name}}">
              @foreach ($parent->kids()->showActive()->orderby('name', 'asc')->get() as $service)
                @if($service->isActive())
                  <option value="{{$service->id}}" >{{$service->name}}</option>
                @endif
              @endforeach
            </optgroup>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        {{ Form::textarea('description', '', array( 'class'=>' form-control', 
                                                    'rows'=>5, 'placeholder'=>'Description of Service')) }}
      </div>
      <div class="form-group">
        {{ Form::text('cost', '', array('class'=>'deactivable form-control', 
                                      'placeholder'=>'Cost', 'style'=>'width:50%;display:inline-block;')) }}

        {{ Form::select('cost_type', ['fixed'=>'Fixed Price', 'onwards'=>'Onwards', 'consultation-fee'=>'Consultation Fee', 
                        'price-on-consultation'=>'Price on Consultation'], '', 
                        array('class'=>'deactivable form-control', 'style'=>'display:inline-block;width:40%;')) }}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        {{ Form::text('name', '', array('class'=>'form-control', 'placeholder'=>'Name of service')) }}
      </div>
      <div class="form-group">
        {{ Form::text('duration', '', array('class'=>'form-control deactivable', 'placeholder'=>'Duration', 'style'=>'display:inline-block;width:50%;')) }}
        {{ Form::select('duration_type', ['minutes'=>'Minutes', 'hours'=>'Hours', 
                                        'days'=>'Days', 'months'=>'Months'], '', 
                                        array('class'=>'deactivable form-control', 'style'=>'display:inline-block;width:40%;')) }}
      </div>
      {{ Form::hidden('business_id',$business->id) }}
      {{ Form::hidden('state', 'active') }}
      <div class="form-group">
        {{ Form::select('menu_group_id',  ['select'=>'Select a Header'] + App\Models\Menu_group::optionables(['business_id'=>$business->id]), 'select', 
                                        array('class'=>'form-control', 'style'=>'width:50%; display:inline-block;')) }} 
        <a href="javascript:void(0)" class="btn btn-warning" id="lnk-add-new-menu-group">Add Header</a>
      </div>                                        
      <!-- <div class="form-group hidden">
      {{ Form::checkbox('state', 'inactive', false, ['id'=>'item-state-inactive-checker']) }} Check if item is inactive
      </div> -->
      {{ Form::token() }}
      <button class="btn btn-primary " style="display:block;width:100%;" type="submit">Save</button>
    </div>
  </div>
{{ BootForm::close() }}
