{{ Form::open(array('id'=>'combo-form', 'class'=>'form-vertical', 'route' => 'admin.combo_items.store', 'style'=>'min-width:900px;', 'files'=>true)) }}
<div class="well">
  <div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <select name="item_ids" multiple="multiple" data-live-search="true"   id="combo-service_ids" class="form-control">
        @foreach ($business->serviceItems()->orderby('name', 'asc')->get() as $item)
          <option value="{{$item->id}}" >{{$item->name}}({{$item->service->name}})</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      {{ Form::textarea('description', '', array('class'=>'form-control', 'rows'=>1, 'placeholder'=>'Description of Service')) }}
    </div>
    <div class="form-group">
      {{ Form::number('cost', '', array('class'=>'form-control', 'oninvalid'=>"Please enter the cost",  'placeholder'=>'Cost')) }}
    </div>
    <div class="form-group">
    {{ Form::select('cost_type', ['fixed'=>'Fixed Price', 'onwards'=>'Onwards', 'consultation-fee'=>'Consultation Fee', 'price-on-consultation'=>'Price on Consultation'], '', array( 'class'=>'form-control')) }}
    </div>
  </div>  
  <div class="col-md-6">
    <div class="form-group">
      {{ Form::text('name', '', array('class'=>'form-control', 'placeholder'=>'Name of service')) }}
    </div>
  <div class="form-group">
  {{ Form::number('duration', '', array('class'=>'form-control',  'placeholder'=>'Duration')) }}
  </div>
  <div class="form-group">
    {{ Form::select('duration_type', ['minutes'=>'Minutes', 'hours'=>'Hours', 'days'=>'Days', 'months'=>'Months'], '', array( 'class'=>'form-control')) }}
  </div>
  <div class="form-group">
    {{ Form::select('grouping', [], '', array( 'class'=>'form-control')) }}
  </div>
  <div class="form-group">
  {{ Form::text('grouping_text', '', array('class'=>'form-control',  'placeholder'=>'Grouping Text')) }}
  </div>

  <div class="form-group">
  {{ Form::hidden('business_id',$business->id) }}
  </div>
  <div class="form-group">
  {{ Form::token() }}
  <button class="btn btn-primary" type="submit">Save</button>
  </div>
  
  </div>
</div>
</div>
{{ BootForm::close() }}
