{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@postServices', $business->id))) }}
<div class="form-group">
  {{ Form::label('services', 'Services:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    <ul id="services-list">
      <?php $allowed_services = $services;?>
      @foreach($services as $service)
        <li>
          @if(count($service->children) >0)
            {{ Form::checkbox('services[]', $service->id, in_array($service->id, $selected_services) ) }}
            {{ $service->name }}
            <ul>
              @foreach($service->children as $child)
                @if(in_array($child->id, $allowed_services))
                  <li>
                    {{ Form::checkbox('services[]', $child->id, in_array($child->id, $selected_services) ) }}
                    {{ $child->name }}
                  </li>
                @endif
              @endforeach
            </ul>
          @endif
        </li>
      @endforeach
    </ul>
  </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
<script>
$(function(){

  $(document).on('click', '.toggles .btn', function(){
    inp = $(this).find('input');
    hd = $(this).parents('.toggles').first().find('input:hidden');
    if(!$(this).hasClass('active')){
      $(hd).val( S($(hd).val() + $(inp).val()).trim() );
    }else{
      $(hd).val( S(S((hd).val()).replaceAll($(inp).val(), '')).trim() );
    }

  });
  
  $('#services-list').bonsai({
    expandAll: true,
    checkboxes: true,
    handleDuplicateCheckboxes: true
  });

})

</script>