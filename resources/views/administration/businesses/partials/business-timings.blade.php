{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@postTimings', $business->id))) }}
<div class="form-group">
  <div class="col-sm-10">
        <fieldset>
            <legend>Timings</legend>
            <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-default btn-xs btn-danger" id="lnk-add-timings">Add Timing(s)</a>
            <div id="timings-here"></div>
            @if(isset($business))
                @foreach($business->timings as $timing)
                    @include('administration.businesses.partials.timings')
                    <?php unset($timing);?>
                @endforeach
            @endif
        </fieldset>

  </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
<div id="timing_holder"  style="display:none" >@include('administration.businesses.partials.timings')</div>

<script>
$(function(){

    $(document).on('click', '.toggles .btn', function(){
      inp = $(this).find('input');
      hd = $(this).parents('.toggles').first().find('input:hidden');
      ctVal = $(hd).val();
      if(!$(this).hasClass('active')){
        $(hd).val( S($(hd).val() + $(inp).val()).trim() );
      }else{
        $(hd).val( S(S(ctVal).replaceAll(S($(inp).val()).trim(), '')).trim() );
      }
    });

    $('#lnk-add-timings').click(function(){
      html = $('#timing_holder').html();
      html = S(html).replaceAll('_REPLACE_', new Date().getTime());
      /*/html = S(html).replaceAll('checked=""', 'checked="checked"');
      //html = S(html).replaceAll('btn btn-default', 'btn btn-default   active');*/
      $('#timings-here').prepend(html.s);
    });

    $(document).on('click', '.delete-existing', function(){
      $(this).parents('.content').first().hide();
      $(this).parents('.content').first().find('.deletable').prop('checked', true);
    });

    $(document).on('click', '#timings-here .delete', function(){
      $(this).parents('.content').first().remove();

    });

    $(document).on('focus', '.timepicker', function(){
      $(this).timepicker({ timeFormat: 'H:i' });
    })

})

</script>