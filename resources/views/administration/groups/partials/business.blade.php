<li class="list-group-item">
  {{ $business['name'] }},{{ $business['zone_cache'] }} {{Form::hidden('businesses[]', $business['id'])}}
  <span class="pull-right">
  {{Form::checkbox('deletableBusinesses[]', $business['id'], '', ['class'=>'deletable','style'=>'display:none'] )}}<a href="javascript:void(0)" class="deletable-link"><i class="fa fa-times"></i></a>
  </span> 
</li>