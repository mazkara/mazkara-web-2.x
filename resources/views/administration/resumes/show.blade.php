@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
<div class="pull-right">
  {{ link_to_route('admin.resumes.edit', 'Edit', array($resume->id), array('class' => 'btn btn-xs btn-info')) }}
  {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>"return confirm('Are you sure?');", 'method' => 'DELETE', 'route' => array('admin.resumes.destroy', $resume->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
  {{ Form::close() }}
</div>
<h1>{{$resume->id}} - {{ $resume->name }}</h1>
<hr/>

    <dl class="dl-horizontal">
      @foreach(['name', 'location', 'nationality', 'phone', 'experience', 'salary'] as $fd)
        @if(isset($resume->$fd))
          <dt>{{ucwords($fd)}}</dt>
          <dd>{{ ucwords($resume->$fd) }}</dd>
        @endif
      @endforeach
        <dt>Specializations</dt>
        <dd>{{ join(',',$resume->specializations->lists('specialization','specialization')->all()) }}</dd>
    </dl>    

    @if($resume->hasCVAttached())
    <div class="well">
      <a href="{{ $resume->doc->url() }}" class="btn btn-sm btn-default">Download Resume</a>
    </div>
    @endif
    <!--
     @include('administration.tags.machine', [ 'obj'=>$resume,
                                              'taggable_type'=>'Resume',
                                              'taggable_id'=>$resume->id]) 
                                            -->

  </div>
  <!--
          @include('administration.tags.machine-code')
        -->

</div>
<p><a href="{{ route('admin.resumes.index') }}" class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Back</a></p>
@stop