@extends('layouts.admin-content')
@section('content')

<div class="row">
  <div class="col-md-10 col-md-offset-2">
    <h1>Edit Highlight</h1>

    @if ($errors->any())
    	<div class="alert alert-danger">
  	    <ul>
          {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    	</div>
    @endif
  </div>
</div>

{{ Form::model($highlight, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.highlights.update', $highlight->id))) }}

  <div class="form-group">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('slug', 'Slug:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('slug', Input::old('slug'), array('class'=>'form-control', 'placeholder'=>'Slug')) }}
    </div>
  </div>

  <div class="form-group">
      {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
      </div>
  </div>

  <div class="form-group">
      {{ Form::label('css', 'Css Class:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('css', Input::old('css'), array('class'=>'form-control', 'placeholder'=>'Css')) }}
      </div>
  </div>
  <div class="form-group">
    {{ Form::label('state', 'Active:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('state', ['active'=>'Active', 'inactive'=>'Inactive'], 
                        ($highlight->isActive()?'active':'inactive'), 
                         array('class'=>'form-control')) }}
    </div>
  </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.highlights.show', 'Cancel', $highlight->id, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}

@stop
