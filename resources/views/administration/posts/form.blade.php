<?php $existing_services = [];?>
@if(isset($post))
  {{ Form::model($post, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.posts.update', $post->id))) }}
  <?php $existing_services = $post->services()->lists('service_id','service_id')->all();?>
@else
  {{ Form::open(array('route' => 'admin.posts.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif
<div class="form-group">
  {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('seo_page_title', 'SEO Page Title:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('seo_page_title', Input::old('seo_page_title'), array('class'=>'form-control', 'placeholder'=>'SEO Page Title')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('seo_page_meta_desc', 'SEO Page Meta Desc:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('seo_page_meta_desc', Input::old('seo_page_meta_desc'), array('class'=>'form-control', 'placeholder'=>'SEO Page Meta Desc')) }}
  </div>
</div>

@if(isset($post))
  <div class="form-group">
    {{ Form::label('slug', 'Slug:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('slug', Input::old('slug'), array('class'=>'form-control', 'placeholder'=>'Slug')) }}
    </div>
  </div>
@endif
<hr/>
<div class="form-group">
  {{ Form::label('cover', 'Cover Photo', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::file('cover', array( 'accept'=>"image/*", 'capture'=>'camera')) }}
    @if(isset($post))
      @if($post->cover)
        <a href="{{ $post->cover->image->url() }}" class="lightbox"><img src="{{ $post->cover->image->url('thumbnail') }}" class="img-thumbnail" /></a>
        {{ Form::checkbox("deletablePhotos[]", $post->cover->id, false ) }}
        Delete?
      @endif
    @endif
  </div>
</div>
<div class="form-group">
  {{ Form::label('cover_photo_title', 'Cover Photo Title:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('cover_photo_title', Input::old('cover_photo_title'), array('class'=>'form-control', 'placeholder'=>'Cover Photo Title')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('cover_photo_alt', 'Cover Photo ALT:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('cover_photo_alt', Input::old('cover_photo_alt'), array('class'=>'form-control', 'placeholder'=>'Cover Photo ALT')) }}
  </div>
</div>
<hr/>
<div class="form-group">
  {{ Form::label('caption', 'Caption:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::textarea('caption', Input::old('caption'), array('class'=>'form-control', 'placeholder'=>'Caption', 'style'=>'height:70px;')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('body', 'Body:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::textarea('body', Input::old('body'), array('class'=>'form-control wysiwyg', 'id'=>'post-body', 'placeholder'=>'Body')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('state', 'State:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::select('state',  Post::getStates(),   Input::old('state'),array('class'=>'form-control', 'placeholder'=>'State')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('published_on', 'Published on:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('published_on', Input::old('published_on'), array('class'=>'form-control dateinput', 'placeholder'=>'Published on')) }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('services', 'Services', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    <select name="services[]" multiple="multiple" data-live-search="true" id="service_ids" class="form-control">
      @foreach (Service::query()->showParents()->orderby('name', 'asc')->get() as $parent)
        <optgroup label="{{$parent->name}}">
          @foreach ($parent->kids()->orderby('name', 'asc')->get() as $service)
            <option value="{{$service->id}}" {{ in_array($service->id, $existing_services)?'selected="selected"':''}}  >{{$service->name}}</option>
          @endforeach
        </optgroup>
      @endforeach
    </select>
  </div>
</div>


<div class="form-group">
  <label class="col-sm-2 control-label">&nbsp;</label>
  <div class="col-sm-10">
    {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
  </div>
</div>

{{ Form::close() }}

<script type="text/javascript">
$(function(){
  var $summernote = $('#post-body');
  $summernote.summernote({
    height: 500,
    fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36'],
    onInit: function(){
      $('.note-image-input').prop('name', 'files[]');
    },

    onImageUpload: function(files, editor, welEditable) {
      data = new FormData();
      data.append("file", files[0]);
      editr = editor;


      $.ajax({
        data: data,
        type: "POST",
        url: "/content/photos/upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(result) {
          $summernote.summernote('editor.insertImage', result.url);
        }
      });
    }    
  });

  $('#service_ids').selectpicker();
  $('#service_ids').selectpicker('val', {{ json_encode(array_values($existing_services)) }});
  $('.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yy-mm-dd", autoclose:true });

});


</script>