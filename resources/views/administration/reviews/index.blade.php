@extends('layouts.admin-users')
@section('content')


<div class="row">
  <div class="col-md-12">
    <h1>All Reviews</h1>

<p>{{ link_to_route('reviews.create', 'Add New Review', null, array('class' => 'btn btn-sm pull-right btn-success')) }}</p>

<div class="well clearfix">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}

    {{ Form::select('business', Business::where('reviews_count', '>', 0)
                                          ->whereOr('ratings_count', '>', 0)->orderby('name', 'asc')->byLocale()
                                          ->lists('name', 'id')->all(), 
                                          Input::get('business'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Select Business')) }}
    {{ Form::select('users', User::where('reviews_count', '>', 0)
                                        ->whereOr('ratings_count', '>', 0)
                                        ->orderby('name', 'asc')
                                        ->lists('name', 'id')->all(), 
                                        Input::get('users'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Select Users')) }}
    {{ Form::select('type', ['all'=>'All', 'ratings'=>'Ratings Only', 'reviews'=>'Reviews Only'],
                                        Input::get('type'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;')) }}

    {{ Form::select('flags', ['pending'=>'Pending'],
                                        Input::get('type'),  array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Select Flag')) }}

    {{ Form::text('start', Input::get('start'), array('class'=>'form-control dateinput col-md-2', 'style'=>'width:100px;', 'placeholder'=>'Start Date')) }}
    {{ Form::text('end', Input::get('end'), array('class'=>'form-control dateinput col-md-2', 'style'=>'width:100px;', 'placeholder'=>'End Date')) }}


    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>


@if ($reviews->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID{{ '#' }}</th>
				<th>Rating</th>
				<th width="60%">Review</th>
        <th>Flags?</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($reviews as $review)
				<tr>
          <td>{{ $review->id }}</td>
					<td>{{{ $review->rating }}}</td>
					<td>

            @if($review->isARating())
            @else
              <div class="well">
                {{{ $review->body }}}
                <p><small><b>tagged:{{ join(',', $review->services()->lists('name','name')->all())}}</b></small></p>
              </div>
            @endif
            <small>
            @if($review->user)
              By
              {{ link_to_route('users.profile.show', $review->user->getFullNameAttribute(), array($review->user_id)) }}
            @else
              From {{ $review->ip }}
            @endif
              For 
              @if($review->business!=null)
              <a href="/{{ MazkaraHelper::getLocale().'/'.$review->business->slug}}" class=" ">
                {{{ $review->business->name }}}
              </a>
              @else
                Unknown Business of ID {{{ $review->business_id }}}

              @endif

              on {{ \Carbon\Carbon::parse($review->created_at)->toDayDateTimeString()}}
            </small>
          </td>
          <td>
            @if(!empty($review->flags))
              <span class="label label-warning">
                {{ $review->flags }}
              </span>
            @endif
          </td>
          <td>
            @if(Auth::user()->can("manage_highlights"))
                                @if($review->isActive())
                    <span class="label label-success"><i class="fa fa-eye"></i></span> <a href="{{ route('admin.reviews.hide', $review) }}" class="btn btn-xs btn-default">hide</a>&nbsp;
                  @else
                    <span class="label label-default"><i class="fa fa-eye-slash"></i></span> <a href="{{ route('admin.reviews.unhide', $review) }}" class="btn btn-xs btn-default">show</a>&nbsp;
                  @endif
{{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onclick'=>'return confirm(\'Are you sure you want to delete this review?\');', 'route' => array('admin.reviews.destroy', $review->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.reviews.edit', 'Edit', array($review->id), array('class' => 'btn btn-xs  btn-info')) }}
            @endif
          </td>


				</tr>


        @foreach($review->comments as $comment)
          <tr class="info">
            <td colspan="2"></td>
            <td >
              <div  style="padding-left:30px;border-left:3px solid #CCC;">
                <small>
                  <a href="javascript:void(0)" class="item-inline-editable"
                     id="body" data-type="textarea" data-pk="{{$comment->id}}" 
                     data-url="{{ route('admin.comments.updatable') }}" 
                     data-title="Edit Comment">{{ $comment->body}}</a> </small> 
                     <p><small> <b>Posted:</b>
                                    on {{ \Carbon\Carbon::parse($comment->created_at)->toDayDateTimeString()}}
                                </small> By 
              {{ link_to_route('users.profile.show', $comment->getDisplayableUsersName(), array($comment->user_id)) }}
</p>
              </div>
            </td>
            <td ></td>
            <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm(\'Delete Comment? Are you sure?\');', 'method' => 'DELETE', 'route' => array('admin.comments.destroy', $comment->id))) }}
              {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}

            </td>
          </tr>
        @endforeach


			@endforeach
		</tbody>
	</table>


  <div class="row">
    <div class="col-md-12">
  {{ $reviews->appends($params)->render() }}
  <div class="pull-right">
    {{ count($reviews) }} / {{ $reviews->total() }} entries
  </div></div>
</div>

@else
	There are no reviews
@endif
</div></div>
<script type="text/javascript">
$(function(){
  $('.item-inline-editable').editable();

  $('input.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });

});

</script>

@stop
