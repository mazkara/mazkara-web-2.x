@extends('layouts.admin-users')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>Ratings and Reviews Report</h1>

    <div class="row">
      <div class="col-md-12">
        <div class="well">
          <div class="row">
            <div class="col-md-12">
              <input class="form-control datepicker" style="width:30%; display:inline-block; margin-right:5px;" value="{{ \Carbon\Carbon::now()->subDays(10)->format('Y-m-d') }}" id="start_date" />
              <input class="form-control datepicker" style="width:30%;  display:inline-block; margin-right:5px;"  value="{{ \Carbon\Carbon::now()->addDays(2)->format('Y-m-d') }}" id="end_date" />
              <div class="btn-group " data-toggle="buttons">
                <label class="btn btn-default active">
                  <input type="radio" value="day" name="interval" id="interval-day" autocomplete="off" checked> Day
                </label>
                <label class="btn btn-default">
                  <input type="radio" value="week" name="interval" id="interval-week" autocomplete="off"> Week
                </label>
                <label class="btn btn-default">
                  <input type="radio" value="month" name="interval" id="interval-month" autocomplete="off"> Month
                </label>
              </div>
              <button  id="btn-get-statistics" class="btn btn-primary">SUBMIT</button>
              <span clas="preloader-statistics" style="display:none;">Loading...</span>
            </div>
          </div>
        </div>


        <div id="user-ratings-graph"></div>
        <div class="row">
          <div class="col-md-4">
            <div class="well text-center">
              <h3 id="range-reviews"></h3>              
              <small>REVIEWS/RATINGS WITHIN DATES</small>
            </div>
          </div>
          <div class="col-md-4">
            <div class="well text-center">
              <h3 id="total-reviews"></h3>              
              <small>TOTAL REVIEWS/RATINGS</small>
            </div>

          </div>
          <div class="col-md-4">
            <div class="well text-center">
              <h3 id="avg-reviews"></h3>              
              <small>AVG. REVIEWS/RATINGS</small>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(function(){
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var chart = new Morris.Area({
    element: 'user-ratings-graph',
    xkey: 'dates',
    pointSize: 0,
    hideHover: true,
    lineWidth: 2,
    fillOpacity: 0.2,
    gridTextSize: 10,
    yLabelFormat: function(y) { y = Math.ceil(y); return y.toString(); },
    ykeys: ['reviews', 'ratings'],
    labels: ['Reviews', 'Ratings'],
    xLabelFormat: function (x) { 
      var date = new Date(x);
      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();      
      return day + ' ' + monthNames[monthIndex] + ' ' + year; 
    },
    dateFormat: function (x) { 
      var date = new Date(x);
      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();      
      return day + ' ' + monthNames[monthIndex] + ' ' + year; 
    }
  });

  $('.datepicker').datepicker({ format: "yyyy-mm-dd", autoclose: true });

  var startEndDates = {
    start_date: $('#start_date').val(),
    end_date : $('#end_date').val(),
    interval: $("input[name=interval]:checked").val()
  };


var getStartEndDates = function(){
  return startEndDates;
};

 function getLiveStatistics(){
    $.ajax({
      data:{start_date: $('#start_date').val(),
            end_date : $('#end_date').val(),
            interval: $("input[name=interval]:checked").val()
          },
      url:"{{ route('admin.reviews.data') }}"
    }).done(function(result){

      $('#avg-reviews').html(result['avg_count']);
      $('#range-reviews').html(result['range_count']);
      $('#total-reviews').html(result['total_count']);
      chart.setData(result['data']);

    });
  };

  getLiveStatistics();

  $('#btn-get-statistics').click(function(){
    startEndDates['start_date'] =  $('#start_date').val();
    startEndDates['end_date'] =  $('#end_date').val();
    getLiveStatistics();
  });

});
</script>
@stop