@extends('layouts.admin-users')
@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-2">
    <h1>Create Account</h1>

    @if ($errors->any())
    	<div class="alert alert-danger">
    	    <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </ul>
    	</div>
    @endif
  </div>
</div>
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.accounts.store'))->encodingType('multipart/form-data') }}
  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  <fieldset><legend>Basic Details</legend>
    {{ BootForm::text('Full name', 'name')->placeholder('Full Name') }}
    {{ BootForm::textarea('About', 'about')->placeholder('A bit about you') }}
    {{ BootForm::select('Gender', 'gender', array('male'=>'Male', 'female'=>'Female', ''=>'Rather not say')) }}
    {{ BootForm::email('Email', 'email')->placeholder('Email address') }}
    {{ BootForm::password('Password', 'password')->placeholder('Password') }}
    {{ BootForm::password('Confirm Password', 'password_confirmation')->placeholder('password confirmation') }}
  </fieldset>
<?php
  $u = Auth::user();
  $u = User::find($u->id);
  if($u->canAccessRoles()):
?>
  <fieldset><legend>Roles</legend>
    @foreach(Role::all() as $role)
      @if(!in_array($role->id, User::$non_selectable_roles))
        {{ BootForm::checkbox($role->name, 'roles[]')->value($role->id) }}
      @endif
    
    @endforeach
  </fieldset>
  <fieldset><legend>Zones</legend>
    @foreach(MazkaraHelper::getCitiesList() as $city)
      {{ BootForm::checkbox($city['name'], 'zones[]')->value($city['id']) }}
    @endforeach
  </fieldset>
<?php endif;?>
  {{ BootForm::token() }}
  {{ BootForm::submit('Create') }}
{{ BootForm::close() }}
@stop