        @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
          </div>
        @endif

@if(isset($merchant))
  {{ Form::model($merchant, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.merchants.update', $merchant->id))) }}
  <div class="form-group">
      {{ Form::label('global_id', 'Global ID:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('global_id', Input::old('global_id'), array('class'=>'form-control', 'placeholder'=>'Global ID')) }}
      </div>
  </div>

@else
  {{ Form::open(array('route' => 'admin.merchants.store', 'class' => 'form-horizontal')) }}
@endif


  <div class="form-group">
      {{ Form::label('name', 'Entity Name:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
      </div>
  </div>

  <div class="form-group">
    {{ Form::label('email', 'Email Address:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('phone', 'Phone:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('phone', Input::old('phone'), array('class'=>'form-control', 'placeholder'=>'Phone')) }}
    </div>
  </div>

  <div class="form-group">
      {{ Form::label('users', 'Assigned Users(select):', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('users-search', '', array('class'=>'form-control', 'id'=>'user-search', 'placeholder'=>'Type to search for users to allocate')) }}
        <p></p>
        <ul class="list-group" id="users-holder">
          @if(isset($merchant))
            @foreach($merchant->users as $user)
              @include('administration.merchants.partials.user', ['user'=>$user])
            @endforeach
          @endif
        </ul>
      </div>
  </div>

  <div class="form-group">
      {{ Form::label('businesses', 'Assigned Venues:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::text('businesses-search', '', array('class'=>'form-control', 'id'=>'business-search', 'placeholder'=>'Type to search for users to allocate')) }}
        <p></p>
        <ul class="list-group" id="businesses-holder">
          @if(isset($merchant))
            @foreach($merchant->businesses as $business)
              @include('administration.merchants.partials.business', ['business'=>$business])
            @endforeach
          @endif
        </ul>
      </div>
  </div>

  <div class="form-group">
      {{ Form::label('sales_admins', 'Sales POC:', array('class'=>'col-md-2 control-label')) }}
      <div class="col-sm-10">
        {{ Form::select('sales_admins[]', User::select()->byPocs()->get()->lists('selectable_full_name', 'id')->all(), isset($merchant)?$merchant->pocId():'', array('class'=>'form-control', 'id'=>'business-search', 'placeholder'=>'Type to search for users to allocate')) }}
      </div>
  </div>
  <div class="form-group">
    {{ Form::label('tan', 'TAN:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('tan', Input::old('tan'), array('class'=>'form-control', 'placeholder'=>'TAN')) }}
    </div>
  </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($merchant))
        {{ link_to_route('admin.merchants.show', 'Cancel', $merchant->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>
</div>

{{ Form::close() }}
<script type="text/javascript">
$(function(){
  $('#user-search').autocomplete({
    source:'/crm/merchants/get-potential-clients',
    select: function(event, ui){
      $('#users-holder').append(ui.item.html);
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( '<div >' + item.id + ' - ' +item.name + '<small>('+item.email+')</small></div>')
      .appendTo( ul );
  };
  $('#business-search').autocomplete({
    source:'/crm/merchants/get-clients-outlets',
    select: function(event, ui){
      $('#businesses-holder').append(ui.item.html);
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( '<div >' + item.id + ' - ' +item.name + '<small>('+item.zone_cache+')</small></div>' )
      .appendTo( ul );
  };  

  $(document).on('click', '.deletable-link', function(){
    $(this).prevAll('.deletable').first().attr('checked', true);
    $(this).parents('li').first().css('backgroundColor', 'red').fadeOut( "slow", function() {
      $(this).remove();
    });
  });


});
</script>