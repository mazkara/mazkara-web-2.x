<li class="list-group-item">
  {{ $user['name'] }}<small>{{ $user['email'] }}</small>{{Form::hidden('users[]', $user['id'])}}
  <span class="pull-right">
    {{Form::checkbox('deletableUsers[]', $user['id'], '', ['class'=>'deletable','style'=>'display:none'] )}}<a href="javascript:void(0)" class="deletable-link"><i class="fa fa-times"></i></a>
  </span> 
</li>