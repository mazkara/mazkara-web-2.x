@extends('layouts.admin-finance')
@section('content')

<div class="row"><div class="col-md-12">
<h1>Global Merchants
<div class="pull-right">
  <a href="{{ route('admin.merchants.export.global')}}" class="btn btn-info">Export Merchants</a>
</div>
</h1>
@if (count($all_merchants)>0)
	<table class="table table-condensed table-striped">
		<thead>
			<tr>
        <th>Global ID</th>
        <th>Local ID</th>
				<th>Name</th>
        <th>TAN</th>
        <th>Phone</th>
        <th>Email</th>
				<th>Location</th>
        <th>Products</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($all_merchants as $merchant)
				<tr>
          <td>{{ $merchant['global_id'] }}</td>
          <td>{{ $merchant['id'] }}</td>
          <td>{{ $merchant['name'] }}</td>
          <td>{{ $merchant['tan'] }}</td>
          <td>{{ str_replace(',', '<br/>', $merchant['phone']) }}</td>
          <td>{{ str_replace(',', '<br/>', $merchant['email']) }}</td>
					<td>{{{ $merchant['city_name'] }}}</td>
					<td>
            @if($merchant['is_bigreach'] == true)
              <span class="label label-primary">BIGREACH</span>
            @endif
            @if($merchant['is_fabogo'] == true)
              <span class="label label-info">FABOGO</span>
            @endif
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no merchants
@endif
</div></div>
@stop