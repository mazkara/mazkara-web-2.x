@extends('layouts.admin-content')
@section('content')

<h1>All Jobs</h1>

<p>{{ link_to_route('admin.jobs.create', 'Add New Job', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($jobs->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Body</th>
				<th>Location</th>
				<th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($jobs as $job)
				<tr>
					<td>{{{ $job->title }}}</td>
					<td>{{{ $job->body }}}</td>
					<td>{{{ $job->location }}}</td>
					<td>{{{ $job->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.jobs.destroy', $job->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.jobs.edit', 'Edit', array($job->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no jobs
@endif

@stop
