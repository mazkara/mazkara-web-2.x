@extends('layouts.admin-crm')
@section('content')
<h1>Show Ad_set</h1>

<p>{{ link_to_route('admin.ad_sets.index', 'Return to All ad_sets', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Campaign_id</th>
				<th>Ad_zone_id</th>
				<th>Slot</th>
				<th>Ad_id</th>
				<th>Status</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $ad_set->campaign_id }}}</td>
			<td>{{{ $ad_set->ad_zone_id }}}</td>
			<td>{{{ $ad_set->slot }}}</td>
			<td>{{{ $ad_set->ad_id }}}</td>
			<td>{{{ $ad_set->status }}}</td>
      <td>
        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.ad_sets.destroy', $ad_set->id))) }}
          {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        {{ link_to_route('admin.ad_sets.edit', 'Edit', array($ad_set->id), array('class' => 'btn btn-info')) }}
      </td>
		</tr>
	</tbody>
</table>
@stop