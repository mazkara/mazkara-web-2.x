<?php $ad_zone_id = $adset->ad_zone_id > 0 ? $adset->ad_zone_id : Ad_zone::first()->id ;?> 
<div id="adset-form">{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->put()->action(route('admin.ad_sets.update', $adset->id)) }}
<div class="row">
  <div class="col-md-10">
    {{ BootForm::bind($adset)}}
    {{ BootForm::select('Ad Zone', 'ad_zone_id', Ad_zone::all()->lists('title', 'id')->all())
                        ->setAttribute('class', 'form-control select-ad-zone')
                        ->setAttribute('data-campaign', $adset->campaign_id )
                        ->setAttribute('data-adset', $adset->id ) }}
    {{ BootForm::select('Slot', 'slot', 
                        Ad_slot::getAvailableSlots($ad_zone_id, $adset->id, $adset->campaign_id))
                        ->setAttribute('class', 'form-control select-ad-slot') }}
    {{ BootForm::token() }}
  </div>
</div>
{{ BootForm::close() }}</div>