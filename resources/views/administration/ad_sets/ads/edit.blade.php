@extends('layouts.admin-crm')
@section('content')
<h2>Campaign #{{{ $campaign->id }}}: {{{ $campaign->title }}}</h2>
<p class="lead">{{ $campaign->caption }}</p>
<b>{{$campaign->starts}} to {{$campaign->ends}}</b>
<hr />

<div id="adset-holder">
  <table class="table ">
    <thead>
      <tr>
        <th>Category</th>
        <th>Business Zone</th>
        <th>Slot</th>
        <th>Creative(s)</th>
      </tr>
    </thead>
    <tbody>
  @include('administration.campaigns.partials.adset')
  </tbody>
  </table>
</div><div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Ad</h1>

    </div>
</div>
@include('administration.ad_sets.ads.form')
{{ link_to_route('admin.campaigns.show', 'Cancel', $campaign->id, array('class' => 'btn btn-lg btn-default')) }}
@stop