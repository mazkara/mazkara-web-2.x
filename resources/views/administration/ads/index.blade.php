@extends('layouts.admin-crm')
@section('content')
<h1>All Ads</h1>

<p>{{ link_to_route('admin.ads.create', 'Add New Ad', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($ads->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>AD</th>
				<th>Campaign/Business Zone/Slot</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($ads as $ad)
				<tr>
					<td class="col-md-4">
					<a href="{{ $ad->url }}"><div class="native-ad">
					    <div class="post-img-content">
					        @if($ad->photo)
					        <img src="{{ $ad->photo->image->url('large')}}" class="img-responsive" />
					        @endif
					        <span class="post-title"><span>{{$ad->title}}</span></span>
					    </div>
					    <div class="content" style="margin-bottom:0px;">
					        <div class="author">
					            {{$ad->caption}}
					        </div>
					    </div>
					</div></a>
					</td>
					<td>
						@if($ad->ad_set && $ad->ad_set->campaign)

							<div class="pull-right">
                <i>Started on: {{$ad->ad_set->campaign->starts}}</i><br/>
								<i>Ends on: {{$ad->ad_set->campaign->ends}}</i>
							</div>
            	<p>{{ link_to_route('admin.campaigns.show', 'Campaign#'.$ad->ad_set->campaign_id, array($ad->ad_set->campaign_id)) }}
            		&nbsp;-&nbsp;
            		<small><a href="{{ MazkaraHelper::slugCityZone($ad->ad_set->business_zone->zones()->first(), ['category'=>[$ad->ad_set->category_id]]) }}" target="_blank">[View Live]</a></small>
            	</p>
							<p><b>Business Zone:</b> {{ $ad->ad_set->business_zone->name }}</p>
							<p><b>Category:</b> {{ $ad->ad_set->category->name }}</p>
							<p><b>Slot:</b> {{ $ad->ad_set->slot }}&nbsp;&nbsp;<b>Ad ID.</b>{{ $ad->id }}</p>
            @else
            	<i>Campaign Not found</i>
            @endif
					</td>
          <td>
            {{ link_to_route('admin.ads.edit', 'Edit', array($ad->id), array('class' => 'btn btn-xs btn-info')) }}
            &nbsp;
            {{ Form::open(array('style' => 'display: inline-block;', 'onclick'=>'return confirm(\'Delete!?!?!?Are you sure?\');', 'method' => 'DELETE', 'route' => array('admin.ads.destroy', $ad->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
            {{ Form::close() }}

          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $ads->render() }}
  <div class="pull-right">
    {{ count($ads) }} / {{ $ads->total() }} entries
  </div></div>
</div>

@else
	There are no ads
@endif
@stop