@if(!isset($ad))
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
          ->post()
          ->action(route('admin.ads.store'))->encodingType('multipart/form-data') }}
@else
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
        ->put()
        ->action(route('admin.ads.update', $ad->id))->encodingType('multipart/form-data') }}
  {{ BootForm::bind($ad) }}
@endif



  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}
    </div>
  @endif
  {{ BootForm::text('Title', 'title')->placeholder('Promotion Title') }}
  {{ BootForm::text('Caption', 'caption')->placeholder('Caption')->setAttribute('maxlength', 140) }}
  {{ BootForm::textarea('Url', 'url')->placeholder('Url') }}
  {{ BootForm::text('Headline', 'headline')->placeholder('Third Row')->setAttribute('maxlength', 10) }}
  {{ BootForm::text('Corner Text', 'corner_text')->placeholder('Text in Corner') }}
  {{ BootForm::select('Corner Text Color', 'corner_text_color', ['white'=>'White', 'black'=>'Black']) }}

  {{ BootForm::file('Photo', 'photo') }}
    @if(isset($ad))
      @if($ad->photo)
        <a href="{{ $ad->photo->image->url() }}" class="lightbox">
          <img src="{{ $ad->photo->image->url('thumbnail') }}" class="img-thumbnail" />
        </a>
        {{ Form::checkbox("deletablePhotos[]", $ad->photo->id, false ) }}
        Delete?
      @endif
    @endif

  {{ BootForm::token() }}

  {{ BootForm::select('Item Linked To', 'itemable_type', ['Business'=>'Business', 'Group'=>'Group/Chain', 'Url'=>'Url']) }}
  {{ BootForm::text('Item ID', 'itemable_id')->placeholder('Enter ID of Business/Chain here or leave empty/zero for Url') }}

  {{ BootForm::submit('Save') }}

{{ BootForm::close() }}

