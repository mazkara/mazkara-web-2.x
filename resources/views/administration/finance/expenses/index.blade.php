@extends('layouts.admin-finance')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="pull-right">
      <p>
        <br/><br/>
        <a href="{{ route('finance.expenses.create') }}" class="btn btn-sm btn-default">
          Add an Expense
        </a>
      </p>
    </div>
    <h1>Expenses</h1>
    <table class="table">
      <thead>
        <tr>
          <th>{{ '#' }}</th>
          <th>TITLE</th>
          <th>DESCRIPTION</th>
          <th>CURRENCY</th>
          <th>AMOUNT</th>
          <th>CITY</th>
          <th>DATED</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($expenses as $expense)
          <tr>
            <td>{{ $expense->id }}</td>
            <td>{{ $expense->title }}</td>
            <td>{{ $expense->desc }}</td>
            <td>{{ $expense->currency }}</td>
            <td>{{ $expense->amount }}</td>
            <td>{{ $expense->city->name }}</td>
            <td>{{ $expense->dated }}</td>
            <td>
              <a href="{{route('finance.expenses.edit', [$expense->id])}}" class="btn btn-xs btn-warning">EDIT</a>
              <a href="{{route('finance.expenses.delete', [$expense->id])}}" onclick="return confirm('Are you sure you want to delete this?')?confirm('Are you absolutely POSITIVELY sure you want to delete this?'):false;" class="btn btn-xs btn-danger">DELETE</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

  <div class="row">
    <div class="col-md-12">
  {{ $expenses->render() }}
  <div class="pull-right">
    {{ count($expenses) }} / {{ $expenses->total() }} entries
  </div></div>
</div>

@stop