@extends('layouts.admin-finance')
@section('content')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.js"></script>
<div class="row">
  <div class="col-md-12">
  <p>
    <div class="alert alert-{{mzk_invoice_state_label_css($invoice->state)}}">{{ucwords($invoice->state)}}</div>
  </p>
  <h2>
    {{ $invoice->title }}
    <p class="pull-left">
      <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Action <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
          @if($invoice->isCleared()==false)
            <li><a id="btn-add-bounce-charges" href="javascript:void(0)" >Add Bounce Charges</a></li>
            <li><a id="btn-add-tax" href="javascript:void(0)" >Add Tax</a></li>
            <li><a id="btn-add-credit-note" href="javascript:void(0)" >Add Credit Note</a></li>
            <li><a id="btn-add-bad-debt" href="javascript:void(0)" >Add Bad Debt</a></li>
            <li role="separator" class="divider"></li>
          @endif
          <li><a href="{{ route('admin.invoices.pdf', array($invoice->id)) }}">
            <i class="fa fa-file-pdf-o"></i>
            Download PDF
          </a></li>
          <li>
            <a href="{{ route('admin.invoices.edit', array($invoice->id)) }}">
              <i class="fa fa-pencil"></i> Edit
            </a>
          </li>
          <li>
            <a href="{{ route('admin.invoices.index') }}">
              <i class="glyphicon glyphicon-triangle-left"></i> 
              Back to Invoices
            </a>
          </li>
        </ul>
      </div>
    </p>
  </h2>
  <div class="row">
    <div class="col-md-6">
      <table class="table">
        <tr>
          <th>MERCHANT</th><td><a href="{{ route('admin.merchants.show', [$invoice->merchant_id]) }}">{{ $invoice->merchant->name }}</a></td>
        </tr>
        <tr>
          <th>SALES POC</th><td>{{ $invoice->poc_name() }}</td>
        </tr>
        <tr>
          <th>DATE</th><td>{{ mzk_f_date($invoice->dated) }}</td>
        </tr>
        <tr>
          <th>START DATE</th><td>{{ mzk_f_date($invoice->start_date) }}</td>
        </tr>
        <tr>
          <th>END DATE</th><td>{{ mzk_f_date($invoice->end_date) }}</td>
        </tr>
        <tr>
          <th>CURRENCY</th><td>{{ $invoice->currency }}</td>
        </tr>
        <tr>
          <th>MAZKARA ENTITY</th><td>{{ mzk_get_entities($invoice->entity_id)['name'] }}</td>
        </tr>
        @if(count($invoice->businesses)>0)
          <tr>
            <th>MAZKARA ENTITY</th><td>
          @foreach($invoice->businesses as $business)
            {{ $business->name }}<br/>
          @endforeach
            </td>
          </tr>
        @endif
      </table>
    </div>
  </div>
  <div class="form-group">
    <div>
      <table class="table">
        <thead>
          <tr>
            <th colspan="2">DESC</th>
          </tr>
        </thead>
        <tbody id="items-here">
          @foreach($invoice->items()->onlyItems()->get() as $item)
            <tr>
              <td colspan="2">
                <a href="javascript:void(0)" class="item-inline-editable" id="desc" data-type="textarea" data-pk="{{$item->id}}" data-url="{{ route('admin.invoices.items.update') }}" data-title="Enter Description">{{ $item->desc }}</a>
              </td>
            </tr>
          @endforeach

          @foreach($invoice->items()->notItems()->get() as $notItem)
            <tr class="warning">
              <td>
                <a href="javascript:void(0)" class="item-inline-editable" id="desc" data-type="textarea" data-pk="{{$notItem->id}}" data-url="{{ route('admin.invoices.items.update') }}" data-title="Enter Description">{{ $notItem->desc }}</a>
                &nbsp;({{ ucwords(str_replace('-', ' ', $notItem->type)) }})

              </td>
              <td class="text-right">
                {{ $invoice->currency }} {{ $notItem->total }}
              </td>
            </tr>
          @endforeach

          @foreach($invoice->payments()->get() as $item)
            <tr class="{{ $item->isCleared() ? 'success' : 'warning'}}">
              <td colspan="2">
                {{ $item->desc }}{{'('.$item->type.($item->isCheque() ? ' - '.$item->state : '' ).')' }}</td>
            </tr>
          @endforeach
          @foreach($invoice->credit_notes()->get() as $item)
            <tr class="danger">
              <td colspan="2">
                {{ $item->desc.'('.$item->type.')' }}
                &nbsp;&nbsp;
                (START DATE: 
                  <a href="javascript:void(0)" class="item-inline-editable" id="start_date" data-type="date" data-pk="{{$item->id}}" data-url="{{ route('admin.credit_notes.updatable') }}" data-title="Edit Start Date">{{ $item->start_date}}</a>)
                &nbsp;&nbsp;
                (END DATE: 
                  <a href="javascript:void(0)" class="item-inline-editable" id="end_date" data-type="date" data-pk="{{$item->id}}" data-url="{{ route('admin.credit_notes.updatable') }}" data-title="Edit End Date">{{ $item->end_date}}</a>)

                &nbsp;&nbsp;

                <a href="{{ route('admin.credit_notes.pdf', array($item->id)) }}" class="btn btn-xs btn-warning">
                  <i class="fa fa-file-pdf-o"></i>
                  Download PDF
                </a>

              </td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          @foreach($invoice->taxes as $tax)
            <tr>
              <td colspan="1" align="right" style="font-size:90%">{{ $tax->name }}({{$tax->percentage.'%'}})</td>
              <td colspan="1" align="right"  style="font-size:90%" id="total-invoice">{{ $invoice->currency }} {{ $tax->amount }}</td>
            </tr>
          @endforeach
          <tr>
            <td colspan="1" align="right"><h5>Pending</h5></td>
            <td colspan="1" align="right" id="total-invoice"><h5>{{ $invoice->currency }} {{ (isset($invoice) ? $invoice->amount_due.' / '.$invoice->total_amount : '') }}</h5></td>
          </tr>
        <?php $has_taxes = count($invoice->taxes)>0?true:false;?>
        @if($has_taxes == true)
          <tr>
            <td colspan="1" align="right"><h5 class="font-size:90%">Total(Excluding Taxes)</h5></td>
            <td colspan="1" align="right" id="total-invoice"><h5 class="font-size:90%">{{ $invoice->currency }} {{ (isset($invoice) ? $invoice->amount : '') }}</h5></td>
          </tr>
          @endif
          <tr>
            <td colspan="1" align="right"><h5 class="font-size:90%">Total<?php echo ($has_taxes == true?'(Including Taxes)':'');?></h5></td>
            <td colspan="1" align="right" id="total-invoice"><h5 class="font-size:90%">{{ $invoice->currency }} {{ (isset($invoice) ? $invoice->total_amount : '') }}</h5></td>
          </tr>
        </tfoot>
      </table>
      <div class="">
        <table class="table">
          <tr>
            <td><b>Current Month Contribution:</b> {{ $invoice->currency }} {{ $invoice->currentMonthContribution()}}</td>
            <td><b>Monthly Average:</b> {{ $invoice->currency }} {{ $invoice->monthlyAverage()}}</td>
            <td><b>Contract Value:</b> {{ $invoice->currency }} {{ $invoice->contractValue()}}</td>
          </tr>
        </table>
      </div>
    </div>
  </div>




<div class="row">
  <div class="col-md-6">
    <div class="panel panel-warning">
      <div class="panel-heading">
        <h3 class="panel-title pull-left">Attachments</h3>
        <a id="btn-add-attachment" href="javascript:void(0)" class="btn btn-xs btn-warning pull-right "><i class="fa fa-paperclip"></i> Attach</a>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
      @if(count($invoice->attachments)>0)
        @foreach($invoice->attachments as $attachment)
          <p><a href="{{ $attachment->image->url() }}"><i class="fa fa-download"></i> {{ $attachment->name }} - {{ $attachment->image_file_name }}</a></p>
        @endforeach
      @endif
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title ">Tags</h3>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
<style type="text/css">
.selectize-input{
  min-width:300px !important;
}
</style>

         @include('administration.tags.machine', [ 'obj'=>$invoice,
                                                  'taggable_type'=>'Invoice',
                                                  'taggable_id'=>$invoice->id]) 



      </div>
    </div>
  </div>

        @include('administration.tags.machine-code')

</div>

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h3 class="panel-title ">Payments</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <form class="form-inline" onsubmit="return false;">
                      <label>SEARCH PAYMENT TO ALLOCATE</label><br/>
                      <input class="form-control" id="search-payment" placeholder="Enter Payment ID" name="search-payment" />&nbsp;
                      <button class="btn form-control btn-sm btn-default" id="btn-search-payment">SEARCH</button>
                    </form>
                  <form action="{{ route('admin.invoice.payments.allocate', [$invoice->id]) }}" id="frm-allocate-payments">
                    <div id="payment-holder" style="margin:10px 0px 10px 0px;"></div>
                    <div class="row">
                      <div class="col-md-6">
                        <b>Amount</b>
                      </div>
                      <div class="col-md-6">
                        <b>Applicable Period</b>
                      </div>
                    </div>
                    <hr/>
                  <?php
                  $start    = (new DateTime($invoice->start_date))->modify('first day of this month');
                  $end      = (new DateTime($invoice->end_date))->modify('first day of next month');
                  $interval = DateInterval::createFromDateString('1 month');
                  $period   = new DatePeriod($start, $interval, $end);
                  $months = [];
                  foreach ($period as $ii=>$dt) {
                    $months[$ii] = $dt->format('m');
                  }

                  foreach ($period as $ii=>$dt) {
                    $start_date = $dt->format("Y-m-1");
                    $end_date = $dt->format("Y-m-t");
                    if($ii==0){
                      $start_date = date('Y-m-d', strtotime($invoice->start_date));
                    }

                    if($months[(count($months)-1)] == $dt->format('m')){
                      $end_date = date('Y-m-d', strtotime($invoice->end_date));
                    }
                    ?>
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="applied_amount form-control" name="amount[{{$ii}}]"  />
                          <br/><input type="hidden" name="start_date[{{$ii}}]"  value="{{ $start_date }}"  />
                          <br/><input type="hidden" name="end_date[{{$ii}}]" value="{{ $end_date }}"  />
                        </div>
                        <div class="col-md-6">
                          {{ $start_date }}
                        to 
                          {{ $end_date }}
                        </div>

                      </div>
                      <?php
                  }
                  ?>
                  <p><a href="javascript:void(0)" id="btn-submit-allocate-payment" class="btn btn-default">Allocate</a></p>
                  </div>
                  </form>
                </div>

              </div>
              <div class="col-md-6">
                <div class="panel panel-info">
                  <div class="panel-heading">
                    <h3 class="panel-title ">Applied Payments</h3>
                  </div>
                  <div class="panel-body">
                    <p>
                      <small>Below is a list of all payments applied and the distribution of how much has been applied to each cycle/period.</small>
                    </p>
                    <table class="table">
                      <tr>
                        <th>Payment</th>
                        <th>Amount </th>
                        <th colspan="2"> Period</th>
                      </tr>

                    @foreach($invoice->payment_applicables as $payment_applicable)
                      <tr>
                        <td>{{ $payment_applicable->id }} Payment {{ '#'.$payment_applicable->payment_id }}</td>
                        <td>{{ $payment_applicable->currency }} {{ $payment_applicable->amount }}</td>
                        <td>{{ mzk_f_date($payment_applicable->start_date) }}&nbsp;to&nbsp;{{ mzk_f_date($payment_applicable->end_date) }}</td>
                        <td><a href="{{ route('admin.payment_applicables.delete', [$payment_applicable->id]) }}" onclick="return confirm('Are you sure you want to delete this payment applicable?')">Delete</a></td>
                      </tr>
                    @endforeach
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('administration.payments.partials.payment_handlebars')

<div id="form-credit-note" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.credit_notes.store'))->encodingType('multipart/form-data') }}
  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Start date', 'start_date')->placeholder('Start date')->addClass('dateinput') }}
  {{ BootForm::text('End date', 'end_date')->placeholder('End date')->addClass('dateinput') }}

  {{ BootForm::text('Amount', 'amount')->placeholder('Amount')->defaultValue($invoice->amount_due) }}
  {{ BootForm::textarea('Details', 'desc')->placeholder('Details/Description') }}
  {{ Form::hidden('invoice_id', $invoice->id) }}
  {{ Form::hidden('type', 'credit-note') }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>
<div id="form-bad-debt" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.credit_notes.store'))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Start date', 'start_date')->placeholder('Start date')->addClass('dateinput') }}
  {{ BootForm::text('End date', 'end_date')->placeholder('End date')->addClass('dateinput') }}

  {{ BootForm::text('Amount', 'amount')->placeholder('Amount') }}
  {{ BootForm::textarea('Details', 'desc')->placeholder('Details/Description') }}
  {{ Form::hidden('invoice_id', $invoice->id) }}
  {{ Form::hidden('type', 'bad-debt') }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>
<div id="form-tax" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.invoices.save.tax'))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif

  {{ BootForm::text('Name', 'name')->placeholder('Tax name? Service Tax?') }}
  {{ BootForm::text('Percentage', 'percentage')->placeholder('Percentage') }}
  {{ Form::hidden('invoice_id', $invoice->id) }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>

<div id="form-bounce-charges" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.invoices.save.bounce-charges'))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif

  {{ BootForm::text('Description', 'desc')->placeholder('Details? Cheque number, amount etc...') }}
  {{ BootForm::text('Amount', 'amount')->placeholder('Penalty Charges') }}
  {{ Form::hidden('invoice_id', $invoice->id) }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>

<div id="form-attachment" class="hidden">
{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.invoices.attach'))->encodingType('multipart/form-data') }}

  @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif

  {{ BootForm::text('Label', 'label')->placeholder('What are you attaching here?') }}
  {{ Form::file('attachments[]', array('id'=>'input-bulk-image-upload', 'multiple'=>'true')) }}
  {{ Form::hidden('invoice_id', $invoice->id) }}

  {{ BootForm::token() }}
{{ BootForm::close() }}
</div>

<script type="text/javascript">
$(function(){

  $('#btn-submit-allocate-payment').click(function(){
    total_amount = 0;
    
    ip = $('#payment-holder').find('input[name=payment_id]');
    if(!ip.length>0){
      alert('No payment is selected?');return false;
    }

    $('input.applied_amount').each(function(index, cm){
      total_amount = total_amount + Number($(cm).val());
    });    

    applicable_amount = $('#payment_amount_applicable').val();

    if(applicable_amount < total_amount){
      alert('You cannot allocate '+total_amount+' when the available amount for this payment is '+applicable_amount);      
      return false;
    }

    if({{ $invoice->amount_due }} < total_amount){
      alert('You cannot allocate '+total_amount+' when the invoice has only {{ $invoice->amount_due }} due');      
      return false;
    }

    $('#frm-allocate-payments').submit();

  });

  $('#btn-search-payment').click(function(){

    $.ajax({
      url: '/finance/payments/search',
      data: { payment_id: $('#search-payment').val() }
    }).success(function(r){
      if(r==false){
        alert('No such payment exists.');
      }else{
        var source   = $("#payment-template").html();
        var template = Handlebars.compile(source);

        var html    = template(r.payment);
        $('#payment-holder').html(html);

        if(r.payment.currency != '{{$invoice->currency}}'){
          alert('You cannot allocate this payment as the currency for both are different!');
        }
      }
    });

    return false;

  });

  validateBounceCharges = function(){
    desc = $('.bootbox-body #desc').first().val();
    amount = $('.bootbox-body #amount').first().val();
    
    if(desc == ''){
      alert('Description is mandatory')
      return false;
    }

    if(amount == ''){
      alert('Amount cannot be negative or zero!');
      return false;
    }
    
    $('.bootbox-body form').first().submit();

  };

  validateTax = function(){
    vl = $('.bootbox-body #percentage').first().val();
    if(vl > 99){
      alert('Thats a lot of tax')
      return false;
    }
    if(vl<=0){
      alert('Amount cannot be negative or zero!');
      return false;
    }
    
    $('.bootbox-body form').first().submit();
  };

  $('#btn-add-bounce-charges').click(function(){
    html = $('#form-bounce-charges').html();

    bootbox.dialog({
      title: "Add Bounce charges",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: validateBounceCharges

          }
        }
      
    });
  });

  $('#btn-add-tax').click(function(){
    html = $('#form-tax').html();

    bootbox.dialog({
      title: "Add Tax",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: validateTax

          }
        }
      
    });
  });

  $('#btn-add-attachment').click(function(){
    html = $('#form-attachment').html();

    bootbox.dialog({
      title: "Attach a File",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: function(){
              $('.bootbox-body form').first().submit(); 
            }
          }
        }
      
    });
  });


  validateCrediteNote = function(){
    vl = $('.bootbox-body #amount').first().val();
    if(vl > {{$invoice->amount_due}}){
      alert('Amount cannot be more than the invoice due amount of {{$invoice->amount_due}}')
      return false;
    }
    if(vl<=0){
      alert('Amount cannot be negative or zero!');
      return false;
    }

    if(vl > {{ $invoice->amount_due }}){
      alert('Maximum amount that can be credited is {{ $invoice->amount_due }}');
      return false;
    }
    
    $('.bootbox-body form').first().submit();
  };

  $('#btn-add-bad-debt').click(function(){
    html = $('#form-bad-debt').html();

    bootbox.dialog({
      title: "Add a Bad Debt",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: validateCrediteNote
          }
        }
      
    });

    $('.bootbox-body input.dateinput').datepicker({ dateFormat: "yyyy-mm-dd", autoclose:true });

  });

  $('.item-inline-editable').editable();

  $('#btn-add-credit-note').click(function(){
    html = $('#form-credit-note').html();
    bootbox.dialog({
      title: "Add a Credit Note",
      message: html,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: validateCrediteNote
          }
        }
    });

    $('.bootbox-body input.dateinput').datepicker({ dateFormat: "yyyy-mm-dd", format: "yyyy-mm-dd", autoclose:true });

  });

});
</script>

@stop
