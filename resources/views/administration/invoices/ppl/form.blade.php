@if(isset($invoice))
  {{ Form::model($invoice, array( 'class' => 'form-horizontal', 'method' => 'POST', 
                                  'route' => array('admin.invoices.ppl.update', $invoice->id))) }}
  <h3>{{ $invoice->title }}</h3>
@else
  {{ Form::open(array('route' => 'admin.invoices.ppl.store', 'class' => 'form-horizontal')) }}
@endif

  <div id="form-holder">
  <div class="form-group">
    {{ Form::label('merchant_id', 'Merchant:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('merchant_id', Merchant::orderby('name', 'asc')->lists('name', 'id')->all(), Input::old('merchant_id'), array('id'=>'merchant-id-selector', 'class'=>'form-control')) }}
    </div>
  </div>
  <div id="businesses-for-merchant-holder" class="form-group">
    @if(isset($invoice))
      @include('administration.merchants.partials.business-for-invoice-form')
    @endif
  </div>

  <div class="form-group">
    {{ Form::label('user_id', 'Sales POC:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('user_id', User::select()->byPocs()->get()->lists('selectable_full_name', 'id')->all(), Input::old('user_id'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('city_id', 'City:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('city_id', Zone::query()->cities()->get()->lists('name', 'id')->all(), Input::old('city_id'), array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('entity_id', 'Mazkara Entity:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('entity_id', mzk_get_entities_as_array(), Input::old('entity_id'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('currency', 'Currency:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('currency', Payment::getCurrencies(), Input::old('currency'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('dated', 'Dated:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('dated', Input::old('dated'), array('class'=>' form-control', 'placeholder'=>'Dated')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('start_dated', 'Start Date:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('start_date', Input::old('start_date'), array('class'=>' form-control', 'placeholder'=>'Start Date')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('end_date', 'End Date:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('end_date', Input::old('end_date'), array('class'=>' form-control', 'placeholder'=>'End Date')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('unit_price', 'Call Unit Price:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 ">
      {{ Form::text('unit_price', Input::old('unit_price'), array('class'=>' form-control', 'placeholder'=>'Dated')) }}
    </div>
  </div>

  <div class="row">
  <div class="col-md-4 col-md-offset-2">
    <a href="javascript:void(0)" class="btn dpb btn-warning" id="lnk-get-call-logs-basic">GET CALL LOGS</a>
  </div>
</div>
  <div class="row">
  <div class="col-md-8 col-md-offset-2">

    <div id="call-logs-holder" class="form-group">
      @if(isset($invoice))
        <?php 

        $call_logs = $invoice->call_logs()->get();
        $data = $invoice->items()->first()->toArray();
        $data = is_array($data) ? $data : ['price'=>0];
        $data['currency'] = $invoice->currency;
        $data['unit_price'] = $data['price'];

        ?>
        @include('administration.invoices.ppl.call-logs-list')
      @endif
    </div>
  </div>
  </div>

</div>

<div class="form-group submit-holder hidden">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

<script type="text/javascript">
$(function(){
  $('.dateinput input').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yyyy-mm-dd", autoclose:true });

  $('#merchant-id-selector').change(function(){
    $.ajax({
      url: '{{ route('admin.merchants.call_logs.businesses') }}', 
      type:'GET',
      data:{ merchant_id: $(this).val()}
    }).done(function(data){
      $('#businesses-for-merchant-holder').html(data['html']);
    });
  });


  $('#lnk-get-call-logs-basic').click(function(){
    texts = $('#form-holder').find('input:text');
    selects = $('#form-holder').find('select');
    checks = $('#businesses-for-merchant-holder').find('input[name="outlets[]"]:checked');
    params = {};

    $(texts).each(function(i, v){
      name = $(texts[i]).attr('name');
      if(name!=undefined){
        params[name] = $(texts[i]).val();
      }
    });

    $(selects).each(function(i, v){
      name = $(selects[i]).attr('name');
      if(name!=undefined){
        params[name] = $(selects[i]).val();
      }
    });

    params['outlets'] = [];

    $(checks).each(function(i, v){
      params['outlets'].push($(checks[i]).val());
    });


    
    $.ajax({
      url: '{{ route('admin.invoices.get.call_logs') }}', 
      type: 'GET',
      data: params
    }).done(function(data){
      $('#call-logs-holder').html(data['html']);
      $('.submit-holder').removeClass('hidden');
    });
  });



});
</script>
