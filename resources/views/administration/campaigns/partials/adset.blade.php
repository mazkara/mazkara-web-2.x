<tr id="adset-{{$adset->id}}" >
  <td>

      <div class="pull-right">
        <i>Started on: {{$adset->campaign->starts}}</i><br/>
        <i>Ends on: {{$adset->campaign->ends}}</i>
      </div>
      <p><a href="{{ MazkaraHelper::slugCityZone($adset->business_zone->zones()->first(), ['category'=>[$adset->category_id]]) }}" target="_blank">[View Live]</a>
      </p>
      <p><b>Business Zone:</b> {{ $adset->business_zone->name }}</p>
      <p><b>Category:</b> {{ $adset->category->name }}</p>
      <p><b>Slot:</b> {{ $adset->slot }}</p>
  </td>

  <td style="width:300px;">
    @foreach($adset->ads()->get() as $ad)
    <b>Ad ID.</b>{{ $ad->id }}
      <a href="{{ $ad->url }}">
        <div class="native-ad">
          <div class="post-img-content">
            @if($ad->photo)
            <img src="{{ $ad->photo->image->url('medium')}}" class="img-responsive" />
            @endif
            @if(strlen($ad->corner_text)>0)
              <span class="post-corner"><span style="color:{{$ad->corner_text_color}}">{{$ad->corner_text}}</span></span>
            @endif
            <span class="post-title"><span>{{$ad->title}}</span></span>

          </div>
          <div class="content">
            <div class="author">{{$ad->caption}}
            <span class="pull-right show-on-hover">
              <a href="{{ route('admin.adsets.edit.ad', $ad->id) }}" class="btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
              <a href="{{ route('admin.adsets.delete.ad', $ad->id) }}" class="btn-xs btn-danger"><i class="fa fa-times"></i></a>
            </span>
            </div>
          </div>
        </div>
      </a>

    @endforeach
    @if($adset->ads()->count() < 3)
      <a href="{{ route('admin.adsets.new.ad', $adset->id) }}" class="btn btn-info " data-adsetID="{{$adset->id}}" >Add Creative</a>
    @endif
  </td>
  <td>
    {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 
                        'route' => array('admin.ad_sets.destroy', $adset->id), 
                        'onsubmit'=>"return confirm('Are you sure?')")) }}
      {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
    {{ Form::close() }}
  </td>
</tr>

