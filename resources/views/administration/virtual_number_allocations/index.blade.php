@extends('layouts.admin-crm')
@section('content')

<h1>All Virtual_number_allocations</h1>

<p>{{ link_to_route('admin.virtual_number_allocations.create', 'Add New Virtual_number_allocation', null, array('class' => 'btn btn-lg btn-success')) }}</p>



<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text(  'byID', Input::get('byID'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;',
                    'placeholder'=>'By Business ID')) }}

    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}
    {{ Form::text(  'bySlipID', Input::get('bySlipID'), array('class'=>'form-control col-md-3', 'style'=>'width:200px;',
                    'placeholder'=>'By Slip ID')) }}
    {{ Form::text(  'byLotID', Input::get('byLotID'), array('class'=>'form-control col-md-3', 'style'=>'width:200px;',
                    'placeholder'=>'By Lot ID')) }}
    
    {{ Form::select('sort', ([ ''=>'Sort by?', 
                              'idAsc'=>'ID Ascending', 
                              'idDesc'=>'ID Descending', 
                              'nameAsc'=>'Name Ascending', 
                              'nameDesc'=>'Name Descending', 
                              'lastUpdateAsc'=>'Last Update Ascending',
                              'lastUpdateDesc'=>'Last Update Descending',
                              ]),
                           Input::get('sort'), array('class'=>'form-control', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>


@if ($businesses->count())
  <div class="list-group">
		@foreach ($businesses as $business)
      <a href="{{ route('admin.businesses.virtual_numbers.show', array($business->id) ) }}" class="list-group-item">
        <div class="pull-right">
          <small>Last Updated {{{ Date::parse($business->updated_at)->ago() }}}</small>
        </div>
        <h4 class="list-group-item-heading">
          {{{ $business->id }}} -          
          {{{ $business->name }}}
        </h4>
        <p class="list-group-item-text">
          <b>Phone:</b>{{{ join( ',', $business->phone) }}}
          <b>Email:</b>{{{ join( ',',$business->email) }}}
          <br/>
          <b>Website:</b>{{{ $business->website }}}
          <b>Zone:</b>{{{ $business->zone_cache }}}
        </p>
      </a>
		@endforeach      
  </div>

  <div class="row">
    <div class="col-md-12">
	{{ $businesses->appends($params)->render() }}
  <div class="pull-right">
    {{ count($businesses) }} / {{ $businesses->total() }} entries
  </div></div>
</div>
@else
	There are no businesses
@endif

@stop

