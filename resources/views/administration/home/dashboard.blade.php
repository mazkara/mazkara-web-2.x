@extends('layouts.admin-content')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Dashboard</h1>
  </div>
</div>
<div class="row">
  <div class="col-lg-2">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-12 text-center">
            <div class="huge">
            {{ mzk_percentage($active_listings, $total_listings) }}
            </div>
            <div>Active<br/>Listings</div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <a href="{{ action('Administration\\BusinessesController@index', array('active'=>'active')) }}" class="btn btn-default btn-xs">Jump</a>
      </div>

    </div>
  </div>
  <div class="col-lg-2">
    <div class="panel panel-yellow">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-12 text-center">
            <div class="huge" title="{{ $rate_card_listings }}/{{ $active_listings }}">
            {{ mzk_percentage($rate_card_listings, $active_listings) }}

            </div>
            <div>Listings With<br/>Rate Cards</div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
          <a href="{{ action('Administration\\BusinessesController@index', array('rate'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
      </div>
    </div>
  </div>

    <div class="col-lg-2">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="huge">
                          {{ mzk_percentage($photo_listings, $active_listings) }}

                        </div>
                        
                        <div>Listings With<br/>Photos</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="{{ action('Administration\\BusinessesController@index', array('photos'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
            </div>
        </div>
    </div>


    <div class="col-lg-2">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="huge">
                          {{ mzk_percentage($map_listings, $active_listings) }}

                        </div>
                        <div>Listings With<br/> Coordinates</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="{{ action('Administration\\BusinessesController@index', array('map'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
            </div>
        </div>
    </div>

    <div class="col-lg-2">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="huge">
                          {{ mzk_percentage($no_map_listings, $active_listings) }}

                        </div>
                        <div>Listings Without<br/> Coordinates</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="{{ action('Administration\\BusinessesController@index', array('map'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
            </div>
        </div>
    </div>

    <div class="col-lg-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="huge">
                          {{ mzk_percentage($businesses_with_packages, $active_listings) }}

                        </div>
                        <div>Listings With<br/>Packages</div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="{{ action('Administration\\BusinessesController@index', array('packages'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
            </div>

        </div>
    </div>

</div>
<div class="row">
  <div class="col-lg-6">

    <table class="table">
      <thead>
        <th colspan="3">
          Categories
        </th>
      </thead>
    @foreach($categories as $category)
      <tr>
        <td>{{ $category['count'] }}</td>
        <td> {{ $category['name'] }}</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('category'=>$category['id'])) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>

    @endforeach

    </table>
    <table class="table">
      <thead>
        <th colspan="3">
          Rate Card Statistics
        </th>
      </thead>
      <tr>
        <td>{{ $no_rate_card_listings }}</td>
        <td> Listings Without Rate Cards</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('rate'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $rate_card_listings }}</td>
        <td> Listings With Rate Cards</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('rate'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
    </table>
      
    <table class="table">
      <thead>
        <th colspan="3">
          Photo Statistics
        </th>
      </thead>
      <tr>
        <td>{{ $photo_listings }}</td>
        <td> Listings With Photos</td>
        <td>
                <a href="{{ action('Administration\\BusinessesController@index', array('photos'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $no_photo_listings }}</td>
        <td> Listings Without Photos</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('photos'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
    </table>


  </div>

  <div class="col-lg-6">
    <table class="table">
      <thead>
        <th colspan="3">
          Location Statistics
        </th>
      </thead>
      <tr>
        <td>{{ $no_map_listings }}</td>
        <td> Listings Without Location</td>
        <td>
            <a href="{{ action('Administration\\BusinessesController@index', array('map'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $map_listings }}</td>
        <td> Listings With Location</td>
        <td>
            <a href="{{ action('Administration\\BusinessesController@index', array('map'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
    </table>
    
    <table class="table">
      <thead>
        <th colspan="3">
          DPP Statistics
        </th>
      </thead>
      @foreach($businesses_with_deals as $ii=>$vv)
      <tr>
        <td>{{ $vv['count'] }}</td>
        <td> Listings With {{ $ii }}</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index').$vv['url'] }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      @endforeach
    </table>

    <table class="table">
      <thead>
        <th colspan="3">
          Other Statistics
        </th>
      </thead>
      <tr>
        <td>{{ $has_services_listings }}</td>
        <td> Listings with Services</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('has_services'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $has_no_services_listings }}</td>
        <td> Listings without Services</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('has_services'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $has_highlights_listings }}</td>
        <td> Listings with Highlight</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('has_highlights'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $has_no_highlights_listings }}</td>
        <td> Listings without Highlight</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('has_highlights'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $has_categories_listings }}</td>
        <td> Listings with Categories</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('has_categories'=>'true')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>
      <tr>
        <td>{{ $has_no_categories_listings }}</td>
        <td> Listings without Categories</td>
        <td>
          <a href="{{ action('Administration\\BusinessesController@index', array('has_categories'=>'false')) }}" class="btn btn-default btn-xs">Jump</a>
        </td>
      </tr>    </table>

  </div>

</div>
@endsection