@extends('layouts.page')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

<h1 class="pt20 pb20 text-center notransform">
   Terms and Conditions of Use
</h1>

<h3>
  1. Terms
</h3>

<p>
  By accessing this web site, you are agreeing to be bound by these 
  web site Terms and Conditions of Use, all applicable laws and regulations, 
  and agree that you are responsible for compliance with any applicable local 
  laws. If you do not agree with any of these terms, you are prohibited from 
  using or accessing this site. The materials contained in this web site are 
  protected by applicable copyright and trade mark law.
</p>

<h3>
  2. Use License
</h3>

<ol type="a" class="pl15">
  <li>
    Permission is granted to temporarily download one copy of the materials 
    (information or software) on Fabogo's web site for personal, 
    non-commercial transitory viewing only. This is the grant of a license, 
    not a transfer of title, and under this license you may not:
    
    <ol type="i" class="pl15">
      <li>modify or copy the materials;</li>
      <li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
      <li>attempt to decompile or reverse engineer any software contained on Fabogo's web site;</li>
      <li>remove any copyright or other proprietary notations from the materials; or</li>
      <li>transfer the materials to another person or "mirror" the materials on any other server.</li>
    </ol>
  </li>
  <li>
    This license shall automatically terminate if you violate any of these restrictions and may be terminated by Fabogo at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.
  </li>
</ol>

<h3>
  3. Disclaimer
</h3>

<ol type="a" class="pl15">
  <li>
    The materials on Fabogo's web site are provided "as is". Fabogo makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Fabogo does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.
  </li>
</ol>

<h3>
  4. Limitations
</h3>

<p>
  In no event shall Fabogo or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on Fabogo's Internet site, even if Fabogo or a Fabogo authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
</p>
      
<h3>
  5. Revisions and Errata
</h3>

<p>
  The materials appearing on Fabogo's web site could include technical, typographical, or photographic errors. Fabogo does not warrant that any of the materials on its web site are accurate, complete, or current. Fabogo may make changes to the materials contained on its web site at any time without notice. Fabogo does not, however, make any commitment to update the materials.
</p>


<h3>7. Items/ Services Pricing</h3>

<p>Transactions, Transaction Price and all commercial terms such as Delivery, Dispatch of services are as per 
per contractual obligations between customers and service providers. Fabogo acts as the facilitator between the service provider and customer for procurement of available services, offers and deals.</p>

<p>Usage of Mazkaras payment and booking facility shall not render
Fabogo liable or responsible for the non-receipt, breach of contract as regards the products and
 / or services of service providers as listed on Fabogo.</p>

<p>www.fabogo.com will NOT deal or provide any services or products to any of OFAC sanctions countries in compliance with the law of UAE.</p>

<h3 class="fs125">7.1. Price Range</h3>

<p>At Fabogo we have customised pricing according to the services rendered by us. The details are provided to you beforehand according to the efficiency and the output of the service. Typically the range of transactions on our website varies from INR 100 to 10,000. (This pricing policy is applicable only in India)</p>

<h3 class="fs125">7.2. Shopping Cart</h3>

<p>Items in your Shopping Cart reflect the current price displayed on the item's product details page. Please note: This price may differ from the price displayed when the item was first placed in your Shopping Cart.</p>

<h3>8. Delivery Policy</h3>
<p>Services booked would be availed at the venue of the service provider or at a mutually agreeable venue between the customer and the service provider.</p>

<p>Deals and offers would be availaed as electronic downloads accessible immediately to users upon successful purchase of an online deliverable.</p>

<h3>9. Refund/Return Policy</h3>

<p>
  <b>For Services from service providers: </b><br/>
  Returns are handled on a case-by-case basis. Any issues regarding unsatisfactory delivery of services from a service provider would be considered within a week of availing the service and would be subject to amicably resolving any issues between the customer and the service provider.
</p>
<p>
  <b>For Electonic downloads:</b><br/>
  Offers and Vouchers cannot be exchanged, refunded or returned after the customer has received the voucher, unless the service for the voucher has been discontinued before the agreed expiry date. 
</p>
<p>Refund will only be done through original mode of payment.</p>
<h3>
  10. Links
</h3>

<p>
  Fabogo has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Fabogo of the site. Use of any such linked web site is at the user's own risk.
</p>

<h3>11. Payments and Disputes</h3>
<p>Fabogo accepts payments online using Visa and MasterCard credit/debit card in AED (or any other currency).  </p>
<p>
</p>


<h3>
  12. Site Terms of Use Modifications
</h3>

<p>
  Fabogo may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.
</p>

<h3>
  13. Governing Law
</h3>

<p>
  Any claim relating to Fabogo's web site shall be governed by the laws of the State of United Arab Emirates without regard to its conflict of law provisions.
</p>


<p>United Arab of Emirates is your Fabogo's country of domicile.</p> 

      
<h3>Contacting Us</h3>

<p>If there are any questions regarding this TOS you may contact us using the information below.<p>

www.fabogo.com<br/>
Creative City<br/>
Fujairah<br/>
PO.Box: 4422<br/>
United Arab Emirates<br/>
hello@fabogo.com
</div>
    </div>
</div>

@stop
