<!DOCTYPE html>
<html lang="en" ng-app="mazkaraApp">
  <head>
    @include('elements.head')
  </head>
  <body>
    @include('elements.post-body')
    <div id="fakeloader"></div>
    @include('elements.nav')
    <div class="container p0 bg-lite-gray no-bg-on-mobile">
      <div id="tf-home" class="text-center" style="">
        <div class="banner-head-content content" >
          <div class="banner-content-form" style="">
            <div class="row">
              <div class="col-md-12 ">
                <div class="show-only-mobile h45"></div>
                <h1 class=" mb0 hide-only-mobile  fw900 np-transform notransform   fs220 dark-teal text-left " style="margin-top:150px;line-height:1.1;padding-left:40px;">
                  <span class="fs220 lato ">Celebrate Fabulous</span>
                </h1>
                <div class="fs150 pl0-mobile  mt0  hide-only-mobile center-on-mobile text-left dark-teal" style="padding-left:40px;margin-bottom:20px;">
                  <b>Find the best salons, spas, fitness & cosmetic centres in {{ ucwords(strtolower(str_replace('-',' ',MazkaraHelper::getLocale()))) }}</b>
                </div>
                <div class="banner-head" style="background-color:transparent;padding-left:40px;padding-right:40px;">
                  <div style="border:3px solid #CCC;background-color:#CCC;" class="border-radius-5 input-group mt5 opaque-on-hover text-left force-center">
                    <div id="search-box-home-holder" class="inner-addon left-addon" style="position:relative;">
                      <label class="glyphicon glyphicon-search" style="font-size:14px;padding:13px 10px; ;" rel="tooltip "></label>
                      {{ Form::text('category_', null, 
                                      array('class'=>'p10 no-border-size op95 
                                                      border-radius-5-mobile 
                                                      border-left-radius-5 
                                                      no-border-radius pointer-cursor 
                                                      pr15 pl15 form-control selectable', 
                                            'id'=>'search-selector',
                                            'style'=>'height:auto; border-color:#fff; text-align:left; ', 
                                            'placeholder'=>'Search for salon, spa or a service')) }}
                    <div style="width:100%;z-index:998;vertical-align:middle;text-align:center;height:120px;background-color:#fff;top:10px;position:absolute;border: 1px solid #ccc; padding-top:70px;display:none;" class="search-box-preloader bt0  border-bottom-radius-5">
                      <b>Coming right up!</b> <img src="{{ mzk_assets('assets/indicator.gif')}}" />
                    </div>
                    </div><div id="search-location-holder" class="inner-addon left-addon" style=" ">
                      <label class="glyphicon glyphicon-map-marker " style="font-size:14px;padding:13px 10px; ;"  rel="tooltip "></label>
                      {{ Form::text('zone_', MazkaraHelper::getLocaleLabel(), 
                                  array('class'=>'form-control no-border-size 
                                                  no-border-radius 
                                                  border-radius-5-mobile 
                                                  op95 pointer-cursor 
                                                  selectable pr15 pl15 p10  ', 
                                        'id'=>'location-selector', 
                                        'style'=>' height:auto; border-bottom-color:#fff;border-right-color:#fff;border-top-color:#fff;border-left-color:#EAEAEA;border-left-width:1px !important;text-align:left; ', 
                                        'placeholder'=>'Please Type a Location')) }}
                    </div>
                    <div class="input-group-btn z90">
                      <button id="search-bar-button" 
                              type="submit" 
                              style="height:auto;margin-left:-12px;font-weight:800;min-width:120px;" 
                              class="btn no-border-size border-radius-5-mobile no-border-radius pl0 border-right-radius-5 op95 btn-turquoise p10">SEARCH</button></div>
                  </div>
                </div>
              <form id="search-bar-form" class="hide-only-mobile navbar-left navbar-form" action="/search/businesses"  style="display:none;" method="POST">
                {{ Form::hidden('zone[]', MazkaraHelper::getLocaleId(), array('class'=>'form-control selectable', 'id'=>'location-selector-name'))}}
                {{ Form::hidden('search', null, array('class'=>'', 'id'=>'search-selector-name'))}}
                {{ Form::hidden('category[]', '', array('class'=>'form-control selectable', 'id'=>'location-selector-category'))}}
                {{ Form::hidden('service[]', '', array('class'=>'form-control selectable', 'id'=>'location-selector-service'))}}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
        <div id="page-full-overlay"></div>
    <!--<div id="tf-home-text-overlay" class="text-center  " style="">
    </div>-->
    <div class="hide-only-mobile bg-white pt10"></div>

    <div class="row show-only-mobile bg-white pt10">
      <div class="col-md-12 pr0-mobile pl0-mobile text-left">
        <span class="fw900 fs110 pl15 pt0  mb10 show-only-mobile"><span class="pl15">{{ strtoupper('ALL SERVICES') }}</span></span>
       </div>
    </div>



    <div class="container bg-white show-only-mobile services-accordian p0">
      <?php $services = MazkaraHelper::getServicesHierarchyList(); ;?>
      <div class="mb10 panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        @foreach($services as $parent)
        <?php 
          $parent['business_count'] = 0;
          $html = '';
          ob_start();
        ?>
        <div id="collapse{{$parent['id']}}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{$parent['id']}}">
          <div class="panel-body p0">
            <ul class="list-group">
              @foreach($parent['children'] as $child)
                <li class="list-group-item">
                  <a class="fs80 dark-gray" href="{{ MazkaraHelper::slugCity(null, ['service'=>[$child['id']]]) }}">
                    {{ $child['name'] }}
                    <span class="pull-right gray fs75 " style="padding-top:2px;"><i class="fa fa-chevron-right"></i></span>
                  </a>
                </li>
                <?php $parent['business_count']+=$child['business_count'];?>
              @endforeach
            </ul>
          </div>
        </div>
        <?php 

        $html = ob_get_contents();
        ob_end_clean();?>

          <div class="panel panel-default mt0">
            <div class="panel-heading p5" role="tab" id="heading{{$parent['id']}}">
              <h4 class="panel-title">
                <a class="dpb notransform" data-toggle="collapse" role="button" data-parent="#accordion" href="#collapse{{$parent['id']}}" aria-expanded="true" aria-controls="collapse{{$parent['id']}}">
                  <span class="pull-left fs110"><i class="pr10 glyph-icon iconz-{{$parent['slug']}}"></i></span>
                  <span class="">{{$parent['name']}} </span>
                  <span class="dpib pull-right notransform">
                    <span class="dpib pr10 medium-gray fs75">
                      {{$parent['business_count']}} venues
                    </span>
                  <i class="indicator glyphicon fs90 fa fa-chevron-down"></i>
                  </span>
                </a>
              </h4>
            </div>
            {{$html}}

          </div>
        @endforeach
      </div>
    </div>

    @yield('content')
    </div>
    @include('elements.footer')
    <script type="text/javascript">
$(function(){
function toggleChevron(e) {
  $(e.target)
      .prev('.panel-heading')
      .find("i.indicator")
      .toggleClass('fa-chevron-down fa-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
})
    </script>
  </body>
</html>