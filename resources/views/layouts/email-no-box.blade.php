<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Fabogo</title>
  <style type="text/css">

@media screen and (max-width: 600px) {
    table[class="container"] {
        width: 95% !important;
    }
}

  #outlook a {padding:0;}
    body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
    img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
    a img {border:none;}
    .image_fix {display:block;}
    p {margin: 1em 0;}
    h1, h2, h3, h4, h5, h6 {color: black !important;}

    h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}

    h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
      color: red !important; 
     }

    h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
      color: purple !important; 
    }

    table td {border-collapse: collapse;}
    .td-post-image,
    .post-image{max-width:190px;}

    table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }

    a {color: #000;}

    @media (max-device-width: 480px) {
      .td-post-image,
      .post-image{width:100%;max-width:100% !important;}

    }
    @media (max-width: 480px) {
      .td-post-image,
      .post-image{ 
        width:100% !important; 
        max-width:480px !important;
      }
    }


    @media only screen and (max-device-width: 480px) {

      a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: black; /* or whatever your want */
            pointer-events: none;
            cursor: default;
          }
          .td-post-image,
          .post-image{width:100%;max-width:480;}

      .td-post-image,
      .post-image{width:100%;max-width:480 !important;}

      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
            text-decoration: default;
            color: orange !important; /* or whatever your want */
            pointer-events: auto;
            cursor: default;
          }
    }


    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
      a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: blue; /* or whatever your want */
            pointer-events: none;
            cursor: default;
          }
          .td-post-image,
          .post-image{max-width:190px;}

      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
            text-decoration: default;
            color: orange !important;
            pointer-events: auto;
            cursor: default;
          }
    }

    @media only screen and (-webkit-min-device-pixel-ratio: 2) {
      /* Put your iPhone 4g styles in here */
    }

    @media only screen and (-webkit-device-pixel-ratio:.75){
      /* Put CSS for low density (ldpi) Android layouts in here */
    }
    @media only screen and (-webkit-device-pixel-ratio:1){
      /* Put CSS for medium density (mdpi) Android layouts in here */
    }
    @media only screen and (-webkit-device-pixel-ratio:1.5){
      /* Put CSS for high density (hdpi) Android layouts in here */
    }
    /* end Android targeting */
    h2{
      color:#181818;
      font-family:Helvetica, Arial, sans-serif;
      font-size:22px;
      line-height: 22px;
      font-weight: normal;
    }
    a.link1{

    }
    a.link2{
      color:#fff;
      text-decoration:none;
      font-family:Helvetica, Arial, sans-serif;
      font-size:16px;
      color:#fff;border-radius:4px;
    }
    p{
      color:#555;
      font-family:Helvetica, Arial, sans-serif;
      font-size:16px;
      line-height:160%;
    }
  </style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"fff",
    "color":"555555",
    "bgItem":"ffffff",
    "title":"181818"
  }
</script>

</head>
<body style="background-color:#F3F5F0;">
  <!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
  <table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class='bgBody'>
  <tr>
    <td>
  <table cellpadding="0" style="max-width:600px" class="container" align="center" cellspacing="0" border="0">
  <tr>
    <td style="background-color:#FFFFFF;">
    <!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
    

    <table cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:600px" class="container">
      <tr>
        <td class='movableContentContainer bgItem'>

          <div class='movableContent' style="background-color:#ffffff;">
            <table cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:600px" class="container">
              <tr height="10">
                <td width="10"></td>
                <td ></td>
              </tr>
              <tr>
                <td width="10"></td>
                <td valign="top" align="center">
                  <div class="contentEditableContainer contentImageEditable">
                    <div class="contentEditable" >
                        <img src="https://s3.amazonaws.com/mazkaracdn/assets/fabogo-logo.png" alt='Logo'   />
                    </div>
                  </div>
                </td>
              </tr>
              <tr height="10">
                <td width="10"></td>
                <td ></td>
              </tr>
            </table>
          </div>
          <div style="max-width:600px">
            <div style="text-center:center">
              @yield('content')
            </div>
          </div>
          <div class='movableContent' style="background-color:#e6e6e6;">
            <table cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:600px" class="container" style="background-color:#E3E3E3;">
              <tr>
                <td width="100%" colspan="2" align="center" style="padding-top:15px;">
                  
             
              <a style="text-decoration:none;" href="https://play.google.com/store/apps/details?id=com.mazkara.user" target="_blank">
                <img height="45" src="https://s3.amazonaws.com/mazkaracdn/assets/home/google-store.png">
              </a>
              <a style="text-decoration:none;" href="https://itunes.apple.com/us/app/id1086643190" target="_blank">
                <img height="45" src="https://s3.amazonaws.com/mazkaracdn/assets/home/apple-store.png">
              </a>
              </td>
              </tr>
              <tr>
                <td height="70" colspan="2" valign="middle" align="center" style="padding:10px;">
                  <div class="contentEditableContainer contentTextEditable">
                    <div class="contentEditable" align="center" >
                      <table border="0" cellspacing="0" cellpadding="0" align='center'>
                        <tr>
                          <td valign="top"  width='55'>
                            <div class="contentEditableContainer contentFacebookEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://www.facebook.com/gofabogo" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/facebook.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='facebook' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://www.linkedin.com/company/fabogo" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/linkedin.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='linkedin' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://instagram.com/gofabogo?" data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/instagram.jpg" data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='instagram' style='margin-right:40x;'>
                                </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentTwitterEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a href="https://twitter.com/gofabogo"data-default="placeholder"  style="text-decoration:none;">
                                <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/twitter.jpg"  data-default="placeholder" data-max-width='40' data-customIcon="true" width='40' height='40' alt='twitter' style='margin-right:40x;'>
                              </a>
                              </div>
                            </div>
                          </td>
                          <td valign="top" width='55'>
                            <div class="contentEditableContainer contentImageEditable" style='display:inline;'>
                              <div class="contentEditable" >
                                <a target='_blank' href="https://plus.google.com/+Fabogo" data-default="placeholder"  style="text-decoration:none;">
                                  <img src="https://s3.amazonaws.com/mazkaracdn/assets/email/google.jpg"  width="40" height="40" data-max-width="40" alt='Google plus' style='margin-right:0px;' />
                                </a>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </table>
                      <p>
                      <span style="font-size:11px;color:#555;font-family:Helvetica, Arial, sans-serif;line-height:200%;">&copy; Mazkara fz llc</span>
                      </p>



                    </div>
                  </div>
                </td>
                <td width="40%" height="70" align="right" valign="top" align='right' style="padding-bottom:20px;">
                </td>
              </tr>
              <tr>
                <td colspan="2" style="padding:10px;">
                  <center>
                  <p style="font-size:10px; padding:10px; margin:10px;">
                    <i>
                      This newsletter was sent to [Email] from www.fabogo.com because you subscribed. Rather not receive our newsletter anymore? [unsubscribe] instantly.
                    </i>
                  </p></center>

                </td>
              </tr>
            </table>

          </div>


        </td>
      </tr>
    </table>

    
    

  </td></tr></table>
    </td>
  </tr>
  </table>
  <!-- End of wrapper table -->



</body>
</html>