@extends('layouts.page')
@section('content')
<div id="tf-home" class="text-center" style="position:absolute; height:350px;top:45px;width:100%;z-index:0;background:url({{ mzk_assets('assets/careers-2.jpg') }}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)">

</div>
    <div class="text-center" style="height:345px;">
      <div class="content " >
<div class="container " style="margin-top:40px;">
        <div class="fs150 mt25 text-right pull-right text-shadow" style="width:400px;">
        <span class="force-white">Fabogo is a glam-tech startup aiming to be the </span>
        <span class="yellow">world's leading online</span><span class="force-white"> engagement platform for 
        the personal grooming industry.</span>
        </div>
      </div></div></div>






<div class="container">
    <div class="row">
        <div class="col-md-8 bg-white">

            <h1 class="text-left notransform">{{$job->title}}</h1>
            <p class="lead text-left">
              <b>Location: {{$cities[$job->location]}}</b>
            </p>
            <div class=" fw300 fs125 dark-gray pb10 html-fix">
                {{$job->body}}
            </div>
  <style type="text/css">
    label{float:none !important; text-align: left !important; padding-bottom:3px !important;}
  </style>


</div>
<div class="col-md-4 ">
    <div class="well">



      {{ BootForm::openHorizontal(['sm' => [12,12], 'lg' => [12,12] ])->post()->action('/apply')->encodingType('multipart/form-data') }}
        @if (Session::get('error'))
          <div class="alert alert-error alert-danger">
            {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
          </div>
        @endif
        @if (Session::get('notice'))
          <div class="alert alert-success">
            {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
          </div>
        @endif


        {{ BootForm::text('Full name*', 'name')->placeholder('Full Name')->required() }}
        {{ Form::hidden('job_id', $job->id) }}
        {{ BootForm::email('Email*', 'email')->placeholder('Email address')->required() }}
        {{ BootForm::text('Phone*', 'phone')->placeholder('Phone Number')->required() }}
        {{ BootForm::textarea('3 reasons why you want to work for Fabogo?', 'message')->placeholder('We\'re curious ;)')->required() }}
        {{ BootForm::file('Attach CV', 'cv')->required() }}
        <div class="form-group">
          <label for="message" class="col-lg-3 control-label"> </label>
            {{ Form::submit('Apply Now', array('class' => 'btn  btn-turquoise')) }}
        </div>

      {{ BootForm::close() }}
    </div>
</div>
</div></div>
<script type="text/javascript">
</script>

@stop
