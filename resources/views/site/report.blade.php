{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post('/report')->encodingType('multipart/form-data') }}

  {{ BootForm::text('Full name', 'name')->placeholder('Full Name')->required() }}
  {{ BootForm::email('Email', 'email')->placeholder('Email address')->required() }}
  {{ BootForm::textarea('Message', 'message')->placeholder('What is wrong')->required() }}
  {{ BootForm::hidden('url', '')->value( Request::url() ) }}

  <div class="form-group">
    <label for="message" class="col-lg-3 control-label"> </label>
    <div class="col-lg-8">
        {{ Form::submit('Send', array('class' => 'btn  btn-primary')) }}
  </div></div>
      
  {{ BootForm::close() }}
