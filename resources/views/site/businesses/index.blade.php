@extends(isset($layout)?$layout:'layouts.master-open')
@section('content')

<div class="row bg-white">
  <div class="col-md-12 ">
    <div class="no-text-shadow  fs90 hide-only-mobile">{{ $breadcrumbs->render() }}</div>
      <div class="row">
        <div class="col-md-9 col-xs-9">
          <div class="p10 show-only-mobile"></div>
          <h1 class=" item-heading pb10 mt0 mb5">
            {{$title}} 
            <small class=" fw500" >
              <a id="lnk-to-change-location" href="javascript:void(0)" class="hide-only-mobile hover-underline fire">CHANGE LOCATION</a>
            </small>
          </h1>
        </div>
        <div class="col-md-3  col-xs-3">
          <p class="pt15 show-only-mobile"></p>
            <div class="text-right gray">({{ $businesses->total() }}&nbsp;venue{{ $businesses->total()==1 ? '':'s' }})</div>

        </div>
      </div>
    <div class="p5 show-only-mobile"></div>
  </div>
</div>
<div class="row bt">
  <div class="row-height">
  <div class="col-md-9  col-height ">
    <div class=" -inside-full-height">
      <div class=" fake-load-it bg-white bb " style="margin-left:-15px; margin-right:-20px;" >
        @include('site.businesses.partials.filter.categories-horizontal')
      </div>
      <div class="row">
        <?php ob_start();?>
          @include('site.businesses.partials.filter.highlights-vertical')


        <?php $filter_html = ob_get_contents(); ob_end_clean();?>
        <div class="hide-only-mobile ">
          <div class="col-md-3 pr10 pl10 br filter-column fake-load-it li-pl0 ">
            <div class="p15 pt5 mt5 pb5 bg-white  ___border-radius-3 mb10">
  {{ $filter_html }}

            </div>
          </div>

        
        </div>
        <div class="show-only-mobile mt_5 p0 pt0 pb0">
          <div class="navbar-header show-only-mobile ba  bg-white ba  mb0">
            <div class="dpb">
              <button data-target="#vertical-navbar-filter" data-toggle="collapse" class="navbar-toggle p0 mb0 collapsed mb0" type="button">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-filter "></i>
              </button>
              <span class="visible-xs navbar-brand p10 " style="height:auto;"><small>Filters</small></span>
            </div>
          </div>
        </div>
        <div class="show-only-mobile">
          <div id="vertical-navbar-filter" style="display:none">
          <div class="col-md-3 filter-column fake-load-it li-pl0 ">
            <div class="p15 pt5 pb5 bg-white  border-radius-3 mb10">
            <!--
            @include('site.businesses.partials.filter.zones')
            -->
            {{ $filter_html }}
            </div>
          </div>

          </div>
        </div>
        <div class="col-md-9 pl0-mobile mt0 pl10 pr10 pr0-mobile li-pl0 ">
          <div class="show-on-load p15 pb0 pt5" style="" >
            <div class="show-only-mobile">
              @if(count($ads)>0)
                <small class=" pb10 ">SPONSORED</small>
                <div class="bxslider">
                  @foreach($ads as $ad)
                      @include('site.businesses.partials.ad-single')
                  @endforeach
                </div>
              @endif
            </div>
          </div>
          @if(count($businesses)>0)
            <div class="list-group fake-load-it">
              <div class="   listing">
                @foreach($businesses as $business)
                <?php $business->is_favourited = in_array($business->id, $favorites) ? true : false;?>
                  @include('site.businesses.partials.single', ['business'=>$business, 'prices'=>$prices, 'services'=>(isset($params['service']) ? $params['service'] : []) ])
                @endforeach
              </div>
            </div>
            <div class="clearfix hide-only-mobile fake-load-it">
              @if($businesses->lastPage() > 1)
                <div class=" pull-left">
                  {{ $businesses->currentPage() }} of
                  {{ $businesses->lastPage() }} pages
                </div>
                <div class=" pull-right">
                  <?php // params to undo
                  unset($params['category']);unset($params['service']);unset($params['city']);
                  ?>
                  <div class=" ">
                    {{ $businesses->appends($params)->render() }}        
                  </div>
                </div>

              @endif
              <div class=" clearfix p10"></div>
              @include('site.businesses.partials.addendum-listings')
              <div class=" clearfix p10"></div>
            </div>

            <div class="show-only-mobile fake-load-it clearfix w100pc pt0 pb10 mb10 p15">
              @include('elements.pagination.mobile', ['paginator'=>$businesses])
            </div>


          @else
            <div class=" mt60 mb20">
              <div class="text-center"><img src="{{mzk_assets('assets/no-results.png')}}"/>
              <div class="p10 mt20">
                <p><b>NO RESULTS FOUND</b></p>
                <p><a class="yellow" href="{{MazkaraHelper::slugCity()}}"><b>Browse all results in {{ ucwords(MazkaraHelper::getLocaleLabel()) }}</b></a></p>
              </div>
            </div>
              @if(isset($suggestions) && (count($suggestions)>0))
                <div class="p10 pt0">
                  <p><b>SUGGESTED VENUES AROUND LOCATION</b></p>
                  <div class="list-group mt20">
                    <ol class=" fake-load-it  list-unstyled listing">
                      @foreach($suggestions as $business)
                      <?php $business->is_favourited = in_array($business->id, $favorites) ? true : false;?>
                        @include('site.businesses.partials.single', ['business'=>$business, 'prices'=>$prices, 'services'=>(isset($params['service']) ? $params['service'] : []) ])
                      @endforeach
                    </ol>
                  </div>
                </div>
              @endif
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 bg-lite-gray-3 hide-only-mobile col-height pt10">
    <div class=" -inside-full-height ">
      @include('elements.coming-soon')
      @include('site.businesses.partials.ad-list')
    </div>
  </div>
</div>
</div>

@section('js')

<script type="text/javascript">
$(function(){
  $('.bxslider').bxSlider({ slideMargin:20, pager:false, onSliderLoad:function(){
   $('.show-on-load').slideDown( "slow");
  }});

//  .slick({
//    slidesToShow: 1,
//    variableWidth: false,
//    infinite: true,
//    nextArrow: '<i class="fa gray fa-chevron-right slicker-next absolute" style="cursor:pointer;top:35%;"></i>',
//    prevArrow: '<i class="fa gray fa-chevron-left slicker-prev  absolute" style="cursor:pointer;top:35%;"></i>'
//  });
  
  $('.toggl-services').click(function(){
    h = $(this).find('.toggl-hide').first();
    console.log(h);

    if($(h).hasClass('hidden')){ // if we are showing all then hide em
      $('#service-list li.hideable').addClass('hidden');
      $(this).find('.toggl-hide').removeClass('hidden');
      $(this).find('.toggl-show').addClass('hidden');
      console.log(1);
    }else{
      $('#service-list li.hideable').removeClass('hidden');
      $(this).find('.toggl-hide').addClass('hidden');
      $(this).find('.toggl-show').removeClass('hidden');
      console.log(2);
    }
  });


  $('.mobile-category-filter').slick({
    slidesToShow: 3,
    infinite: false,
    arrows: false
  });
  $('.mobile-category-filter').removeClass('hidden');

  function setupPopoversForMobileAndDesk(){
    $(".popover-mobile").popover({
      container: 'body',
      html: true,
      content: function () {
        p = $(this).data('phone').split('|');
        html = '';
        for(i in p){
          html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
        }
        return html;
      }
    }).click(function(e) {
        e.preventDefault();
    });

    $(".popover-desk").popover({

        container: 'body',
        html: true,
        content: function () {
          p = $(this).data('phone').split('|');
          html = '';
          for(i in p){
            html = html + '<div><a class="btn btn-default dpb text-center" href="javascript:void(0)">'+p[i]+'</a></div>';
          }
          return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });
  }

  setupPopoversForMobileAndDesk();

  $('.call-button').click(function(){
    dta = { business_id: $(this).data('business') };
    $.ajax({
      url:'/businesses/increment/calls/count', method:'GET', data: dta
    });
    ga('send', 'event', 'click on call button', ($(this).hasClass('popover-desk')?'desktop':'mobile'), '{{ Request::url() }}');
    
    fbq('trackCustom', 'CallToClick', {
      business_id: dta.business_id
    });

  });

  $('.single-native-ctrable').click(function(){
    dta = { ad_id: $(this).data('ad') };
    $.ajax({
      url:'/ads/increment/ctr', method:'GET', data: dta
    });
  });

  $('.package-card-holder').each(function(iv, elem){
    $(elem).magnificPopup({
      delegate: 'a.image', 
      type: 'image',
      gallery: {
        enabled: true
      }
    });
  });

  $('.star-rating-single').rating();//{ readOnly: true, cancel: false, half: true,

  /*$('.filter').focus(function(){
    rel = $(this).attr('rel');
    $('#'+rel).slideDown();
  });

  $('.filter').blur(function(){
    rel = $(this).attr('rel');
    $('#'+rel).slideUp();
  });*/

  $('.filter').focus(function(){
    rel = $(this).attr('rel');
    $('#'+rel).find('.showable').removeClass('hidden');//slideDown();
  });

//  $('.filter').blur(function(){
//    rel = $(this).attr('rel');
//    $('#'+rel).find('li').addClass('hidden');
//  });

  $('#zones-filter-holder').mouseleave(function(){
    f = $('#zones-filter-holder').find('.filter').first();
    rel = $(f).attr('rel');
    $('#'+rel).find('li').addClass('hidden');
    $(f).blur();
  });

  
  
  $('.filter').keyup(function(event){
    var ul = $('#' + $(this).attr('rel'));
    if($(this).val() == ''){
      lis = $(ul).find('li');
      $(lis).addClass('hidden');
      lis = lis.slice(0, 15);
      $(lis).removeClass('hidden');
      return;
    }
            
    var search = $(this).val();
    var rg = new RegExp('('+$.unique(search.split(" ")).join("|")+')',"i");
    $(ul).find('li a').each(function(){
      c = $(ul).find('li.hidden');
      /*if(c.length < 5)*/
      {
        if($.trim($(this).text()).search(rg) == -1) {
          $(this).parent().addClass('hidden');
        } else {
          if(c.length > 15){
            $(this).parent().removeClass('hidden');
          }
        }        
      }
    });
  })
})
</script>
@stop
@endsection
