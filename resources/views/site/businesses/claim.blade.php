@extends('layouts.parallax')

@section('content')
@include('site.businesses.partials.cover-or-parallax-desktop-mobile', ['business'=>$business, 'phones'=>$business->phones])
<div class="container">
<div class="row">
  <div class="col-md-9">
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-4">

        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
        @include('site.businesses.partials.show.categories', ['business'=>$business])
        @include('site.businesses.partials.show.services', ['business'=>$business])
      </div>
      <div class="col-md-8">
        <h2 class="text-center fw300 notransform lnh">Is this your business?</h2>
        <p class="lead text-center fs125">Send us your contact details, a customer support executive would contact you for verification after which
        we'll give you complimentary access to managing your company profile on Fabogo.</p>

        {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->encodingType('multipart/form-data') }}

         @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
              {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
            </div>
          @endif
          @if (Session::get('notice'))
            <div class="alert alert-success">
              {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
            </div>
          @endif
          {{ BootForm::text('Full name', 'name')->placeholder('Your Name')->required() }}
          {{ BootForm::text('Phone', 'phone')->placeholder('Phone Number')->required() }}
          {{ BootForm::email('Email', 'email')->placeholder('Email address')->required() }}
          {{ BootForm::hidden('business_id', '')->value( $business->id) }}
          {{ BootForm::token() }}
          {{ BootForm::submit('Claim your company') }}
          {{ BootForm::close() }}

      </div>
    </div>
  </div>
  <div class="col-md-3">
    <small class=" pb10 mb5">FEATURED</small>
    @foreach($suggested_spas as $native_ad)
      @include('site.businesses.partials.business-niblet', ['business'=>$native_ad])
    @endforeach

  </div>
</div>
</div>
<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))

@stop
@endsection