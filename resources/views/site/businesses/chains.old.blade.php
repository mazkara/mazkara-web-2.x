@extends(isset($layout)?$layout:'layouts.parallax-with-search')

@section('content')
<?php mzk_timer_start('inside index- view');?>
<img src="{{mzk_cloudfront_image($chain->getCoverUrl('xlarge'))}}" width="100%" />

  <div class="container">
<div class="row">
  <div class="col-md-12 pt20">
    <div class="no-text-shadow hide-only-mobile">{{ $breadcrumbs->render() }}</div>

      <div id="sortable-niblet hide-only-mobile" class="pull-right">
        <span class="hide-only-mobile">Sort By</span>
        <?php 
          $prms = $params;
          unset($prms['category']);
          unset($prms['service']);
          unset($prms['city']);
          $prms['sort'] = 'popularity';
        ?>
        <a class="btn hide-only-mobile no-border-radius btn-xs btn-default {{ $params['sort']=='popularity'?'bg-dark-gray':''}}" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}" rel="nofollow">Popularity</a>
        <?php $prms['sort'] = 'rating';//unset($prms['sort']);// = 'rating';?>
        <a class="btn btn-xs no-border-radius hide-only-mobile btn-default {{ $params['sort']=='rating'?'bg-dark-gray':''}}" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}" rel="nofollow">Ratings</a>
        <?php $prms['sort'] = 'name';//unset($prms['sort']);// = 'rating';?>
        <a class="btn hide-only-mobile no-border-radius btn-xs btn-default {{ $params['sort']=='name'?'bg-dark-gray':''}}" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}" rel="nofollow">Name</a>
      </div>
    <h1 class=" item-heading pb10 mb5">{{$title}} <small>({{ $businesses->total() }} venue{{ $businesses->total()==1 ? '':'s' }})</small></h1>
    <div class="p5 show-only-mobile"></div>
    @if(isset($secondary_title) && !empty($secondary_title)&&(1==2))
    <p class="center-on-mobile hide-only-mobile">{{$secondary_title}}  </p>
    @endif
    @if(1==2)
    
    <p class="center-on-mobile hide-only-mobile">
      <small>
        @if(isset($nearby_businesses) && ($nearby_businesses!=false))
          {{ $businesses->total() - $nearby_businesses->total() }} {{$what}} in {{ $where}}
          @if($nearby_businesses->total()>0)
            and {{$nearby_businesses->total() }} from surrounding areas
          @endif
        @else
          {{ $businesses->total() }} {{ $what }} in {{ $where }}
        @endif

        @if(isset($sub_zone))
          @if(isset($params['nearby']))
          <?php 
          $prms = $params;
          unset($prms['nearby']);?>
          <a rel="nofollow" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">(Include surrounding areas)</a>
          @else
          <?php 
          $prms = $params;
          $prms['nearby']='false';?>
          <a rel="nofollow" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">(Exclude surrounding areas)</a>
          @endif
        @endif

      </small>
    </p>
    @endif
    <hr class="hide-only-mobile mt0 mb0"/>
  </div>
</div>
<div class="row">
  <div class="col-md-9  li-pl0 ">
    <div class="sidebar-filter mr5 hidden " >
      <div class="p10 pb0 dpb clearfix">
        @foreach($filter as $ii=>$vv)
          <?php 
          $afilter = $params;
          if(is_array($afilter[$ii])):
            foreach($afilter[$ii] as $ix=>$iv){
              unset($afilter[$ii][$ix]);
              ?>
                <a rel="nofollow" class="btn-xs btn btn-default border-radius-15" href="{{ URL::to(URL::current().'?'.http_build_query($afilter))}} ">{{ucwords($vv[$iv])}}
                <i class="fa fw300 fa-times"></i></a>&nbsp;
              <?php
              $afilter = $params;

            }
          ?>
          <?php
          else:
            unset($afilter[$ii]);
          ?>
            <a rel="nofollow" class="btn-xs btn btn-default border-radius-15" href="{{ URL::to(URL::current().'?'.http_build_query($afilter))}}"><b>{{ucwords($ii)}}</b> {{$vv}}
              <i class="fa fw300 fa-times"></i></a>&nbsp;
          <?php
          endif;
          ?>

      @endforeach
      </div>


        </div>

    <div class="row">
      <div class="col-md-12">
        <div class="navbar-header show-only-mobile ba">
          <div class="dpb">
          <button type="button" class="navbar-toggle collapsed mb0" data-toggle="collapse" data-target="#vertical-navbar-filter">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-filter  "></i>
          </button>
          <span class="visible-xs navbar-brand "><small>Filters</small></span>
        </div>
      </div>

        <div id="vertical-navbar-filter" class="navbar-collapse collapse p10 bg-lite-gray" role="navigation">

          @include('site.businesses.partials.filter.vertical')
        </div>
      </div>



    </div>
    <div class="list-group mt20">
      <ol class="  list-unstyled listing">
        @foreach($businesses as $business)
          @include('site.businesses.partials.single', ['business'=>$business])
        @endforeach
      </ol>
    </div>
    <div class="clearfix">
      <div class="pull-left">
        {{ $businesses->currentPage() }} of
        {{ $businesses->lastPage() }} pages
      </div>
      <div class="pull-right">
        <?php // params to undo
        unset($params['category']);unset($params['service']);unset($params['city']);
        ?>
        {{ $businesses->appends($params)->render() }}        
      </div>
    </div>

  </div>
  <div class="col-md-3  pt20">
    @include('elements.coming-soon')
    {{ trim($chain->pre_side_html) }}
    <p>
      <img width="100%" src="{{mzk_cloudfront_image($chain->getBannerUrl())}}" />
    </p>


  </div>
</div>
</div>
@section('js')

<script type="text/javascript">
$(function(){
  function setupPopoversForMobileAndDesk(){
  $(".popover-mobile").popover({
        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="tel:'+p[i]+'">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });


  $(".popover-desk").popover({

        container: 'body',
        html: true,
        content: function () {
            p = $(this).data('phone').split('|');
            html = '';
            for(i in p){
              html = html + '<div><a class="btn btn-default dpb text-center" href="javascript:void(0)">'+p[i]+'</a></div>';
            }
            return html;
        }
    }).click(function(e) {
        e.preventDefault();
    });

 
  }
setupPopoversForMobileAndDesk();

  $('.call-button').click(function(){
    console.log($(this).data('business'));
    dta = { business_id: $(this).data('business') };

    $.ajax({
              url:'/businesses/increment/calls/count',
              method:'GET',
              data: dta
            });
  })

  $('.single-native-ctrable').click(function(){

    dta = { ad_id: $(this).data('ad') };

    $.ajax({
      url:'/ads/increment/ctr', method:'GET', data: dta
    });
  })



  $('.package-card-holder').each(function(iv, elem){
    $(elem).magnificPopup({
      delegate: 'a.image', 
      type: 'image',
      gallery: {
        enabled: true
      }
    });

  })
  $('.star-rating-single').rating();//{ readOnly: true, cancel: false, half: true,

  $('.filter').focus(function(){
    rel = $(this).attr('rel');
    $('#'+rel).slideDown();
  });

  $('.filter').blur(function(){
    rel = $(this).attr('rel');
    $('#'+rel).slideUp();
  });
  
  $('.filter').keyup(function(event){

    var ul = $('#' + $(this).attr('rel'));

    if($(this).val() == ''){
      lis = $(ul).find('li');
      $(lis).addClass('hidden');

      lis = lis.slice(0, 15);

      $(lis).removeClass('hidden');
      return;
    }
            
    var search = $(this).val();
       
    var rg = new RegExp('('+$.unique(search.split(" ")).join("|")+')',"i");

    $(ul).find('li a').each(function(){
      c = $(ul).find('li.hidden');
      /*if(c.length < 5)*/
      {
        if($.trim($(this).text()).search(rg) == -1) {
            $(this).parent().addClass('hidden');
        } else {
          if(c.length > 15){
            $(this).parent().removeClass('hidden');
          }
        }        
      }
    });
  })
})
</script>
@stop
<?php mzk_timer_stop('inside index- view');?>
@endSection