@if(!isset($all_params))
<?php   return;?>
@endif
@if($businesses->currentPage() > 1 )
<?php   return;?>
@endif
@if(isset($all_params['category']) && isset($all_params['service']) )
  @if((count($all_params['service'])+count($all_params['category']))==0)
    <?php   return;?>
  @endif
@endif
@if(!isset($all_params['category']) && !isset($all_params['service']) )
  <?php   return;?>
@endif


<hr/>


<div class="b_g-white pl0  " >
  <h2 class="item-headers fs125 gray  pt0 mt0">
    Related searches leading to this page
  </h2>
<?php 
  $urls = [];
  $c = 'salons';
  if(isset($all_params['category']) && count($all_params['category'])>0):
    $c = array_pop($all_params['category']);
    $c = strtolower(str_plural(Category::find($c)->name));
  endif;

  if(isset($all_params['service']) && count($all_params['service'])>0):
    $c = array_pop($all_params['service']);
    $c = strtolower(Service::find($c)->name);
  endif;
  $city_name = isset($where) && strlen($where)>0?$where:MazkaraHelper::getLocale();
  $city_name = strtolower($city_name);

  $lnks = [ $c.' in '.$city_name, 
            'top '.$c.' in '.$city_name, 
            'best '.$c.' in '.$city_name, 
            'Top 10 '.$c.' in '.$city_name, 
            $c.' reviews', 
            $c.' deals in '.$city_name, 
            $c.' offers in '.$city_name, 
            $c.' rates in '.$city_name, 
            $c.' coupons in '.$city_name, 
            'mens '.$c.' in '.$city_name];

  foreach($lnks as $lnk):
    $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'?'._apfabtrack(['body']).'">'.$lnk.'</a>';
  endforeach;

  shuffle($urls);

?>
<span class="gray">{{ join(',&nbsp;', $urls) }}</span>


</div>

