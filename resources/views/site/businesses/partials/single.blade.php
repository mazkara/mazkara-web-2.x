<?php $business_url =  MazkaraHelper::slugSingle($business);?>

<div class=" single-business-element mb10  ">
  <article class=" clearfix hide-only-mobile relative" style="padding:0px;">
    <div  class="relative border-radius-3 ba bg-white clearfix">
      <div class="pull-left pr10 relative">
        <span style="position:absolute;top:7px;left:10px;">
          @if(Auth::check())
            <a href="javascript:void(0)" style="font-size:20px;" title="Favorite {{ $business->name }}" class=" {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart hide-when-unfavorite"></i> 
              <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
            </a>
          @else
            <a href="/users/login" style="font-size:20px;" title="Favorite {{ $business->name }}" class="page-scroll ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart-o " style="color:#fff;"></i> 
            </a>
          @endif
        </span>
        <a title="{{{ $business->name }}}" href="{{ $business_url }}?{{_apfabtrack(['body','venue-image'])}}" class="border-left-radius-3 link-single-venue " >
          {{ 
              ViewHelper::businessThumbnail($business, [ 'width'=>'250', 
                                                      'height'=>'173', 
                                                      'class'=>'border-left-radius-3  fw300',
                                                      'meta'=>'medium',
                                                      //'no-meta'=>'small', 
                                'style'=>'width:250px; height:173px; padding-top:46px;font-size:40px;'])
            }}
        </a>
      </div>
      <div class="">
        <div class="pos-relative p10" >
          <div class="pull-right search-item-rating dpib text-right ">
            {{ ViewHelper::starRating($business->average_rating, ['size' => 'ms', 
                                                                  'show_num_votes' => true, 'show_no_votes'=>false,
                                                                  'total_votes' => $business->accumulatedReviewsCount()]) }}

          </div>
          <h3 class="search-item-name fw700 ovf-hidden mb0" style="margin-top:0px;">
            <a title="{{{ $business->name }}}" href="{{ $business_url }}?{{_apfabtrack(['body','venue-title'])}}" class="link-single-venue w100pc ovf-hidden dpib hover-underline result-title">
              {{{ mzk_str_trim($business->name, 35) }}}
            </a>
          </h3>
          <div class="mt2 mb2">
          </div>
          <div class="mt2 pr15  single-listing-address">
            <span class="color-black pl1 "><i class="fa fa-map-marker  "></i>
            {{ $business->zone_cache }}</span>
            <span title="{{ $business->getAddressAsString() }}" class="search-item-address medium-gray"> › {{ $business->getAddressAsString() }}</span>
          </div>
          <?php
          $highlights =$business->getMeta('highlights');
          $highlights = is_array($highlights)?$highlights:[];
          $costOptions = Business::getCostOptions();
          ?>
          <div class="mt10 mb2  ">


            <div class="pull-left dark-gray">
              <ul class="list-unstyled p0 m0 list-inline">
                <?php ksort($highlights);?>
              @foreach((mzk_secure_iterable($highlights)) as $ii=>$highlight)
                @if(MazkaraHelper::isActiveHighlight($ii))
                <li class="p0 mb5">
                  <div class="text-center dark-gray">
                    <!--<a rel="nofollow" title="{{$highlight}} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['highlights'=>[$ii]]) }}" class="dpib search-item-text pr5">-->
                      <div class="dpib" >
                        <i class=" dark-gray fs125 {{ mzk_icon_highlights(MazkaraHelper::getHighlightSlug($ii)) }}"></i>
                      </div>
                      <div class="fs75 dark-gray">{{ ($highlight)}}</div>
                    <!--</a>-->
                  </div>
                </li>
                @endif
              @endforeach
              @if($business->cost_estimate > 0)
                <li class="p0 mb5">
                  <div class="text-center dark-gray">
                    <div class="dpib" >
                      <i  class="fs125 dark-gray {{mzk_icon_cost_estimate($business->cost_estimate)}}"></i>
                    </div>
                    <div class="fs75 dark-gray">{{ ($costOptions[$business->cost_estimate])}}</div>
                  </div>
                </li>
              @endif
            </ul>

            @if($business->image_count > 0)
              <a href="{{MazkaraHelper::slugSinglePhotos($business)}}?{{_apfabtrack(['body','venue'])}}" class="btn-xs hover-color-gray hover-border-gray dark-gray border-radius-3 fs75 mb5 btn text-center " style="padding:3px;border:1px solid #444444;color:#444444;width:70px;">
                {{ $business->image_count }} PHOTO(S)
              </a><br/>
            @endif

            @if($business->rate_card_count > 0)
              <a href="{{MazkaraHelper::slugSingleRateCards($business)}}?{{_apfabtrack(['body','venue'])}}" class="btn-xs dark-gray hover-color-gray hover-border-gray fs75 btn text-center border-radius-3  "  style="padding:3px;border:1px solid #444444;width:70px;color:#444444;">
                VIEW MENU
              </a>
            @endif


            </div>


          </div>
          <div class=""></div>

        </div>

        <div class="search-item-links  dpib pt5 ">
          <?php       
            $offers = $business->getMeta('offers');
            $offers_block = '';
          ?>
          @if( (count($offers)>0))
            <a class="btn hidden blue-btn-arrow btn-lite-blue btn-xs mr0 mb5 toggler" rel="offers-for-{{$business->id}}" href="javascript:void(0)">Offers</a>
            <?php ob_start();?>
              <div class=" clearfix " id="offers-for-{{$business->id}}">
                <table class="mb0 border-gray table">
                  @foreach(mzk_secure_iterable($offers) as $ii=>$offer)
                    @if( strtotime($offer->valid_until) >= strtotime(date('Y-m-d')))
                    <?php //$offer = mzk_array_to_object($offer);?>
                    <tr class="">
                      <td class=" bg-white color-black" style="border-top:3px solid #F1F1F1;">
                      <div style="overflow:hidden;text-overflow:ellipsis;height:auto;display:inline-block;">  {{ mzk_offer_title($offer) }}</div>
                      </td>
                      <td style="border-top:3px solid #F1F1F1;" width="10%" class="bg-white color-black">
                          <span style="text-decoration:line-through;" class="medium-gray">{{ mzk_price($offer->original_price, '&nbsp;') }}
                      </td>
                      <td style="border-top:3px solid #F1F1F1;" width="10%" class="text-right fw700 bg-white fire pr0"><b>{{ mzk_offer_price($offer, '&nbsp;') }}</b></td>
                      @if(Offer::areClaimable($offer))
                        <td style="border-top:3px solid #F1F1F1;" width="10%" class="text-right bg-lite-gray-2 pr0">
                          <a class="btn btn-pink btn-xs br0 no-border-radius p5 ajax-popup-link" href="{{ route('offers.get.claim.form', [$offer->id]) }}">CLAIM</a>
                        </td>
                      @endif
                    </tr>
                    @endif
                  @endforeach
                </table>
              </div>
            <?php $offers_block = ob_get_contents(); ob_end_clean();?>
          @endif
          <div class="clear"></div>
        </div>
        
      </div>

            <div class="absolute b10 r10 text-right">
              <div class="">
                <?php $service_flag = false;?>
                @if(isset($prices[$business->id]))
                  @foreach($prices[$business->id] as $one_price)
                      <?php $service_flag = true;
                      $cost_price = mzk_get_starting_price($one_price['starting_price'], MazkaraHelper::getCitySlugFromID($business->city_id));
                      ?>
                      @if(trim($cost_price)!='')
                      <span class="dpb p5 fs90 bg-lite-gray mb5">
                        {{ $one_price['service_name'] }}
                        - {{ $cost_price }}
                      </span>
                      @endif
                  @endforeach
                @endif

              </div>

                <?php 
                    $phones = $business->getMeta('current_numbers'); 
                    $phones = is_array($phones) ? $phones:[];//$business->displayablePhone();?>
                  <!--<a data-placement="top" data-business="{{$business->id}}" 
                    class="call-button  btn border-radius-15  btn-green-line  popover-desk" 
                    data-toggle="popover" data-phone="{{ count($phones)>0 ? join('|', $phones) : 'No phone number available.' }}" data-container="body" type="button" data-html="true" href="javascript:void(0)" ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">CALL NOW</a>
                  -->
            </div>
            <a rel="nofollow" href="#" style="visibility:hidden;" class="search-item-text pr5">&nbsp;</a>
              <div class="clear"></div> 




    </div>
{{ $offers_block }}        

  </article>
  <div class="  clearfix  show-only-mobile">
    @include('site.businesses.partials.single-mobile')
  </div>
</div>
