<div class=" _deal-item  col-xs-6 col-lg-6" title="{{ $one_deal->title }}" rel="deal-content-{{ $one_deal->id }}">
  <div class="offer offer-radius {{ strtolower($one_deal->type) == 'discount'?'offer-primary':(strtolower($one_deal->type) == 'package'?'offer-success':'offer-warning')}} mb0">
    <div class="shape">
      <div class="shape-text">
        {{ strtoupper($one_deal->type)}}               
      </div>
    </div>
    <div class="top-half p10 pt5 pb5">
      <h2 class="group pt10 mt5 inner list-group-item-heading">
        <a href="{{MazkaraHelper::slugDeal($one_deal->business, $one_deal)}}">{{ $one_deal->title }}</a>
      </h2>
      <p class="group inner list-group-item-text">
        {{ $one_deal->caption }}</p>
    </div>
    <div class="bottom-half offer-radius pt5">
    <div class="offer-content offer-radius">
      <div class="caption">
        <div class="row">
          <div class="col-xs-4">
            <p class="lead mb0 pb0 green"><sup>{{ mzk_currency_symbol() }}</sup>{{ ViewHelper::money($one_deal->offer_amount, '') }}</p>
          </div>
          <div class="col-xs-8">
            @foreach($one_deal->artworks as $ii=>$one_photo)
              <?php if($ii>3){
                break;
              }?>
              <a href=""><img src="{{ $one_photo->image->url('micro')}}" class="img-rounded" width="35" height="35"/> </a>
            @endforeach
          </div>

        </div>
      </div>
    </div>
  </div>
  </div>
</div>