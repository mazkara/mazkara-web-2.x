@if(Auth::check())
    @if($business->canCurrentUserAccessClientDashboard(Auth::user()))
      <a href="/redirect/client/dashboard?id={{$business->id}}" class="btn pt10 btn-block btn-default mb10"> VIEW CLIENT DASHBOARD</a>
    @endif
@endif