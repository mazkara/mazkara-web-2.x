<div class="mb10 bg-white p10">
  <div class="row">
    <div class="col-md-12">
      <div class="">
        <span class="pull-left mr10">
          <a href="{{MazkaraHelper::slugSingle($business)}}?{{_apfabtrack(['rhs','venue-image'])}}">
            {{ ViewHelper::businessThumbnail($business, [ 'meta'=>'thumbnail', 
                                                          'width'=>'60', 'height'=>'60', 
                                                          'style'=>'height:60px;width:60px; padding-top:3px;']) }}
          </a>
        </span>
        <h5 class="ovf-hidden search-item-name force-black media-heading mb0 notransform bolder   ">
          <a title="{{{ $business->name }}}" href="{{ MazkaraHelper::slugSingle($business) }}?{{_apfabtrack(['rhs','venue-title'])}}" class="result-title fs80">{{{ $business->getTrimmedName(23) }}} </a>
        </h5>
        <div class="pull-right pr10">
          @if(Auth::check())
            <a href="javascript:void(0)" style="font-size: 20px; display: inline-block; margin-top: -5px;" title="Shortlist {{ $business->name }}" class=" {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart hide-when-unfavorite"></i> 
              <i class="fa fa-heart-o hide-when-favorite" ></i> 
            </a>
          @else
            <a href="/users/login" style="font-size: 20px; display: inline-block; margin-top: -5px;" title="Favorite {{ $business->name }}" class="page-scroll ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
              <i class="fa fa-heart-o " ></i> 
            </a>
          @endif
        </div>
        <span title="{{ $business->zone_cache }}" class="search-item-address ">
          › {{ mzk_str_trim($business->zone_cache, 27) }}
        </span><br/>
        {{ ViewHelper::starRateBasic($business->rating_average, 'xxs', false, 10 /* just a hack*/) }}
      </div>
    </div>
  </div>
</div>