
@if($business->timings()->count()>0)
          <div class="bbb pr15 fs125">
    <h2 class="item-headers pt5 pt0 mt0"><i class="fa fa-clock-o"></i> Opening Hours</h2>

    <ul class="list-unstyled">
      @foreach(['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', "Sat"] as $day)
        <li>
          <div class="row">
            <div class="col-md-3">{{strtoupper($day)}} </div>
            <div class="col-md-9">
            @foreach($business->timings as $timing)
              @if(strstr( $timing->daysOfWeek, $day))
                {{ ViewHelper::time($timing->open)}} - 
                {{ ViewHelper::time($timing->close)}} <br/>
              @endif
            @endforeach
          </div>
        </div>
        </li>
      @endforeach

    </ul>
  </div>
@endif