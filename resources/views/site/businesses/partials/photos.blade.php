<?php $total_photos = isset($total_photos)?$total_photos:5;?>
@if(count($photos) >0)
  <div class="     ">
    <h2 class="item-headers">PHOTOS</h2>
    <div class="photos-holder">
      @foreach($photos as $ii=>$one_photo)
        @if((count($photos)>($total_photos)) && (($ii+1) ==(count($photos))))
          <a  href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}?{{ _apfabtrack(['body', 'venue-image']) }}" 
              class="image ba mr5 hide border-radius-5" 
              style="display:inline-block"><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" alt="{{ $business->name }} + Photo {{ $ii+1 }}" class="venue-thumbnail border-radius-5 op85-on-hover" /></a>
        @else
          <a  href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}?{{ _apfabtrack(['body', 'venue-image']) }}" 
              class="image ba {{($ii+1)%(ceil($total_photos/5))==0?'mr5':''}} 
                              {{ ($ii>=($total_photos-1))?'hide':''}} 
                    border-radius-5  " style="display:inline-block"><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" data-sequence="{{($ii+1)}}" alt="{{ $business->name }} + Photo {{ $ii+1 }}" class="venue-thumbnail border-radius-5 op85-on-hover" /></a>
        @endif
      @endforeach
      @if(count($photos)>($total_photos))
        <a style="background-image:url( {{ mzk_cloudfront_image($one_photo->image->url('small')) }});display:inline-block; vertical-align:middle; text-align:center" 
           class="fs150 trigger-gallery-open sq-card border-radius-5  mb0 ba fw300 image btn btn-default p0 bg-lite-gray  venue-thumbnail " 
           href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}?{{ _apfabtrack(['body', 'venue-image']) }}">
          <div class="va-container va-container-v va-container-h overlap-transparent">
            <div class="va-middle text-center force-white" style="z-index:999;">
              +{{count($photos)-(4)}}
            </div>
          </div>
        </a>
      @endif
    </div>
  </div>
@endif
