<?php $genders = [4,5,6];?>
  <li class="list-group-item  bb0 pt20">
    <b>GENDER</b>
  </li>
      <li class="list-group-item pb20  bt0 bb0">
        <ul class="list-unstyled padded5">
          @foreach($highlights as $highlight)
          <?php 
          if(!in_array($highlight['id'], $genders))
            continue;
          $prms = $params;
          unset($prms['service']);
          unset($prms['category']);
          unset($prms['city']);

          if(!isset($params['highlights'])){
            $prms['highlights'] = array();
          }

          $key = -1;

          if(in_array($highlight['id'], $prms['highlights'])){

            $key = array_search($highlight['id'], $prms['highlights']);
            unset($prms['highlights'][$key]);

          }else{
            // show only the current highlight
            //foreach($genders as $gender){
              //$key = array_search($gender, $prms['highlights']);
              //unset($prms['highlights'][$key]);
            //}
            $prms['highlights'][] = $highlight['id'];
            $key = -1;
          }
          ?>
          <li>
            @if($key>=0)
              <a class="pull-left" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                <i class="fa fa-check-square selected mr5"></i>

                <b>{{ $highlight['name'] }}</b>

              </a>
            @else
              <a class="pull-left" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
              <i class="fa gray fa-square-o mr5"></i>
                {{ $highlight['name'] }}
              </a>
            @endif
            <div class="clearfix"></div>
          </li>
          @endforeach
        </ul>
      </li>
      <li class="list-group-item pt20 bb0">
        <b>HIGHLIGHTS</b>
      </li>
      <li class="list-group-item bt0 bb0">
        <ul class="list-unstyled padded5">


          @foreach($highlights as $highlight)
          <?php 
          if(in_array($highlight['id'], $genders))
            continue;

          $prms = $params;
          unset($prms['service']);
          unset($prms['category']);
          unset($prms['city']);

          if(!isset($params['highlights'])){
            $prms['highlights'] = array();
          }

          $key = -1;

          if(in_array($highlight['id'], $prms['highlights'])){
            $key = array_search($highlight['id'], $prms['highlights']);
            unset($prms['highlights'][$key]);
          }else{
            $prms['highlights'][] = $highlight['id'];
            $key = -1;
          }
          ?>
          <li>
            @if($key>=0)
              <a class="pull-left" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                <i class="fa fa-check-square selected mr5"></i>

                <b>{{ $highlight['name'] }}</b>

              </a>

            @else
              <a class="pull-left" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
              <i class="fa gray fa-square-o mr5"></i>
                {{ $highlight['name'] }}
              </a>
            @endif
            <div class="clearfix"></div>
          </li>
          @endforeach
        </ul>
      </li>