  <div class="hide-only-mobile">
  <ul class="padded8 dp-flex mb0" style="margin-left: auto;  margin-right:auto ; align-items: stretch; justify-content: space-between;width:90%">
    <li class="dpb text-center">
      @if(!isset($params['category']))
        <a title="Clear filter" 
            href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), []) }}?{{_apfabtrack(['body', 'filter'])}}" 
            class="fs110 color-black p5 pl0 pr0 bbb-turquoise">
          <b>All</b>
        </a>
      @else
        <a href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), []) }}?{{_apfabtrack(['body', 'filter'])}}" 
            class="fs110 p5 dark-gray bbb-gray-on-hover pl0 pr0">
          All
        </a>
      @endif
      <div class="clearfix"></div>
    </li>
    @foreach($categories as $category)
    <?php 
    $prms = [];//$params;
    //if(!isset($params['category'])){ // uncomment for multiple
    $prms['category'] = array();
    //}
    $prms['category'][] = $category['id']; 
    ?>
    <li class="dpb text-center">
      @if(isset($params['category']) && (in_array($category['id'], $params['category'])))
        <?php unset($prms['category']);?>
        <a  title="Clear filter" 
            href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}" 
            class="fs110 color-black p5 pl0 pr0 bbb-turquoise">
          <b>{{ $category['name'] }}</b>
        </a>
      @else
        <a  href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}" 
            class="fs110 p5 dark-gray bbb-gray-on-hover pl0 pr0">
          {{ $category['name'] }}
        </a>
      @endif
      <div class="clearfix"></div>
    </li>
    @endforeach
  </ul>
</div>
  <div class="show-only-mobile force-gray">
  <ul class="mobile-category-filter hidden padded12 mb0" >
    <li class="dpb text-center">
        
        <a  href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), []) }}?{{_apfabtrack(['body', 'filter'])}}" 
            class="fs110  color-black  pb10 pt10 bbb-turquoise">
          All
        </a>
</li>
    @foreach($categories as $category)
      @if(isset($params['category']) && (in_array($category['id'], $params['category'])))
        <?php unset($prms['category']);?>

    <li class="dpb text-center">
        <a  title="Clear filter" 
            href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}" 
            class="fs110  color-black  pb10 pt10 bbb-turquoise">
          <b>{{ str_replace(' ', '&nbsp;', $category['name']) }}</b>
        </a>
      </li>
      @endif

    @endforeach

    @foreach($categories as $category)
    <?php 
    $prms = [];//$params;
    //if(!isset($params['category'])){ // uncomment for multiple
    $prms['category'] = array();
    //}
    $prms['category'][] = $category['id']; 
    ?>
      @if(isset($params['category']) && (in_array($category['id'], $params['category'])))
      @else
    <li class=" text-center">
        <a  href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}" 
            class="fs110  dark-gray pb10 pt10 ">
          {{ str_replace(' ', '&nbsp;', $category['name']) }}
        </a>
    </li>
        
      @endif
    @endforeach
  </ul>
</div>