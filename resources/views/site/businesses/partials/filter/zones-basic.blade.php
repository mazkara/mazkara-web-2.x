<div class="pt5 mb10 pb10" id="zones-filter-holder"  classss="p15 bg-white pt5 border-radius-3 pb5 mb10">
  <div class="  pt5 pl0 pr0 p15 pb5 bb mb5"><b>WHERE</b></div>
  <ul class="list-unstyled mb0  bb">
    <li class="filter-holder list-group-item p0 b0 pointer-cursor  mb10">
      <div  class="inner-addon pointer-cursor right-addon" style=" ">
        <label class="glyphicon glyphicon-chevron-down fs75 pr0 " style="z-index:99;" rel="tooltip "></label>
        <input class="filter form-control no-box-shadow pl0 pr0 pointer-cursor b0" rel="zone-list"  placeholder="Type Location" />
      </div>
    </li>
  </ul>
  @if(isset($sub_zone))
    <?php $afilter = $params;?>
    <ul  class="list-unstyled  mb0 ">
      <li class="bb pb5 pt5 ovf-hidden nowrap ">
        <a title="Clear filter" href="{{ MazkaraHelper::slugCity(null, $afilter) }}" class="w100pc">
            <span class="pull-left fs110 pr5"><i class="fa fa-dot-circle-o selected mr5"></i></span>
            <span class=" fw900 ovf-hidden nowrap ">{{ $sub_zone->name }}</span>
        </a>
      </li>
    </ul>
  @endif
  <ul id="zone-list"  class="list-unstyled  mb0">
    <?php 
      $surrounding_zones = isset($surrounding_zones)?$surrounding_zones:[];
      $surrounding_zones_list = [];
    ?>
    @foreach($surrounding_zones as $i=>$zone)
    <?php 
    $surrounding_zones_list[] = $zone->id;
    $prms = $params;
    unset($prms['city']);
    
    //if(!isset($params['zone'])){
      $prms['zone'] = array();
    //}

    $prms['zone'] = [$zone->id];

    $prms['subzone']= $zone->slug;
    ?>
      @if(isset($sub_zone) && ($sub_zone->id == $zone->id))
      @else
    <li class=" hidden showable bb pb5 fw900 clearfix ovf-hidden nowrap pt5">
        <a href="{{ MazkaraHelper::slugCityZone($zone, $prms)}}">
            <span class="pull-left fs110 pr5"><i class="fa fa-circle-o mr5"></i></span>

            <span class=" fw900 ovf-hidden nowrap">{{ $zone->name }}</span>
        </a>
    </li>
        @endif
    @endforeach

    @foreach($zones as $i=>$zone)
    <?php 
    if(in_array($zone['id'], $surrounding_zones_list)){
      continue;
    }
    $prms = $params;
    unset($prms['city']);
    
    //if(!isset($params['zone'])){
      $prms['zone'] = array();
    //}

    $prms['zone'] = [$zone['id']];

    $prms['subzone']= $zone['slug'];
    ?>
      @if(isset($sub_zone) && ($sub_zone->id == $zone['id']))
      @else

    <li class="{{ (count($surrounding_zones_list)==0 && $i<5) ? 'showable ':'' }} hidden bb pb5 fw900 ovf-hidden nowrap pt5">
        <a href="{{ MazkaraHelper::slugCityZone($zone, $prms)}}">
            <span class="pull-left fs110 pr5"><i class="fa fa-circle-o mr5"></i></span>
            <span class=" fw900 ovf-hidden nowrap">{{ $zone['name'] }}</span>

          
        </a>
    </li>
        @endif
    @endforeach
  </ul>
</div>