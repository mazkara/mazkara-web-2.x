<ul class="list-unstyled pb10 list-compressed vertical-filter list-inline mb0">
  <?php $genders = [4,5,6]; ?>
  @foreach($highlights as $highlight)
    <?php 
    if(!in_array($highlight['id'], $genders))
      continue;
    $prms = $params;
    unset($prms['service']);
    unset($prms['category']);
    unset($prms['city']);

    if(!isset($params['highlights'])){
      $prms['highlights'] = array();
    }

    $key = -1;

    if(in_array($highlight['id'], $prms['highlights'])){

      $key = array_search($highlight['id'], $prms['highlights']);
      unset($prms['highlights'][$key]);

    }else{
      // show only the current highlight
      //foreach($genders as $gender){
        //$key = array_search($gender, $prms['highlights']);
        //unset($prms['highlights'][$key]);
      //}
      $prms['highlights'][] = $highlight['id'];
      $key = -1;
    }
    ?><li>
    @if($key>=0)
      <a class="pull-left btn btn-default  no-border-radius btn-sm" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
        <i class="fa fa-check-square selected mr5"></i>
        <b>{{ strtoupper($highlight['name']) }}</b>

      </a>
    @else
      <a class="pull-left btn btn-default  no-border-radius btn-sm" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
      <i class="fa gray fa-square-o mr5"></i>
        {{ strtoupper($highlight['name']) }}
      </a>
    @endif</li>
  @endforeach

          @foreach($highlights as $highlight)
          <?php 
          if(in_array($highlight['id'], $genders))
            continue;

          $prms = $params;
          unset($prms['service']);
          unset($prms['category']);
          unset($prms['city']);

          if(!isset($params['highlights'])){
            $prms['highlights'] = array();
          }

          $key = -1;

          if(in_array($highlight['id'], $prms['highlights'])){
            $key = array_search($highlight['id'], $prms['highlights']);
            unset($prms['highlights'][$key]);
          }else{
            $prms['highlights'][] = $highlight['id'];
            $key = -1;
          }
          ?><li>
            @if($key>=0)
              <a class="no-border-radius pull-left  btn btn-default btn-sm" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                <i class="fa fa-check-square selected mr5"></i>

                <b>{{ strtoupper($highlight['name']) }}</b>

              </a>

            @else
              <a class="pull-left btn no-border-radius  btn-default btn-sm" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
              <i class="fa gray fa-square-o mr5"></i>
                {{ strtoupper($highlight['name']) }}
              </a>
            @endif</li>
          @endforeach
    <li>
      @if(isset($params['open']) && ($params['open']=='now'))
        <?php $prms = $params; unset($prms['open']);unset($prms['service']);unset($prms['category']);?>
        <a title="Clear filter" rel="nofollow" class="pull-left no-border-radius  btn btn-default btn-sm" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa fa-check-square selected mr5"></i>
          <b>OPEN NOW</b>
        </a>
      @else
        <?php $prms = $params; unset($prms['city']);$prms['open'] = 'now';unset($prms['service']);unset($prms['category']);?>
        <a class="pull-left btn btn-default no-border-radius btn-sm pl5 " rel="nofollow" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa gray fa-square-o mr5 " ></i>
          OPEN NOW
        </a>
      @endif
      <div class="clearfix"></div>
    </li>


    @if(MazkaraHelper::getLocale()!='pune')
      <li>
      <div class="btn-group pull-left ">
        <?php $costOptions = Business::getCostOptions(); unset($costOptions[0]); ?>
          
        @if(isset($params['cost']) && ($params['cost']!=''))
          <a href="#" class="btn btn-default no-border-radius btn-sm pl5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-check-square selected mr5 hidden"></i>
            <i class="{{ mzk_icon_cost_estimate($params['cost']) }}" ></i>

            <b>{{ strtoupper($costOptions[$params['cost']]) }}</b> <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            @foreach($costOptions as $ii=>$vv)
              @if($vv!=$params['cost'])
                <?php $prms = $params; unset($prms['city']);$prms['cost'] = $ii;unset($prms['service']);unset($prms['category']);?>
                <li>
                  <a href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
                    <i class="{{ mzk_icon_cost_estimate($ii) }}" ></i>
                    {{ strtoupper($vv) }}
                  </a>
                </li>
              @endif
            @endforeach
            <?php $prms = $params; unset($prms['cost']);unset($prms['service']);unset($prms['category']);?>
            <li>
              <a title="Clear filter" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
                Clear
              </a>
            </li>
          </ul>

        @else
          <a href="#" class="btn btn-default no-border-radius btn-sm pl5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            PRICE <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            @foreach($costOptions as $ii=>$vv)
              <?php $prms = $params; unset($prms['city']);$prms['cost'] = $ii;unset($prms['service']);unset($prms['category']);?>
              <li>
                <a href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
                  <i class="{{ mzk_icon_cost_estimate($ii) }}" ></i>
                  {{ strtoupper($vv) }}
                </a>
              </li>
            @endforeach
          </ul>

        @endif
      </div>

        <div class="clearfix"></div>
      </li>
    @endif



    <li>
      @if(isset($params['specials']) && ($params['specials']=='true'))
        <?php $prms = $params; unset($prms['specials']);unset($prms['service']);unset($prms['category']);?>
        <a title="Clear filter" rel="nofollow" class="pull-left no-border-radius  btn btn-pink btn-sm" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <span class="fa-stack fa-sm">
            <i class="fa  fa-square  force-white fa-stack-1x" ></i>
            <i class="fa fa-check fa-inverse selected  fa-stack-1x"></i>
          </span>
          {{ strtoupper(mzk_label('specials')) }}
        </a>
      @else
        <?php $prms = $params; unset($prms['city']);$prms['specials'] = 'true';unset($prms['service']);unset($prms['category']);?>
        <a class="pull-left btn btn-pink no-border-radius btn-sm  pl5" rel="nofollow" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
          <i class="fa gray fa-square mr5 force-white" ></i>
          {{ strtoupper(mzk_label('specials')) }}
        </a>
      @endif
      <div class="clearfix"></div>
    </li>
  </ul>
  <div class="show-only-mobile">
    <ul class="list-unstyled">
      @include('site.businesses.partials.filter.zones')
    </ul>
  </div>

