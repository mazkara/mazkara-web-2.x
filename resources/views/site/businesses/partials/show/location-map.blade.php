<div class="pt10 pb10 bbb " >
  <span class="fs125 fw900 pb10 dpb">

    <a target-"_blank" href="https://www.google.com/maps/dir/Current+Location/{{$business->geolocation_latitude}},{{$business->geolocation_longitude}}" class="show-only-mobile">
      Location
      <small class="fs80 fw300">(Get Directions)</small>
    </a>

    <a href="{{MazkaraHelper::slugSingleLocation($business)}}" class="hide-only-mobile">
      Location
      <small class="fs80 fw300">(Get Directions)</small>
    </a>
  </span>
  <p>
    <b>{{ $business->getZoneName() }}</b> ›
    <span>{{ $business->getAddressAsString() }}</span>
    @if(count($business->chain) > 0)
      @if(count($business->chain->businesses)>0)
        <p>
          <a href="{{ MazkaraHelper::slugCityChain(null, $business->chain)}}" class="text-success">
            {{ count($business->chain->businesses) - 1 }} other venues
          </a>
        </p>
      @endif
    @endif
  </p>
</div>
