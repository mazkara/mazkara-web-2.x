<div class="   bg-lite-gray -3 text-left" >

  @if($business->isLocationSet())
    <div class="item-map-holder "><div id="item-map" class=" item-map mr10 bbb" style="height:250px"></div></div>
  @endif
  <p class="p10">
    <b >{{ $business->getZoneName() }}</b> ›
    <span >{{ $business->getAddressAsString() }}</span>
  </p>

  @if($chain_businesses>0)
    <div class="p10   mb0 mb0 mb0 mb0 pb0 mb0  pt0" style="margin-top:-10px;">
      <a href="{{ MazkaraHelper::slugCityChain(null, $business->chain)}}" class="dark-sea">
        {{ $chain_businesses - 1 }} more venues
      </a>
      <div class="bbd pt10"></div>
    </div>
  @endif


</div>
