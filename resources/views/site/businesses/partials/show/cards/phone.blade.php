@if(is_array($phones))
  <div class=" p10 text-center " >
    <p class="show-only-mobile mt20 mb20 ">&nbsp;</p>
    <span class="hoverable" >
      <div class="hide-on-hover pt10 pb10 mt10 btn btn-turquoise   desktop btn-lg call-button" data-business="{{$business->id}}"> PHONE NUMBER</div>
      <div class="show-on-hover">
        @if(count($phones)>0)
          @foreach($phones as $ii=>$phone)
          <?php $phones[$ii] = ViewHelper::phone($phone);?>
          @endforeach
            <p class="fs125 ">
              <span class="fa-stack hidden fa">
                <i class="fa fa-circle fa-stack-2x fw300 fs150 green"></i>
                <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
              </span>        
              <span class="fs110 lato fw900">{{ join(', ', $phones) }}</span>
            </p>

        @else
          <p class="fs125 ">
              <i class="fa hidden fa-warning fa-inverse yellow"></i>
            <span class="fw900 lato">No phone number available</span>
          </p>
        @endif

      <p class="text-success">Prior bookings are recommended</p>
        
      </div>
    </span>
  </div>
@endif
