<?php 
if(!isset($phones)):
  $phones = $business->displayablePhone();
endif;
?>
@if(is_array($phones))
  <div class="pt10 pb10 mt10 bbb "  >
    <p class="show-only-mobile mt20 mb20 ">&nbsp;</p>
    @if(count($phones)>0)
      @foreach($phones as $phone)
        <p class="fs125 ">
          <span class="fa-stack hidden fa">
            <i class="fa fa-circle fa-stack-2x fw300 fs150 green"></i>
            <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
          </span>        
          <span class="fw900 lato">{{ ViewHelper::phone($phone) }}</span>
        </p>
      @endforeach
    @else
      <p class="fs125 ">
        <i class="fa fa-warning hidden fa-inverse yellow"></i>
        <span class="fw900" >No phone number available</span>
      </p>
    @endif
    <p class="text-success">Prior bookings are recommended</p>
  </div>
@endif
