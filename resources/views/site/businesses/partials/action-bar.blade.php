<?php
$no_rating = isset($no_rating)?$no_rating:false;
?>
    <div class="row">
      <div class="col-md-12">
        <div class="mt5 mb5">
          @if(count($phones)==1)
            <center>
              <a class="show-only-mobile btn btn-success btn mb10 call-button" data-business="{{$business->id}} " style="width:40%; " href="tel:{{ $phones[0] }}">Call Now</a>
            </center>
          @else
            <center>
              <?php $phone = count($phones)>0 ? join('|', $phones):'No phone number available.' ;?>
              <a data-placement="top" style="width:40%; " class="show-only-mobile mb10 btn btn-success btn popover-mobile call-button" data-business="{{$business->id}} " 
              data-toggle="popover" data-phone="{{ $phone }}" data-container="body" type="button" data-html="true" href="javascript:void(0)" ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">Call Now</a>
            </center>
          @endif
          @if(Auth::check())
            <a href="javascript:void(0)" class="btn mr2  {{ Auth::check()? ($business->favourited(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default" rel="{{$business->id}}"  data-route="businesses">
              <i class="fa fa-heart "></i> 
            </a>
            @if($no_rating == false)
              @if($myreview)
                <a href="#reviews" title="Check your Review" class="btn mr2 review-btn  page-scroll btn-default" >
                  <i class="fa fa-pencil pr5"></i><span class="hide-only-mobile">{{ $myreview->isARating() ? 'Write':'Edit'}} Review</span>
                </a>
              @else
                <a href="#reviews" title=" Review" class="btn mr2 review-btn  page-scroll btn-default ">
                  <i class="fa fa-pencil pr5"></i><span class="hide-only-mobile">Write Review</span>
                </a>
              @endif
            @endif

            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('moderator'))
            <a href="/content/businesses/{{$business->id}}" title="Edit as Admin" class="btn mr2  btn-default">
              <i class="fa fa-shield"></i>
            </a>

            @endif

          @else
            <a href="/users/login" class="btn ajax-popup-link mr2 btn-default " rel="{{$business->id}}">
              <i class="fa fa-heart"></i> 
            </a>
            <a href="/users/login" class="btn mr2 ajax-popup-link btn-default review-btn" >
              <i class="fa fa-pencil pr5"></i><span class="hide-only-mobile">Write Review</span>
            </a>
          @endif
          <!--<a href="javascript:void(0)" class="report-link btn btn-default btn" title="Report an issue">
            <span class="fa fa-warning"></span>
          </a>-->
          <a href="javascript:void(0)" class="sharable btn mr2 btn-default btn" data-sharable="Business" data-sharableid="{{$business->id}}" title="Share on Facebook">
            <span class="fa fa-share-square-o"></span>
            <span class="hide-only-mobile">Share</span>
          </a>
            <span class="btn-group mr2  hidden-mobile">
              <span class="btn btn-default ">
                Your Rating
              </span>
            <span class="btn btn-default pt5 pb0 pl10 pr15" style="min-height:34px;" >
              <?php $default_rating = $myreview ? $myreview->rating : $business->ratingByIP(Request::getClientIp()); ?>

              {{ Form::select('rate', ([''=>'']+Review::ratingOptionsMain()), ceil($default_rating), array( 'class'=>'form-control hidden', 'id'=>'raterupper',  
                                                              'data-score'=>$default_rating, 'data-size'=>'xs', 'rel'=>$business->id)) }}
                <span class="pointer-cursor   pl0 fs90    gray review-clear-rating-single " ref="rating"><i class=" fa fa-times"></i></span>

            </span></span>          
        </div>
      </div>
    </div>
