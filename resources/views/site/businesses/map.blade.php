@extends('layouts.parallax')

@section('preheader')
@stop

@section('content')

<div>
@include('site.businesses.partials.cover-or-parallax-desktop-mobile', ['business'=>$business, 'phones'=>$phones])


<div class="container">
<div class="row" >  
  <div class="col-md-9 bg-white pt20">
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12">
        @include('site.businesses.partials.show.phone', ['business'=>$business])
        @include('site.businesses.partials.show.location-map', ['business'=>$business])
        <div id="item-map" class="ba item-map  mb10" style="height:400px"></div>

        <div class="pt10 pb10">
          <h3>Get directions to {{ $business->name }}</h3>
        </div>
        <div class=" row pb10">
          <div class="col-md-6">
          <div class="input-group">
            <input id="source-location" name="source-location" class=" form-control " />
            <input id="source-x" type="hidden" />
            <input id="source-y" type="hidden" />
            <span class="input-group-btn">
              <button id="submit-search-button" class="btn btn-primary" type="button">Go!</button>
            </span>
          </div><!-- /input-group -->
        </div>
          <div class="col-md-4">
            <button id="current-locale-button" class="btn btn-warning" type="button">Use My Current Location</button>
          </div>
</div>
        <div class="pb15 bbb">
          <ul id="instructions" class="pb10 list-unstyled pt10"></ul>
        </div>




      </div>
    </div>
  </div>
  <div class="col-md-3   pt20">
  @if($business->showAdsOnVenue())
    @include('site.businesses.partials.ad-list')
  @else
    @include('site.businesses.partials.fb-likebox')

  @endif
    <small class=" pb10 mb5">FEATURED</small>
    @foreach($suggested_spas as $native_ad)
      @include('site.businesses.partials.business-niblet', ['business'=>$native_ad])
    @endforeach

  </div>
</div>
</div>
</div>
  <script src="https://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>

<?php ob_start();?>
@include('elements.report')
<?php $reportable = ob_get_contents(); ob_end_clean();?>
@section('js')
@include('site.businesses.partials.show.js', compact('reportable', 'business'))

<script type="text/javascript">
$(function(){



});

</script>

@stop
@endsection