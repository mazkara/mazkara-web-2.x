
    <div class=" mb10 ">
          <div  class="bg-white relative border-radius-3 ba clearfix">

      <div class="pull-left  mr10 relative">

      @if($post->hasCover())
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body-mobile','image'])}}" class=" border-left-radius-3 link-single-venue " >
          <img src="{{ $post->getCoverUrl('small') }}" width="145" class="border-left-radius-3  fw300"  />
        </a>
      @endif

      </div>
      <div class="">
        <div class="pos-relative">



          <h3 class="  fw700 mt10 dpb nowrap ovf-hidden mb0">


            <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack('body-mobile', 'post-title')}}" class="link-single-venue dark-gray  ovf-hidden hover-underline result-title">
              {{{ ($post->title) }}}
            </a>

            <div class="search-item-rating dpib ">
              <div class="clear"></div>
            </div>
          </h3>
          <div class="mt2 mb2 pr10 single-listing-address">
            <span class="search-item-address">›  {{{ ($post->caption) }}}</span>
          </div>

            <div class="pull-left pr15">
              <a href="{{route('users.profile.show', $post->author->id)}}?{{_apfabtrack(['body-mobile','post-author-image', 'post-id-'.$post->id])}}">
                {{ ViewHelper::userAvatar($post->author)}}
              </a>
            </div>
              <a href="{{route('users.profile.show', $post->author->id)}}?{{_apfabtrack(['body-mobile','post-author-name', 'post-id-'.$post->id])}}">
                {{ $post->authors_full_name }}
              </a>
            <br/>
            <span class="medium-gray">{{ $post->authors_designation }}</span>

              <div class="clear"></div> 


          <div class="mt15  bottom5 absolute right5">
            <span>
              <i class=" flaticon flaticon-eye110 op80 medium-gray"></i>
              <span class="medium-gray fs90"><b>{{ $post->views }} Views</b></span>
            </span>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <span>
              <i class="fa fa-thumbs-up op80 {{ $post->num_likes()>0 ? 'lite-blue':'medium-gray' }} "></i>
            </span>
            <span class="fs90 medium-gray"><b> {{  count($post->likes) }} Likes</b></span>

          </div>
          <div class=""></div>
        </div>
      </div>

      </div>

    </div>
  </article>

