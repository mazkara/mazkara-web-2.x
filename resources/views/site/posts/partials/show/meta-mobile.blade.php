      <div class="pt15 mt15  bb  clearfix ">
        <div class="pull-left mb10 mr10">
          {{ViewHelper::userAvatar($post->author, ViewHelper::$avatar45)}}
        </div>
        <div>
          <b>
            <a href="{{route('users.profile.show', $post->author->id)}}">
              {{$post->author->name}}
            </a>
          </b><br/>
          {{$post->authors_designation }}
          <div class="pull-right medium-gray mt10 mr0">
            POSTED ON {{ strtoupper(mzk_f_date($post->published_on, 'jS M'))}}
          </div>
        </div>
      </div>
      <div class="text-right mt10 mb10">
        <div class="pull-left">
          <span class="medium-gray">
            <i class=" flaticon flaticon-eye110 op80 "></i> {{ $post->views }} Views
          </span> 
          &nbsp;




        </div>


      </div>

      <div class="clearfix"></div>


      @if((Auth::check()) && $post->isEditableBy(Auth::user()->id))
        @include('site.posts.partials.publish-status')
      @endif
