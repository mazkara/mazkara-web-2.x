<div  class=" clearfix">

  @if($post->hasCover())
  <div class="">
    <a title="{{ $post->caption }}" href="{{ $post->url() }}" class="gallery" >
      <img src="{{ $post->cover->image->url('medium') }}" width="100%" class="ba"  />
    </a>
  </div>
  @endif

</div>
