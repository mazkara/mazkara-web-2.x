
<div class="border-radius-5 ba toggl-review-form pointer-cursor mb15 dark-gray ">
  <!-- 
  <p>Tip: A great review covers service, experience and ambience. 
  Got recommendations for your favourite service? Include that 
  too! And remember, your review needs to be at least 140 
  characters long :)</p> -->
  <div class="p10 pull-left">Write a review for {{$business->name}}</div>
  <a href="javascript:void(0)" class="btn no-border-radius toggl-review-form border-right-radius-5 pull-right btn-turquoise force-white pb10 pt10 bl0"><b>Publish Review</b></a>
  <div class="clearfix"></div>
</div>

<div class="well p10" id="basic-review-form" style="display:none">
  <div>
  <?php $selected_services  = []; ?>

@if(isset($review)&& is_object($review))
  <?php 
    foreach($review->services as $review_service){
      $selected_services[] = $review_service->id;
    };?>
  {{ Form::model($review, array('class' => 'form-horizontal', 'method' => 'POST', 'id'=>'form-manage-review', 'route' => array('businesses.review.update', $review->id))) }}
@else
  <?php $selected_services  = [];?>
    {{ Form::open(array('route' => 'reviews.store', 'id'=>'form-manage-review', 'method' => 'POST', 'class' => 'form-horizontal')) }}
@endif 
<div class="  "> 
  <h4 class="item-headers pt0 mt0 hide-only-mobile">
    Write a review for {{ $business->name }}
  </h4>
  @if (Session::has('message'))
    <div class="flash alert alert-danger">
      <p>{{ Session::get('message') }}
        <ul>
          {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
      </p>
    </div>
  @endif
  <div class="fs75 fw900 gray">YOUR RATING</div>
  <div class="text-center form-group">
    {{ Form::label('rating', 'Rating:', array('class'=>'col-md-2 control-label ', 'style'=>'display:none')) }}
    <div class="col-sm-12  rating-a">
      {{ Form::select('rating', ([''=>'']+Review::ratingOptionsMain()), ceil(Input::get('rating')), array('class'=>'form-control ', 'id'=>'rating', 'style'=>'width:150px;')) }}
      <span class="pointer-cursor dpib pull-left pl15 fs110 pt5   gray review-clear-rating " ref="rating"><i class=" fa fa-times"></i></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-12">
      <select name="services[]" placeholder="Tag up to 3 services that you availed at this venue" multiple="multiple" title="Tag up to 3 services"   data-max-option="3" data-live-search="true" id="service_ids" class="form-control">
        @foreach ($services as $parent=>$children)
          <optgroup label="{{$parent}}">
            @foreach ($children as $service)
              <option value="{{$service['id']}}" {{ in_array($service['id'], $selected_services)?'selected':'' }} >{{$service['name']}}</option>
            @endforeach
          </optgroup>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group mb0">
    <div class="col-sm-12">
      {{ Form::textarea('body', Input::old('body'), 
                        array('class'=>'mb5 form-control', 'id'=>'text-review-body', 
                              'placeholder'=>'Had an interesting experience? Write a review for other users!')) }}
    </div>
  </div>
  {{ Form::hidden('business_id', $business->id) }}
  <div class="">
    <a id="submit-review-button" href="javascript:void(0)" class="btn text-center submit-btn submit-review-button pull-right m10 pt10  pb10 btn-turquoise"><b>Publish Review</b></b></a>

    <a href="javascript:void(0)" class="pull-left lnk-close-review-form btn-danger force-white mt5 btn pt5">Cancel</a>
  </div>
  <div class="clearfix"></div>
</div>

      {{ Form::close() }}
</div>
</div>
