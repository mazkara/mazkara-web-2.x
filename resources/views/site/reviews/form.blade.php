
<div class="  col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 review-frm">
  <div class="row">
    <div class="bg-white p0 col-md-10 col-md-offset-1">
    <div class="bg-white p0 text-center mt0 ">
      <h3>Review {{ $business->name}}!</h3>
    </div>


@if(isset($review)&& is_object($review))
  {{ Form::model($review, array('class' => 'form-horizontal', 'method' => 'PATCH', 'id'=>'form-manage-review', 'route' => array('businesses.review.update', $review->id))) }}
@else
  {{ Form::open(array('route' => 'reviews.store', 'id'=>'form-manage-review', 'class' => 'form-horizontal')) }}
@endif 
<div class="p15 bg-lite-gray "> 
        @if (Session::has('message'))
          <div class="flash alert alert-danger">
            <p>{{ Session::get('message') }}
              <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
              </ul>
            </p>

          </div>
        @endif

        <div class="text-center form-group">
          {{ Form::label('rating', 'Rating:', array('class'=>'col-md-2 control-label ', 'style'=>'display:none')) }}
          <div class="col-sm-12 rating-a">

            {{ Form::text('rating', Input::old('rating'), array( 'class'=>'form-control hidden  ',  'data-show-clear'=>"false", 
                                                              'data-score'=>Input::old('rating'), 'data-size'=>'lg', 'rel'=>$business->id)) }}
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
          <select name="services[]" multiple="multiple" title="Tag up to 3 services that you availed at this venue"   data-max-option="3" data-live-search="true" id="service_ids" class="form-control">
            
            @foreach ($services as $parent=>$children)
              <optgroup label="{{$parent}}">
                @foreach ($children as $service)
                  
                    <option value="{{$service['id']}}" >{{$service['name']}}</option>
                  
                @endforeach
              </optgroup>
            @endforeach
          </select>
        </div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            {{ Form::textarea('body', Input::old('body'), 
                              array('class'=>'form-control', 'id'=>'text-review-body', 
                                    'placeholder'=>'Had an interesting experience? Write a review for other users!')) }}
          </div>
        </div>
        {{ Form::hidden('business_id', $business->id) }}
      <a id="submit-review-button" href="javascript:void(0)" style="width:100%" class="btn text-center submit-btn submit-review-button m10 p10 btn-turquoise"><b>POST REVIEW</b></a>
      </div>

      {{ Form::close() }}
</div>

</div>

<script type="text/javascript">
$(function(){

  $('#service_ids').selectpicker({maxOptions:3});
  @if(isset($review) && is_object($review))
    $('#service_ids').selectpicker('val', [{{ join(',', $review->services()->lists('service_id','service_id')->all()) }}]);
  @endif
  $('#rating').barrating({
    showClear: true,
    clearButton:'<i class="fa fa-times"></i>',
    size:'xs',
    starCaptions: function(val) {
      if(val <= 1){
        return 'Poor';
      }
      if(val <= 1.5){
        return 'One and half';
      }
      if(val <= 2.5){
        return 'Average';
      }
      if(val <= 3.5){
        return 'Good';
      }
      if(val <= 4.5){
        return 'Very Good';
      }
      if(val <= 5){
        return 'Fabulous';
      }
    }
  });

  $('#submit-review-button').on('click', function () {
    rev_business_id = $('#form-manage-review').find('input[name=business_id]').first();
    rev_rating = $('#form-manage-review').find('input[name=rating]').first();
    rev_body = $('#form-manage-review').find('textarea[name=body]').first();
    rev_services = $('#form-manage-review').find('#service_ids').first();
    missing = [];

    if($(rev_rating).val() == 0){
      missing.push('You cannot add a Review without a rating! ');
    }

    if($(rev_body).val().length < 50){
      missing.push('Less than 50 characters? C\'mon babes you can do better than that!');
    }

    if(missing.length>0){
      alert(missing.join("\n"));
      return false;
    }else{
      var $btn = $(this).html('Saving Review...')
      frm = $('#submit-review-button').parents('form').first();
      data = {
        rating:   $(rev_rating).val(),
        body:     $(rev_body).val(),
        services: $(rev_services).val(),
        business_id: $(rev_business_id).val()
      };

      $.ajax({
        type: "POST",
        url: $('#form-manage-review').attr('action'),
        data: data,
        success: function(result) {
          $.magnificPopup.instance.items[0] = {
            src: result['html'], type: 'inline' };
          $.magnificPopup.instance.updateItemHTML();
          return false;
        }
      });



    }

  })



$('#text-review-body').textcounter({
    min: 50,
    max: 2000000,
    countDownText: "Characters Left: ",
    minimumErrorText: "Minimum Review should have 50 characters!",
    counterErrorClass: "text-danger",
    countContainerClass:"p10",
    counterText: "Review Length: "
});



});

</script>
