@foreach($reviews as $review)
  @include('site.reviews.partials.single', ['review'=>$review])
@endforeach
@if($reviews->hasMorePages())
  <a href="javascript:void(0)" data-next="{{1+$reviews->currentPage()}}" class="dpb mt10 ba pb10 pt10 text-center reviews-next-btn ">
    LOAD MORE
  </a>
@endif