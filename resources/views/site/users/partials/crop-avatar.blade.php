<div class="col-md-6 col-md-offset-3 ">
  <div class="row">
    <div class="bg-white p0 col-md-10 col-md-offset-1">
    <div class="bg-yellow p10 text-center mt0 ">
      <h3>ADJUST YOUR PHOTO</h3>
    </div>
    <div class="form-crop">
      <div class="p10 mb10 mt10 text-left" id="crop-area">
        <div>
          <img id="sample_picture" style="max-width: 100%;" src="{{ $image_url }}" />
          {{ Form::hidden('picture_x',        '',   ['id'=>"picture_x"]) }}
          {{ Form::hidden('picture_y',        '',   ['id'=>"picture_y"]) }}
          {{ Form::hidden('picture_width',    '',   ['id'=>"picture_width"]) }}
          {{ Form::hidden('picture_height',   '',   ['id'=>"picture_height"]) }}
          {{ Form::hidden('picture_scale_x',  '',   ['id'=>"picture_scale_x"]) }}
          {{ Form::hidden('picture_scale_x',  '',   ['id'=>"picture_scale_x"]) }}
          {{ Form::hidden('img_url', $image_url) }}
        </div>
        <p class="p10"><a href="javascript:void(0)" class="lnk-save-avatar btn w100pc btn btn-turquoise">SAVE MY PROFILE PHOTO</a></p>
      </div>
    </div>
    <div class="form-crop-submit-message hidden p25">
      <p>Sweet! We're giving your photograph a fabulous Fabogo treatment - should be done in a few seconds!</p>

    </div>

  </div>
  </div>
</div>
