@extends('layouts.email')

@section('content')
<div class='movableContent'>
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="container">
    <tr>
      <td width="100%" colspan="2" align="center" style="padding-bottom:10px;padding-top:25px;">
        <div class="contentEditableContainer contentTextEditable">
          <p style="text-align:center;font-size:20px;" >
              Hey {{ $user['name']}}!
              
          </p>
        </div>
      </td>
    </tr>
    <tr>
      <td width="100%" colspan="2" align="center" style="padding-bottom:10px;padding-top:0px;">
        <div class="contentEditableContainer contentTextEditable">
          <p style="text-align:center;font-size:38px;margin-top:10px; margin-bottom:0px;" >
              Welcome to Fabogo!
          </p>
          <p style="text-align:center;font-size:20px;margin-top:0px;">
            The Ultimate Beauty and Wellness Guide.
          </p>
        </div>
      </td>
    </tr>
    <tr>
      <td width="50%" align="left" style="padding:10px;">
        <table width="100%">
          <tr>
            <td style="width:40px;">
              <img style="width:30px;"  src="https://s3.amazonaws.com/mazkaracdn/assets/home/01.png"/>
            </td>
            <td style="padding-right:10px;">
              <p style="font-size:12px">
                Discover Salons, Spas and Fitness Centers in your Neighbourhood.
              </p>
            </td>
          </tr>
          <tr>
            <td style="width:40px;">
              <img style="width:40px;"  src="https://s3.amazonaws.com/mazkaracdn/assets/home/02.png"/>
            </td>
            <td style="padding-right:10px;padding-left:10px;">
              <p style="font-size:12px">
              Search for exclusives specials and offers published directly by the venue.
            </p>
            </td>
          </tr>
        </table>
      </td>
      <td width="50%" align="left" style="padding:10px;">
        <table width="100%">
          <tr>
            <td style="width:40px;">
              <img style="width:40px;"  src="https://s3.amazonaws.com/mazkaracdn/assets/home/03.png"/>
            </td>
            <td style="padding-right:10px;padding-left:10px;">
              <p style="font-size:12px">

                Write reviews for your favourite spas salons and fitness centers.
              </p>
            </td>
          </tr>
          <tr>
            <td style="width:40px;">
              <img style="width:40px;"  src="https://s3.amazonaws.com/mazkaracdn/assets/home/04.png"/>
            </td>
            <td style="padding-right:10px;padding-left:10px;">
              <p style="font-size:12px">
                Stay tuned, with beauty trends, tricks and tips through Fabogo Stories.
              </p>
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr>
      <td width="100%" colspan="2" align="center">
        <p style="padding:10px;margin:10px;">For more details write to hello@fabogo.com</p>
      </td>
    </tr>
  </table>
</div>

@stop