@extends('layouts.parallax')
@section('preheader')
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "91cce174-96e7-4c79-8290-329bed8eca64", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
@stop
@section('content')
<div class="container bg-lite-gray">
  <div class="row">
    <div class="row-height">
      <div class="col-md-1 hide-only-mobile col-height "></div>
      <div class="col-md-7 col-height " style="vertical-align:top">
        <div class="inside-full-height">
          <div class="pt15 mt15  bb  clearfix ">
            <div class="pull-left mb10 mr10">
              {{ViewHelper::userAvatar($post->author, ViewHelper::$avatar45)}}
            </div>
            <div>
              <b>
                <a href="{{route('users.profile.show', $post->author->id)}}">
                  {{$post->author->name}}
                </a>
              </b><br/>
              {{$post->authors_designation }}
              <div class="pull-right medium-gray mt10 mr0">
                POSTED ON {{ strtoupper(mzk_f_date($post->published_on, 'jS M'))}}
              </div>
            </div>
          </div>
      <div class="text-right mt10 mb10">
        <div class="pull-left">
          <span class="medium-gray">
            <i class=" flaticon flaticon-eye110 op80 "></i> {{ $post->views }} Views
          </span> 
          &nbsp;
          <span class="medium-gray">
            @if(Auth::check())
              <a href="javascript:void(0)" class="btn btn-xs medium-gray fav-blue {{ Auth::check()? ($post->liked(Auth::user()->id)? 'favourited' : 'favourite'):'favourite' }} btn-default" rel="{{$post->id}}"  data-route="posts">
                <i class="fa fa-thumbs-up "></i> LIKE
              </a>&nbsp;{{ $post->num_likes()}} 
              @if($post->isEditableBy(Auth::user()->id))
                @if($post->isEditable())
                  <a href="/posts/{{$post->id}}/edit" class="btn medium-gray btn-xs btn-default " rel="{{$post->id}}">
                    <i class="fa fa-pencil"></i> Edit Post
                  </a>
                @endif
              @endif

            @else
              <a href="/users/login" class="btn btn-xs medium-gray ajax-popup-link btn-default " rel="{{$post->id}}">
                <i class="fa fa-thumbs-up"></i> LIKE 
              </a>&nbsp;{{ $post->num_likes()}} 
            @endif
          </span>




        </div>

        <div class="pull-right">
          <span class='st_sharethis' displayText='ShareThis'></span>
          <span class='st_facebook' displayText='Facebook'></span>
          <span class='st_twitter' displayText='Tweet'></span>
          <span class='st_googleplus' displayText='Google +'></span>
          <span class='st_pinterest' displayText='Pinterest'></span>
          <span class='st_' displayText=''></span>
        </div>

      </div>

      <div class="clearfix"></div>


      @if((Auth::check()) && $post->isEditableBy(Auth::user()->id))
        @include('site.posts.partials.publish-status')

      @endif



      <h1 class="pt10 mt10 text-center fw500 item-name mt20">{{$post->title}}</h1>
        <div class="mb10 mt10 fs125">
          {{ $post->caption }}
        </div>
          
      @if($post->isVideo())
        <iframe width="100%" height="450" src="{{ $post->videoUrl() }}" frameborder="0" allowfullscreen>
        </iframe>
        <div class="well fs80">
          {{ $post->caption }}
        </div>
      @else
        @if($post->hasCover())
          <img src="{{ $post->cover->image->url('xlarge') }}" width="100%" class="ba"  />
        @endif
        <div class="pt10 page-font-override">
          {{ $post->body}}
        </div>

      @endif
      <hr />
      <p>
        <b>COMMENTS {{ $post->comments->count()>0 ? '('.$post->comments->count().')' : '' }}</b>
      </p>
      <div class="mt10 pt10 clearfix ">
        @foreach($post->comments as $comment)
          <div class="media pb10 pt10 bb">
            @if(Auth::check())
              @if($comment->isDeletableBy(Auth::user()))
                {{ Form::open(array('style' => 'display: inline-block;', 
                                    'class'=>'pull-right confirmable',  'method' => 'DELETE', 
                                    'route' => array('comments.destroy', $comment->id))) }}
                  <a href="javascript:void(0)" class="submit-parent-form"><i class="fa fa-times"></i></a>
                {{ Form::close() }}
              @endif
            @endif

            <div class="media-left">
              <a href="/users/{{$comment->user_id}}/profile">
                {{ ViewHelper::userAvatar($comment->user, ViewHelper::$avatar35) }}
              </a>
            </div>
            <div class="media-body dark-gray">
              <b>{{$comment->getDisplayableUsersName()}}</b> 
              <span class="medium-gray">{{$comment->body}}</span>
              <p>
                <small class="gray">
                  {{{ Date::parse($comment->updated_at)->ago() }}}
                </small>
              </p>
            </div>
          </div>
        @endforeach
      </div>
      @if(Auth::check())
        <?php $user = Auth::user();?>
        <div class="media pb10 pt10 ">
          <div class="media-left">
            <a href="/users/{{$user->id}}/profile" >
              {{ViewHelper::userAvatar($user, ViewHelper::$avatar35)}}
            </a>
          </div>
          <div class="media-body" style="width: 100%;">
            {{ Form::open(array('route' => 'comments.store')) }}
              {{ Form::hidden('commentable_type', 'Post')}}
              {{ Form::hidden('commentable_id', $post->id)}}
              {{ Form::hidden('type', 'comment')}}
              {{ Form::text('body', '', ['class'=>'form-control count-limiter', 'placeholder'=>'Write a comment'])}}
            {{ Form::close()}}
          </div>
        </div>
      @else
        <div class="well mt10">
          <a href="/users/create" id="ajax-login-link" class=" ajax-popup-link" title="Sign in to comment"  data-toggle="tooltip" data-placement="left" >
            Sign in
          
           to leave a comment</a>
        </div>
      @endif
      <p class="p10 ">&nbsp;</p>
</div>
    </div>
    <div class="col-md-3 hide-only-mobile col-height bg-lite-gray-3 pt20" style="vertical-align:top">
      @include('elements.coming-soon')
      @if($service)
        <div class="fw500 pb10 mb10">POPULAR VENUES FOR {{ strtoupper($service->name)}}</div>

        @foreach($businesses_with_prices as $business)
          @include('site.businesses.partials.business-service-niblet')
        @endforeach

      @endif

      @if(count($suggested_posts)>0)
        <div class="fw500 pb10 mb10">TRENDING STORIES</div>
        @foreach($suggested_posts as $post)
          @include('site.posts.partials.post-niblet')
        @endforeach
      @endif

      @include('site.businesses.partials.ad-list')

    </div>
        <div class="col-md-1 hide-only-mobile col-height bg-lite-gray-3" style="vertical-align:top"></div>

  </div>
  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$(function(){
  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });


});
</script>

@stop