<h1>All Posts</h1>

<p>{{ link_to_route('admin.posts.create', 'Add New Post', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($posts->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Slug</th>
				<th>Caption</th>
				<th>Body</th>
				<th>Author_id</th>
				<th>State</th>
				<th>Published_on</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($posts as $post)
				<tr>
					<td>{{{ $post->title }}}</td>
					<td>{{{ $post->slug }}}</td>
					<td>{{{ $post->caption }}}</td>
					<td>{{{ $post->body }}}</td>
					<td>{{{ $post->author_id }}}</td>
					<td>{{{ $post->state }}}</td>
					<td>{{{ $post->published_on }}}</td>
          <td>
            {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.posts.destroy', $post->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            {{ link_to_route('admin.posts.edit', 'Edit', array($post->id), array('class' => 'btn btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no posts
@endif
